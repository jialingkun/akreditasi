<!doctype html>
<html>
<?php
require "../Cookies.php";
require "../Database/DatabaseConnection.php";

$periode = $_GET["periode"];

$query="select NamaPeriode from periode where idPeriode=$periode";
$data = mysqli_query($db, $query);
$row = mysqli_fetch_assoc($data);
$namaPeriode = $row['NamaPeriode'];

$prodi = $_GET['prodi'];

$query="select namaProdi from prodi where idProdi='$prodi'";
$data = mysqli_query($db, $query);
$row = mysqli_fetch_assoc($data);
$namaProdi = $row['namaProdi'];

$jabatan = $_GET['jabatan'];

//ambil user Kaprodi
$query="SELECT username FROM `isi_borang` NATURAL JOIN user WHERE idProdi=$prodi AND idPeriode=$periode GROUP BY username limit 1";
$data = mysqli_query($db, $query);
$row = mysqli_fetch_assoc($data);
$userKaprodi = $row['username'];

//ambil user Auditor
$query="SELECT username FROM `isi_borang` WHERE username NOT IN (SELECT username FROM `isi_borang` NATURAL JOIN user WHERE idProdi=$prodi AND idPeriode=$periode GROUP BY username) AND idProdi=$prodi AND idPeriode=$periode limit 1";
$data = mysqli_query($db, $query);
$row = mysqli_fetch_assoc($data);
$userAuditor = $row['username'];


if ($jabatan=="Kaprodi") {
	$usernameFix = $userKaprodi;
	$jabatanInverse = "Auditor";
}else{
	$usernameFix = $userAuditor;
	$jabatanInverse = "Kaprodi";
}-

$standar= 3;

//cek data auditor
$revisi="Telah direvisi";
$username= $usernameFix;
$query="select username from isi_borang where idProdi='$prodi' and idPeriode='$periode' and username='$username' and standar='$standar' limit 1";
$data = mysqli_query($db, $query);
$count = mysqli_num_rows($data);
if ($count<1){
	//ambil username kaprodi
	$query="select username from isi_borang where idProdi='$prodi' and idPeriode='$periode' and username!='$username' and standar='$standar' limit 1";
	$data = mysqli_query($db, $query);
	$row = mysqli_fetch_assoc($data);
	$username = $row['username'];
	$revisi="Tanpa Revisi";
}

if ($jabatan=="Kaprodi") {
	$revisi="";
}

?>
<head>
	<title>Admin</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/style.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../css/loading.css">
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script src="../js/jquery.min.js"></script>
	<script src="../js/form.js"></script>
	<script src="../js/standar3.js"></script>

</head>
<body onload="loadAllJs()">
	<div class='wrapper'>
		<header>
			<nav class="navbar navbar-default navbar-fixed-top">
				<div id="navbar" class="navbar-collapse collapse">
					<div class="navbar-header">
						<a class="navbar-brand" href="#"><?php echo $jabatan ?></a>
					</div>
					<ul class="nav navbar-nav">
						<?php echo "
						<li><a href='HomeKaprodi.php?periode=$periode&jabatan=$jabatan&prodi=$prodi'>Profil</a></li>
						<li><a href='Standar1.php?periode=$periode&jabatan=$jabatan&prodi=$prodi'>Standar 1</a></li>
						<li><a href='Standar2.php?periode=$periode&jabatan=$jabatan&prodi=$prodi'>Standar 2</a></li>
						<li class='active'><a href='Standar3.php?periode=$periode&jabatan=$jabatan&prodi=$prodi'>Standar 3</a></li>
						<li><a href='Standar4.php?periode=$periode&jabatan=$jabatan&prodi=$prodi'>Standar 4</a></li>
						<li><a href='Standar5.php?periode=$periode&jabatan=$jabatan&prodi=$prodi'>Standar 5</a></li>
						<li><a href='Standar6.php?periode=$periode&jabatan=$jabatan&prodi=$prodi'>Standar 6</a></li>
						<li><a href='Standar7.php?periode=$periode&jabatan=$jabatan&prodi=$prodi'>Standar 7</a></li>
						<li><a href='Nilai.php?periode=$periode&jabatan=$jabatan&prodi=$prodi'>Nilai</a></li>
						
						<li><a href='pilihProdi.php?periode=$periode' class='col-md-offset-7'>Kembali</a></li>	
						<li><a href='Standar3.php?periode=$periode&jabatan=$jabatanInverse&prodi=$prodi'>Ke $jabatanInverse</a></li>
						<li><a href='Logout.php'>Log Out</a></li>
						"; ?>
					</ul>
				</div>
			</nav>
			<div class='text-center'>
				<br>
				<br>
				<h1>STANDAR 3</h1>
				<h4>KEMAHASISWAAN DAN LULUSAN</h4>
				<h5><?php echo $namaProdi ?></h5>
				<h5>Periode: <?php echo $namaPeriode ?></h5>
				<h5><b><?php echo $revisi?></b></h5>
			</div>
		</header>
	</div>
	
	<div class = "container">
		<hr class="colorgraph"><br>
		<div class="row">
			<div class="col-md-12">
				<form action="prosesStandar3.php" method="post" class="form-horizontal">

					<?php 
					// default id prodi yang disubmit khusus auditor
					echo "
					<input type='hidden' name='prodi' value='".$prodi."'>";
					?>

					<div class="fixed-button">
						<button type="button" class="btn btn-default" id="hideButton">Hide</button>
						<button type="button" class="btn btn-default" id="showButton">Show</button>
					</div>

					<div class="form-group">
						<label class="col-sm-2 text-right">3.1</label> 
						<label class="col-sm-8">Profil Mahasiswa dan Lulusan</label>
					</div>

					<div class="form-group">
						<label class="col-sm-2 text-right">3.1.1</label> 
						<label class="col-sm-8">Data seluruh mahasiswa regular dan lulusannya dalam lima tahun terakhir:</label>
					</div>

					<!-- tabel 1.1 -->
					<div class="form-group">

						<?php
						//data retrieve for table
						$butir = "3.1.1";
						$tabledata = array_fill(0, 75, '');
						$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY NO DESC, kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$tabledata[$index] = $row['isi_float'];
								$index++;
							}
						}
						?>

						<div class="col-sm-8 col-sm-offset-2 table-wrapper table-fixed">
							<table class="tableBorang" style="width:174%;" id="sumTable1">
								<thead>
									<tr>
										<th rowspan="2">Tahun Akademik</th>
										<th rowspan="2">Daya Tampung</th>
										<th colspan="2">Jumlah Calon Mahasiswa Reguler</th>
										<th colspan="2">Jumlah Mahasiswa Baru</th>
										<th colspan="2">Jumlah Total Mahasiswa</th>
										<th colspan="2">Jumlah Lulusan</th>
										<th colspan="3">IPK Lulusan Reguler</th>
										<th colspan="3">Presentase lulusan Reguler dengan IPK :</th>
									</tr>
									<tr>
										<th>Ikut Seleksi</th>
										<th>Lulus Seleksi</th>
										<th>Reguler bukan Transfer</th>
										<th>Transfer</th>
										<th>Reguler bukan Transfer</th>
										<th>Transfer</th>
										<th>Reguler bukan Transfer</th>
										<th>Transfer</th>
										<th>Min</th>
										<th>Rat</th>
										<th>Max</th>
										<th><2.75</th>
										<th>2.75 - 3.50</th>
										<th>>3.50</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
										<td>(4)</td>
										<td>(5)</td>
										<td>(6)</td>
										<td>(7)</td>
										<td>(8)</td>
										<td>(9)</td>
										<td>(10)</td>
										<td>(11)</td>
										<td>(12)</td>
										<td>(13)</td>
										<td>(14)</td>
										<td>(15)</td>
										<td>(16)</td>
									</tr>

									<?php  
									$index = 0;
									for ($row=4; $row >= 0; $row--) {
										echo "<tr>";

									//nama TS
										if ($row>0) {
											echo "<td>TS-".$row."</td>";
										}else{
											echo "<td>TS</td>";
										}

									//kolom input
										for ($column=2; $column <= 16; $column++) { 
											echo "<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='3_1_1/".$column."/TS".$row."' onchange='updateNilai11(".($column-1).")' value='".$tabledata[$index]."'></td>";
											$index++;
										}
										echo "</tr>";
									}
									?>

									<tr>
										<td>Jumlah</td>
										<td id="3.1.1/2"></td>
										<td id="3.1.1/3"></td>
										<td id="3.1.1/4"></td>
										<td id="3.1.1/5"></td>
										<td id="3.1.1/6"></td>
										<td id="3.1.1/7"></td>
										<td id="3.1.1/8"></td>
										<td id="3.1.1/9"></td>
										<td id="3.1.1/10"></td>

									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 text-right">3.1.2</label> 
						<label class="col-sm-8">Data mahasiswa non-reguler dalam lima tahun terakhir:</label>
					</div>
					
					<!-- tabel 1.2 -->
					<div class="form-group" align="center">

						<?php
						//data retrieve for table
						$butir = "3.1.2";
						$tabledata = array_fill(0, 75, '');
						$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY NO DESC, kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$tabledata[$index] = $row['isi_float'];
								$index++;
							}
						}
						?>

						<div class="col-sm-8 col-sm-offset-2">
							<table class="tableBorang" id="sumTable2">
								<thead>
									<tr>
										<th rowspan="2">Tahun Akademik</th>
										<th rowspan="2">Daya Tampung</th>
										<th colspan="2">Jumlah Calon Mahasiswa</th>
										<th colspan="2">Jumlah Mahasiswa Baru</th>
										<th colspan="2">Jumlah Total Mahasiswa</th>
									</tr>
									<tr>
										<th>Ikut Seleksi</th>
										<th>Lulus Seleksi</th>
										<th>Reguler bukan Transfer</th>
										<th>Transfer</th>
										<th>Reguler bukan Transfer</th>
										<th>Transfer</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
										<td>(4)</td>
										<td>(5)</td>
										<td>(6)</td>
										<td>(7)</td>
										<td>(8)</td>
									</tr>

									<?php
									$index = 0;
									for ($row=4; $row >= 0; $row--) {
										echo "<tr>";

									//nama TS
										if ($row>0) {
											echo "<td>TS-".$row."</td>";
										}else{
											echo "<td>TS</td>";
										}

									//kolom input
										for ($column=2; $column <= 8; $column++) { 
											echo "<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='3_1_2/".$column."/TS".$row."' onchange='updateNilai12(".($column-1).")' value='".$tabledata[$index]."'></td>";
											$index++;
										}
										echo "</tr>";
									}
									?>

									<tr>
										<td>Jumlah</td>
										<td id="3.1.2/2"></td>
										<td id="3.1.2/3"></td>
										<td id="3.1.2/4"></td>
										<td id="3.1.2/5"></td>
										<td id="3.1.2/6"></td>
										<td id="3.1.2/7"></td>
										<td id="3.1.2/8"></td>

									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group"> 
						<p class="col-sm-8 col-sm-offset-2">Catatan: <br />
							TS:Tahun akademik penuh terakhir saat pengisian borang <br />
							Min: IPK Minimum; Rat:IPK Rata-rata; Mak:IPK Maksimum <br />
							Catatan: <br />
							(1) Mahasiswa program reguler adalah mahasiswa yang mengikuti program pendidikan secara penuh waktu (baik kelas pagi, siang, sore, malam, dan di seluruh kampus). <br />
							(2) Mahasiswa program non-reguler adalah mahasiswa yang mengikuti program pendidikan secara paruh waktu. <br />
							(3) Mahasiswa transfer adalah mahasiswa yang masuk ke program studi dengan mentransfer mata kuliah yang telah diperolehnya dari PS lain, baik dari dalam PT maupun luar PT.<br />
						</p>
						<div class="col-sm-2"></div>
					</div>
					
					<!-- nilai -->
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="5">3.1.1.a</td>
									<td colspan="2">Rasio calon mahasiswa yang ikut seleksi : daya tampung 	</td>
								</tr>
								<tr>
									<td>Jumlah kolom 3 (Jumlah yang ikut seleksi)</td>
									<td id="nilai/3.1.1.a/1"></td>
								</tr>
								<tr>
									<td>Jumlah kolom 2 (Daya tampung PS)</td>
									<td id="nilai/3.1.1.a/2"></td>
								</tr>
								<tr>
									<td>Rasio = (jumlah kolom 3)/(jumlah kolom 2)</td>
									<td id="nilai/3.1.1.a/3"></td>
								</tr>
								<tr>
									<td>Nilai</td>
									<td id="nilai/3.1.1.a/nilai"></td>
									<input type="hidden" id="nilai/3.1.1.a/nilaihid" name="nilai/3_1_1_a" value="">
								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="5">3.1.1.b</td>
									<td colspan="2">Rasio mahasiswa baru reguler yang melakukan registrasi : calon mahasiswa baru reguler yang lulus seleksi</td>
								</tr>
								<tr>
									<td>Jumlah kolom 5 (Jumlah mhs baru reguler yang melakukan registrasi)</td>
									<td id="nilai/3.1.1.b/1"></td>
								</tr>
								<tr>
									<td>Jumlah kolom 4 (Jumlah calon mhs yang lulus seleksi)</td>
									<td id="nilai/3.1.1.b/2"></td>
								</tr>
								<tr>
									<td>Rasio=(jumlah kolom 5)/jumlah kolom 4)</td>
									<td id="nilai/3.1.1.b/3"></td>
								</tr>
								<tr>
									<td>Nilai</td>
									<td id="nilai/3.1.1.b/nilai"></td>
									<input type="hidden" id="nilai/3.1.1.b/nilaihid" name="nilai/3_1_1_b" value="">
								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="5">3.1.1.c</td>
									<td colspan="2">Rasio mahasiswa baru transfer terhadap mahasiswa baru bukan transfer</td>
								</tr>
								<tr>
									<td>TMBT = total mahasiswa baru transfer untuk program S1 reguler dan S1 non-reguler</td>
									<td id="nilai/3.1.1.c/1"></td>
								</tr>
								<tr>
									<td>TMB = total mahasiswa baru bukan transfer untuk program S1 reguler dan S1 non-reguler</td>
									<td id="nilai/3.1.1.c/2"></td>
								</tr>
								<tr>
									<td>RM = TMBT/TMB</td>
									<td id="nilai/3.1.1.c/3"></td>
								</tr>
								<tr>
									<td>Nilai</td>
									<td id="nilai/3.1.1.c/nilai"></td>
									<input type="hidden" id="nilai/3.1.1.c/nilaihid" name="nilai/3_1_1_c" value="">
								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="3">3.1.1.d</td>
									<td colspan="2">Rata-rata Indeks Prestasi Kumulatif (IPK) selama lima tahun terakhir</td>
								</tr>
								<tr>
									<td>IPK</td>
									<td id="nilai/3.1.1.d/1"></td>
								</tr>
								<tr>
									<td>Nilai</td>
									<td id="nilai/3.1.1.d/nilai"></td>
									<input type="hidden" id="nilai/3.1.1.d/nilaihid" name="nilai/3_1_1_d" value="">
								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center" colspan="2">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="7">3.1.2</td>
									<td colspan="3">Penerimaan mahasiswa non reguler.</td>
								</tr>
								<tr>
									<td>0</td>
									<td>Jumlah mahasiswa yang diterima mengakibatkan beban dosen sangat berat, melebihi 19 sks.</td>
									<td></td>
								</tr>
								<tr>
									<td>1</td>
									<td>Jumlah mahasiswa yang diterima mengakibatkan beban dosen relatif berat, yaitu lebih dari 17 s.d. 19 sks.</td>
									<td></td>
								</tr>
								<tr>
									<td>2</td>
									<td>Jumlah mahasiswa yang diterima masih memungkinkan dosen mengajar seluruh mahasiswa dengan total beban lebih dari 15  s.d. 17 sks.</td>
									<td></td>
								</tr>
								<tr>
									<td>3</td>
									<td>Jumlah mahasiswa yang diterima masih memungkinkan dosen mengajar seluruh mahasiswa dengan total beban lebih dari 13  s.d. 15 sks.</td>
									<td></td>
								</tr>
								<tr>
									<td>4</td>
									<td>Jumlah mahasiswa yang diterima masih memungkinkan dosen mengajar seluruh mahasiswa dengan total beban mendekati ideal, yaitu kurang atau sama dengan 13 sks. </td>
									<td></td>
								</tr>
								<tr>
									<td colspan="2">Nilai</td>
									<?php 
									//retrieve Nilai
									$butir = "3.1.2";
									$tabledata = array_fill(0, 1, '');
									$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY sub_butir";
									$data = mysqli_query($db, $query);
									$count = mysqli_num_rows($data);
									if ($count>0) {
										while ($row = mysqli_fetch_assoc($data)) {
											$tabledata[0] = $row['isi'];
										}
									}
									?>
									<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="nilai/3_1_2" value=<?php echo "'".$tabledata[0]."'";?>></td>
								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">

						<?php
						//data retrieve for table
						$butir = "3.1.3";
						$query="SELECT isi_char, isi_float, isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							$categoryIndex = 2;
							while ($row = mysqli_fetch_assoc($data)) {
								if ($index==0) {
									$tabledata[$index] = $row['isi_float'];
								}else if ($index == $categoryIndex) {
									$tabledata[$index] = $row['isi_char'];
									$categoryIndex = $categoryIndex + 3;
								}else{
									$tabledata[$index] = $row['isi_text'];
								}
								
								$index++;
							}
						}else{
							$tabledata[0] = 1;
							$tabledata[1] = "";
							$tabledata[2] = "";
							$tabledata[3] = "";
							
						}
						?>

						<label class="col-sm-2 text-right">3.1.3</label> 
						<label class="col-sm-8">Pencapaian prestasi/reputasi mahasiswa dalam tiga tahun terakhir di bidang akademik dan non-akademik (misalnya prestasi dalam penelitian dan lomba karya ilmiah, olahraga, dan seni).</label>
						<div class="col-sm-2"></div>
					</div>

					<!-- tabel 1.3 -->
					<div class="form-group" align="center">
						<div class="col-sm-8 col-sm-offset-2">
							<?php 
							echo "<input type='hidden' id='3.1.3/counter' name='3_1_3/counter' value=".$tabledata[0].">";
							?>
							<table class="tableBorang" id="addTable3">
								<thead>
									<tr>
										<th>No.</th>
										<th>Nama Kegiatan dan Waktu Penyelenggaraan</th>
										<th width="20%">Tingkat (Lokal, Wilayah, Nasional, atau Internasional)</th>
										<th>Prestasi yang Dicapai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
										<td>(4)</td>
									</tr>
									<?php 
									$index = 1;
									for ($row=1; $row <= $tabledata[0]; $row++) { 
										?>
										<tr>
											<td><?php echo $row;?></td>
											<td><textarea rows="2" style="width:100%;" <?php echo "name='3_1_3/2/no".$row."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td> <select class="form-control" id="" onchange="updateNilai13()" <?php echo "name='3_1_3/3/no".$row."'"; ?> >
												<option disabled <?php if ($tabledata[$index]=="") {echo "selected";} ?>>--Pilih Tingkat--</option>
												<option <?php if ($tabledata[$index]=="Lokal") {echo "selected";} ?>>Lokal</option>
												<option <?php if ($tabledata[$index]=="Regional") {echo "selected";} ?>>Regional</option>
												<option <?php if ($tabledata[$index]=="Nasional") {echo "selected";} ?>>Nasional</option>
												<option <?php if ($tabledata[$index]=="Internasional") {echo "selected";} ?>>Internasional</option>
												<option <?php if ($tabledata[$index]=="Kosong") {echo "selected";} ?>>Kosong</option>
											</select></td> <?php $index++; ?>
											<td><textarea rows="2" style="width:100%;" <?php echo "name='3_1_3/4/no".$row."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
										</tr>
										
										<?php
									}


									?>

									<tr>
										<td colspan="4"><button type="button" onclick="addRow13('addTable3','3.1.3/counter')"><span class="glyphicon glyphicon-plus"></span></button></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center" colspan="2">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="6">3.1.3</td>
									<td colspan="3">Penghargaan atas prestasi mahasiswa di bidang nalar, bakat dan minat.</td>
								</tr>
								<tr>
									<td>1</td>
									<td>Tidak ada bukti penghargaan.</td>
									<td></td>
								</tr>
								<tr>
									<td>2</td>
									<td>Ada bukti penghargaan juara lomba ilmiah, olah raga, maupun seni tingkat lokal PT.</td>
									<td></td>
								</tr>
								<tr>
									<td>3</td>
									<td>Ada bukti penghargaan juara lomba ilmiah, olah raga, maupun seni tingkat wilayah.</td>
									<td></td>
								</tr>
								<tr>
									<td>4</td>
									<td>Ada bukti penghargaan juara lomba ilmiah, olah raga, maupun seni tingkat nasional atau internasional.</td>
									<td></td>
								</tr>
								<tr>
									<td colspan="2">Nilai</td>
									<td id="nilai/3.1.3/nilai">1</td>
									<input type="hidden" id="nilai/3.1.3/nilaihid" name="nilai/3_1_3" value="1">
								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 text-right">3.1.4</label> 
						<label class="col-sm-8">Data jumlah mahasiswa reguler tujuh tahun terakhir:</label>
					</div>
					
					<!-- tabel 1.4 -->
					<?php
						//data retrieve for table
					$butir = "3.1.4";
					$tabledata = array_fill(0, 32, '');
					$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY NO DESC, kolom ASC;";
					$data = mysqli_query($db, $query);
					$count = mysqli_num_rows($data);
					if ($count>0) {
						$index = 0;
						while ($row = mysqli_fetch_assoc($data)) {
							$tabledata[$index] = $row['isi_float'];
							$index++;
						}
					}
					?>
					<div class="form-group">
						<div class="col-sm-8 col-sm-offset-2 table-wrapper table-fixed">
							<table class="tableBorang" style="width:120%; table-layout:auto">
								<thead>
									<tr>
										<th rowspan="2">Tahun Masuk</th>
										<th colspan="7">Jumlah Mahasiswa Reguler (bukan transfer) per Angkatan pada Tahun*</th>
										<th rowspan="2">Jumlah Lulusan s.d. TS (dari Mahasiswa Reguler)</th>
									</tr>
									<tr>
										<th>TS-6</th>
										<th>TS-5</th>
										<th>TS-4</th>
										<th>TS-3</th>
										<th>TS-2</th>
										<th>TS-1</th>
										<th>TS</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
										<td>(4)</td>
										<td>(5)</td>
										<td>(6)</td>
										<td>(7)</td>
										<td>(8)</td>
										<td>(9)</td>
									</tr>
									<tr>
										<td>TS-6</td>
										<td><div style="float:left;padding-right:5%">(a) </div><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_1_4/2/TS6" id="3.1.4/a" onchange="updateNilai14(1)" style="width:72%;" value=<?php echo "'".$tabledata[0]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_1_4/3/TS6" onchange="updateNilai14()" value=<?php echo "'".$tabledata[1]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_1_4/4/TS6" onchange="updateNilai14()" value=<?php echo "'".$tabledata[2]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_1_4/5/TS6" onchange="updateNilai14()" value=<?php echo "'".$tabledata[3]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_1_4/6/TS6" onchange="updateNilai14()" value=<?php echo "'".$tabledata[4]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_1_4/7/TS6" onchange="updateNilai14()" value=<?php echo "'".$tabledata[5]."'";?>></td>
										<td><div style="float:left;padding-right:5%">(b) </div><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_1_4/8/TS6" id="3.1.4/b" onchange="updateNilai14(1)" style="width:72%;" value=<?php echo "'".$tabledata[6]."'";?>></td>
										<td><div style="float:left;padding-right:5%">(c) </div><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_1_4/9/TS6" id="3.1.4/c" onchange="updateNilai14(1)" style="width:72%;" value=<?php echo "'".$tabledata[7]."'";?>></td>
									</tr>
									<tr>
										<td>TS-5</td>
										<td class="voidColor"></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_1_4/3/TS5" onchange="updateNilai14()" value=<?php echo "'".$tabledata[8]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_1_4/4/TS5" onchange="updateNilai14()" value=<?php echo "'".$tabledata[9]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_1_4/5/TS5" onchange="updateNilai14()" value=<?php echo "'".$tabledata[10]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_1_4/6/TS5" onchange="updateNilai14()" value=<?php echo "'".$tabledata[11]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_1_4/7/TS5" onchange="updateNilai14()" value=<?php echo "'".$tabledata[12]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_1_4/8/TS5" onchange="updateNilai14()" value=<?php echo "'".$tabledata[13]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_1_4/9/TS5" onchange="updateNilai14()" value=<?php echo "'".$tabledata[14]."'";?>></td>
									</tr>
									<tr>
										<td>TS-4</td>
										<td class="voidColor"></td>
										<td class="voidColor"></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_1_4/4/TS4" onchange="updateNilai14()" value=<?php echo "'".$tabledata[15]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_1_4/5/TS4" onchange="updateNilai14()" value=<?php echo "'".$tabledata[16]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_1_4/6/TS4" onchange="updateNilai14()" value=<?php echo "'".$tabledata[17]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_1_4/7/TS4" onchange="updateNilai14()" value=<?php echo "'".$tabledata[18]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_1_4/8/TS4" onchange="updateNilai14()" value=<?php echo "'".$tabledata[19]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_1_4/9/TS4" onchange="updateNilai14()" value=<?php echo "'".$tabledata[20]."'";?>></td>
									</tr>
									<tr>
										<td>TS-3</td>
										<td class="voidColor"></td>
										<td class="voidColor"></td>
										<td class="voidColor"></td>
										<td><div style="float:left;padding-right:5%">(d) </div><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_1_4/5/TS3" id="3.1.4/d" onchange="updateNilai14(1)" style="width:72%;" value=<?php echo "'".$tabledata[21]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_1_4/6/TS3" onchange="updateNilai14()" value=<?php echo "'".$tabledata[22]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_1_4/7/TS3" onchange="updateNilai14()" value=<?php echo "'".$tabledata[23]."'";?>></td>
										<td><div style="float:left;padding-right:5%">(e) </div><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_1_4/8/TS3" id="3.1.4/e" onchange="updateNilai14(1)" style="width:72%;" value=<?php echo "'".$tabledata[24]."'";?>></td>
										<td><div style="float:left;padding-right:5%">(f) </div><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_1_4/9/TS3" id="3.1.4/f" onchange="updateNilai14(1)" style="width:72%;" value=<?php echo "'".$tabledata[25]."'";?>></td>
									</tr>
									<tr>
										<td>TS-2</td>
										<td class="voidColor"></td>
										<td class="voidColor"></td>
										<td class="voidColor"></td>
										<td class="voidColor"></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_1_4/6/TS2" onchange="updateNilai14()" value=<?php echo "'".$tabledata[26]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_1_4/7/TS2" onchange="updateNilai14()" value=<?php echo "'".$tabledata[27]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_1_4/8/TS2" onchange="updateNilai14()" value=<?php echo "'".$tabledata[28]."'";?>></td>
										<td class="voidColor"></td>
									</tr>
									<tr>
										<td>TS-1</td>
										<td class="voidColor"></td>
										<td class="voidColor"></td>
										<td class="voidColor"></td>
										<td class="voidColor"></td>
										<td class="voidColor"></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_1_4/7/TS1" onchange="updateNilai14()" value=<?php echo "'".$tabledata[29]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_1_4/8/TS1" onchange="updateNilai14()" value=<?php echo "'".$tabledata[30]."'";?>></td>
										<td class="voidColor"></td>
									</tr>
									<tr>
										<td>TS</td>
										<td class="voidColor"></td>
										<td class="voidColor"></td>
										<td class="voidColor"></td>
										<td class="voidColor"></td>
										<td class="voidColor"></td>
										<td class="voidColor"></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_1_4/8/TS0" onchange="updateNilai14()" value=<?php echo "'".$tabledata[31]."'";?>></td>
										<td class="voidColor"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="form-group"> 
						<p class="col-sm-8 col-sm-offset-2">Catatan: <br />
							Gunakan data mahasiswa yang diterima tahun awal, jika PS belum memiliki data pada TS-6.
						</p>
						<div class="col-sm-2"></div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center" colspan="2">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="5">3.1.4.a</td>
									<td colspan="3">Persentase kelulusan tepat waktu (K<sub>TW</sub>)</td>
								</tr>
								<tr>
									<td>(d)</td>
									<td>Jumlah mahasiswa yang diterima pada TS-3 (4 tahun yang lalu)</td>
									<td id="nilai/3.1.4.a/1"></td>
								</tr>
								<tr>
									<td>(f)</td>
									<td>Jumlah mahasiswa yang diterima pada TS-3 yang telah lulus.</td>
									<td id="nilai/3.1.4.a/2"></td>
								</tr>
								<tr>
									<td colspan="2">KTW (Kelulusan tepat waktu dalam persen, kisaran nilai 0 - 100.)</td>
									<td id="nilai/3.1.4.a/3"></td>
								</tr>
								<tr>
									<td colspan="2">Nilai</td>
									<td id="nilai/3.1.4.a/nilai"></td>
									<input type="hidden" id="nilai/3.1.4.a/nilaihid" name="nilai/3_1_4_a" value="">
								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center" colspan="2">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="6">3.1.4.b</td>
									<td colspan="3">Persentase mahasiswa yang DO atau mengundurkan diri (M<sub>DO</sub>)</td>
								</tr>
								<tr>
									<td>(a)</td>
									<td>Jumlah mahasiswa baru yang diterima pada TS-6 (7 tahun yang lalu)</td>
									<td id="nilai/3.1.4.b/1"></td>
								</tr>
								<tr>
									<td>(b)</td>
									<td>Jumlah mahasiswa baru yang diterima pada TS-6 yang masih terdaftar pada TS</td>
									<td id="nilai/3.1.4.b/2"></td>
								</tr>
								<tr>
									<td>(c)</td>
									<td>Jumlah mahasiswa baru yang diterima pada TS-6 yang telah lulus</td>
									<td id="nilai/3.1.4.b/3"></td>
								</tr>
								<tr>
									<td colspan="2">MDO (Persentase mahasiswa DO/Mengundurkan diri)</td>
									<td id="nilai/3.1.4.b/4"></td>
								</tr>
								<tr>
									<td colspan="2">Nilai</td>
									<td id="nilai/3.1.4.b/nilai"></td>
									<input type="hidden" id="nilai/3.1.4.b/nilaihid" name="nilai/3_1_4_b" value="">
								</tr>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<?php
						//data retrieve for table
						$butir = "3.2";
						$tabledata = array_fill(0, 6, '');
						$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$tabledata[$index] = $row['isi_text'];
								$index++;
							}
						}
						?>
						<label class="col-sm-2 text-right">3.2</label> 
						<label class="col-sm-8">Layanan kepada Mahasiswa</label>
					</div>

					<div class="form-group"> 
						<p class="col-sm-8 col-sm-offset-2">
							Tabel setiap jenis pelayanan kepada mahasiswa Program Studi (Bila tidak ada, biarkan kosong)
						</p>
						<div class="col-sm-2"></div>
					</div>
					
					<!-- tabel 2 -->
					<div class="form-group" align="center">
						<div class="col-sm-8 col-sm-offset-2">
							<table class="tableBorang" style="width:100%;">
								<thead>
									<tr>
										<th>No.</th>
										<th>Jenis Pelayanan kepada Mahasiswa</th>
										<th>Bentuk kegiatan, Pelaksanaan dan Hasilnya</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Bimbingan dan konseling</td>
										<td><textarea name="3_2/3/no1" id="3.2/3/no1" rows="15" onchange="updateNilai2()" style="width:100%;"><?php echo $tabledata[0];?></textarea></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Minat dan bakat (ekstra kurikuler)</td>
										<td><textarea name="3_2/3/no2" id="3.2/3/no2" rows="15" onchange="updateNilai2()" style="width:100%;"><?php echo $tabledata[1];?></textarea></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Pembinaan soft skills</td>
										<td><textarea name="3_2/3/no3" id="3.2/3/no3" rows="15" onchange="updateNilai2()" style="width:100%;"><?php echo $tabledata[2];?></textarea></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Beasiswa</td>
										<td><textarea name="3_2/3/no4" id="3.2/3/no4" rows="15" onchange="updateNilai2()" style="width:100%;"><?php echo $tabledata[3];?></textarea></td>
									</tr>
									<tr>
										<td>5</td>
										<td>Kesehatan</td>
										<td><textarea name="3_2/3/no5" id="3.2/3/no5" rows="15" onchange="updateNilai2()" style="width:100%;"><?php echo $tabledata[4];?></textarea></td>
									</tr>
									<tr>
										<td>6</td>
										<td>Lainnya</td>
										<td><textarea name="3_2/3/no6" rows="15" onchange="updateNilai2()" style="width:100%;"><?php echo $tabledata[5];?></textarea></td>
									</tr>

								</tbody>
							</table>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center" colspan="2">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="7">3.2.1</td>
									<td colspan="3">Layanan kepada mahasiswa.  Jenis pelayanan kepada mahasiswa antara lain: (1) Bimbingan dan konseling, (2) Minat dan bakat (ekstra kurikuler), (3) Pembinaan soft skill, (4) Layanan beasiswa, dan (5) Layanan kesehatan.</td>
								</tr>
								<tr>
									<td>0</td>
									<td>Kurang dari 2 unit pelayanan.</td>
									<td></td>
								</tr>
								<tr>
									<td>1</td>
									<td>Ada 2 jenis unit pelayanan.</td>
									<td></td>
								</tr>
								<tr>
									<td>2</td>
									<td>Ada jenis layanan nomor 1 sampai dengan nomor 2.</td>
									<td></td>
								</tr>
								<tr>
									<td>3</td>
									<td>Ada jenis layanan nomor 1 sampai dengan nomor 3.</td>
									<td></td>
								</tr>
								<tr>
									<td>4</td>
									<td>Ada semua (5 jenis) pelayanan mahasiswa yang dapat diakses.</td>
									<td></td>
								</tr>
								<tr>
									<td colspan="2">Nilai</td>
									<td id="nilai/3.2.1/nilai">0</td>
									<input type="hidden" id="nilai/3.2.1/nilaihid" name="nilai/3_2_1" value="">

								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="2">3.2.2</td>
									<td>Kualitas layanan kepada mahasiswa. Untuk setiap jenis pelayanan pada butir 3.2.1, pemberian skor sebagai berikut: 4: sangat baik, 3 :baik, 2: cukup, 1: kurang, 0: sangat kurang. Kisaran nilai 0 - 20.</td>

									<?php 
									//retrieve Nilai
									$butir = "3.2.2";
									$subButir = "1";
									$tabledata = array_fill(0, 1, '');
									$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND sub_butir='".$subButir."' ORDER BY sub_butir";
									$data = mysqli_query($db, $query);
									$count = mysqli_num_rows($data);
									if ($count>0) {
										while ($row = mysqli_fetch_assoc($data)) {
											$tabledata[0] = $row['isi'];
										}
									}
									?>
									<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" id="nilai/3.2.2/1" name="nilai/3_2_2/1" onchange="updateNilai2()" value=<?php echo "'".$tabledata[0]."'";?>></td>
								</tr>
								<tr>
									<td>Nilai</td>
									<td id="nilai/3.2.2/nilai"></td>
									<input type="hidden" id="nilai/3.2.2/nilaihid" name="nilai/3_2_2" value="">
								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 text-right">3.3</label> 
						<label class="col-sm-8">Evaluasi Lulusan</label>
					</div>

					<div class="form-group">
						<?php
						//data retrieve for table
						$butir = "3.3.1";
						$tabledata = array_fill(0, 37, '');
						$query="SELECT isi_char,isi_float,isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							$descIndex = 6;
							while ($row = mysqli_fetch_assoc($data)) {
								if ($index==0) {
									$tabledata[$index] = $row['isi_text'];
								}else if ($index==1) {
									$tabledata[$index] = $row['isi_char'];
								}else if ($index==$descIndex) {
									$tabledata[$index] = $row['isi_text'];
									$descIndex = $descIndex + 5;
								}else{
									$tabledata[$index] = $row['isi_float'];
								}
								
								$index++;
							}
						}
						?>
						<label class="col-sm-2 text-right">3.3.1</label> 
						<label class="col-sm-8">Evaluasi Kinerja lulusan oleh Pihak Pengguna Lulusan</label>
					</div>

					<div class="form-group"> 
						<p class="col-sm-8 col-sm-offset-2">Adakah studi pelacakan (tracer study) untuk mendapatkan hasil evaluasi kinerja lulusan dengan pihak pengguna?
						</p>
						<div class="col-sm-2"></div>
					</div>

					<div class="form-group"> 
						<div class="radio col-sm-8 col-sm-offset-2">
							<label><input type="radio" name="3_3_1/radio" value="tidak" <?php if ($tabledata[1]=="tidak") {echo "checked";} ?>>Tidak Ada</label>
						</div>
						<div class="col-sm-2"></div>
						<div class="radio col-sm-8 col-sm-offset-2">
							<label><input type="radio" name="3_3_1/radio" value="ada" <?php if ($tabledata[1]!="tidak") {echo "checked";} ?>>Ada</label>
						</div>
						<div class="col-sm-2"></div>
					</div>

					<div class="form-group"> 
						<p class="col-sm-8 col-sm-offset-2">Jika ada, uraikan metode, proses dan mekanisme kegiatan studi pelacakan tersebut.  Jelaskan pula bentuk tindak lanjut dari hasil kegiatan ini.
						</p>
						<div class="col-sm-2"></div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<textarea type="text" class="form-control" rows="20" name="3_3_1/penjelasan" placeholder="" maxlength="60000"><?php echo $tabledata[0]; ?></textarea>
						</div>
					</div>

					<div class="form-group"> 
						<p class="col-sm-8 col-sm-offset-2">Hasil studi pelacakan dirangkum dalam tabel berikut: <br>
							Nyatakan angka persentasenya(*)  pada kolom yang sesuai. <br>
							Jika tidak ada tracer study, maka setiap sel diisi nol.
						</p>
						<div class="col-sm-2"></div>
					</div>
					
					<!-- tabel 3.1 -->
					<div class="form-group" align="center">
						<div class="col-sm-8 col-sm-offset-2">
							<table class="tableBorang" id="sumTable31" style="width:100%;">
								<thead>
									<tr>
										<th rowspan="3" style="width:5%">No.</th>
										<th rowspan="3" style="width:25%">Jenis Kemampuan</th>
										<th colspan="4">Tanggapan Pihak Pengguna</th>
										<th rowspan="3">Rencana Tindak Lanjut oleh Program Studi</th>
									</tr>
									<tr>
										<th style="width:8%">Sangat Baik</th>
										<th style="width:8%">Baik</th>
										<th style="width:8%">Cukup</th>
										<th style="width:8%">Kurang</th>
									</tr>
									<tr>
										<th>(%)</th>
										<th>(%)</th>
										<th>(%)</th>
										<th>(%)</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
										<td>(4)</td>
										<td>(5)</td>
										<td>(6)</td>
										<td>(7)</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Integritas (etika dan moral)</td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_3_1/3/no1" onchange="updateNilai31(2)" value=<?php echo "'".$tabledata[2]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_3_1/4/no1" onchange="updateNilai31(3)" value=<?php echo "'".$tabledata[3]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_3_1/5/no1" onchange="updateNilai31(4)" value=<?php echo "'".$tabledata[4]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_3_1/6/no1" onchange="updateNilai31(5)" value=<?php echo "'".$tabledata[5]."'";?>></td>
										<td><textarea name="3_3_1/7/no1" id="" rows="6" style="width:100%;"><?php echo $tabledata[6]; ?></textarea></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Keahlian berdasarkan bidang ilmu (profesionalisme)</td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_3_1/3/no2" onchange="updateNilai31(2)" value=<?php echo "'".$tabledata[7]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_3_1/4/no2" onchange="updateNilai31(3)" value=<?php echo "'".$tabledata[8]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_3_1/5/no2" onchange="updateNilai31(4)" value=<?php echo "'".$tabledata[9]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_3_1/6/no2" onchange="updateNilai31(5)" value=<?php echo "'".$tabledata[10]."'";?>></td>
										<td><textarea name="3_3_1/7/no2" id="" rows="6" style="width:100%;"><?php echo $tabledata[11]; ?></textarea></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Bahasa Inggris</td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_3_1/3/no3" onchange="updateNilai31(2)" value=<?php echo "'".$tabledata[12]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_3_1/4/no3" onchange="updateNilai31(3)" value=<?php echo "'".$tabledata[13]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_3_1/5/no3" onchange="updateNilai31(4)" value=<?php echo "'".$tabledata[14]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_3_1/6/no3" onchange="updateNilai31(5)" value=<?php echo "'".$tabledata[15]."'";?>></td>
										<td><textarea name="3_3_1/7/no3" id="" rows="6" style="width:100%;"><?php echo $tabledata[16]; ?></textarea></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Penggunaan Teknologi Informasi</td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_3_1/3/no4" onchange="updateNilai31(2)" value=<?php echo "'".$tabledata[17]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_3_1/4/no4" onchange="updateNilai31(3)" value=<?php echo "'".$tabledata[18]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_3_1/5/no4" onchange="updateNilai31(4)" value=<?php echo "'".$tabledata[19]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_3_1/6/no4" onchange="updateNilai31(5)" value=<?php echo "'".$tabledata[20]."'";?>></td>
										<td><textarea name="3_3_1/7/no4" id="" rows="6" style="width:100%;"><?php echo $tabledata[21]; ?></textarea></td>
									</tr>
									<tr>
										<td>5</td>
										<td>Komunikasi</td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_3_1/3/no5" onchange="updateNilai31(2)" value=<?php echo "'".$tabledata[22]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_3_1/4/no5" onchange="updateNilai31(3)" value=<?php echo "'".$tabledata[23]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_3_1/5/no5" onchange="updateNilai31(4)" value=<?php echo "'".$tabledata[24]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_3_1/6/no5" onchange="updateNilai31(5)" value=<?php echo "'".$tabledata[25]."'";?>></td>
										<td><textarea name="3_3_1/7/no5" id="" rows="6" style="width:100%;"><?php echo $tabledata[26]; ?></textarea></td>
									</tr>
									<tr>
										<td>6</td>
										<td>Kerjasama tim</td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_3_1/3/no6" onchange="updateNilai31(2)" value=<?php echo "'".$tabledata[27]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_3_1/4/no6" onchange="updateNilai31(3)" value=<?php echo "'".$tabledata[28]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_3_1/5/no6" onchange="updateNilai31(4)" value=<?php echo "'".$tabledata[29]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_3_1/6/no6" onchange="updateNilai31(5)" value=<?php echo "'".$tabledata[30]."'";?>></td>
										<td><textarea name="3_3_1/7/no6" id="" rows="6" style="width:100%;"><?php echo $tabledata[31]; ?></textarea></td>
									</tr>
									<tr>
										<td>7</td>
										<td>Pengembangan diri</td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_3_1/3/no7" onchange="updateNilai31(2)" value=<?php echo "'".$tabledata[32]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_3_1/4/no7" onchange="updateNilai31(3)" value=<?php echo "'".$tabledata[33]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_3_1/5/no7" onchange="updateNilai31(4)" value=<?php echo "'".$tabledata[34]."'";?>></td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="3_3_1/6/no7" onchange="updateNilai31(5)" value=<?php echo "'".$tabledata[35]."'";?>></td>
										<td><textarea name="3_3_1/7/no7" id="" rows="6" style="width:100%;"><?php echo $tabledata[36]; ?></textarea></td>
									</tr>
									<tr>
										<td colspan="2">Total</td>
										<td><div id="3.3.1/a"></div>(a)</td>
										<td><div id="3.3.1/b"></div>(b)</td>
										<td><div id="3.3.1/c"></div>(c)</td>
										<td><div id="3.3.1/d"></div>(d)</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="form-group"> 
						<p class="col-sm-8 col-sm-offset-2">Catatan : <br> 
							Sediakan dokumen pendukung pada saat asesmen lapangan <br>
							(*) persentase tanggapan pihak pengguna = [(jumlah tanggapan pada peringkat) : (jumlah tanggapan yang ada)] x 100

						</p>
						<div class="col-sm-2"></div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center" colspan="2">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="7">3.3.1.a</td>
									<td colspan="3">Upaya pelacakan dan perekaman data lulusan.</td>
								</tr>
								<tr>
									<td>0</td>
									<td>Tidak ada upaya pelacakan lulusan.</td>
									<td></td>
								</tr>
								<tr>
									<td>1</td>
									<td>Upaya pelacakan lulusan dilakukan sekedarnya dan hasilnya tidak terekam.</td>
									<td></td>
								</tr>
								<tr>
									<td>2</td>
									<td>Upaya pelacakan dilakukan sekedarnya dan hasilnya terekam.</td>
									<td></td>
								</tr>
								<tr>
									<td>3</td>
									<td>Ada upaya yang intensif untuk melacak  lulusan, tetapi hasilnya belum  terekam secara komprehensif.</td>
									<td></td>
								</tr>
								<tr>
									<td>4</td>
									<td>Ada upaya yang intensif untuk melacak  lulusan dan datanya terekam secara komprehensif.</td>
									<td></td>
								</tr>
								<tr>
									<td colspan="2">Nilai</td>
									
									<?php 
									//retrieve Nilai
									$butir = "3.3.1.a";
									$tabledata = array_fill(0, 1, '');
									$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY sub_butir";
									$data = mysqli_query($db, $query);
									$count = mysqli_num_rows($data);
									if ($count>0) {
										while ($row = mysqli_fetch_assoc($data)) {
											$tabledata[0] = $row['isi'];
										}
									}
									?>

									<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="nilai/3_3_1_a" value=<?php echo "'".$tabledata[0]."'";?>></td>
								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center" colspan="2">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="7">3.3.1.b</td>
									<td colspan="3">Penggunaan hasil pelacakan untuk perbaikan: (1) proses pembelajaran, (2) penggalangan dana, (3) informasi pekerjaan, dan (4) membangun jejaring.</td>
								</tr>
								<tr>
									<td>0</td>
									<td>Tidak ada tindak lanjut.</td>
									<td></td>
								</tr>
								<tr>
									<td>1</td>
									<td>Hasil pelacakan untuk perbaikan 1 item.</td>
									<td></td>
								</tr>
								<tr>
									<td>2</td>
									<td>Hasil pelacakan untuk perbaikan 2 item.</td>
									<td></td>
								</tr>
								<tr>
									<td>3</td>
									<td>Hasil pelacakan untuk perbaikan 3 item.</td>
									<td></td>
								</tr>
								<tr>
									<td>4</td>
									<td>Hasil pelacakan untuk perbaikan 4 item.</td>
									<td></td>
								</tr>
								<tr>
									<td colspan="2">Nilai</td>

									<?php 
									//retrieve Nilai
									$butir = "3.3.1.b";
									$tabledata = array_fill(0, 1, '');
									$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY sub_butir";
									$data = mysqli_query($db, $query);
									$count = mysqli_num_rows($data);
									if ($count>0) {
										while ($row = mysqli_fetch_assoc($data)) {
											$tabledata[0] = $row['isi'];
										}
									}
									?>

									<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="nilai/3_3_1_b" value=<?php echo "'".$tabledata[0]."'";?>></td>
								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center" colspan="2">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="7">3.3.1.c</td>
									<td colspan="3">Pendapat pengguna (employer) lulusan terhadap kualitas alumni.</td>
								</tr>
								<tr>
									<td>(a)</td>
									<td>Jumlah persentase untuk respons sangat baik (Kisaran nilai 0 - 700%)</td>
									<td id="nilai/3.3.1.c/1"></td>
								</tr>
								<tr>
									<td>(b)</td>
									<td>Jumlah persentase untuk respons baik (Kisaran nilai 0 - 700%)</td>
									<td id="nilai/3.3.1.c/2"></td>
								</tr>
								<tr>
									<td>(c)</td>
									<td>Jumlah persentase untuk respons cukup (Kisaran nilai 0 - 700%)</td>
									<td id="nilai/3.3.1.c/3"></td>
								</tr>
								<tr>
									<td>(d)</td>
									<td>Jumlah persentase untuk respons kurang (Kisaran nilai 0 - 700%)</td>
									<td id="nilai/3.3.1.c/4"></td>
								</tr>
								<tr>
									<td colspan="2">Skor Akhir (Jumlah a + b + c + d paling besar = 700%)</td>
									<td id="nilai/3.3.1.c/5"></td>
								</tr>
								<tr>
									<td colspan="2">Nilai</td>
									<td id="nilai/3.3.1.c/nilai"></td>
									<input type="hidden" id="nilai/3.3.1.c/nilaihid" name="nilai/3_3_1_c" value="">
								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">
						<?php
						//data retrieve
						$butir = "3.3.2";
						$tabledata = array_fill(0, 2, '');
						$query="SELECT isi_float,isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								if ($index==0) {
									$tabledata[$index] = $row['isi_text'];
								}else if ($index==1) {
									$tabledata[$index] = $row['isi_float'];
								}
								
								$index++;
							}
						}
						?>
						<label class="col-sm-2 text-right">3.3.2</label> 
						<label class="col-sm-8">Rata-rata waktu tunggu lulusan untuk memperoleh pekerjaan yang pertama = <input type="text" pattern="[0-9]+([\.][0-9]+)?" style="width:5%;" name="3_3_2" id="3.3.2" onchange="updateNilai32()" value=<?php echo "'".$tabledata[1]."'";?>> bulan</label>
					</div>

					<div class="form-group"> 
						<p class="col-sm-8 col-sm-offset-2">(Jika belum ada lulusan, maka diisi angka 0.) <br> 
							(Jika PS kedinasan yang lulusannya semua sudah bekerja saat kuliah, diisi angka 1.) <br> <br>
							Jelaskan bagaimana data ini diperoleh:
						</p>
						<div class="col-sm-2"></div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<textarea type="text" class="form-control" rows="10" name="3_3_2/penjelasan" placeholder="" maxlength="60000"><?php echo $tabledata[0]; ?></textarea>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="2">3.3.2</td>
									<td>RMT (Rata-rata masa tunggu lulusan untuk bekerja).</td>
									<td id="nilai/3.3.2/1"></td>
								</tr>
								<tr>
									<td>Nilai</td>
									<td id="nilai/3.3.2/nilai"></td>
									<input type="hidden" id="nilai/3.3.2/nilaihid" name="nilai/3_3_2" value="">
								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">
						<?php
						//data retrieve
						$butir = "3.3.3";
						$tabledata = array_fill(0, 2, '');
						$query="SELECT isi_float,isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								if ($index==0) {
									$tabledata[$index] = $row['isi_text'];
								}else if ($index==1) {
									$tabledata[$index] = $row['isi_float'];
								}
								$index++;
							}
						}
						?>
						<label class="col-sm-2 text-right">3.3.3</label> 
						<label class="col-sm-8">Persentase lulusan yang bekerja pada bidang yang sesuai dengan keahliannya = <input type="text" pattern="[0-9]+([\.][0-9]+)?" style="width:5%;" name="3_3_3" id="3.3.3" onchange="updateNilai33()" value=<?php echo "'".$tabledata[1]."'";?>> % (Kisaran 0 - 100)</label>
					</div>

					<div class="form-group"> 
						<p class="col-sm-8 col-sm-offset-2">Jelaskan bagaimana data ini diperoleh:
						</p>
						<div class="col-sm-2"></div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<textarea type="text" class="form-control" rows="10" name="3_3_3/penjelasan" placeholder="" maxlength="60000"><?php echo $tabledata[0]; ?></textarea>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="2">3.3.3</td>
									<td>Persentase kesesuaian bidang kerja dengan bidang studi (keahlian) lulusan.  Kisaran nilai 0-100%.</td>
									<td id="nilai/3.3.3/1"></td>
								</tr>
								<tr>
									<td>Nilai</td>
									<td id="nilai/3.3.3/nilai"></td>
									<input type="hidden" id="nilai/3.3.3/nilaihid" name="nilai/3_3_3" value="">
								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">
						<?php
						//data retrieve
						$butir = "3.4";
						$tabledata = array_fill(0, 1, '');
						$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							while ($row = mysqli_fetch_assoc($data)) {
								$tabledata[0] = $row['isi_text'];
							}
						}
						?>
						<label class="col-sm-2 text-right">3.4</label> 
						<label class="col-sm-8">Himpunan Alumni</label>
					</div>

					<div class="form-group"> 
						<p class="col-sm-8 col-sm-offset-2">
							Jelaskan apakah lulusan program studi memiliki himpunan alumni.  Jika memiliki, jelaskan aktivitas dan hasil kegiatan dari himpunan alumni untuk kemajuan program studi dalam kegiatan akademik dan non akademik, meliputi sumbangan dana, sumbangan fasilitas, keterlibatan dalam kegiatan, pengembangan jejaring, dan penyediaan fasilitas.
						</p>
						<div class="col-sm-2"></div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<textarea type="text" class="form-control" rows="15" name="3_4" placeholder="" maxlength="60000" ><?php echo $tabledata[0]; ?></textarea>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center" colspan="2">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="7">3.4.1</td>
									<td colspan="3">Bentuk partisipasi lulusan dan alumni untuk kegiatan akademik: Sumbangan dana, Sumbangan fasilitas, Keterlibatan dalam kegiatan akademik, Pengembangan jejaring, Penyediaan fasilitas untuk kegiatan akademik.</td>
								</tr>
								<tr>
									<td>0</td>
									<td>Tidak ada partisipasi alumni.</td>
									<td></td>
								</tr>
								<tr>
									<td>1</td>
									<td>Hanya 1 bentuk partisipasi saja yang dilakukan oleh alumni.</td>
									<td></td>
								</tr>
								<tr>
									<td>2</td>
									<td>Hanya 2 bentuk partisipasi yang dilakukan oleh alumni.</td>
									<td></td>
								</tr>
								<tr>
									<td>3</td>
									<td>3-4 bentuk partisipasi dilakukan oleh alumni.</td>
									<td></td>
								</tr>
								<tr>
									<td>4</td>
									<td>Semua bentuk partisipasi dilakukan oleh alumni.</td>
									<td></td>
								</tr>
								<tr>
									<td colspan="2">Nilai</td>

									<?php 
									//retrieve Nilai
									$butir = "3.4.1";
									$tabledata = array_fill(0, 1, '');
									$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY sub_butir";
									$data = mysqli_query($db, $query);
									$count = mysqli_num_rows($data);
									if ($count>0) {
										while ($row = mysqli_fetch_assoc($data)) {
											$tabledata[0] = $row['isi'];
										}
									}
									?>

									<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="nilai/3_4_1" value=<?php echo "'".$tabledata[0]."'";?>></td>
								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center" colspan="2">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="7">3.4.2</td>
									<td colspan="3">Bentuk partisipasi lulusan dan alumni untuk kegiatan non akademik: Sumbangan dana, Sumbangan fasilitas, Keterlibatan dalam kegiatan non akademik, Pengembangan jejaring, Penyediaan fasilitas untuk kegiatan non akademik. </td>
								</tr>
								<tr>
									<td>0</td>
									<td>Tidak ada partisipasi alumni.</td>
									<td></td>
								</tr>
								<tr>
									<td>1</td>
									<td>Hanya 1 bentuk partisipasi saja yang dilakukan oleh alumni.</td>
									<td></td>
								</tr>
								<tr>
									<td>2</td>
									<td>Hanya 2 bentuk partisipasi yang dilakukan oleh alumni.</td>
									<td></td>
								</tr>
								<tr>
									<td>3</td>
									<td>3-4 bentuk partisipasi dilakukan oleh alumni.</td>
									<td></td>
								</tr>
								<tr>
									<td>4</td>
									<td>Semua bentuk partisipasi dilakukan oleh alumni.</td>
									<td></td>
								</tr>
								<tr>
									<td colspan="2">Nilai</td>

									<?php 
									//retrieve Nilai
									$butir = "3.4.2";
									$tabledata = array_fill(0, 1, '');
									$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY sub_butir";
									$data = mysqli_query($db, $query);
									$count = mysqli_num_rows($data);
									if ($count>0) {
										while ($row = mysqli_fetch_assoc($data)) {
											$tabledata[0] = $row['isi'];
										}
									}
									?>
									<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="nilai/3_4_2" value=<?php echo "'".$tabledata[0]."'";?>></td>
								</tr>
							</table>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>
</body>

<script>
	$(document).ready(function() {
		$("form input, form select, form textarea").attr('readonly',true);
		$("button").hide();
		$("#editButton").show();
		$("#hideButton").show();
	});

	$("#editButton").click(function() {
		$("form input, form select, form textarea").attr('disabled',false);
		$("button").not($("#hideButton,#showButton")).show();
		$("#editButton").hide();
	});

	$("#hideButton").click(function() {
		$(".form-group").not($(".tableNilai").parent().parent()).hide();
		$("#showButton").show();
		$("#hideButton").hide();
	});

	$("#showButton").click(function() {
		$(".form-group").show();
		$("#showButton").hide();
		$("#hideButton").show();
	});

	$('form').submit(function() {
		$('body').hide();
		$("header").hide();
		$("html").prepend("<div class='ball'></div><div class='ball1'></div><h1 style='margin-left:43%'>MEMPROSES<h1>");
	});
</script>

</html>