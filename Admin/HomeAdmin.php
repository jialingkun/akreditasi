<!doctype html>
<html>
<?php
require "../Cookies.php";
?>
<head>
	<title>Admin</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/style.css" rel="stylesheet">
	<link href="../font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>
	<div class='wrapper'>
		<header>
			<nav class="navbar navbar-default navbar-fixed-top">
				<div id="navbar" class="navbar-collapse collapse col-md-offset-3">
					<ul class="nav navbar-nav">
						<li class="active"><a href="HomeAdmin.php">Home</a></li>
						<li><a href="Prodi.php">Edit Prodi</a></li>
						<li><a href="EditProfil.php">Edit Profil</a></li>
						<li><a href="UbahPassword.php">Ubah Password</a></li>
						<li><a href="Logout.php" class="col-md-offset-10">Log Out</a></li>
					</ul>
				</div>
			</nav>
			<div class='text-center'>
				<br>
				<br>
				<h1>Periode Simulasi</h1>
			</div>
		</header>
	</div>
	
	<div class = "container">
		<hr class="colorgraph"><br>
		<div class="row">

			<?php 
			echo '<a href="Periode.php" class="btn btn-sm btn-success col-sm-offset-1"><span class="glyphicon glyphicon-plus"></span> Tambah Periode</a>';
				// display data in table
			echo "<table class='table table-hover' style='width:83%; margin-top:1%;' align='center'>";
			echo "<thead> <tr> <th>No. Urut</th> <th>Nama Periode</th> <th>Status</th><th></th><th>data</th></tr> </thead>";

			require "../Database/DatabaseConnection.php";
			$query="SELECT * FROM periode ORDER BY idPeriode DESC;";
			$data = mysqli_query($db, $query);
			while($row = mysqli_fetch_assoc($data)){
				?>
				<tbody> <tr>
					<td><?php echo $row['idPeriode'];?></td>
					<td><?php echo $row['NamaPeriode'];?></td>

					<?php if($row['aktif'] == 0) { 
						echo "<td><span class='label label-danger'>OFF</span></td>";
						echo "<td><a href='aktifPeriode.php?periode=".$row['idPeriode']."' class='btn btn-sm btn-primary'><span class='glyphicon glyphicon-ok'></span> Aktifkan</a></td>";
					}else{ 
						echo "<td><span class='label label-success'>ON </span></td>";
						echo "<td><a href='tutupPeriode.php' class='btn btn-sm btn-danger'><span class='glyphicon glyphicon-remove'></span> Matikan</a></td>";
					}
					?>
					<td><a href="pilihProdi.php?periode=<?php echo $row['idPeriode'];?>" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-search"></span> Lihat Data</a></td>
					
				</tr></tbody>
				<?php
			}
			echo "</table>"; 
			?>

		</div>
	</div>

</body>

</html>