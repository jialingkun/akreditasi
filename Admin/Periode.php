<!doctype html>

<?php
	require "../Cookies.php";
	require "../Database/DatabaseConnection.php";
	$query='select * from periode where aktif = 1';
	$data = mysqli_query($db, $query);
?>

<html>

<head>
	<title>Priode</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/style.css" rel="stylesheet">
	<link href="../font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>
	
	<div class='wrapper'>
		<header>
			<nav class="navbar navbar-default navbar-fixed-top">
				<div id="navbar" class="navbar-collapse collapse col-md-offset-3">
					<ul class="nav navbar-nav ">
						<li><a href="HomeAdmin.php">Home</a></li>
						<li><a href="Prodi.php">Edit Prodi</a></li>
						<li><a href="EditProfil.php">Edit Profil</a></li>
						<li><a href="UbahPassword.php">Ubah Password</a></li>
						<li><a href="Logout.php" class="col-md-offset-10">Log Out</a></li>
					</ul>
				</div>
			</nav>
			<div class='text-center'>
				<br>
				<br>
				<h1>Periode Aktif</h1>
			</div>
		</header>
	</div>
	
	
	<div class = "container">
		<hr class="colorgraph"><br>
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<?php
					if(mysqli_num_rows($data)>0){
						echo "<div class='col-md-10 col-md-offset-2'>";
						$row = mysqli_fetch_assoc($data);
						echo "<label>";
						echo "Periode yang sekarang aktif :"."<br>";
						echo "</label>";
						echo "</div>";
						
						echo "<div class='text-center'>";
						echo "<label>";
						echo $row['NamaPeriode']."<br>";
						echo "</label>";
						echo "</div>";
					}else{
						echo "<div class='col-md-offset-3'>";
						echo "<label>";
						echo "Tidak ada periode aktif";
						echo "</label>";
						echo "</div>";
					}
				?>
				<br>
				<br>
				<form action="ProsesTambahPriode.php" method="post" class="form-signin">
					<div class="col-md-offset-4">
						<label>Tambah Periode</label>
					</div>
					<input type="text" class="form-control" name="NamaPeriode" required="" autofocus=""/> 
					<br>
					<button class="btn btn-lg btn-primary btn-block" name="Submit">Tambah</button>
				</form>
			</div>
		</div>
	</div>
</body>

</html>