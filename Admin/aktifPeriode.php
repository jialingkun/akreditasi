<?php
require "../Cookies.php";
 // check if the 'id' variable is set in URL, and check that it is valid
if (isset($_GET['periode']))
{
 	 // connect to the database
	require "../Database/DatabaseConnection.php";

	$id = mysqli_escape_string($db, $_GET['periode']);

	$query="SELECT idPeriode FROM periode";
	$data = mysqli_query($db, $query);
	$total = mysqli_num_rows($data);

	$aktif=1;
	$nonaktif=0;

	while($row = mysqli_fetch_assoc($data)){
		if($row["idPeriode"] != $id){
			$query="UPDATE `periode` SET `aktif`=".$nonaktif." WHERE idPeriode =".$row["idPeriode"];
			mysqli_query($db, $query);
		}else{
			$query="UPDATE `periode` SET `aktif`=".$aktif." WHERE idPeriode =".$row["idPeriode"];
			mysqli_query($db, $query);
		}
	}

	?>
	<script>
		window.location = "HomeAdmin.php"
	</script>
	<?php   
}

?>

