<!doctype html>
<html>
<?php
require "../Cookies.php";
require "../Database/DatabaseConnection.php";

$periode = $_GET["periode"];

$query="select NamaPeriode from periode where idPeriode=$periode";
$data = mysqli_query($db, $query);
$row = mysqli_fetch_assoc($data);
$namaPeriode = $row['NamaPeriode'];

$prodi = $_GET['prodi'];

$query="select namaProdi from prodi where idProdi='$prodi'";
$data = mysqli_query($db, $query);
$row = mysqli_fetch_assoc($data);
$namaProdi = $row['namaProdi'];

$jabatan = $_GET['jabatan'];

//ambil user Kaprodi
$query="SELECT username FROM `isi_borang` NATURAL JOIN user WHERE idProdi=$prodi AND idPeriode=$periode GROUP BY username limit 1";
$data = mysqli_query($db, $query);
$row = mysqli_fetch_assoc($data);
$userKaprodi = $row['username'];

//ambil user Auditor
$query="SELECT username FROM `isi_borang` WHERE username NOT IN (SELECT username FROM `isi_borang` NATURAL JOIN user WHERE idProdi=$prodi AND idPeriode=$periode GROUP BY username) AND idProdi=$prodi AND idPeriode=$periode limit 1";
$data = mysqli_query($db, $query);
$row = mysqli_fetch_assoc($data);
$userAuditor = $row['username'];


if ($jabatan=="Kaprodi") {
	$usernameFix = $userKaprodi;
	$jabatanInverse = "Auditor";
}else{
	$usernameFix = $userAuditor;
	$jabatanInverse = "Kaprodi";
}-

$standar= 1;

//cek data auditor
$revisi="Telah direvisi";
$username= $usernameFix;
$query="select username from isi_borang where idProdi='$prodi' and idPeriode='$periode' and username='$username' and standar='$standar' limit 1";
$data = mysqli_query($db, $query);
$count = mysqli_num_rows($data);
if ($count<1){
	//ambil username kaprodi
	$query="select username from isi_borang where idProdi='$prodi' and idPeriode='$periode' and username!='$username' and standar='$standar' limit 1";
	$data = mysqli_query($db, $query);
	$row = mysqli_fetch_assoc($data);
	$username = $row['username'];
	$revisi="Tanpa Revisi";
}

if ($jabatan=="Kaprodi") {
	$revisi="";
}

?>
<head>
	<title>Admin</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/style.css" rel="stylesheet">
	<link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="../css/loading.css">
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script src="../js/jquery.min.js"></script>
</head>

<body>
	<div class='wrapper'>
		<header>
			<nav class="navbar navbar-default navbar-fixed-top">
				<div id="navbar" class="navbar-collapse collapse">
					<div class="navbar-header">
						<a class="navbar-brand" href="#"><?php echo $jabatan ?></a>
					</div>
					<ul class="nav navbar-nav">
						<?php echo "
						<li><a href='HomeKaprodi.php?periode=$periode&jabatan=$jabatan&prodi=$prodi'>Profil</a></li>
						<li class='active'><a href='Standar1.php?periode=$periode&jabatan=$jabatan&prodi=$prodi'>Standar 1</a></li>
						<li><a href='Standar2.php?periode=$periode&jabatan=$jabatan&prodi=$prodi'>Standar 2</a></li>
						<li><a href='Standar3.php?periode=$periode&jabatan=$jabatan&prodi=$prodi'>Standar 3</a></li>
						<li><a href='Standar4.php?periode=$periode&jabatan=$jabatan&prodi=$prodi'>Standar 4</a></li>
						<li><a href='Standar5.php?periode=$periode&jabatan=$jabatan&prodi=$prodi'>Standar 5</a></li>
						<li><a href='Standar6.php?periode=$periode&jabatan=$jabatan&prodi=$prodi'>Standar 6</a></li>
						<li><a href='Standar7.php?periode=$periode&jabatan=$jabatan&prodi=$prodi'>Standar 7</a></li>
						<li><a href='Nilai.php?periode=$periode&jabatan=$jabatan&prodi=$prodi'>Nilai</a></li>
						
						<li><a href='pilihProdi.php?periode=$periode' class='col-md-offset-7'>Kembali</a></li>	
						<li><a href='Standar1.php?periode=$periode&jabatan=$jabatanInverse&prodi=$prodi'>Ke $jabatanInverse</a></li>
						<li><a href='Logout.php'>Log Out</a></li>
						"; ?>
					</ul>
				</div>
			</nav>
			<div class='text-center'>
				<br>
				<br>
				<h1>STANDAR 1</h1>
				<h4>VISI, MISI, TUJUAN DAN SASARAN, SERTA STRATEGI PENCAPAIAN</h4>
				<h5><?php echo $namaProdi ?></h5>
				<h5>Periode: <?php echo $namaPeriode ?></h5>
				<h5><b><?php echo $revisi?></b></h5>
			</div>
		</header>
	</div>
	
	<div class = "container">
		<hr class="colorgraph"><br>
		<div class="row">
			<div class="col-md-12">
				<form action="ProsesStandar1.php" method="post" class="form-horizontal">
					<?php 
					// default id prodi yang disubmit khusus auditor
					echo "
					<input type='hidden' name='prodi' value='".$prodi."'>";
					?>
					<div class="fixed-button">
						<button type="button" class="btn btn-default" id="hideButton">Hide</button>
						<button type="button" class="btn btn-default" id="showButton">Show</button>
					</div>
					<div class="form-group">
						<label class="col-sm-2 text-right">1.1</label> 
						<label class="col-sm-8">Visi, Misi, Tujuan, dan Sasaran serta Strategi Pencapaian</label>
					</div>
					<div class="form-group">
						<label class="col-sm-2 text-right">1.1.1</label> 
						<label class="col-sm-8">Mekanisme penyusunan visi, misi, tujuan dan sasaran Program Studi, serta pihak-pihak yang dilibatkan :</label>
					</div>
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "1.1.1";
							$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
							$data = mysqli_query($db, $query);
							$row = mysqli_fetch_assoc($data);
							$print = $row["isi_text"];
							?>
							<textarea type="text" class="form-control" rows="20" name="1_1_1" placeholder="" maxlength="60000"><?php echo $print;?></textarea>
						</label>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 text-right">1.1.2 </label> 
						<label class="col-sm-8">Visi :</label>
					</div>
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "1.1.2";
							$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
							$data = mysqli_query($db, $query);
							$row = mysqli_fetch_assoc($data);
							$print = $row["isi_text"];
							?>
							<textarea type="text" class="form-control" rows="10" name="1_1_2" placeholder="" maxlength="60000"><?php echo $print;?></textarea>
						</label>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 text-right">1.1.3 </label> 
						<label class="col-sm-8">Misi :</label>
					</div>
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "1.1.3";
							$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
							$data = mysqli_query($db, $query);
							$row = mysqli_fetch_assoc($data);
							$print = $row["isi_text"];
							?>
							<textarea type="text" class="form-control" rows="10" name="1_1_3" placeholder="" maxlength="60000"><?php echo $print;?></textarea>
						</label>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 text-right">1.1.4 </label> 
						<label class="col-sm-8">Tujuan :</label>
					</div>
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "1.1.4";
							$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
							$data = mysqli_query($db, $query);
							$row = mysqli_fetch_assoc($data);
							$print = $row["isi_text"];
							?>
							<textarea type="text" class="form-control" rows="15" name="1_1_4" placeholder="" maxlength="60000"><?php echo $print;?></textarea>
						</label>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 text-right">1.1.5 </label> 
						<label class="col-sm-8">Sasaran dan Strategi Pencapaian :</label>
					</div>
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "1.1.5";
							$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
							$data = mysqli_query($db, $query);
							$row = mysqli_fetch_assoc($data);
							$print = $row["isi_text"];
							?>
							<textarea type="text" class="form-control" rows="25" name="1_1_5" placeholder="" maxlength="60000"><?php echo $print;?></textarea>
						</label>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "1.1.a";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="6">1.1.a</td>
										<td colspan="3">Kejelasan dan kerealistikan visi, misi, tujuan, dan sasaran program studi</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Memiliki visi, misi, tujuan, dan sasaran yang kurang jelas dan tidak realistik</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Memiliki visi, misi, tujuan, dan sasaran yang cukup jelas namun kurang realistik</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Memiliki visi, misi, tujuan, dan sasaran jelas dan  realistik</td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Memiliki visi, misi, tujuan, dan sasaran yang sangat jelas dan sangat realistik</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control"  placeholder="" value='<?php echo $print;?>' name="nilai/1_1_a"/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "1.1.b";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="6">1.1.b</td>
										<td colspan="3">Strategi pencapaian sasaran dengan rentang waktu yang jelas dan didukung oleh dokumen</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Strategi pencapaian sasaran: (1) tanpa adanya tahapan waktu yang jelas, (2) didukung dokumen yang kurang lengkap</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Strategi pencapaian sasaran: (1) dengan tahapan waktu yang jelas, dan cukup realistik, (2) didukung dokumen yang cukup lengkap</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Strategi pencapaian sasaran: (1) dengan tahapan waktu yang jelas, dan realistik, (2) didukung dokumen yang  lengkap</td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Strategi pencapaian sasaran:(1) dengan tahapan waktu yang jelas dan sangat realistik, (2) didukung dokumen yang sangat lengkap</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control"  placeholder="" value='<?php echo $print;?>' name="nilai/1_1_b"/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 text-right">1.2 </label> 
						<label class="col-sm-8">Sosialisasi :</label>
					</div>
					<div class="form-group">
						<label class="col-sm-2 text-right"></label> 
						<label class="col-sm-9">Upaya penyebaran/sosialisasi visi, misi dan tujuan Program Studi serta pemahaman sivitas akademika (dosen dan mahasiswa) dan tenaga kependidikan :</label>
					</div>
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "1.2";
							$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
							$data = mysqli_query($db, $query);
							$row = mysqli_fetch_assoc($data);
							$print = $row["isi_text"];
							?>
							<textarea type="text" class="form-control" rows="25" name="1_2" placeholder="" maxlength="60000"><?php echo $print;?></textarea>
						</label>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "1.2";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="6">1.2</td>
										<td colspan="3">Efektivitas sosialisasi visi, misi PS: tingkat pemahaman sivitas akademika</td>	
									</tr>
									<tr>
										<td>1</td>
										<td>Tidak dipahami oleh seluruh sivitas akademika dan tenaga kependidikan</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Kurang dipahami oleh  sivitas akademika  dan tenaga kependidikan</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Dipahami dengan baik oleh sebagian  sivitas akademika dan tenaga kependidikan</td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Dipahami dengan baik oleh seluruh sivitas akademika  dan tenaga kependidikan</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control" placeholder="" value='<?php echo $print;?>' name="nilai/1_2"/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>
	
</body>

<script>
	$(document).ready(function() {
		$("form input, form select, form textarea").attr('readonly',true);
		$("button").hide();
		$("#editButton").show();
		$("#hideButton").show();
	});

	$("#editButton").click(function() {
		$("form input, form select, form textarea").attr('disabled',false);
		$("button").not($("#hideButton,#showButton")).show();
		$("#editButton").hide();
	});

	$("#hideButton").click(function() {
		$(".form-group").not($(".tableNilai").parent().parent()).hide();
		$("#showButton").show();
		$("#hideButton").hide();
	});

	$("#showButton").click(function() {
		$(".form-group").show();
		$("#showButton").hide();
		$("#hideButton").show();
	});

	$('form').submit(function() {
		$('body').hide();
		$("header").hide();
		$("html").prepend("<div class='ball'></div><div class='ball1'></div><h1 style='margin-left:43%'>MEMPROSES<h1>");
	});
</script>

</html>