<!doctype html>
<html>
<?php
	require "../Cookies.php";
?>
<head>
	<title>Admin</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/style.css" rel="stylesheet">
	<link href="../font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>
	<div class='wrapper'>
		<header>
			<nav class="navbar navbar-default navbar-fixed-top">
				<div id="navbar" class="navbar-collapse collapse col-md-offset-3">
					<ul class="nav navbar-nav">
						<li><a href="HomeAdmin.php">Home</a></li>
						<li><a href="Prodi.php">Edit Prodi</a></li>
						<li><a href="EditProfil.php">Edit Profil</a></li>
						<li class="active"><a href="UbahPassword.php">Ubah Password</a></li>
						<li><a href="Logout.php" class="col-md-offset-10">Log Out</a></li>
					</ul>
				</div>
			</nav>
			<div class='text-center'>
				<br>
				<br>
				<h1>Ubah Password</h1>
			</div>
		</header>
	</div>
	
	<div class = "container">
		<hr class="colorgraph"><br>
		<div class="row">
			<div class='col-md-6 col-md-offset-3'>
				<form class="form-horizontal" role="form" action="ProsesUbahPassword.php" method="post">
					<div class="form-group">
						<label class="control-label col-md-4">Password Lama</label>
						<div class="col-md-5">
							<input type="password" class="form-control" name="PasswordLama" required="" autofocus="">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-4">Password Baru</label>
						<div class="col-md-5">
							<input type="password" class="form-control" name="PasswordBaru" required="" >
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-4">Ulangi Password</label>
						<div class="col-md-5">
							<input type="password" class="form-control" name="PasswordBaru1" required="">
						</div>
					</div>
					<div class="form-group">        
						<div class="col-md-1 col-md-offset-4 ">
							<button type="submit" class="btn btn-primary">Ubah Password</button>
						</div>
					</div>
				</form>
			</div>	
		</div>
	</div>
	
</body>

</html>