<!doctype html>
<html>
<?php 

require "../Cookies.php";
require "../Database/DatabaseConnection.php";

$idPeriode = $_GET['periode'];
$query="select NamaPeriode from periode where idPeriode=$idPeriode;";
$data = mysqli_query($db, $query);
$row = mysqli_fetch_assoc($data);
$periode = $row['NamaPeriode'];
?>

<head>
	<title>Login</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/style.css" rel="stylesheet">
	<link href="../font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>
	<div class='wrapper'>
		<header>
			<nav class="navbar navbar-default navbar-fixed-top">
				<div id="navbar" class="navbar-collapse collapse col-md-offset-3">
					<ul class="nav navbar-nav">
						<li><a href="HomeAdmin.php">Lihat Data</a></li>
						<li><a href="Periode.php">Periode</a></li>
						<li><a href="Prodi.php">Prodi</a></li>
						<li><a href="EditProfil.php">Edit Profil</a></li>
						<li><a href="UbahPassword.php">Ubah Password</a></li>
						<li><a href="Logout.php" class="col-md-offset-9">Log Out</a></li>
					</ul>
				</div>
			</nav>
			<div class='text-center' style="margin-top:5%">
				<h1>Pilih Prodi</h1>
			</div>
			<br>
			<hr class="colorgraph">
			<br>
			<div class='text-center'>
				<label>Periode: <?php echo $periode; ?></label>
			</div>
			<br>
		</header>
	</div>


	<div class = "container">
		<form action="Nilai.php" method="get" class="form-horizontal">
		<input type="hidden" name="periode" value=<?php echo $idPeriode ?>>
		<input type="hidden" name="jabatan" value="Kaprodi">
			<div class="col-md-4 col-md-offset-4">
				<select class="form-control" name="prodi">
					<?php 
					$query='select * from prodi where idProdi!=0';
					$data = mysqli_query($db, $query);
					while ($row = mysqli_fetch_assoc($data)){
						echo "<option value='".$row['idProdi']."'>".$row['namaProdi']."</option>";
					}
					?>
				</select> 
			</div>
			<div class="col-md-4 col-md-offset-4" style="margin-top:3%" align="center">
				<button type="submit" class="btn btn-primary">Lanjutkan</button>
			</div>
		</form>
	</div>
</body>

</html>