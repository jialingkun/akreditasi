<!doctype html>
<html>
<?php
	require "../Cookies.php";
?>
<head>
	<title>Admin</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/style.css" rel="stylesheet">
	<link href="../font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script src="../js/bootstrap.min.js"></script>
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>
	<div class='wrapper'>
		<header>
			<nav class="navbar navbar-default navbar-fixed-top">
				<div id="navbar" class="navbar-collapse collapse col-md-offset-4">
					<ul class="nav navbar-nav">
						<li><a href="HomeAdmin.php">Lihat Rekap</a></li>
						<li><a href="Periode.php">Periode</a></li>
						<li class="active"><a href="EditProfil.php">Edit Profil</a></li>
						<li><a href="UbahPassword.php">Ubah Password</a></li>
						<li><a href="Logout.php" class="col-md-offset-10">Log Out</a></li>
					</ul>
				</div>
			</nav>
			<div class='text-center'>
				<br>
				<br>
				<h1>Edit User</h1>
			</div>
		</header>
	</div>
	
	<div class = "container">
		<hr class="colorgraph"><br>
		
		<form class="form-horizontal" role="form" action="ProsesEditUser.php" method="post">

			<?php 

			if (isset($_GET['Username'])){
				require "../Database/DatabaseConnection.php";

				$username = mysqli_escape_string($db, $_GET['Username']);

				echo "<input type='hidden' name='Username' value='".$username."'>";

				$query='SELECT * FROM user WHERE username="'.$username.'"';
				$result = mysqli_query($db, $query); 
				$data = mysqli_fetch_assoc($result);  
			}

			?>


			<div class="form-group">
				<label class="control-label col-sm-4">NIP</label>
				<div class="col-sm-2">
					<input type="text" class="form-control" name="NIP" required="" autofocus="" value="<?php echo $data['nip'] ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4">Nama</label>
				<div class="col-sm-4">
					<input type="text" class="form-control" name="Nama" required="" autofocus="" value="<?php echo $data['nama'] ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4">Username</label>
				<div class="col-sm-2">
					<input type="text" class="form-control" name="Username" required="" autofocus="" value="<?php echo $data['username'] ?>" disabled>
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-4">Password</label>
				<div class="col-sm-2">
					<input type="password" class="form-control" name="Password" required="" autofocus="" value="<?php echo $data['password'] ?>">
				</div>
			</div>


			<div class="form-group">
				<label class="control-label col-sm-4">Jabatan</label>
				<div class="col-sm-3">
					<select class="form-control" name="Prodi">
						<?php
						$query="SELECT * FROM prodi";
						$result = mysqli_query($db, $query);
						while($row = mysqli_fetch_assoc($result)){
							if ($row['idProdi']==$data['idProdi']) {
								if ($row['idProdi']==0) {
									echo "<option selected value='".$row['idProdi']."'>".$row['namaProdi']."</option>";	
								} else{
									echo "<option selected value='".$row['idProdi']."'>Kaprodi ".$row['namaProdi']."</option>";
								}
							}else{
								if ($row['idProdi']==0) {
									echo "<option value='".$row['idProdi']."'>".$row['namaProdi']."</option>";	
								} else{
									echo "<option value='".$row['idProdi']."'>Kaprodi ".$row['namaProdi']."</option>";
								}
							}
							
						}
						?>
					</select>
				</div>
			</div>


			<div class="form-group">
				<label class="control-label col-sm-4">Aktif</label>
				<div class="col-sm-2" style="padding-left:3%">
					<div class="checkbox">
					<?php 
						if ($data['Aktif']>0) {
							echo "<input type='checkbox' name='Aktif' value='1' checked>";
						}
						else{
							echo "<input type='checkbox' name='Aktif' value='1'>";
						}
					 ?>
					</div>
				</div>
			</div>



			<div class="form-group">        
				<div class="col-sm-offset-5 col-sm-5">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>
		</form>



	</div>

</body>

</html>