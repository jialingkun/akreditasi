<!doctype html>
<html>
<?php
require "../Database/DatabaseConnection.php";
require "../Cookies.php";

$null=0;
$kolom1=1;
$kolom2=2;
$kolom3=3;
$kolom4=4;
$kolom5=5;
$kolom6=6;
$kolom7=7;
$kolom8=8;
$kolom9=9;
$subno1=1;
$subno2=2;
$subno3=3;
$subno4=4;
$no1=1;
$no2=2;
$no3=3;
$no4=4;
$no5=5;
$no6=6;

$periode = $_GET["periode"];

$query="select NamaPeriode from periode where idPeriode=$periode";
$data = mysqli_query($db, $query);
$row = mysqli_fetch_assoc($data);
$namaPeriode = $row['NamaPeriode'];

$prodi = $_GET['prodi'];

$query="select namaProdi from prodi where idProdi='$prodi'";
$data = mysqli_query($db, $query);
$row = mysqli_fetch_assoc($data);
$namaProdi = $row['namaProdi'];

$jabatan = $_GET['jabatan'];

//ambil user Kaprodi
$query="SELECT username FROM `isi_borang` NATURAL JOIN user WHERE idProdi=$prodi AND idPeriode=$periode GROUP BY username limit 1";
$data = mysqli_query($db, $query);
$row = mysqli_fetch_assoc($data);
$userKaprodi = $row['username'];

//ambil user Auditor
$query="SELECT username FROM `isi_borang` WHERE username NOT IN (SELECT username FROM `isi_borang` NATURAL JOIN user WHERE idProdi=$prodi AND idPeriode=$periode GROUP BY username) AND idProdi=$prodi AND idPeriode=$periode limit 1";
$data = mysqli_query($db, $query);
$row = mysqli_fetch_assoc($data);
$userAuditor = $row['username'];


if ($jabatan=="Kaprodi") {
	$usernameFix = $userKaprodi;
	$jabatanInverse = "Auditor";
}else{
	$usernameFix = $userAuditor;
	$jabatanInverse = "Kaprodi";
}-

$standar= 6;

//cek data auditor
$revisi="Telah direvisi";
$username= $usernameFix;
$query="select username from isi_borang where idProdi='$prodi' and idPeriode='$periode' and username='$username' and standar='$standar' limit 1";
$data = mysqli_query($db, $query);
$count = mysqli_num_rows($data);
if ($count<1){
	//ambil username kaprodi
	$query="select username from isi_borang where idProdi='$prodi' and idPeriode='$periode' and username!='$username' and standar='$standar' limit 1";
	$data = mysqli_query($db, $query);
	$row = mysqli_fetch_assoc($data);
	$username = $row['username'];
	$revisi="Tanpa Revisi";
}

if ($jabatan=="Kaprodi") {
	$revisi="";
}

$standarProfil= 0;

		//cek data dosen di halaman profil
$usernameProfil= $usernameFix;
$query="select username from isi_borang where idProdi='$prodi' and idPeriode='$periode' and username='$usernameProfil' and standar='$standarProfil' limit 1";
$data = mysqli_query($db, $query);
$count = mysqli_num_rows($data);
if ($count<1){
			//ambil username kaprodi
	$query="select username from isi_borang where idProdi='$prodi' and idPeriode='$periode' and username!='$usernameProfil' and standar='$standarProfil' limit 1";
	$data = mysqli_query($db, $query);
	$row = mysqli_fetch_assoc($data);
	$usernameProfil = $row['username'];
}

$butir = "dosen_ps";
$query="SELECT isi_char FROM isi_borang WHERE username='".$usernameProfil."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom='2' ORDER BY no,sub_no,kolom ASC;";
$data = mysqli_query($db, $query);
$countDosenPS = mysqli_num_rows($data);
?>
<head>
	<title>Admin</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/style.css" rel="stylesheet">
	<link href="../font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="../css/loading.css">
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script src="../js/jquery.min.js"></script>
	<script src="../js/form.js"></script>
	<script src="../js/standar6.js"></script>
</head>

<body onload='TotalJsFUll()'>
	<div class='wrapper'>
		<header>
			<nav class="navbar navbar-default navbar-fixed-top">
				<div id="navbar" class="navbar-collapse collapse">
					<div class="navbar-header">
						<a class="navbar-brand" href="#"><?php echo $jabatan ?></a>
					</div>
					<ul class="nav navbar-nav">
						<?php echo "
						<li><a href='HomeKaprodi.php?periode=$periode&jabatan=$jabatan&prodi=$prodi'>Profil</a></li>
						<li><a href='Standar1.php?periode=$periode&jabatan=$jabatan&prodi=$prodi'>Standar 1</a></li>
						<li><a href='Standar2.php?periode=$periode&jabatan=$jabatan&prodi=$prodi'>Standar 2</a></li>
						<li><a href='Standar3.php?periode=$periode&jabatan=$jabatan&prodi=$prodi'>Standar 3</a></li>
						<li><a href='Standar4.php?periode=$periode&jabatan=$jabatan&prodi=$prodi'>Standar 4</a></li>
						<li><a href='Standar5.php?periode=$periode&jabatan=$jabatan&prodi=$prodi'>Standar 5</a></li>
						<li class='active'><a href='Standar6.php?periode=$periode&jabatan=$jabatan&prodi=$prodi'>Standar 6</a></li>
						<li><a href='Standar7.php?periode=$periode&jabatan=$jabatan&prodi=$prodi'>Standar 7</a></li>
						<li><a href='Nilai.php?periode=$periode&jabatan=$jabatan&prodi=$prodi'>Nilai</a></li>
						
						<li><a href='pilihProdi.php?periode=$periode' class='col-md-offset-7'>Kembali</a></li>	
						<li><a href='Standar6.php?periode=$periode&jabatan=$jabatanInverse&prodi=$prodi'>Ke $jabatanInverse</a></li>
						<li><a href='Logout.php'>Log Out</a></li>
						"; ?>
					</ul>
				</div>
			</nav>
			<div class='text-center'>
				<br>
				<br>
				<h1>STANDAR 6</h1>
				<h4>PEMBIAYAAN, PRASARANA, SARANA, DAN SISTEM INFORMASI</h4>
				<h5><?php echo $namaProdi ?></h5>
				<h5>Periode: <?php echo $namaPeriode ?></h5>
				<h5><b><?php echo $revisi?></b></h5>
			</div>
		</header>
	</div>
	
	<div class = "container">
		<hr class="colorgraph"><br>
		<div class="row">
			<div class="col-md-12">
				<form action="ProsesStandar6.php" method="post" class="form-horizontal">
					
					<?php 
					// default id prodi yang disubmit khusus auditor
					echo "
					<input type='hidden' name='prodi' value='".$prodi."'>";
					?>
					
					<div class="fixed-button">
						<button type="button" class="btn btn-default" id="hideButton">Hide</button>
						<button type="button" class="btn btn-default" id="showButton">Show</button>
					</div>
					<!--61-->					
					<div class="form-group">
						<label class="col-sm-2 text-right">6.1</label> 
						<label class="col-sm-8">Pengelolaan Dana</label>
					</div>
					
					<div class="form-group"> 
						<div class="col-sm-offset-2 col-sm-8">
							<p style="text-indent:5em;">Keterlibatan aktif Program Studi harus tercerminkan dalam dokumen tentang proses perencanaan, pengelolaan, dan pelaporan serta pertanggungjawaban penggunaan dana kepada pemangku kepentingan melalui mekanisme yang transparan dan akuntabel
							</p>
							<p style="text-indent:5em;">Keterlibatan PS dalam perencanaan anggaran dan pengelolaan dana
							</p>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "6.1";
							$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
							$data = mysqli_query($db, $query);
							$row = mysqli_fetch_assoc($data);
							$print = $row["isi_text"];
							?>
							<textarea type="text" class="form-control" rows="15" name="6_1" placeholder="" maxlength="60000"><?php echo $print; ?></textarea>
						</label>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "6.1";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="7">6.1</td>
										<td colspan="2">Keterlibatan program studi dalam perencanaan target kinerja, perencanaan kegiatan/ kerja dan perencanaan alokasi dan pengelolaan dana</td>
									</tr>
									<tr>
										<td>0</td>
										<td>Program studi tidak dilibatkan dalam perencanaan/ alokasi dan pengelolaan dana</td>
										<td></td>
									</tr>
									<tr>
										<td>1</td>
										<td>Program studi hanya diminta untuk memberikan masukan. Perencanaan alokasi dan pengelolaan dana dilakukan oleh Fakultas/ Sekolah Tinggi</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Program studi dilibatkan dalam perencanaan alokasi, namun pengelolaan dana dilakukan oleh Fakultas/Sekolah Tinggi</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Program studi tidak diberi otonomi, tetapi dilibatkan dalam melaksanakan perencanaan alokasi  dan pengelolaan dana</td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Program studi secara otonom melaksanakan perencanaan alokasi  dan pengelolaan dana</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control"  placeholder="" value='<?php echo $print;?>' name="nilai/6_1"/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!--621-->						
					<div class="form-group">
						<label class="col-sm-2 text-right">6.2</label> 
						<label class="col-sm-8">Perolehan dan Alokasi Dana</label>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 text-right">6.2.1</label> 
						<label class="col-sm-8">Realisasi perolehan dan alokasi dana (termasuk hibah) dalam juta rupiah termasuk gaji,  selama tiga tahun terakhir*:</label>
					</div>
					<!--PTsendiri-->				
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">PT Sendiri</label>
						<div class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "6.2.1";
							$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$null."' AND no='".$null."' AND sub_no='".$subno1."'";
							$data = mysqli_query($db, $query);
							$count = mysqli_num_rows($data);
							if ($count>0) {
								$row = mysqli_fetch_assoc($data);
								$ValueCounter = $row["isi_float"];
							}else{
								$ValueCounter = 0;
							}
							echo "<input type='hidden' id='6.2.1/1/counter' name='6_2_1/1/counter' value='".$ValueCounter."'>";
							?>
							<table class="tableBorang" id="addTable21" style="width:100%;">
								<thead>
									<tr>
										<th rowspan="2">Sumber Dana</th>
										<th rowspan="2">Jenis Dana</th>
										<th colspan="3">Jumlah dana (Juta Rupiah)</th>
									</tr>
									<tr>
										<th>TS-2</th>
										<th>TS-1</th>
										<th>TS</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
										<td>(4)</td>
										<td>(5)</td>
									</tr>
									<?php
									for($i=1;$i<=$ValueCounter;$i++){
										$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$i."' AND sub_no='".$subno1."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print1 = $row["isi_char"];

										$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$i."' AND sub_no='".$subno1."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print2 = $row["isi_float"];

										$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom4."' AND no='".$i."' AND sub_no='".$subno1."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print3 = $row["isi_float"];

										$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom5."' AND no='".$i."' AND sub_no='".$subno1."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print4 = $row["isi_float"];

										echo "<tr>";
										echo "<td>PT Sendiri</td>";
										echo "<td><input type='text' class='form-control' name='6_2_1/1/2/".$i."' value='".$print1."'></td>";
										echo "<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='6_2_1/1/3/".$i."' value='".$print2."' onchange='penambahan11(2)'></td>";
										echo "<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='6_2_1/1/4/".$i."' value='".$print3."' onchange='penambahan12(3)'></td>";
										echo "<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='6_2_1/1/5/".$i."' value='".$print4."' onchange='penambahan13(4)'></td>";
										echo "</tr>";
									}
									?>
									<tr>
										<td colspan="5"><button type="button" onclick="addRow211('addTable21','6.2.1/1/counter')"><span class="glyphicon glyphicon-plus"></span></button></td>
									</tr>
									<tr>
										<td colspan="2">Total</td>
										<td id="1TS"></td>
										<td id="1TS2"></td>
										<td id="1TS3"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!--Yayasan-->					
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">Yayasan</label>
						<div class="col-sm-offset-2 col-sm-8">
							<?php
							$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$null."' AND no='".$null."' AND sub_no='".$subno2."'";
							$data = mysqli_query($db, $query);
							$count = mysqli_num_rows($data);
							if ($count>0) {
								$row = mysqli_fetch_assoc($data);
								$ValueCounter = $row["isi_float"];
							}else{
								$ValueCounter = 0;
							}
							echo "<input type='hidden' id='6.2.1/2/counter' name='6_2_1/2/counter' value='".$ValueCounter."'>";
							?>
							<table class="tableBorang" id="addTable212" style="width:100%;">
								<thead>
									<tr>
										<th rowspan="2">Sumber Dana</th>
										<th rowspan="2">Jenis Dana</th>
										<th colspan="3">Jumlah dana (Juta Rupiah)</th>
									</tr>
									<tr>
										<th>TS-2</th>
										<th>TS-1</th>
										<th>TS</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
										<td>(4)</td>
										<td>(5)</td>
									</tr>
									<?php
									for($i=1;$i<=$ValueCounter;$i++){
										$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$i."' AND sub_no='".$subno2."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print1 = $row["isi_char"];

										$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$i."' AND sub_no='".$subno2."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print2 = $row["isi_float"];

										$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom4."' AND no='".$i."'  AND sub_no='".$subno2."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print3 = $row["isi_float"];

										$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom5."' AND no='".$i."'  AND sub_no='".$subno2."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print4 = $row["isi_float"];

										echo "<tr>";
										echo "<td>Yayasan</td>";
										echo "<td><input type='text' class='form-control' name='6_2_1/2/2/".$i."' value='".$print1."'></td>";
										echo "<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='6_2_1/2/3/".$i."' value='".$print2."' onchange='penambahan21(2)'></td>";
										echo "<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='6_2_1/2/4/".$i."' value='".$print3."' onchange='penambahan22(3)'></td>";
										echo "<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='6_2_1/2/5/".$i."' value='".$print4."' onchange='penambahan23(4)'></td>";
										echo "</tr>";
									}
									?>
									<tr>
										<td colspan="5"><button type="button" onclick="addRow212('addTable212','6.2.1/2/counter')"><span class="glyphicon glyphicon-plus"></span></button></td>
									</tr>
									<tr>
										<td colspan="2">Total</td>
										<td id="2TS"></td>
										<td id="2TS2"></td>
										<td id="2TS3"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!--Diknas-->					
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">Diknas</label>
						<div class="col-sm-offset-2 col-sm-8">
							<?php
							$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$null."' AND no='".$null."' AND sub_no='".$subno3."'";
							$data = mysqli_query($db, $query);
							$count = mysqli_num_rows($data);
							if ($count>0) {
								$row = mysqli_fetch_assoc($data);
								$ValueCounter = $row["isi_float"];
							}else{
								$ValueCounter = 0;
							}
							echo "<input type='hidden' id='6.2.1/3/counter' name='6_2_1/3/counter' value='".$ValueCounter."'>";
							?>
							<table class="tableBorang" id="addTable213" style="width:100%;">
								<thead>
									<tr>
										<th rowspan="2">Sumber Dana</th>
										<th rowspan="2">Jenis Dana</th>
										<th colspan="3">Jumlah dana (Juta Rupiah)</th>
									</tr>
									<tr>
										<th>TS-2</th>
										<th>TS-1</th>
										<th>TS</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
										<td>(4)</td>
										<td>(5)</td>
									</tr>
									<?php
									for($i=1;$i<=$ValueCounter;$i++){
										$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$i."' AND sub_no='".$subno3."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print1 = $row["isi_char"];

										$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$i."' AND sub_no='".$subno3."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print2 = $row["isi_float"];

										$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom4."' AND no='".$i."'  AND sub_no='".$subno3."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print3 = $row["isi_float"];

										$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom5."' AND no='".$i."'  AND sub_no='".$subno3."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print4 = $row["isi_float"];

										echo "<tr>";
										echo "<td>Diknas</td>";
										echo "<td><input type='text' class='form-control' name='6_2_1/3/2/".$i."' value='".$print1."'></td>";
										echo "<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='6_2_1/3/3/".$i."' value='".$print2."' onchange='penambahan31(2)'></td>";
										echo "<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='6_2_1/3/4/".$i."' value='".$print3."' onchange='penambahan32(3)'></td>";
										echo "<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='6_2_1/3/5/".$i."' value='".$print4."' onchange='penambahan33(4)'></td>";
										echo "</tr>";
									}
									?>
									<tr>
										<td colspan="5"><button type="button" onclick="addRow213('addTable213','6.2.1/3/counter')"><span class="glyphicon glyphicon-plus"></span></button></td>
									</tr>
									<tr>
										<td colspan="2">Total</td>
										<td id="3TS"></td>
										<td id="3TS2"></td>
										<td id="3TS3"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!--lain-lain-->				
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">Lain-Lain</label>
						<div class="col-sm-offset-2 col-sm-8">
							<?php
							$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$null."' AND no='".$null."' AND sub_no='".$subno4."'";
							$data = mysqli_query($db, $query);
							$count = mysqli_num_rows($data);
							if ($count>0) {
								$row = mysqli_fetch_assoc($data);
								$ValueCounter = $row["isi_float"];
							}else{
								$ValueCounter = 0;
							}
							echo "<input type='hidden' id='6.2.1/4/counter' name='6_2_1/4/counter' value='".$ValueCounter."'>";
							?>
							<table class="tableBorang" id="addTable214" style="width:100%;">
								<thead>
									<tr>
										<th rowspan="2">Sumber Dana</th>
										<th rowspan="2">Jenis Dana</th>
										<th colspan="3">Jumlah dana (Juta Rupiah)</th>
									</tr>
									<tr>
										<th>TS-2</th>
										<th>TS-1</th>
										<th>TS</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
										<td>(4)</td>
										<td>(5)</td>
									</tr>
									<?php
									for($i=1;$i<=$ValueCounter;$i++){
										$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$i."' AND sub_no='".$subno4."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print1 = $row["isi_char"];

										$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$i."' AND sub_no='".$subno4."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print2 = $row["isi_float"];

										$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom4."' AND no='".$i."'  AND sub_no='".$subno4."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print3 = $row["isi_float"];

										$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom5."' AND no='".$i."'  AND sub_no='".$subno4."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print4 = $row["isi_float"];

										echo "<tr>";
										echo "<td>Lain-Lain</td>";
										echo "<td><input type='text' class='form-control' name='6_2_1/4/2/".$i."' value='".$print1."'></td>";
										echo "<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='6_2_1/4/3/".$i."' value='".$print2."' onchange='penambahan41(2)'></td>";
										echo "<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='6_2_1/4/4/".$i."' value='".$print3."' onchange='penambahan42(3)'></td>";
										echo "<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='6_2_1/4/5/".$i."' value='".$print4."' onchange='penambahan43(4)'></td>";
										echo "</tr>";
									}
									?>
									<tr>
										<td colspan="5"><button type="button" onclick="addRow214('addTable214','6.2.1/4/counter')"><span class="glyphicon glyphicon-plus"></span></button></td>
									</tr>
									<tr>
										<td colspan="2">Total</td>
										<td id="4TS"></td>
										<td id="4TS2"></td>
										<td id="4TS3"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">Total Biaya</label>
						<div class="col-sm-offset-2 col-sm-8">
							<table class="tableBorang" style="width:100%;">
								<thead>
									<tr>
										<th>TS-2</th>
										<th>TS-1</th>
										<th>TS</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td id="totalTS"></td>
										<td id="totalTS1"></td>
										<td id="totalTS2"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<!--Penggunaan dana-->					
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">Penggunaan Biaya</label>
						<div class="col-sm-offset-2 col-sm-8">
							<table class="tableBorang" style="width:100%;">
								<thead>
									<?php
									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$no1."' AND sub_no='".$no5."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print1 = $row["isi_float"];

									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom5."' AND no='".$no1."' AND sub_no='".$no5."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print2 = $row["isi_float"];

									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom7."' AND no='".$no1."' AND sub_no='".$no5."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print3 = $row["isi_float"];

									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$no2."' AND sub_no='".$no5."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print4 = $row["isi_float"];

									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom5."' AND no='".$no2."' AND sub_no='".$no5."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print5 = $row["isi_float"];

									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom7."' AND no='".$no2."' AND sub_no='".$no5."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print6 = $row["isi_float"];

									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$no3."' AND sub_no='".$no5."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print7 = $row["isi_float"];

									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom5."' AND no='".$no3."' AND sub_no='".$no5."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print8 = $row["isi_float"];

									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom7."' AND no='".$no3."' AND sub_no='".$no5."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print9 = $row["isi_float"];

									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$no4."' AND sub_no='".$no5."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print10 = $row["isi_float"];

									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom5."' AND no='".$no4."' AND sub_no='".$no5."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print11 = $row["isi_float"];

									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom7."' AND no='".$no4."' AND sub_no='".$no5."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print12 = $row["isi_float"];

									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$no5."' AND sub_no='".$no5."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print13 = $row["isi_float"];

									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom5."' AND no='".$no5."' AND sub_no='".$no5."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print14 = $row["isi_float"];

									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom7."' AND no='".$no5."' AND sub_no='".$no5."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print15 = $row["isi_float"];

									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$no6."' AND sub_no='".$no5."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print16 = $row["isi_float"];

									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom5."' AND no='".$no6."' AND sub_no='".$no5."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print17 = $row["isi_float"];

									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom7."' AND no='".$no6."' AND sub_no='".$no5."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print18 = $row["isi_float"];
									?>
									<tr>
										<th rowspan="3" style="width:4%;">No.</th>
										<th rowspan="3">Jenis Penggunaan</th>
										<th colspan="6">Jumlah Dana dalam Juta Rupiah dan Persentase</th>
									</tr>
									<tr>
										<th colspan="2">TS-2</th>
										<th colspan="2">TS-1</th>
										<th colspan="2">TS</th>
									</tr>
									<tr>
										<th>Rp</th>
										<th>%</th>
										<th>Rp</th>
										<th>%</th>
										<th>Rp</th>
										<th>%</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
										<td>(4)</td>
										<td>(5)</td>
										<td>(6)</td>
										<td>(7)</td>
										<td>(8)</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Pendidikan</td>
										<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name="PendidikanTS31" id="kolom11" onchange="penggunaaDana1()" <?php echo "value='".$print1."'"; ?>></td>
										<td id="pendidikan3" style="width:10%;"></td>
										<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name="PendidikanTS21" id="kolom12" onchange="penggunaaDana1()" <?php echo "value='".$print2."'"; ?>></td>
										<td id="pendidikan2" style="width:10%;"></td>
										<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name="PendidikanTS11" id="kolom13" onchange="penggunaaDana1()" <?php echo "value='".$print3."'"; ?>></td>
										<td id="pendidikan1" style="width:10%;"></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Penelitian</td>
										<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name="PenelitianTS31" id="kolom21" onchange="penggunaaDana1()" <?php echo "value='".$print4."'"; ?>></td>
										<td id="penelitian3" style="width:10%;"></td>
										<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name="PenelitianTS21" id="kolom22" onchange="penggunaaDana1()" <?php echo "value='".$print5."'"; ?>></td>
										<td id="penelitian2" style="width:10%;"></td>
										<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name="PenelitianTS11" id="kolom23" onchange="penggunaaDana1()" <?php echo "value='".$print6."'"; ?>></td>
										<td id="penelitian1" style="width:10%;"></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Pengabdian Kepada Masyarakat</td>
										<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name="PegabdianTS31" id="kolom31" onchange="penggunaaDana1()" <?php echo "value='".$print7."'"; ?>></td>
										<td id="pengabdian3" style="width:10%;"></td>
										<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name="PegabdianTS21" id="kolom32" onchange="penggunaaDana1()" <?php echo "value='".$print8."'"; ?>></td>
										<td id="pengabdian2" style="width:10%;"></td>
										<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name="PegabdianTS11" id="kolom33" onchange="penggunaaDana1()" <?php echo "value='".$print9."'"; ?>></td>
										<td id="pengabdian1" style="width:10%;"></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Investasi Prasarana</td>
										<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name="PrasaranaTS31" id="kolom41" onchange="penggunaaDana1()" <?php echo "value='".$print10."'"; ?>></td>
										<td id="prasarana3" style="width:10%;"></td>
										<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name="PrasaranaTS21" id="kolom42" onchange="penggunaaDana1()" <?php echo "value='".$print11."'"; ?>></td>
										<td id="prasarana2" style="width:10%;"></td>
										<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name="PrasaranaTS11" id="kolom43" onchange="penggunaaDana1()" <?php echo "value='".$print12."'"; ?>></td>
										<td id="prasarana1" style="width:10%;"></td>
									</tr>
									<tr>
										<td>5</td>
										<td>Investasi Sarana</td>
										<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name="SaranaTS31" id="kolom51" onchange="penggunaaDana1()" <?php echo "value='".$print13."'"; ?>></td>
										<td id="sarana3" style="width:10%;"></td>
										<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name="SaranaTS21" id="kolom52" onchange="penggunaaDana1()" <?php echo "value='".$print14."'"; ?>></td>
										<td id="sarana2" style="width:10%;"></td>
										<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name="SaranaTS11" id="kolom53" onchange="penggunaaDana1()" <?php echo "value='".$print15."'"; ?>></td>
										<td id="sarana1" style="width:10%;"></td>
									</tr>
									<tr>
										<td>6</td>
										<td>Invertasi SDM</td>
										<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name="SDMTS31" id="kolom61" onchange="penggunaaDana1()" <?php echo "value='".$print16."'"; ?>></td>
										<td id="sdm3" style="width:10%;"></td>
										<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name="SDMTS21" id="kolom62" onchange="penggunaaDana1()" <?php echo "value='".$print17."'"; ?>></td>
										<td id="sdm2" style="width:10%;"></td>
										<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name="SDMTS11" id="kolom63" onchange="penggunaaDana1()" <?php echo "value='".$print18."'"; ?>></td>
										<td id="sdm1" style="width:10%;"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="11">6.2.1</td>
										<td colspan="3">Penggunaan dana untuk operasional dan pengembangan</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Rata-rata dana pendidikan per tahun</td>
										<td id="danapendidikan"></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Rata-rata dana penelitian per tahun</td>
										<td id="danapenelitian"></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Rata-rata dana PkM per tahun</td>
										<td id="danapkm"></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Investasi prasarana per tahun</td>
										<td id="danaprasarana"></td>
									</tr>
									<tr>
										<td>5</td>
										<td>Investasi sarana per tahun</td>
										<td id="danasarana"></td>
									</tr>
									<tr>
										<td>6</td>
										<td>Investasi SDM per tahun</td>
										<td id="danasdm"></td>
									</tr>
									<tr>
										<td>7</td>
										<td>Total dana operasional dan pengembangan per tahun</td>
										<td id="ratadana"></td>
									</tr>
									<tr>
										<?php
										$butir="3.1.1";
										$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='1' AND idPeriode='1' AND standar='3' AND butir ='".$butir."' AND kolom='".$kolom7."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print1 = $row["isi_float"];
										echo "<input type='hidden' id='jumlahmahasiswa' value='".$print1."'>";
										?>
										<td>8</td>
										<td>Jumlah mahasiswa pada TS (Tabel 3.1.1)</td>
										<td id="jumlahmahasiswa1"></td>
									</tr>
									<tr>
										<td colspan="2">Dom</td>
										<td id="dom"></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td id="nilai621"></td>
										<input type='hidden' name='nilai/6_2_1' id='nilai/6.2.1'>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 text-right">6.2.2</label> 
						<label class="col-sm-8">Tuliskan dana untuk kegiatan penelitian pada tiga tahun terakhir yang melibatkan dosen yang bidang keahliannya sesuai dengan program studi, dengan mengikuti format tabel berikut:</label>
					</div>
					
					<!--622-->					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "6.2.2";
							$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$null."' AND no='".$null."'";
							$data = mysqli_query($db, $query);
							$count = mysqli_num_rows($data);
							if ($count>0) {
								$row = mysqli_fetch_assoc($data);
								$ValueCounter = $row["isi_float"];
							}else{
								$ValueCounter = 0;
							}
							echo "<input type='hidden' id='6.2.2/counter' name='6_2_2/counter' value='".$ValueCounter."'>";
							
							?>
							<table class="tableBorang" id="addTable22" style="width:100%;">
								<thead>
									<tr>
										<th>Tahun</th>
										<th>Judul Penelitian</th>
										<th>Sumber dan Jenis Dana</th>
										<th>Jumlah Dana*(dalam juta rupiah)</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
										<td>(4)</td>
									</tr>
									<?php
									for($i=1;$i<=$ValueCounter;$i++){
										$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom1."' AND no='".$i."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print1 = $row["isi_char"];

										$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$i."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print2 = $row["isi_text"];

										$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$i."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print3 = $row["isi_char"];

										$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom4."' AND no='".$i."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print4 = $row["isi_float"];

										echo "<tr>";
										echo "<td><input type='text' pattern='[0-9]+([\-][0-9]+)?' class='form-control' name='6_2_2/1/".$i."' value='".$print1."'></td>";
										echo "<td><textarea type='text' class='form-control' name='6_2_2/2/".$i."'>".$print2."</textarea></td>";
										echo "<td><input type='text' class='form-control' name='6_2_2/3/".$i."' value='".$print3."'></td>";
										echo "<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='6_2_2/4/".$i."' value='".$print4."' onchange='total622(3)'></td>";
										echo "</tr>";
									}
									?>
									<tr>
										<td colspan="4"><button type="button" onclick="addRow22('addTable22','6.2.2/counter')"><span class="glyphicon glyphicon-plus"></span></button></td>
									</tr>
									<tr>
										<td colspan="3">Total</td>
										<td id="totaldana622"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "6.2.2";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="5">6.2.2</td>
										<td colspan="3">Dana penelitian dalam tiga tahun terakhir</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Total dana penelitian dalam tiga tahun terakhir</td>
										<td id="totaldana3tahun"></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Jumlah dosen tetap dengan keahlian sesuai PS</td>
										<td id="jumlahdosen"><?php echo $countDosenPS ;?></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Rata-rata dana penelitian per dosen per tahun</td>
										<td id="rataratadosen"></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td id='nilai622'></td>
										<input type='hidden' name='nilai/6_2_2' id='nilai/6.2.2'>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 text-right">6.2.3</label> 
						<label class="col-sm-8">Dana yang diperoleh dari/untuk kegiatan pelayanan/pengabdian kepada masyarakat pada tiga tahun terakhir:</label>
					</div>
					<!--623-->					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "6.2.3";
							$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$null."' AND no='".$null."'";
							$data = mysqli_query($db, $query);
							$count = mysqli_num_rows($data);
							if ($count>0) {
								$row = mysqli_fetch_assoc($data);
								$ValueCounter = $row["isi_float"];
							}else{
								$ValueCounter = 0;
							}
							echo "<input type='hidden' id='6.2.3/counter' name='6_2_3/counter' value='".$ValueCounter."'>";
							?>
							<table class="tableBorang" id="addTable23" style="width:100%;">
								<thead>
									<tr>
										<th>Tahun</th>
										<th>Judul Penelitian</th>
										<th>Sumber dan Jenis Dana</th>
										<th>Jumlah Dana*(dalam juta rupiah)</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
										<td>(4)</td>
									</tr>
									<?php
									for($i=1;$i<=$ValueCounter;$i++){
										$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom1."' AND no='".$i."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print1 = $row["isi_char"];

										$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$i."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print2 = $row["isi_text"];

										$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$i."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print3 = $row["isi_char"];

										$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom4."' AND no='".$i."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print4 = $row["isi_float"];

										echo "<tr>";
										echo "<td><input type='text' pattern='[0-9]+([\-][0-9]+)?' class='form-control' name='6_2_3/1/".$i."' value='".$print1."'></td>";
										echo "<td><textarea type='text' class='form-control' name='6_2_3/2/".$i."'>".$print2."</textarea></td>";
										echo "<td><input type='text' class='form-control' name='6_2_3/3/".$i."' value='".$print3."'></td>";
										echo "<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='6_2_3/4/".$i."' value='".$print4."' onchange='total623(3)'></td>";
										echo "</tr>";
									}
									?>
									<tr>
										<td colspan="4"><button type="button" onclick="addRow23('addTable23','6.2.3/counter')"><span class="glyphicon glyphicon-plus"></span></button></td>
									</tr>
									<tr>
										<td>Total</td>
										<td></td>
										<td></td>
										<td id="totaldana623"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "6.2.3";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="5">6.2.3</td>
										<td colspan="3">Dana yang diperoleh dalam rangka pelayanan/pengabdian kepada masyarakat dalam tiga  tahun terakhir</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Total dana PkM dalam tiga tahun terakhir</td>
										<td id="totaldanatigatahun"></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Jumlah dosen tetap PS</td>
										<td id="totaldosen"><?php echo $countDosenPS ;?></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Rata-rata dana PkM per dosen per tahun</td>
										<td id="rataratapkm"></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td id='nilai623'></td>
										<input type='hidden' name='nilai/6_2_3' id='nilai/6.2.3'>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<!--63-->						
					<div class="form-group">
						<label class="col-sm-2 text-right">6.3</label> 
						<label class="col-sm-8">Prasarana</label>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 text-right">6.3.1</label> 
						<label class="col-sm-8">Data ruang kerja dosen tetap yang bidang keahliannya sesuai  dengan Program Studi:</label>
					</div>
					<!--631-->						
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="tableBorang" id="addTable31" style="width:100%;">
								<thead>
									<tr>
										<th>Ruang Kerja Dosen</th>
										<th>Jumlah Ruang</th>
										<th>Jumlah Luas (m<sup>2</sup>)</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
									</tr>
									<?php
									$butir = "6.3.1";
									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$no1."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print1 = $row["isi_float"];

									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$no1."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print2 = $row["isi_float"];

									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$no2."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print3 = $row["isi_float"];

									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$no2."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print4 = $row["isi_float"];

									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$no3."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print5 = $row["isi_float"];

									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$no3."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print6 = $row["isi_float"];

									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$no4."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print7 = $row["isi_float"];

									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$no4."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print8 = $row["isi_float"];
									?>
									<tr>
										<td>Satu ruang untuk lebih dari 4 dosen</td>
										<td><input type="text" pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='6_3_1/2/1' <?php echo "value='".$print1."'" ?>></td>
										<td>(A)=<input type="text" pattern='[0-9]+([\.][0-9]+)?' name='6_3_1/3/1' id='ruang1' <?php echo "value='".$print2."'" ?> onchange="penjumlahan631(2)"></td>
									</tr>
									<tr>
										<td>Satu ruang untuk 3 - 4 dosen</td>
										<td><input type="text" pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='6_3_1/2/2' <?php echo "value='".$print3."'" ?>></td>
										<td>(B)=<input type="text" pattern='[0-9]+([\.][0-9]+)?' name='6_3_1/3/2' id='ruang2' <?php echo "value='".$print4."'" ?> onchange="penjumlahan631(2)"></td>
									</tr>
									<tr>
										<td>Satu ruang untuk 2 dosen</td>
										<td><input type="text"  pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='6_3_1/2/3' <?php echo "value='".$print5."'" ?>></td>
										<td>(C)=<input type="text" pattern='[0-9]+([\.][0-9]+)?' name='6_3_1/3/3' id='ruang3' <?php echo "value='".$print6."'" ?> onchange="penjumlahan631(2)"></td>
									</tr>
									<tr>
										<td>Satu ruang untuk 1 dosen (bukan pejabat struktural)</td>
										<td><input type="text" pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='6_3_1/2/4' <?php echo "value='".$print7."'" ?>></td>
										<td>(D)=<input type="text" pattern='[0-9]+([\.][0-9]+)?' name='6_3_1/3/4' id='ruang4' <?php echo "value='".$print8."'" ?> onchange="penjumlahan631(2)"></td>
									</tr>
									<tr>
										<td>Total</td>
										<td class="voidColor"></td>
										<td id="Total6313">(T)=</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "6.3.1";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND sub_butir='".$no1."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="11">6.3.1</td>
										<td colspan="3">Luas ruang kerja dosen.</td>
									</tr>
									<tr>
										<td>a</td>
										<td>Luas semua ruang untuk > 4 dosen</td>
										<td id='ruang1n'></td>
									</tr>
									<tr>
										<td>b</td>
										<td>Luas semua ruang untuk 3 - 4 dosen</td>
										<td id='ruang2n'></td>
									</tr>
									<tr>
										<td>c</td>
										<td>Luas semua ruang untuk 2 dosen</td>
										<td id='ruang3n'></td>
									</tr>
									<tr>
										<td>d</td>
										<td>Luas semua ruang untuk 1 dosen</td>
										<td id='ruang4n'></td>
									</tr>
									<tr>
										<td>A</td>
										<td>A=a+2b+3c+4d</td>
										<td id='A'></td>
									</tr>
									<tr>
										<td>B</td>
										<td>B=a+b+c+d</td>
										<td id='B'></td>
									</tr>
									<tr>
										<td>SLRD</td>
										<td>A/B</td>
										<td id='SLRD'></td>
									</tr>
									<tr>
										<td colspan='2'>Banyaknya dosen tetap dengan bidang sesuai PS</td>
										<td id='totaldosen'><?php echo $countDosenPS; ?></td>
									</tr>
									<tr>
										<td colspan='2'>Skor kelengkapan sarana dan kenyamanan ruangan (-1.5 s.d. 1.5)</td>
										<td><input type="text" pattern='[0-9]+([\.][0-9]+)?' class="form-control" id="kenyamanan" value='<?php echo $print;?>' name="kenyamanan" onchange='nilaiborang631()'/></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td id='nilai631'></td>
										<input type='hidden' name='nilai/6_3_1' id='nilai/6.3.1'>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 text-right">6.3.2</label> 
						<label class="col-sm-8">Data prasarana(kantor, ruang kelas, ruang laboratorium, studio, ruang perpustakaan, kebun percobaan, dsb. kecuali ruang dosen) yang dipergunakan Program Studi dalam proses belajar mengajar:</label>
					</div>
					<!--632-->						
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8 table-wrapper">
							<?php
							$butir = "6.3.2";
							$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$null."' AND no='".$null."'";
							$data = mysqli_query($db, $query);
							$count = mysqli_num_rows($data);
							if ($count>0) {
								$row = mysqli_fetch_assoc($data);
								$ValueCounter = $row["isi_float"];
							}else{
								$ValueCounter = 0;
							}
							echo "<input type='hidden' id='6.3.2/counter' name='6_3_2/counter' value='".$ValueCounter."'>";
							?>
							<table class="tableBorang" id="addTable32" style="width:120%;">
								<thead>
									<tr>
										<th rowspan='2'>No.</th>
										<th rowspan='2'>Jenis Prasarana</th>
										<th rowspan='2'>Jumlah Unit</th>
										<th rowspan='2'>Total Luas (m<sup>2</sup>)</th>
										<th colspan='2' style='width:12%;'>Kepemilikan</th>
										<th colspan='2'>Kondisi</th>
										<th rowspan='2'>Utilisasi (Jam/minggu)</th>
									</tr>
									<tr>
										<th>SD</th>
										<th>SW</th>
										<th style='width:8%;'>Terawat</th>
										<th>Tidak Terawat</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
										<td>(4)</td>
										<td>(5)</td>
										<td>(6)</td>
										<td>(7)</td>
										<td>(8)</td>
										<td>(9)</td>
									</tr>
									<?php
									for($i=1;$i<=$ValueCounter;$i++){
										$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$i."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print1 = $row["isi_char"];
										
										$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$i."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print2 = $row["isi_char"];
										
										$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom4."' AND no='".$i."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print3 = $row["isi_char"];

										$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom5."' AND no='".$i."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print4 = $row["isi_float"];

										$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom6."' AND no='".$i."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print5 = $row["isi_float"];
										
										$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom7."' AND no='".$i."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print6 = $row["isi_float"];

										$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom8."' AND no='".$i."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print7 = $row["isi_float"];
										
										$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom9."' AND no='".$i."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print8 = $row["isi_char"];
										
										$check1="";
										$check2="";
										$check3="";
										$check4="";
										if($print4 == 1){
											$check1 = "checked";
										}
										if($print5 == 1){
											$check2 = "checked";
										}
										if($print6 == 1){
											$check3 = "checked";
										}
										if($print7 == 1){
											$check4 = "checked";
										}
										
										echo "<tr>";
										echo "<td>".$i."</td>";
										echo "<td><input type='text' class='form-control' name='6_3_2/2/".$i."' value='".$print1."'></td>";
										echo "<td><input type='text' pattern='[0-9]+([\-][0-9]+)?' class='form-control' name='6_3_2/3/".$i."' value='".$print2."'></td>";
										echo "<td><input type='text' pattern='[0-9]+([\-][0-9]+)?' class='form-control' name='6_3_2/4/".$i."' value='".$print3."'></td>";
										echo "<td><input type='checkbox' name='6_3_2/5/".$i."' value='1' ".$check1.">";
										echo "<td><input type='checkbox' name='6_3_2/6/".$i."' value='1' ".$check2.">";
										echo "<td><input type='checkbox' name='6_3_2/7/".$i."' value='1' ".$check3.">";
										echo "<td><input type='checkbox' name='6_3_2/8/".$i."' value='1' ".$check4.">";
										echo "<td><input type='text' pattern='[0-9]+([\-][0-9]+)?' class='form-control' name='6_3_2/9/".$i."' value='".$print8."'></td>";
										echo "</tr>";
									}
									?>
									<tr>
										<td colspan="9"><button type="button" onclick="addRow32('addTable32','6.3.2/counter')"><span class="glyphicon glyphicon-plus"></span></button></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">Keterangan:</label>
						<label class="col-sm-offset-2 col-sm-8">SD = Milik PT/Fakultas/jurusan sendiri; SW = Sewa/Kontrak/Kerjasama</label>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "6.3.2";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="7">6.3.2</td>
										<td colspan="3">Prasarana (kantor, ruang kelas, ruang laboratorium, studio, ruang perpustakaan, kebun percobaan, dsb. kecuali  ruang dosen) yang dipergunakan PS dalam proses pembelajaran.</td>
									</tr>
									<tr>
										<td colspan='2'>Prasarana (kecuali  ruang dosen) dalam proses pembelajaran.</td>
										<td></td>
									</tr>
									<tr>
										<td>1</td>
										<td>Prasarana kurang lengkap dan mutunya kurang baik.</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Prasarana cukup lengkap dan mutunya cukup untuk proses pembelajaran.</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Prasarana lengkap dan mutunya baik untuk proses pembelajaran</td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Prasarana lengkap dan mutunya sangat baik untuk proses pembelajaran. </td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control" value='<?php echo $print;?>' name="nilai/6_3_2"/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!--633-->						
					<div class="form-group">
						<label class="col-sm-2 text-right">6.3.3</label> 
						<label class="col-sm-8">Data prasarana lain yang menunjang (misalnya tempat olah raga, ruang bersama, ruang himpunan mahasiswa, poliklinik) dengan mengikuti format tabel berikut:</label>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8 table-wrapper">
							<?php
							$butir = "6.3.3";
							$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$null."' AND no='".$null."'";
							$data = mysqli_query($db, $query);
							$count = mysqli_num_rows($data);
							if ($count>0) {
								$row = mysqli_fetch_assoc($data);
								$ValueCounter = $row["isi_float"];
							}else{
								$ValueCounter = 0;
							}
							echo "<input type='hidden' id='6.3.3/counter' name='6_3_3/counter' value='".$ValueCounter."'>";
							?>
							<table class="tableBorang" id="addTable33" style="width:120%;">
								<thead>
									<tr>
										<th rowspan='2'>No.</th>
										<th rowspan='2'>Jenis Prasarana</th>
										<th rowspan='2'>Jumlah Unit</th>
										<th rowspan='2'>Total Luas (m<sup>2</sup>)</th>
										<th colspan='2' style='width:12%;'>Kepemilikan</th>
										<th colspan='2'>Kondisi</th>
										<th rowspan='2'>Utilisasi (Jam/minggu)</th>
									</tr>
									<tr>
										<th>SD</th>
										<th>SW</th>
										<th style='width:8%;'>Terawat</th>
										<th>Tidak Terawat</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
										<td>(4)</td>
										<td>(5)</td>
										<td>(6)</td>
										<td>(7)</td>
										<td>(8)</td>
										<td>(9)</td>
									</tr>
									<?php
									for($i=1;$i<=$ValueCounter;$i++){
										$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$i."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print1 = $row["isi_char"];
										
										$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$i."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print2 = $row["isi_char"];
										
										$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom4."' AND no='".$i."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print3 = $row["isi_char"];

										$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom5."' AND no='".$i."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print4 = $row["isi_float"];

										$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom6."' AND no='".$i."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print5 = $row["isi_float"];
										
										$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom7."' AND no='".$i."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print6 = $row["isi_float"];

										$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom8."' AND no='".$i."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print7 = $row["isi_float"];
										
										$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom9."' AND no='".$i."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print8 = $row["isi_char"];
										
										$check1="";
										$check2="";
										$check3="";
										$check4="";
										if($print4 == 1){
											$check1 = "checked";
										}
										if($print5 == 1){
											$check2 = "checked";
										}
										if($print6 == 1){
											$check3 = "checked";
										}
										if($print7 == 1){
											$check4 = "checked";
										}
										
										echo "<tr>";
										echo "<td>".$i."</td>";
										echo "<td><input type='text' class='form-control' name='6_3_3/2/".$i."' value='".$print1."'></td>";
										echo "<td><input type='text' pattern='[0-9]+([\-][0-9]+)?' class='form-control' name='6_3_3/3/".$i."' value='".$print2."'></td>";
										echo "<td><input type='text' pattern='[0-9]+([\-][0-9]+)?' class='form-control' name='6_3_3/4/".$i."' value='".$print3."'></td>";
										echo "<td><input type='checkbox' name='6_3_3/5/".$i."' value='1' ".$check1." onchange='check1(4)'>";
										echo "<td><input type='checkbox' name='6_3_3/6/".$i."' value='1' ".$check2." onchange='check2(5)'>";
										echo "<td><input type='checkbox' name='6_3_3/7/".$i."' value='1' ".$check3." onchange='check3(6)'>";
										echo "<td><input type='checkbox' name='6_3_3/8/".$i."' value='1' ".$check4." onchange='check4(7)'>";
										echo "<td><input type='text' pattern='[0-9]+([\-][0-9]+)?' class='form-control' name='6_3_3/9/".$i."' value='".$print8."'  onchange='totaljam(8)'></td>";
										echo "</tr>";
									}
									?>
									<tr>
										<td colspan="9"><button type="button" onclick="addRow33('addTable33','6.3.3/counter')"><span class="glyphicon glyphicon-plus"></span></button></td>
									</tr>
									<tr>
										<td colspan='4'>Total</td>
										<td id='TotalSD'></td>
										<td id='TotalSW'></td>
										<td id='TotalTerawat'></td>
										<td id='TotalTidakTerawat'></td>
										<td id="utilitasperminggu"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">Keterangan:</label>
						<label class="col-sm-offset-2 col-sm-8">SD = Milik PT/Fakultas/jurusan sendiri; SW = Sewa/Kontrak/Kerjasama</label>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "6.3.3";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="7">6.3.3</td>
										<td colspan="3">Prasarana lain yang menunjang (misalnya tempat olah raga, ruang bersama, ruang himpunan mahasiswa, poliklinik)</td>
									</tr>
									<tr>
										<td>0</td>
										<td>Tidak ada prasarana penunjang.</td>
										<td></td>
									</tr>
									<tr>
										<td>1</td>
										<td>Prasarana penunjang kurang lengkap dan mutunya kurang baik.</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Prasarana penunjang cukup lengkap dan mutunya cukup untuk memenuhi kebutuhan mahasiswa.</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Prasarana penunjang lengkap dan mutunya baik untuk memenuhi kebutuhan mahasiswa.</td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Prasarana penunjang lengkap dan mutunya baik untuk memenuhi kebutuhan mahasiswa.</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control" value='<?php echo $print;?>' name="nilai/6_3_3"/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!--64-->						
					<div class="form-group">
						<label class="col-sm-2 text-right">6.4</label> 
						<label class="col-sm-8">Sarana Pelaksanaan Kegiatan Akademik</label>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 text-right">6.4.1</label> 
						<p class="col-sm-8">Pustaka (buku teks, karya ilmiah, dan jurnal; termasuk juga dalam bentuk CD-ROM dan media lainnya)</p> 
						<p class="col-sm-offset-2 col-sm-8">Rekapitulasi jumlah ketersediaan pustaka yang relevan dengan bidang Program Studi:</p>
					</div>
					
					<div class="form-group">
						<p class="col-sm-offset-2 col-sm-8">Tabel 1. Rekapitulasi jumlah ketersediaan pustaka yang relevan dengan bidang Program Studi </p>
					</div>
					<!--641-->						
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="tableBorang" id="addTable41" style="width:100%;">
								<thead>
									<tr>
										<th>Jenis Pustaka</th>
										<th>Jumlah Judul</th>
										<th>Jumlah Copy</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
									</tr>
									<?php
									$butir="6.4.1";
									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$no1."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print1 = $row["isi_float"];

									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$no1."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print2 = $row["isi_float"];

									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$no2."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print3 = $row["isi_float"];

									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$no3."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print4 = $row["isi_float"];

									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$no4."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print5 = $row["isi_float"];

									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$no5."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print6 = $row["isi_float"];

									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$no5."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print7 = $row["isi_float"];

									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$no6."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print8 = $row["isi_float"];

									$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$no6."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print9 = $row["isi_float"];
									?>
									<tr>
										<td>Buku teks</td>
										<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='6_4_1/1/2' id="totalBuku" onchange="penjumlahan641(1)" <?php echo "value='".$print1."'" ?>></td>
										<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='6_4_1/1/3' <?php echo "value='".$print2."'" ?>></td>
									</tr>
									<tr>
										<td>Jurnal nasional yang terakreditasi</td>
										<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='6_4_1/2/2' onchange="penjumlahan641(1)" <?php echo "value='".$print3."'" ?>></td>
										<td class='voidColor'></td>
									</tr>
									<tr>
										<td>jurnal interanasional</td>
										<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='6_4_1/3/2' onchange="penjumlahan641(1)" <?php echo "value='".$print4."'" ?>></td>
										<td class='voidColor'></td>
									</tr>
									<tr>
										<td>Prosiding</td>
										<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='6_4_1/4/2' onchange="penjumlahan641(1)" <?php echo "value='".$print5."'" ?>></td>
										<td class='voidColor'></td>
									</tr>
									<tr>
										<td>Skripsi/Tesis</td>
										<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='6_4_1/5/2' id="ta" onchange="penjumlahan641(1)" <?php echo "value='".$print6."'" ?>></td>
										<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='6_4_1/5/3' <?php echo "value='".$print7."'" ?>></td>
									</tr>
									<tr>
										<td>Disertasi</td>
										<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='6_4_1/6/2' id="sertasi" onchange="penjumlahan641(1)" <?php echo "value='".$print8."'" ?>></td>
										<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='6_4_1/6/3' <?php echo "value='".$print9."'" ?>></td>
									</tr>
									<tr>
										<td>Total</td>
										<td id='Total641'></td>
										<td></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<p class="col-sm-offset-2 col-sm-8">Jurnal/prosiding seminar yang tersedia/yang diterima secara teratur (lengkap), terbitan 3 tahun terakhir:</p>
					</div>
					
					<div class="form-group">
						<p class="col-sm-offset-2 col-sm-8">Tabel 2.   Jurnal yang tersedia/yang diterimasecarateratur (lengkap),   terbitan 3 tahun terakhir</p>
					</div>
					<!--JournalDikti-->						
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">Jurnal Terakreditasi DIKTI</label>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "6.4.1";
							$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$null."' AND no='".$null."' AND sub_no='".$subno1."'";
							$data = mysqli_query($db, $query);
							$count = mysqli_num_rows($data);
							if ($count>0) {
								$row = mysqli_fetch_assoc($data);
								$ValueCounter = $row["isi_float"];
							}else{
								$ValueCounter = 0;
							}
							echo "<input type='hidden' id='6.4.1/1/counter' name='6_4_1/1/counter' value='".$ValueCounter."'>";
							?>
							<table class="tableBorang" id="addTable412" style="width:100%;">
								<thead>
									<tr>
										<th>Jenis</th>
										<th>Nama Jurnal</th>
										<th>RincianTahun dan Nomor</th>
										<th>Jumlah</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
										<td>(4)</td>
									</tr>
									<?php
									for($i=1;$i<=$ValueCounter;$i++){
										$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$i."' AND sub_no='".$subno1."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print1 = $row["isi_text"];

										$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$i."' AND sub_no='".$subno1."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print2 = $row["isi_text"];

										$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom4."' AND no='".$i."' AND sub_no='".$subno1."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print3 = $row["isi_float"];

										echo "<tr>";
										echo "<td>Jurnal Terakreditasi DIKTI</td>";
										echo "<td><textarea type='text' class='form-control' name='6_4_1/1/2/".$i."'>".$print1."</textarea></td>";
										echo "<td><textarea type='text' class='form-control' name='6_4_1/1/3/".$i."'>".$print2."</textarea></td>";
										echo "<td><input type='text' pattern='[0-9]+([\-][0-9]+)?' class='form-control' name='6_4_1/1/4/".$i."' value='".$print3."'></td>";
										echo "</tr>";
									}
									?>
									<tr>
										<td colspan="4"><button type="button" onclick="addRow412('addTable412','6.4.1/1/counter')"><span class="glyphicon glyphicon-plus"></span></button></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!--JournalInternational-->		
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">Jurnal Internasional</label>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "6.4.1";
							$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$null."' AND no='".$null."' AND sub_no='".$subno2."'";
							$data = mysqli_query($db, $query);
							$count = mysqli_num_rows($data);
							if ($count>0) {
								$row = mysqli_fetch_assoc($data);
								$ValueCounter = $row["isi_float"];
							}else{
								$ValueCounter = 0;
							}
							echo "<input type='hidden' id='6.4.1/2/counter' name='6_4_1/2/counter' value='".$ValueCounter."'>";
							?>
							<table class="tableBorang" id="addTable413" style="width:100%;">
								<thead>
									<tr>
										<th>Jenis</th>
										<th>Nama Jurnal</th>
										<th>RincianTahun dan Nomor</th>
										<th>Jumlah</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
										<td>(4)</td>
									</tr>
									<?php
									for($i=1;$i<=$ValueCounter;$i++){
										$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$i."' AND sub_no='".$subno2."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print1 = $row["isi_text"];

										$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$i."' AND sub_no='".$subno2."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print2 = $row["isi_text"];

										$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom4."' AND no='".$i."' AND sub_no='".$subno2."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print3 = $row["isi_float"];

										echo "<tr>";
										echo "<td>Jurnal Internasional</td>";
										echo "<td><textarea type='text' class='form-control' name='6_4_1/2/2/".$i."'>".$print1."</textarea></td>";
										echo "<td><textarea type='text' class='form-control' name='6_4_1/2/3/".$i."'>".$print2."</textarea></td>";
										echo "<td><input type='text' pattern='[0-9]+([\-][0-9]+)?' class='form-control' name='6_4_1/2/4/".$i."' value='".$print3."'></td>";
										echo "</tr>";
									}
									?>
									<tr>
										<td colspan="4"><button type="button" onclick="addRow413('addTable413','6.4.1/2/counter')"><span class="glyphicon glyphicon-plus"></span></button></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!--Prosiding-->						
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">Prosiding</label>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "6.4.1";
							$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$null."' AND no='".$null."' AND sub_no='".$subno3."'";
							$data = mysqli_query($db, $query);
							$count = mysqli_num_rows($data);
							if ($count>0) {
								$row = mysqli_fetch_assoc($data);
								$ValueCounter = $row["isi_float"];
							}else{
								$ValueCounter = 0;
							}
							echo "<input type='hidden' id='6.4.1/3/counter' name='6_4_1/3/counter' value='".$ValueCounter."'>";
							?>
							<table class="tableBorang" id="addTable414" style="width:100%;">
								<thead>
									<tr>
										<th>Jenis</th>
										<th>Nama Jurnal</th>
										<th>RincianTahun dan Nomor</th>
										<th>Jumlah</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
										<td>(4)</td>
									</tr>
									<?php
									for($i=1;$i<=$ValueCounter;$i++){
										$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$i."' AND sub_no='".$subno3."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print1 = $row["isi_text"];

										$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$i."' AND sub_no='".$subno3."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print2 = $row["isi_text"];

										$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom4."' AND no='".$i."' AND sub_no='".$subno3."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print3 = $row["isi_float"];

										echo "<tr>";
										echo "<td>Prosiding</td>";
										echo "<td><textarea type='text' class='form-control' name='6_4_1/3/2/".$i."'>".$print1."</textarea></td>";
										echo "<td><textarea type='text' class='form-control' name='6_4_1/3/3/".$i."'>".$print2."</textarea></td>";
										echo "<td><input type='text' pattern='[0-9]+([\-][0-9]+)?' class='form-control' name='6_4_1/3/4/".$i."' value='".$print3."'></td>";
										echo "</tr>";
									}
									?>
									<tr>
										<td colspan="4"><button type="button" onclick="addRow414('addTable414','6.4.1/3/counter')"><span class="glyphicon glyphicon-plus"></span></button></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<p class="col-sm-offset-2 col-sm-8">Catatan * = termasuke-journal.   </p>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="3">6.4.1.a</td>
										<td colspan="2">Bahan pustaka berupa buku teks</td>
										<td id="jumlahTotalBuku"></td>
									</tr>
									<tr>
										<td colspan='2'>Skor</td>
										<td id="nilaiBuku1"></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td id="nilai641a"></td>
										<input type='hidden' name='nilai/6_4_1_a' id='penilaianBuku1'>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="3">6.4.1.b</td>
										<td colspan="2">Bahan pustaka: disertasi/tesis/skripsi/TA</td>
										<td id="totalTaSkripsi"></td>
									</tr>
									<tr>
										<td colspan='2'>Skor</td>
										<td id="nilaiTaSkripsi"></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td id="penilaianTA"></td>
										<input type='hidden' name='nilai/6_4_1_b' id='penilaianTAnilai'>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "6.4.1.c";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];

								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND sub_butir='".$no1."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print1 = $row["isi"];

								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND sub_butir='".$no2."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print2 = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="9">6.4.1.c</td>
										<td colspan="3">Bahan pustaka berupa jurnal ilmiah terakreditasi DIKTI. Skor butir ini</td>
									</tr>
									<tr>
										<td colspan='2'>Jumlah judul jurnal terakreditasi DIKTI yang nomornya lengkap</td>
										<td><input type='text' pattern='[0-9]+([\-][0-9]+)?' class='form-control' name='journallengkapdikti' <?php echo "value='".$print1."'"?>></td>
									</tr>
									<tr>
										<td colspan='2'>Jumlah judul jurnal terakreditasi DIKTI yang nomornya tidak lengkap</td>
										<td><input type='text' pattern='[0-9]+([\-][0-9]+)?' class='form-control' name='journaltidaklengkapdikti' <?php echo "value='".$print2."'"?>></td>
									</tr>
									<tr>
										<td>0</td>
										<td>Tidak memiliki jurnal terakreditasi DIKTI</td>
										<td></td>
									</tr>
									<tr>
										<td>1</td>
										<td>Tidak ada jurnal yang nomornya lengkap</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Memiliki satu judul jurnal, nomornya lengkap</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Memiliki dua judul jurnal, nomornya lengkap</td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Memiliki tiga atau lebih judul jurnal, nomornya lengkap</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" id="skordikti" class="form-control" value='<?php echo $print;?>' name='nilai/6_4_1_c'/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "6.4.1.d";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];

								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND sub_butir='".$no1."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print1 = $row["isi"];

								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND sub_butir='".$no2."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print2 = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="7">6.4.1.d</td>
										<td colspan="3">Bahan pustaka  berupa jurnal ilmiah internasional. Skor butir ini</td>
									</tr>
									<tr>
										<td colspan='2'>Jumlah judul jurnal internasional yang nomornya lengkap</td>
										<td><input type='text' pattern='[0-9]+([\-][0-9]+)?' class='form-control' name='journallengkapinternasional' <?php echo "value='".$print1."'"?>></td>
									</tr>
									<tr>
										<td colspan='2'>Jumlah judul jurnal internasional yang nomornya tidak lengkap</td>
										<td><input type='text' pattern='[0-9]+([\-][0-9]+)?' class='form-control' name='journaltidaklengkapinternasional' <?php echo "value='".$print2."'"?>></td>
									</tr>
									<tr>	
										<td>2</td>
										<td>Tidak ada jurnal yang nomornya lengkap</td>
										<td></td>
									</tr>
									<tr>	
										<td>3</td>
										<td>Satu judul jurnal yang nomornya lengkap</td>
										<td></td>
									</tr>
									<tr>	
										<td>4</td>
										<td>Dua atau lebih judul jurnal yang nomornya lengkap</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" id="skordikti" class="form-control" value='<?php echo $print;?>' name='nilai/6_4_1_d'/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="2">6.4.1.e</td>
										<td colspan="2">Jumlah prosiding seminar</td>
										<td id="jumlahprosi"></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td id='nilai641e1'></td>
										<input type='hidden' name='nilai/6_4_1_e' id='nilai641e'>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!--642-->						
					<div class="form-group">
						<label class="col-sm-2 text-right">6.4.2</label> 
						<p class="col-sm-8">Sumber-sumber pustaka di lembaga lain (lembaga perpustakaan/ sumber dari internet beserta  alamat website) yang biasa diakses/dimanfaatkan oleh dosen dan mahasiswa Program Studi ini.</p>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "6.4.2";
							$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$null."' AND no='".$null."'";
							$data = mysqli_query($db, $query);
							$count = mysqli_num_rows($data);
							if ($count>0) {
								$row = mysqli_fetch_assoc($data);
								$ValueCounter = $row["isi_float"];
							}else{
								$ValueCounter = 0;
							}
							echo "<input type='hidden' id='6.4.2/counter' name='6_4_2/counter' value='".$ValueCounter."'>";
							?>
							<table class="tableBorang" id="addTable42" style="width:100%;">
								<thead>
									<tr>
										<th>No.</th>
										<th>Nama Lembaga</th>
										<th>Alamat Website</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
									</tr>
									<?php
									for($i=1;$i<=$ValueCounter;$i++){
										$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$i."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print1 = $row["isi_text"];

										$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$i."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print2 = $row["isi_text"];

										echo "<tr>";
										echo "<td>".$i."</td>";
										echo "<td><input type='text' class='form-control' name='6_4_2/2/".$i."' value='".$print1."'></td>";
										echo "<td><input type='text' class='form-control' name='6_4_2/3/".$i."' value='".$print2."'></td>";
										echo "</tr>";
									}
									?>
									<tr>
										<td colspan="3"><button type="button" onclick="addRow42('addTable42','6.4.2/counter')"><span class="glyphicon glyphicon-plus"></span></button></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "6.4.2";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="7">6.4.2</td>
										<td colspan="3">Akses ke perpustakaan di luar PT atau sumber pustaka lainnya.</td>
									</tr>
									<tr>
										<td colspan='2'>Nilai untuk akses ke perpustakaan lainnya (termasuk on-line)</td>
										<td></td>
									</tr>
									<tr>	
										<td>1</td>
										<td>Tidak ada perpustakaan di luar PT yang dapat diakses </td>
										<td></td>
									</tr>
									<tr>	
										<td>2</td>
										<td>Ada perpustakaan di luar PT yang dapat diakses dan cukup baik fasilitasnya.</td>
										<td></td>
									</tr>
									<tr>	
										<td>3</td>
										<td>Ada perpustakaan di luar PT yang dapat diakses dan baik fasilitasnya.</td>
										<td></td>
									</tr>
									<tr>	
										<td>4</td>
										<td>Ada beberapa perpustakaan di luar PT yang dapat diakses dan sangat baik fasilitasnya, atau jika nilai rata-rata dari butir 6.4.1 ³ 3.</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control" value='<?php echo $print;?>' name='nilai/6_4_2'/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!--643-->						
					<div class="form-group">
						<label class="col-sm-2 text-right">6.4.3</label> 
						<p class="col-sm-8">Peralatan utama yang digunakan di laboratorium (tempat praktikum, bengkel, studio, ruang simulasi, rumah sakit, puskesmas/balai kesehatan, green house, lahan untuk pertanian, dan sejenisnya) yang dipergunakan dalam proses pembelajaran di jurusan/Fakultas:</p>
					</div>
					<!--643Software-->						
					<div class="form-group">
						<p class="col-sm-offset-2 col-sm-8">Software</p>
					</div>
					
					<div class="form-group">
						<?php
							//data retrieve for table
						$butir = "6.4.3";
							//retrieve counter
						$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom='0' ORDER BY no ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0){
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$counterdata[$index] = $row['isi_float'];	
								$index++;
							}
						}else{
							$counterdata[0] = 1;
							$counterdata[1] = 1;
						}

							//retrieve data
						$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom!='0' ORDER BY no,sub_no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$tabledata[$index] = $row['isi_char'];
								$index++;
							}
						}else{
							$tabledata[0] = "";
							$tabledata[1] = "";
							$tabledata[2] = "";
							$tabledata[3] = "";
							$tabledata[4] = "";
							$tabledata[5] = "";
							$tabledata[6] = "";
							$tabledata[7] = "";
						}
						?>

						<div class="col-sm-offset-2 col-sm-9 table-wrapper">
							<input type="hidden" id="6.4.3/counter" name="6_4_3/counter" <?php echo "value='".$counterdata[0]."'";?>>
							<table class="tableBorang" id="table643" style="width:140%;">
								<thead>
									<tr>
										<th rowspan="2">No.</th>
										<th rowspan="2">Nama Labora-torium</th>
										<th rowspan="2">Jenis Peralatan Utama</th>
										<th rowspan="2">Jumlah Unit</th>
										<th colspan="2">Kepemilikan</th>
										<th colspan="2">Kondisi</th>
										<th rowspan="2">Rata-rata Waktu Penggunaan (jam/minggu)</th>
									</tr>
									<tr>
										<th>SD</th>
										<th>SW</th>
										<th>Terawat</th>
										<th>Tidak Terawat</th>
									</tr>

								</thead>
								<tbody>
									<tr>
										<td style="width:4%;">(1)</td>
										<td>(2)</td>
										<td style="width:25%;">(3)</td>
										<td style="width:18%;">(4)</td>
										<td style="width:11%;">(5)</td>
										<td style="width:6%;">(6)</td>
										<td style="width:6%;">(7)</td>
										<td>(8)</td>
										<td>(9)</td>
									</tr>
									<?php 
									$index = 0;
									for ($no=1; $no <= $counterdata[0] ; $no++) { 
										echo "<tbody>";
										echo "<input type='hidden' id='6.4.3/counter/no".$no."' value='".$counterdata[$no]."' name='6_4_3/counter/no".$no."'>";
									//first tr
										?>
										<tr>
											<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>><?php echo $no; ?></td>
											<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>>
												<input type='text' class='form-control' <?php echo "name='6_4_3/2/no".$no."/sub1' value='".$tabledata[$index]."'";?>><?php $index++;?>
											</td>
											<td><input type='text' class='form-control' style='width:100%;' <?php echo "name='6_4_3/3/no".$no."/sub1' value='".$tabledata[$index]."'"; ?>><?php $index++;?></td>
											<td><input type='text' class='form-control' type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' style='width:100%;' <?php echo "name='6_4_3/4/no".$no."/sub1' value='".$tabledata[$index]."'"; ?>><?php $index++;?></td>
											<td><input type='checkbox' class='form-control' value='1' <?php echo "name='6_4_3/5/no".$no."/sub1' "; if ($tabledata[$index]=="1") { echo "checked";} $index++; ?>></td>
											<td><input type='checkbox' class='form-control' value='1' <?php echo "name='6_4_3/6/no".$no."/sub1' "; if ($tabledata[$index]=="1") { echo "checked";} $index++; ?>></td>
											<td><input type='checkbox' class='form-control' value='1' <?php echo "name='6_4_3/7/no".$no."/sub1' "; if ($tabledata[$index]=="1") { echo "checked";} $index++; ?>></td>
											<td><input type='checkbox' class='form-control' value='1' <?php echo "name='6_4_3/8/no".$no."/sub1' "; if ($tabledata[$index]=="1") { echo "checked";} $index++; ?>></td>
											<td><input class='form-control' type='text' pattern='[0-9]+([\])?' class='form-control' style='width:100%;' <?php echo "name='6_4_3/9/no".$no."/sub1' value='".$tabledata[$index]."'"; ?>><?php $index++;?></td>
										</tr>

										<?php
									//sub tr
										for ($sub=2; $sub <= $counterdata[$no]; $sub++) { 
											?>
											<tr>
												<td><input type='text' class='form-control' style='width:100%;' <?php echo "name='6_4_3/3/no".$no."/sub".$sub."' value='".$tabledata[$index]."'"; ?>><?php $index++;?></td>
												<td><input type='text' class='form-control' type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' style='width:100%;' <?php echo "name='6_4_3/4/no".$no."/sub".$sub."' value='".$tabledata[$index]."'"; ?>><?php $index++;?></td>
												<td><input type='checkbox' class='form-control' value='1' <?php echo "name='6_4_3/5/no".$no."/sub".$sub."' "; if ($tabledata[$index]=="1") { echo "checked";} $index++; ?>></td>
												<td><input type='checkbox' class='form-control' value='1' <?php echo "name='6_4_3/6/no".$no."/sub".$sub."' "; if ($tabledata[$index]=="1") { echo "checked";} $index++; ?>></td>
												<td><input type='checkbox' class='form-control' value='1' <?php echo "name='6_4_3/7/no".$no."/sub".$sub."' "; if ($tabledata[$index]=="1") { echo "checked";} $index++; ?>></td>
												<td><input type='checkbox' class='form-control' value='1' <?php echo "name='6_4_3/8/no".$no."/sub".$sub."' "; if ($tabledata[$index]=="1") { echo "checked";} $index++; ?>></td>
												<td><input type='text' class='form-control' pattern='[0-9]+([\])?' class='form-control' style='width:100%;' <?php echo "name='6_4_3/9/no".$no."/sub".$sub."' value='".$tabledata[$index]."'"; ?>><?php $index++;?></td>
											</tr>
											<?php
										}

									//add row button
										?>
										<tr>
											<td colspan='7'><button type='button' <?php echo "onclick='addrowtbody43(\"table643\",".$no.",\"6.4.3/counter/no".$no."\")'";?>><span class="glyphicon glyphicon-plus"></span></button></td>
										</tr>
										<?php
										echo "</tbody>";
									}
									?>
									<tr>
										<td colspan="9"><button type="button" onclick="addtbody43('table643','6.4.3/counter')"><span class="glyphicon glyphicon-plus"></span></button></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!--643Hardware-->						
					<div class="form-group">
						<p class="col-sm-offset-2 col-sm-8">Hardware</p>
					</div>
					
					<div class="form-group">
						<?php
							//data retrieve for table
						$butir = "6.4.3/1";
							//retrieve counter
						$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom='0' ORDER BY no ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0){
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$counterdata[$index] = $row['isi_float'];	
								$index++;
							}
						}else{
							$counterdata[0] = 1;
							$counterdata[1] = 1;
						}

							//retrieve data
						$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom!='0' ORDER BY no,sub_no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$tabledata[$index] = $row['isi_char'];
								$index++;
							}
						}else{
							$tabledata[0] = "";
							$tabledata[1] = "";
							$tabledata[2] = "";
							$tabledata[3] = "";
							$tabledata[4] = "";
							$tabledata[5] = "";
							$tabledata[6] = "";
							$tabledata[7] = "";
						}
						?>

						<div class="col-sm-offset-2 col-sm-9 table-wrapper">
							<input type="hidden" id="6.4.3/1/counter" name="6.4.3/1/counter" <?php echo "value='".$counterdata[0]."'";?>>
							<table class="tableBorang" id="table6432" style="width:140%;">
								<thead>
									<tr>
										<th rowspan="2">No.</th>
										<th rowspan="2">Nama Labora-torium</th>
										<th rowspan="2">Jenis Peralatan Utama</th>
										<th rowspan="2">Jumlah Unit</th>
										<th colspan="2">Kepemilikan</th>
										<th colspan="2">Kondisi</th>
										<th rowspan="2">Rata-rata Waktu Penggunaan (jam/minggu)</th>
									</tr>
									<tr>
										<th>SD</th>
										<th>SW</th>
										<th>Terawat</th>
										<th>Tidak Terawat</th>
									</tr>

								</thead>
								<tbody>
									<tr>
										<td style="width:4%;">(1)</td>
										<td>(2)</td>
										<td style="width:25%;">(3)</td>
										<td style="width:18%;">(4)</td>
										<td style="width:11%;">(5)</td>
										<td style="width:6%;">(6)</td>
										<td style="width:6%;">(7)</td>
										<td>(8)</td>
										<td>(9)</td>
									</tr>
									<?php 
									$index = 0;
									for ($no=1; $no <= $counterdata[0] ; $no++) { 
										echo "<tbody>";
										echo "<input type='hidden' id='6.4.3/1/counter/no".$no."' value='".$counterdata[$no]."' name='6.4.3/1/counter/no".$no."'>";
									//first tr
										?>
										<tr>
											<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>><?php echo $no; ?></td>
											<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>>
												<input type='text' class='form-control' <?php echo "name='6.4.3/1/2/no".$no."/sub1' value='".$tabledata[$index]."'";?>><?php $index++;?>
											</td>
											<td><input type='text' class='form-control' style='width:100%;' <?php echo "name='6.4.3/1/3/no".$no."/sub1' value='".$tabledata[$index]."'"; ?>><?php $index++;?></td>
											<td><input type='text' class='form-control' type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' style='width:100%;' <?php echo "name='6.4.3/1/4/no".$no."/sub1' value='".$tabledata[$index]."'"; ?>><?php $index++;?></td>
											<td><input type='checkbox' class='form-control' value='1' <?php echo "name='6.4.3/1/5/no".$no."/sub1' "; if ($tabledata[$index]=="1") { echo "checked";} $index++; ?>></td>
											<td><input type='checkbox' class='form-control' value='1' <?php echo "name='6.4.3/1/6/no".$no."/sub1' "; if ($tabledata[$index]=="1") { echo "checked";} $index++; ?>></td>
											<td><input type='checkbox' class='form-control' value='1' <?php echo "name='6.4.3/1/7/no".$no."/sub1' "; if ($tabledata[$index]=="1") { echo "checked";} $index++; ?>></td>
											<td><input type='checkbox' class='form-control' value='1' <?php echo "name='6.4.3/1/8/no".$no."/sub1' "; if ($tabledata[$index]=="1") { echo "checked";} $index++; ?>></td>
											<td><input type='text' class='form-control' pattern='[0-9]+([\])?' class='form-control' style='width:100%;' <?php echo "name='6.4.3/1/9/no".$no."/sub1' value='".$tabledata[$index]."'"; ?>><?php $index++;?></td>
										</tr>

										<?php
									//sub tr
										for ($sub=2; $sub <= $counterdata[$no]; $sub++) { 
											?>
											<tr>
												<td><input type='text' class='form-control' style='width:100%;' <?php echo "name='6.4.3/1/3/no".$no."/sub".$sub."' value='".$tabledata[$index]."'"; ?>><?php $index++;?></td>
												<td><input type='text' class='form-control' type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' style='width:100%;' <?php echo "name='6.4.3/1/4/no".$no."/sub".$sub."' value='".$tabledata[$index]."'"; ?>><?php $index++;?></td>
												<td><input type='checkbox' class='form-control' value='1' <?php echo "name='6.4.3/1/5/no".$no."/sub".$sub."' "; if ($tabledata[$index]=="1") { echo "checked";} $index++; ?>></td>
												<td><input type='checkbox' class='form-control' value='1' <?php echo "name='6.4.3/1/6/no".$no."/sub".$sub."' "; if ($tabledata[$index]=="1") { echo "checked";} $index++; ?>></td>
												<td><input type='checkbox' class='form-control' value='1' <?php echo "name='6.4.3/1/7/no".$no."/sub".$sub."' "; if ($tabledata[$index]=="1") { echo "checked";} $index++; ?>></td>
												<td><input type='checkbox' class='form-control' value='1' <?php echo "name='6.4.3/1/8/no".$no."/sub".$sub."' "; if ($tabledata[$index]=="1") { echo "checked";} $index++; ?>></td>
												<td><input type='text' class='form-control' type='text' pattern='[0-9]+([\])?' class='form-control' style='width:100%;' <?php echo "name='6.4.3/1/9/no".$no."/sub".$sub."' value='".$tabledata[$index]."'"; ?>><?php $index++;?></td>
											</tr>
											<?php
										}

									//add row button
										?>
										<tr>
											<td colspan='7'><button type='button' <?php echo "onclick='addrowtbody432(\"table6432\",".$no.",\"6.4.3/1/counter/no".$no."\")'";?>><span class="glyphicon glyphicon-plus"></span></button></td>
										</tr>
										<?php
										echo "</tbody>";
									}
									?>
									<tr>
										<td colspan="9"><button type="button" onclick="addtbody432('table6432','6.4.3/1/counter')"><span class="glyphicon glyphicon-plus"></span></button></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "6.4.3";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="7">6.4.3</td>
										<td colspan="3">Ketersediaan, akses dan pendayagunaan sarana utama di laboratorium. </td>
									</tr>
									<tr>
										<td>0</td>
										<td>Sangat kurang, kegiatan praktikum praktis tidak pernah dilakukan.</td>
										<td></td>
									</tr>
									<tr>	
										<td>1</td>
										<td>Kurang memadai, sehingga kegiatan praktikum dilaksanakan kurang dari batas minimal.</td>
										<td></td>
									</tr>
									<tr>	
										<td>2</td>
										<td>Cukup memadai, sebagian besar dalam kondisi baik, namun tidak mungkin digunakan di luar kegiatan praktikum terjadwal.</td>
										<td></td>
									</tr>
									<tr>	
										<td>3</td>
										<td>Memadai, sebagian besar dalam kondisi baik, dan PS memiliki akses yang baik (masih memungkinkan menggunakannya di luar kegiatan praktikum terjadwal, walau terbatas).</td>
										<td></td>
									</tr>
									<tr>	
										<td>4</td>
										<td>Sangat memadai, terawat dengan sangat baik, dan PS memiliki akses yang sangat baik (memiliki fleksibilitas dalam menggunakannya di luar kegiatan praktikum terjadwal).</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control" value='<?php echo $print;?>' name='nilai/6_4_3'/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!--65-->						
					<div class="form-group">
						<label class="col-sm-2 text-right">6.5</label> 
						<label class="col-sm-8">Sistem Informasi</label>
					</div>
					<!--651-->						
					<div class="form-group">
						<label class="col-sm-2 text-right">6.5.1</label> 
						<p class="col-sm-8">Jelaskan sistem informasi dan fasilitas yang digunakan oleh Program Studi untuk proses pembelajaran (hardware, software, e-learning, perpustakaan, dll.).</p>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "6.5.1";
							$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
							$data = mysqli_query($db, $query);
							$row = mysqli_fetch_assoc($data);
							$print = $row["isi_text"];
							?>
							<textarea type="text" class="form-control" rows="20" name="6_5_1" placeholder="" maxlength="60000"><?php echo $print;?></textarea>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "6.5.1";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="6">6.5.1</td>
										<td colspan="3">Sistem informasi dan fasilitas yang digunakan PS dalam PBM.</td>
									</tr>
									<tr>	
										<td>1</td>
										<td>Proses pembelajaran dilakukan secara manual.Pengelolaan koleksi perpustakaan menggunakan komputer stand alone, atau secara manual.</td>
										<td></td>
									</tr>
									<tr>	
										<td>2</td>
										<td>Sebagian dengan komputer, namun tidak terhubung dengan jaringan luas/internet.Kebanyakan software yang digunakan belum berlisensi.  Koleksi perpustakaan dikelola dengan komputer yang tidak terhubung jaringan.</td>
										<td></td>
									</tr>
									<tr>	
										<td>3</td>
										<td>Dengan komputer yang terhubung dengan jaringan luas/internet, software yang berlisensi dengan jumlah yang memadai. Tersedia fasilitas e-learning namun belum dimanfaatkan secara efektif.  Koleksi perpustakaan dapat diakses secara on-line namun masih ada kendala dalam kecepatan akses.</td>
										<td></td>
									</tr>
									<tr>	
										<td>4</td>
										<td>Dengan komputer yang terhubung dengan jaringan luas/internet, software yang berlisensi dengan jumlah yang memadai. Tersedia fasilitas e-learning yang digunakan secara baik, dan akses on-line ke koleksi perpustakaan.</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control" value='<?php echo $print;?>' name='nilai/6_5_1'/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!--652-->					
					<div class="form-group">
						<label class="col-sm-2 text-right">6.5.2</label> 
						<p class="col-sm-8">Beri tanda √ pada kolom yang sesuai dengan aksesibilitas tiap jenis data, dengan mengikuti format tabel berikut:</p>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "6.5.2";
							$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$null."' AND no='".$null."'";
							$data = mysqli_query($db, $query);
							$count = mysqli_num_rows($data);
							if ($count>0) {
								$row = mysqli_fetch_assoc($data);
								$ValueCounter = $row["isi_float"];
							}else{
								$ValueCounter = 0;
							}
							echo "<input type='hidden' id='6.5.2/counter' name='6_5_2/counter' value='".$ValueCounter."'>";
							?>
							<table class="tableBorang" id="addTable52" style="width:100%;">
								<thead>
									<tr>
										<th rowspan="2" style="width:5%">No.</th>
										<th rowspan="2">Jenis Data</th>
										<th colspan="4">Sistem Pengolahan Data</th>
									</tr>
									<tr>
										<th>Secara Manual</th>
										<th>Dengan Komputer Tanpa Jaringan</th>
										<th>Dengan Komputer Jaringan Lokal (LAN)</th>
										<th>Dengan Komputer Jaringan Luas (WAN)</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
										<td>(4)</td>
										<td>(5)</td>
										<td>(6)</td>
									</tr>
									<?php
									for($i=1;$i<=$ValueCounter;$i++){
										$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$i."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print1 = $row["isi_text"];
										
										$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$i."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print2 = $row["isi_float"];

										$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom4."' AND no='".$i."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print3 = $row["isi_float"];
										
										$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom5."' AND no='".$i."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print4 = $row["isi_float"];

										$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom6."' AND no='".$i."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print5 = $row["isi_float"];
										
										$check1="";
										$check2="";
										$check3="";
										$check4="";
										if($print2 == 1){
											$check1 = "checked";
										}
										if($print3 == 1){
											$check2 = "checked";
										}
										if($print4 == 1){
											$check3 = "checked";
										}
										if($print5 == 1){
											$check4 = "checked";
										}
										
										echo "<tr>";
										echo "<td>".$i."</td>";
										echo "<td><input type='text' class='form-control' name='6_5_2/2/".$i."' value='".$print1."'></td>";
										echo "<td><input type='checkbox' name='6_5_2/3/".$i."' value='1' ".$check1." onchange='totalcheck6521(2)'>";
										echo "<td><input type='checkbox' name='6_5_2/4/".$i."' value='1' ".$check2." onchange='totalcheck6522(3)' >";
										echo "<td><input type='checkbox' name='6_5_2/5/".$i."' value='1' ".$check3." onchange='totalcheck6523(4)'>";
										echo "<td><input type='checkbox' name='6_5_2/6/".$i."' value='1' ".$check4." onchange='totalcheck6524(5)'>";
										echo "</tr>";
									}
									?>
									<tr>
										<td colspan="5"><button type="button" onclick="addRow52('addTable52','6.5.2/counter')"><span class="glyphicon glyphicon-plus"></span></button></td>
									</tr>
									<tr>
										<td colspan="2">Total</td>
										<td id="check6521"></td>
										<td id="check6522"></td>
										<td id="check6523"></td>
										<td id="check6524"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="7">6.5.2</td>
										<td colspan="3">Aksesibilitas data dalam sistem informasi. </td>
									</tr>
									<tr>	
										<td>1</td>
										<td>Banyak tanda √ pada kolom (3)--> Kisaran 0-11</td>
										<td id="totalcheck6521"></td>
									</tr>
									<tr>	
										<td>2</td>
										<td>Banyak tanda √ pada kolom (4)--> Kisaran 0-11</td>
										<td id="totalcheck6522"></td>
									</tr>
									<tr>	
										<td>3</td>
										<td>Banyak tanda √ pada kolom (5)--> Kisaran 0-11</td>
										<td id="totalcheck6523"></td>
									</tr>
									<tr>	
										<td>4</td>
										<td>Banyak tanda √ pada kolom (6)--> Kisaran 0-11</td>
										<td id="totalcheck6524"></td>
									</tr>
									<tr>
										<td colspan='2'>Skor</td>
										<td id="skor652"></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td id="nilai652"></td>
										<input type="hidden" name="nilai/6_5_2" id="nilai6521">
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
				</form>
			</div>
		</div>
	</div>
</body>

<script>
	$(document).ready(function() {
		$("form input, form select, form textarea").attr('readonly',true);
		$("button").hide();
		$("#editButton").show();
		$("#hideButton").show();
	});

	$("#editButton").click(function() {
		$("form input, form select, form textarea").attr('disabled',false);
		$("button").not($("#hideButton,#showButton")).show();
		$("#editButton").hide();
	});

	$("#hideButton").click(function() {
		$(".form-group").not($(".tableNilai").parent().parent()).hide();
		$("#showButton").show();
		$("#hideButton").hide();
	});

	$("#showButton").click(function() {
		$(".form-group").show();
		$("#showButton").hide();
		$("#hideButton").show();
	});

	$('form').submit(function() {
		$('body').hide();
		$("header").hide();
		$("html").prepend("<div class='ball'></div><div class='ball1'></div><h1 style='margin-left:43%'>MEMPROSES<h1>");
	});
</script>

</html>