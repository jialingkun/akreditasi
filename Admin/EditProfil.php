<!doctype html>
<html>
<?php
	require "../Cookies.php";
?>
<head>
	<title>Admin</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/style.css" rel="stylesheet">
	<link href="../font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<style>
		.nav.navbar-nav li {
			float: left;
		}
	</style>
</head>

<body>
	<div class='wrapper'>
		<header>
			<nav class="navbar navbar-default navbar-fixed-top">
				<div id="navbar" class="navbar-collapse collapse col-md-offset-3">
					<ul class="nav navbar-nav">
						<li><a href="HomeAdmin.php">Home</a></li>
						<li><a href="Prodi.php">Edit Prodi</a></li>
						<li class="active"><a href="EditProfil.php">Edit Profil</a></li>
						<li><a href="UbahPassword.php">Ubah Password</a></li>
						<li><a href="Logout.php" class="col-md-offset-10">Log Out</a></li>
					</ul>
				</div>
			</nav>
			<div class='text-center'>
				<br>
				<br>
				<h1>Edit Profil</h1>
			</div>
		</header>
	</div>
	
	<div class = "container">
		<hr class="colorgraph"><br>
		<div class="row">
			
			<?php 
				echo '<a href="TambahUser.php" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-plus"></span> Tambah Data</a>';
				// display data in table
			
				echo "<table class='table table-hover' >";
				echo "<thead> <tr> <th>NIP</th> <th>Nama</th> <th>Username</th> <th>Password</th> <th>Prodi</th> <th>Status</th></tr> </thead>";

				require "../Database/DatabaseConnection.php";
				$query="SELECT * FROM user NATURAL JOIN prodi where status!='Admin' ORDER BY aktif DESC, idProdi ASC;";
				$data = mysqli_query($db, $query);
				while($row = mysqli_fetch_assoc($data)){
			?>
				<tbody> <tr>
				<td><?php echo $row['nip'];?></td>
				<td><?php echo $row['nama'];?></td>
				<td><?php echo $row['username'];?></td>
				<td><?php echo $row['password'];?></td>
				<td><?php echo $row['namaProdi'];?></td>

				<?php if($row['Aktif'] == 0) { 
					echo "<td><span class='label label-danger'>OFF</span></td>";}
					else{ 
						echo "<td><span class='label label-success'>ON </span></td>";
					}
				?>
					<td><a href="EditUser.php?Username=<?php echo $row['username'];?>" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-pencil"></span> Ubah</a></td>
					<td><a href="DeleteUser.php?Username=<?php echo $row['username'];?>" class="btn btn-sm btn-danger" onclick = "return confirm('PERHATIAN: \nMenghapus user dapat berakibat pada terhapusnya data isian user tersebut. \nAlternatif lain adalah dengan menonaktifkan user. \nApakah anda yakin ingin menghapus?');"><span class="glyphicon glyphicon-trash"></span> Hapus</a></td>
				</tr></tbody>
			<?php
				}
				echo "</table>"; 
			?>

		</div>
	</div>

	</body>

	</html>