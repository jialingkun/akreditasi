<!doctype html>

<?php
	require "../Cookies.php";
	require "../Database/DatabaseConnection.php";
?>

<html>

<head>
	<title>Priode</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/style.css" rel="stylesheet">
	<link href="../font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>	
	<div class='wrapper'>
		<header>
			<nav class="navbar navbar-default navbar-fixed-top">
				<div id="navbar" class="navbar-collapse collapse col-md-offset-3">
					<ul class="nav navbar-nav ">
						<li><a href="HomeAdmin.php">Home</a></li>
						<li class="active"><a href="Prodi.php">Edit Prodi</a></li>
						<li><a href="EditProfil.php">Edit Profil</a></li>
						<li><a href="UbahPassword.php">Ubah Password</a></li>
						<li><a href="Logout.php" class="col-md-offset-10">Log Out</a></li>
					</ul>
				</div>
			</nav>
			<div class='text-center'>
				<br>
				<br>
				<h1>Prodi</h1>
			</div>
		</header>
	</div>
	
	
	<div class = "container">
		<hr class="colorgraph"><br>
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<a href="TambahProdi.php" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-plus"></span>Tambah Prodi</a>
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>No</th>
							<th>ID</th>
							<th>Nama</th>
							<td></th>
						</tr>
					</thead>
					<tbody>
						<?php
							$i = 1;
							$query="SELECT * FROM prodi where idProdi !=0";
							$data = mysqli_query($db, $query);
							//$row = mysqli_fetch_assoc($data);
							
							while($row = mysqli_fetch_assoc($data)){
								echo "<tr>";
								echo "<td>".$i."</td>";
								echo "<td>".$row["idProdi"]."</td>";
								echo "<td>".$row["namaProdi"]."</td>";
								?>
								<td><a href="DeleteProdi.php?idProdi=<?php echo $row['idProdi'];?>" class="btn btn-sm btn-danger" onclick = "return confirm('PERHATIAN: \n Menghapus Prodi dapat berakibat pada terhapusnya data. \n Apakah anda yakin ingin menghapus?');"><span class="glyphicon glyphicon-trash"></span> Hapus</a></td>
								<?php
								echo "</tr>";
								$i++;
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>

</html>