<!doctype html>
<html>
<?php
require "../CookiesKaprodi.php";
require "../Database/DatabaseConnection.php";

$query='select idPeriode,NamaPeriode from periode where aktif = 1';
$data = mysqli_query($db, $query);
$row = mysqli_fetch_assoc($data);

$namaPeriode = $row['NamaPeriode'];
$periode = $row["idPeriode"];
$username= $_COOKIE['LPMKa'];

$query="select idProdi from user where username='".$username."'";
$data = mysqli_query($db, $query);
$row = mysqli_fetch_assoc($data);
$prodi = $row["idProdi"];

//retrieve data dosen PS
$butir = "dosen_ps";
$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom='2' ORDER BY no,sub_no,kolom ASC;";
$data = mysqli_query($db, $query);
$countDosenPS = mysqli_num_rows($data);
$dosenPS[0] = "-";
if ($countDosenPS>0) {
	$index = 1;
	while ($row = mysqli_fetch_assoc($data)) {
		$dosenPS[$index] = $row['isi_char'];
		$index++;
	}
}

$standar= 7;

?>
<head>
	<title>Kaprodi</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/style.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../css/loading.css">
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script src="../js/jquery.min.js"></script>
	<script src="../js/form.js"></script>
	<script src="../js/standar7.js"></script>

</head>
<body onload="loadAllJs()">
	<div class='wrapper'>
		<header>
			<nav class="navbar navbar-default navbar-fixed-top">
				<div id="navbar" class="navbar-collapse collapse col-sm-offset-1">
					<ul class="nav navbar-nav">
						<li><a href="HomeKaprodi.php">Home</a></li>
						<li><a href="Standar1.php">Standar 1</a></li>
						<li><a href="Standar2.php">Standar 2</a></li>
						<li><a href="Standar3.php">Standar 3</a></li>
						<li><a href="Standar4.php">Standar 4</a></li>
						<li><a href="Standar5.php">Standar 5</a></li>
						<li><a href="Standar6.php">Standar 6</a></li>
						<li class="active"><a href="Standar7.php">Standar 7</a></li>
						<li><a href="Nilai.php">Nilai</a></li>
						<li><a href="Logout.php" class="col-sm-offset-9">Log Out</a></li>
					</ul>
				</div>
			</nav>
			<div class='text-center'>
				<br>
				<br>
				<h1>STANDAR 7</h1>
				<h4>PENELITIAN, PELAYANAN/PENGABDIAN KEPADA MASYARAKAT, DAN KERJASAMA</h4>
				<h5>Periode: <?php echo $namaPeriode ?></h5>
			</div>
		</header>
	</div>
	
	<div class = "container">
		<hr class="colorgraph"><br>
		<div class="row">
			<div class="col-md-12">
				<form action="ProsesStandar7.php" method="post" class="form-horizontal">
					<div class="fixed-button">
						<button type="submit" class="btn btn-primary">Simpan</button>
						<button type="button" class="btn btn-success" id="editButton">Edit</button>
					</div>

					<!-- Dummy table untuk simpan nama dosen -->
					<div class="form-group" style="visibility:collapse;">
						<?php
						//data retrieve for table
						$butir = "dosen_ps";
						//retrieve counter
						$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom='0' ORDER BY no ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0){
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$counterdata[$index] = $row['isi_float'];	
								$index++;
							}
						}else{
							$counterdata[0] = 1;
							$counterdata[1] = 1;
						}

						//retrieve data
						$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom!='0' ORDER BY no,sub_no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$tabledata[$index] = $row['isi_char'];
								$index++;
							}
						}else{
							$tabledata[0] = "";
							$tabledata[1] = "";
							$tabledata[2] = "";
							$tabledata[3] = "";
							$tabledata[4] = "";
							$tabledata[5] = "";
							$tabledata[6] = "";
							$tabledata[7] = "";
						}
						?>
						<div class="col-sm-offset-2 col-sm-9 table-wrapper">
							<input type="hidden" id="4.3.1/counter" name="4_3_1/counter" <?php echo "value='".$counterdata[0]."'";?>>
							<table class="tableBorang" id="table431" style="width:195%;">
								<thead>
									<tr>
										<th>No.</th>
										<th>Nama Dosen</th>
										<th>NIDN</th>
										<th>Tgl. Lahir</th>
										<th>Jabatatan Akademik</th>
										<th>Gelar Akademik</th>
										<th colspan="2">Pendidikan S1, S2, S3 dan Asal PT</th>
										<th>Bidang Keahlian Untuk Setiap Jenjang Pendidikan</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="width:3%;">(1)</td>
										<td style="width:16%;">(2)</td>
										<td style="width:9%;">(3)</td>
										<td style="width:9%;">(4)</td>
										<td style="width:12%;">(5)</td>
										<td style="width:6%;">(6)</td>
										<td style="width:6%;">(7)</td>
										<td style="width:23%;">(8)</td>
										<td>(9)</td>
									</tr>
								</tbody>

								<?php 
								$index = 0;
								for ($no=1; $no <= $counterdata[0] ; $no++) { 
									echo "<tbody>";
									echo "<input type='hidden' id='4.3.1/counter/no".$no."' value='".$counterdata[$no]."'>";
									//first tr
									?>
									<tr>
										<td <?php echo "rowspan='".$counterdata[$no]."'" ?>><?php echo $no; ?></td>
										<td <?php echo "rowspan='".$counterdata[$no]."' id='4.3.1/2/no".$no."'";?>><?php echo $tabledata[$index]; $index++;?></td>
										<td <?php echo "rowspan='".$counterdata[$no]."'" ?>><?php echo $tabledata[$index]; $index++;?></td>
										<td <?php echo "rowspan='".$counterdata[$no]."'" ?>><?php echo $tabledata[$index]; $index++;?></td>
										<td <?php echo "rowspan='".$counterdata[$no]."'" ?>><?php echo $tabledata[$index]; $index++;?></td>
										<td><?php echo $tabledata[$index]; $index++;?></td>
										<td><?php echo $tabledata[$index]; $index++;?></td>
										<td><?php echo $tabledata[$index]; $index++;?></td>
										<td><?php echo $tabledata[$index]; $index++;?></td>
									</tr>

									<?php
									//sub tr
									for ($sub=2; $sub <= $counterdata[$no]; $sub++) { 
										?>
										<tr>
											<td><?php echo $tabledata[$index]; $index++;?></td>
											<td><?php echo $tabledata[$index]; $index++;?></td>
											<td><?php echo $tabledata[$index]; $index++;?></td>
											<td><?php echo $tabledata[$index]; $index++;?></td>
										</tr>
										<?php
									}
									echo "</tbody>";
								}
								?>
							</table>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 text-right">7.1</label> 
						<label class="col-sm-8">Penelitian Dosen Tetap yang Bidang Keahliannya Sesuai dengan PS</label>
					</div>

					<div class="form-group">
						<label class="col-sm-2 text-right">7.1.1</label> 
						<div class="col-sm-8">Tuliskan jumlah judul penelitian yang sesuai dengan bidang keilmuan PS, yang dilakukan oleh dosen tetap yang bidang keahliannya sesuai dengan PS selama tiga tahun terakhir dengan mengikuti format tabel berikut:</div>
					</div>

					<div class="form-group">

						<?php
						//data retrieve for table
						$butir = "7.1.1";
						$tabledata = array_fill(0, 15, '');
						$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY NO ASC, kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$tabledata[$index] = $row['isi_float'];
								$index++;
							}
						}
						?>

						<div class="col-sm-8 col-sm-offset-2">
							<table class="tableBorang" style="width:100%;" id="tabel711">
								<thead>
									<tr>
										<th>Sumber Pembiayaan</th>
										<th>TS-2</th>
										<th>TS-1</th>
										<th>TS</th>
										<th>Jumlah</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="width:45%;">(1)</td>
										<td>(2)</td>
										<td>(3)</td>
										<td>(4)</td>
										<td style="width:15%;">(5)</td>
									</tr>

									<?php  
									$namarow[0]="Pembiayaan sendiri oleh peneliti";
									$namarow[1]="PT yang bersangkutan";
									$namarow[2]="Depdiknas (hibah)";
									$namarow[3]="Institusi dalam negeri di luar Depdiknas";
									$namarow[4]="Institusi luar negeri";

									$index = 0;
									for ($row=1; $row <= 5; $row++) {
										echo "<tr>";

									//nama TS
										echo "<td>".$namarow[$row-1]."</td>";

									//kolom input
										for ($column=2; $column <= 4; $column++) { 
											echo "<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='7_1_1/".$column."/no".$row."' onchange='updateNilai11(".$row.")' value='".$tabledata[$index]."'></td>";
											$index++;
										}

										echo "<td id='7.1.1/no".$row."'></td>";
										echo "</tr>";
									}
									?>
								</tbody>
							</table>
						</div>
					</div>


					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="7">7.1.1</td>
									<td colspan="2">Jumlah penelitian yang sesuai dengan bidang keilmuan PS, yang dilakukan oleh dosen tetap yang bidang keahliannya sama dengan PS, selama 3 tahun.</td>
								</tr>
								<tr>
									<td>Jumlah penelitian dengan biaya LN</td>
									<td id="nilai/7.1.1/1"></td>
								</tr>
								<tr>
									<td>Jumlah penelitian dengan biaya luar (PT)</td>
									<td id="nilai/7.1.1/2"></td>
								</tr>
								<tr>
									<td>Jumlah penelitian dengan biaya PT/sendiri</td>
									<td id="nilai/7.1.1/3"></td>
								</tr>
								<tr>
									<td>Jumlah dosen tetap dengan bidang sesuai PS</td>
									<td id="nilai/7.1.1/4"><?php echo $countDosenPS ?></td>
								</tr>
								<tr>
									<td>Nilai Kasar (NK)</td>
									<td id="nilai/7.1.1/5"></td>
								</tr>
								<tr>
									<td>Nilai</td>
									<td id="nilai/7.1.1/nilai"></td>
									<input type="hidden" id="nilai/7.1.1/nilaihid" name="nilai/7_1_1">
								</tr>
							</table>
						</div>
					</div>


					<div class="form-group"> 
						<label class="col-sm-2 text-right">7.1.2</label> 
						<p class="col-sm-8">Adakah mahasiswa tugas akhir yang dilibatkan dalam penelitian dosen dalam tiga tahun terakhir? </p>
						<div class="col-sm-2"></div>
					</div>

					<div class="form-group"> 
						<?php
						//data retrieve for table
						$butir = "7.1.2";
						$tabledata = array_fill(0, 3, '');
						$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY NO DESC, kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$tabledata[$index] = $row['isi_char'];
								$index++;
							}
						}
						?>
						<div class="radio col-sm-8 col-sm-offset-2">
							<label><input type="radio" name="7_1_2/radio" value="tidak" <?php if ($tabledata[0]!="ada") {echo "checked";} ?>>Tidak Ada</label>
						</div>
						<div class="col-sm-2"></div>
						<div class="radio col-sm-8 col-sm-offset-2">
							<label><input type="radio" name="7_1_2/radio" value="ada" <?php if ($tabledata[0]=="ada") {echo "checked";} ?>>Ada</label>
						</div>
						<div class="col-sm-2"></div>
					</div>

					<div class="form-group"> 
						<div class="col-sm-8 col-sm-offset-2">Jika ada, banyaknya mahasiswa PS yang ikut serta dalam penelitian dosen adalah
							<input type="text" pattern="[0-9]+([\.][0-9]+)?" style="width:5%;" name="7_1_2/1" id="7.1.2/1" onchange="updateNilai12()" value=<?php echo "'".$tabledata[1]."'";?>>
							orang, dari 
							<input type="text" pattern="[0-9]+([\.][0-9]+)?" style="width:5%;" name="7_1_2/2" id="7.1.2/2" onchange="updateNilai12()" value=<?php echo "'".$tabledata[2]."'";?>> mahasiswa yang melakukan tugas akhir melalui skripsi.
						</div>
						<div class="col-sm-2"></div>
					</div>


					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="5">7.1.2</td>
									<td colspan="2">Keterlibatan mahasiswa yang melakukan tugas akhir dalam penelitian dosen</td>
								</tr>
								<tr>
									<td>Jumlah mahasiswa yang melakukan tugas akhir (TA)</td>
									<td id="nilai/7.1.2/1"></td>
								</tr>
								<tr>
									<td>Jumlah mahasiswa TA yang terlibat penelitian dosen</td>
									<td id="nilai/7.1.2/2"></td>
								</tr>
								<tr>
									<td>Persentase mahasiswa yang terlibat</td>
									<td id="nilai/7.1.2/3"></td>
								</tr>
								<tr>
									<td>Nilai</td>
									<td id="nilai/7.1.2/nilai"></td>
									<input type="hidden" id="nilai/7.1.2/nilaihid" name="nilai/7_1_2">
								</tr>
							</table>
						</div>
					</div>


					<div class="form-group">
						<label class="col-sm-2 text-right">7.1.3</label> 
						<label class="col-sm-8">Tuliskan judul artikel ilmiah/karya ilmiah/karya seni/buku yang dihasilkan selama tiga tahun terakhir oleh dosen tetap yang bidang keahliannya sesuai dengan PS dengan mengikuti format tabel berikut:</label>
						<div class="col-sm-2"></div>
					</div>

					<div class="form-group" align="center">
						<?php
						//data retrieve for table
						$butir = "7.1.3";
						$query="SELECT isi_char, isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								if ($index==0) {
									$tabledata[$index] = $row['isi_float'];
								}else{
									$tabledata[$index] = $row['isi_char'];
								}

								$index++;
							}
						}else{
							$tabledata[0] = 1;
							$tabledata[1] = "";
							$tabledata[2] = "";
							$tabledata[3] = "";
							$tabledata[4] = "";
							$tabledata[5] = "";
							$tabledata[6] = "";
							$tabledata[7] = "";
							$tabledata[8] = "";

						}
						?>
						<div class="col-sm-8 col-sm-offset-2 table-wrapper">
							<?php 
							echo "<input type='hidden' id='7.1.3/counter' name='7_1_3/counter' value=".$tabledata[0].">";
							?>
							<table class="tableBorang" id="tabel713" style="width:167%;">
								<thead>
									<tr>
										<th rowspan="2">No.</th>
										<th rowspan="2">judul</th>
										<th rowspan="2">Nama Dosen</th>
										<th rowspan="2">Dihasilkan/ Dipublikasikan pada</th>
										<th rowspan="2">Tahun Penyajian/ Publikasi</th>
										<th colspan="3">Tingkat</th>
									</tr>
									<tr>
										<th>Lokal</th>
										<th>Nasional</th>
										<th>Internasional</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td style="width:23%;">(2)</td>
										<td style="width:26%;">(3)</td>
										<td style="width:22%;">(4)</td>
										<td style="width:7%;">(5)</td>
										<td>(6)</td>
										<td>(7)</td>
										<td>(8)</td>
									</tr>
									<?php 
									$index = 1;
									for ($row=1; $row <= $tabledata[0]; $row++) { 
										?>
										<tr>
											<td><?php echo $row;?></td>
											<td><textarea rows="3" style="width:100%;" <?php echo "name='7_1_3/2/no".$row."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td> <select class="form-control" id="" <?php echo "name='7_1_3/3/no".$row."'"; ?> >
												<option>-</option>
												<?php
												for ($i=1; $i <= $countDosenPS; $i++) {
													if ($tabledata[$index]==$dosenPS[$i]) {
														echo "<option selected>".$dosenPS[$i]."</option>";
													}
													else{
														echo "<option>".$dosenPS[$i]."</option>";	
													}
												}
												$index++;
												?>
											</select></td>
											<td><textarea rows="3" style="width:100%;" <?php echo "name='7_1_3/4/no".$row."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td><textarea rows="3" style="width:100%;" <?php echo "name='7_1_3/5/no".$row."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td><input type='checkbox' class='form-control' value='1' onchange='updateNilai13(5)' <?php echo "name='7_1_3/6/no".$row."'"; if ($tabledata[$index]=="1") { echo "checked";} $index++; ?>></td>
											<td><input type='checkbox' class='form-control' value='1' onchange='updateNilai13(6)' <?php echo "name='7_1_3/7/no".$row."'"; if ($tabledata[$index]=="1") { echo "checked";} $index++; ?>></td>
											<td><input type='checkbox' class='form-control' value='1' onchange='updateNilai13(7)' <?php echo "name='7_1_3/8/no".$row."'"; if ($tabledata[$index]=="1") { echo "checked";} $index++; ?>></td>

										</tr>

										<?php
									}
									?>

									<tr>
										<td colspan="8"><button type="button" onclick="addRow13('tabel713','7.1.3/counter')"><span class="glyphicon glyphicon-plus"></span></button>
										<button type="button" onclick="removerow('tabel713','7.1.3/counter')"><span class="glyphicon glyphicon-minus"></span></button></td>
									</tr>
									<tr>
										<td colspan="5">Jumlah</td>
										<td id="7.1.3/6/jumlah"></td>
										<td id="7.1.3/7/jumlah"></td>
										<td id="7.1.3/8/jumlah"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="7">7.1.3</td>
									<td colspan="2">Jumlah artikel ilmiah yang dihasilkan oleh dosen tetap yang bidang keahliannya sama dengan PS, selama 3 tahun.</td>
								</tr>
								<tr>
									<td>Jumlah keterlibatan dosen dalam artikel/karya ilmiah/seni tingkat internasional</td>
									<td id="nilai/7.1.3/1"></td>
								</tr>
								<tr>
									<td>Jumlah keterlibatan dosen dalam artikel/karya ilmiah/seni tingkat nasional</td>
									<td id="nilai/7.1.3/2"></td>
								</tr>
								<tr>
									<td>Jumlah keterlibatan dosen dalam artikel/karya ilmiah/seni tingkat lokal</td>
									<td id="nilai/7.1.3/3"></td>
								</tr>
								<tr>
									<td>Jumlah dosen tetap dengan bidang sesuai PS</td>
									<td id="nilai/7.1.3/4"><?php echo $countDosenPS ?></td>
								</tr>
								<tr>
									<td>Nilai Kasar (NK)</td>
									<td id="nilai/7.1.3/5"></td>
								</tr>
								<tr>
									<td>Nilai</td>
									<td id="nilai/7.1.3/nilai"></td>
									<input type="hidden" id="nilai/7.1.3/nilaihid" name="nilai/7_1_3">
								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 text-right">7.1.4</label> 
						<div class="col-sm-8">Karya dosen dan atau mahasiswa Program Studi yang telah memperoleh/sedang memproses perlindungan Hak atas Kekayaan Intelektual (HaKI) selama tiga tahun terakhir.</div>
					</div>

					<div class="form-group">

						<?php
						//data retrieve for table
						$butir = "7.1.4";
						$query="SELECT isi_float, isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								if ($index==0) {
									$tabledata[$index] = $row['isi_float'];
								}else{
									$tabledata[$index] = $row['isi_text'];
								}
								$index++;
							}
						}else{
							$tabledata[0] = 1;
							$tabledata[1] = "";

						}
						?>
						<div class="col-sm-8 col-sm-offset-2">
							<?php 
							echo "<input type='hidden' id='7.1.4/counter' name='7_1_4/counter' value=".$tabledata[0].">";
							?>
							<table class="tableBorang" style="width:100%;" id="tabel714">
								<thead>
									<tr>
										<th style="width:5%">No.</th>
										<th>Karya</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
									</tr>
									<?php 
									$index = 1;
									for ($row=1; $row <= $tabledata[0]; $row++) { 
										?>
										<tr>
											<td><?php echo $row;?></td>
											<td><textarea rows="2" style="width:100%;" onchange="updateNilai14()" <?php echo "name='7_1_4/2/no".$row."' id='7.1.4/2/no".$row."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
										</tr>

										<?php
									}
									?>
									<tr>
										<td colspan="4"><button type="button" onclick="addRow14('tabel714','7.1.4/counter')"><span class="glyphicon glyphicon-plus"></span></button>
										<button type="button" onclick="removerow('tabel714','7.1.4/counter')"><span class="glyphicon glyphicon-minus"></span></button></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="3">7.1.4</td>
									<td colspan="2">Karya PS/institusi memperoleh perlindungan HaKI dalam 3 tahun terakhir</td>
								</tr>
								<tr>
									<td>Jumlah karya yang memperoleh HaKI.</td>
									<td id="nilai/7.1.4/1"></td>
								</tr>
								<tr>
									<td>Nilai</td>
									<td id="nilai/7.1.4/nilai"></td>
									<input type="hidden" id="nilai/7.1.4/nilaihid" name="nilai/7_1_4">
								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 text-right">7.2</label> 
						<label class="col-sm-8">Kegiatan Pelayanan/Pengabdian kepada Masyarakat (PkM)</label>
					</div>

					<div class="form-group">
						<label class="col-sm-2 text-right">7.2.1</label> 
						<div class="col-sm-8">jumlah kegiatan Pelayanan/Pengabdian kepada Masyarakat yang sesuai dengan bidang keilmuan PS selama tiga tahun terakhir yang dilakukan oleh dosen tetap yang bidang keahliannya sesuai dengan PS</div>
					</div>

					<div class="form-group">

						<?php
							//data retrieve for table
						$butir = "7.2.1";
						$tabledata = array_fill(0, 15, '');
						$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY NO ASC, kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$tabledata[$index] = $row['isi_float'];
								$index++;
							}
						}
						?>

						<div class="col-sm-8 col-sm-offset-2">
							<table class="tableBorang" style="width:100%;" id="tabel721">
								<thead>
									<tr>
										<th>Sumber Pembiayaan</th>
										<th>TS-2</th>
										<th>TS-1</th>
										<th>TS</th>
										<th>Jumlah</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="width:45%;">(1)</td>
										<td>(2)</td>
										<td>(3)</td>
										<td>(4)</td>
										<td style="width:15%;">(5)</td>
									</tr>

									<?php  
									$namarow[0]="Pembiayaan sendiri oleh dosen";
									$namarow[1]="PT yang bersangkutan";
									$namarow[2]="Depdiknas";
									$namarow[3]="Institusi dalam negeri di luar Depdiknas";
									$namarow[4]="Institusi luar negeri";

									$index = 0;
									for ($row=1; $row <= 5; $row++) {
										echo "<tr>";

											//nama TS
										echo "<td>".$namarow[$row-1]."</td>";

											//kolom input
										for ($column=2; $column <= 4; $column++) { 
											echo "<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='7_2_1/".$column."/no".$row."' onchange='updateNilai21(".$row.")' value='".$tabledata[$index]."'></td>";
											$index++;
										}

										echo "<td id='7.2.1/no".$row."'></td>";
										echo "</tr>";
									}
									?>
								</tbody>
							</table>
						</div>
					</div>


					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="7">7.2.1</td>
									<td colspan="2">Jumlah kegiatan pelayanan/pengabdian kepada masyarakat (PkM) yang dilakukan oleh dosen tetap yang bidang keahliannya sama dengan PS selama tiga tahun.</td>
								</tr>
								<tr>
									<td>Jumlah kegiatan PkM dengan biaya LN</td>
									<td id="nilai/7.2.1/1"></td>
								</tr>
								<tr>
									<td>Jumlah kegiatan PkM dengan biaya luar (PT)</td>
									<td id="nilai/7.2.1/2"></td>
								</tr>
								<tr>
									<td>Jumlah kegiatan PkM dengan biaya PT/sendiri</td>
									<td id="nilai/7.2.1/3"></td>
								</tr>
								<tr>
									<td>Jumlah dosen tetap dengan bidang sesuai PS</td>
									<td id="nilai/7.2.1/4"><?php echo $countDosenPS ?></td>
								</tr>
								<tr>
									<td>Nilai Kasar (NK)</td>
									<td id="nilai/7.2.1/5"></td>
								</tr>
								<tr>
									<td>Nilai</td>
									<td id="nilai/7.2.1/nilai"></td>
									<input type="hidden" id="nilai/7.2.1/nilaihid" name="nilai/7_2_1">
								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">
						<?php
							//data retrieve for table
						$butir = "7.2.2";
						$tabledata = array_fill(0, 2, '');
						$query="SELECT isi_char,isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								if ($index==0) {
									$tabledata[$index] = $row['isi_text'];
								}else{
									$tabledata[$index] = $row['isi_char'];
								}
								$index++;
							}
						}
						?>
						<label class="col-sm-2 text-right">7.2.2</label> 
						<div class="col-sm-8">Adakah mahasiswa yang dilibatkan dalam kegiatan pelayanan/pengabdian kepada masyarakat dalam tiga tahun terakhir?</div>
					</div>

					<div class="form-group"> 

						<div class="radio col-sm-8 col-sm-offset-2">
							<label><input type="radio" name="7_2_2/radio" value="tidak" <?php if ($tabledata[1]=="tidak") {echo "checked";} ?>>Tidak</label>
						</div>
						<div class="col-sm-2"></div>
						<div class="radio col-sm-8 col-sm-offset-2">
							<label><input type="radio" name="7_2_2/radio" value="ya" <?php if ($tabledata[1]!="tidak") {echo "checked";} ?>>Ya</label>
						</div>
						<div class="col-sm-2"></div>
					</div>

					<div class="form-group"> 
						<p class="col-sm-8 col-sm-offset-2">Jika Ya, jelaskan tingkat partisipasi dan bentuk keterlibatan mahasiswa dalam kegiatan pelayanan/pengabdian kepada masyarakat.
						</p>
						<div class="col-sm-2"></div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<textarea type="text" class="form-control" rows="20" name="7_2_2/penjelasan" placeholder="" maxlength="60000"><?php echo $tabledata[0]; ?></textarea>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center" colspan="2">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="7">7.2.2</td>
									<td colspan="3">Keterlibatan mahasiswa dalam kegiatan PkM.</td>
								</tr>
								<tr>
									<td>0</td>
									<td>Mahasiswa tidak dilibatkan dalam kegiatan PkM.</td>
									<td></td>
								</tr>
								<tr>
									<td>1</td>
									<td>Keterlibatan mahasiswa sangat kurang.</td>
									<td></td>
								</tr>
								<tr>
									<td>2</td>
									<td>Mahasiswa hanya diminta sebagai tenaga pembantu.</td>
									<td></td>
								</tr>
								<tr>
									<td>3</td>
									<td>Mahasiswa terlibat penuh, namun tanggung jawab ada pada dosen Pembina.</td>
									<td></td>
								</tr>
								<tr>
									<td>4</td>
									<td>Mahasiswa terlibat penuh dan diberi tanggung jawab.</td>
									<td></td>
								</tr>
								<tr>
									<td colspan="2">Nilai</td>
									<?php 
									//retrieve Nilai
									$butir = "7.2.2";
									$tabledata = array_fill(0, 1, '');
									$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY sub_butir";
									$data = mysqli_query($db, $query);
									$count = mysqli_num_rows($data);
									if ($count>0) {
										while ($row = mysqli_fetch_assoc($data)) {
											$tabledata[0] = $row['isi'];
										}
									}
									?>
									<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="nilai/7_2_2" value=<?php echo "'".$tabledata[0]."'";?>></td>
								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 text-right">7.3</label> 
						<label class="col-sm-8">Kegiatan Kerjasama dengan Instansi Lain</label>
					</div>

					<div class="form-group">
						<label class="col-sm-2 text-right">7.3.1</label> 
						<div class="col-sm-8">Instansi dalam negeri yang menjalin kerjasama yang terkait dengan PS dalam tiga tahun terakhir:</div>
					</div>

					<div class="form-group">

						<?php
						//data retrieve for table
						$butir = "7.3.1";
						$query="SELECT isi_float, isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								if ($index==0) {
									$tabledata[$index] = $row['isi_float'];
								}else{
									$tabledata[$index] = $row['isi_text'];
								}
								$index++;
							}
						}else{
							$tabledata[0] = 1;
							$tabledata[1] = "";
							$tabledata[2] = "";
							$tabledata[3] = "";
							$tabledata[4] = "";
							$tabledata[5] = "";

						}
						?>
						<div class="col-sm-8 col-sm-offset-2 table-wrapper">
							<?php 
							echo "<input type='hidden' id='7.3.1/counter' name='7_3_1/counter' value=".$tabledata[0].">";
							?>
							<table class="tableBorang" style="width:115%;" id="tabel731">
								<thead>
									<tr>
										<th rowspan="2">No.</th>
										<th rowspan="2">Nama Instansi</th>
										<th rowspan="2">Jenis Kegiatan</th>
										<th colspan="2">Kurun Waktu Kerja Sama</th>
										<th rowspan="2">Manfaat yang Telah Diperoleh</th>
									</tr>
									<tr>
										<th>Mulai</th>
										<th>Berakhir</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="width:4%;">(1)</td>
										<td style="width:17%;">(2)</td>
										<td>(3)</td>
										<td style="width:10%;">(4)</td>
										<td style="width:10%;">(5)</td>
										<td>(6)</td>
									</tr>
									<?php 
									$index = 1;
									for ($row=1; $row <= $tabledata[0]; $row++) { 
										?>
										<tr>
											<td><?php echo $row;?></td>
											<td><textarea rows="3" style="width:100%;" <?php echo "name='7_3_1/2/no".$row."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td><textarea rows="3" style="width:100%;" <?php echo "name='7_3_1/3/no".$row."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td><textarea rows="3" style="width:100%;" <?php echo "name='7_3_1/4/no".$row."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td><textarea rows="3" style="width:100%;" <?php echo "name='7_3_1/5/no".$row."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td><textarea rows="3" style="width:100%;" <?php echo "name='7_3_1/6/no".$row."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
										</tr>

										<?php
									}
									?>
									<tr>
										<td colspan="6"><button type="button" onclick="addRow31('tabel731','7.3.1/counter')"><span class="glyphicon glyphicon-plus"></span></button>
										<button type="button" onclick="removerow('tabel731','7.3.1/counter')"><span class="glyphicon glyphicon-minus"></span></button></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center" colspan="2">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="6">7.3.1</td>
									<td colspan="3">Kegiatan kerjasama dengan instansi di DN dalam tiga tahun terakhir.</td>
								</tr>
								<tr>
									<td>1</td>
									<td>Belum ada atau tidak ada kerjasama.</td>
									<td></td>
								</tr>
								<tr>
									<td>2</td>
									<td>Ada kerjasama dengan institusi di dalam negeri, kurang dalam jumlah. Sebagian besar relevan dengan bidang keahlian PS.</td>
									<td></td>
								</tr>
								<tr>
									<td>3</td>
									<td>Ada kerjasama dengan institusi di dalam negeri, cukup dalam jumlah.  Sebagian besar relevan dengan bidang keahlian PS</td>
									<td></td>
								</tr>
								<tr>
									<td>4</td>
									<td>Ada kerjasama dengan institusi di dalam negeri, banyak dalam jumlah.  Semuanya  relevan dengan bidang keahlian PS.</td>
									<td></td>
								</tr>
								<tr>
									<td colspan="2">Nilai</td>
									<?php 
									//retrieve Nilai
									$butir = "7.3.1";
									$tabledata = array_fill(0, 1, '');
									$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY sub_butir";
									$data = mysqli_query($db, $query);
									$count = mysqli_num_rows($data);
									if ($count>0) {
										while ($row = mysqli_fetch_assoc($data)) {
											$tabledata[0] = $row['isi'];
										}
									}
									?>
									<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="nilai/7_3_1" value=<?php echo "'".$tabledata[0]."'";?>></td>
								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 text-right">7.3.2</label> 
						<div class="col-sm-8">Instansi luar negeri yang menjalin kerjasama yang terkait dengan PS dalam tiga tahun terakhir.</div>
					</div>

					<div class="form-group">

						<?php
						//data retrieve for table
						$butir = "7.3.2";
						$query="SELECT isi_float, isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								if ($index==0) {
									$tabledata[$index] = $row['isi_float'];
								}else{
									$tabledata[$index] = $row['isi_text'];
								}
								$index++;
							}
						}else{
							$tabledata[0] = 1;
							$tabledata[1] = "";
							$tabledata[2] = "";
							$tabledata[3] = "";
							$tabledata[4] = "";
							$tabledata[5] = "";

						}
						?>
						<div class="col-sm-8 col-sm-offset-2 table-wrapper">
							<?php 
							echo "<input type='hidden' id='7.3.2/counter' name='7_3_2/counter' value=".$tabledata[0].">";
							?>
							<table class="tableBorang" style="width:115%;" id="tabel732">
								<thead>
									<tr>
										<th rowspan="2">No.</th>
										<th rowspan="2">Nama Instansi</th>
										<th rowspan="2">Jenis Kegiatan</th>
										<th colspan="2">Kurun Waktu Kerja Sama</th>
										<th rowspan="2">Manfaat yang Telah Diperoleh</th>
									</tr>
									<tr>
										<th>Mulai</th>
										<th>Berakhir</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="width:4%;">(1)</td>
										<td style="width:17%;">(2)</td>
										<td>(3)</td>
										<td style="width:10%;">(4)</td>
										<td style="width:10%;">(5)</td>
										<td>(6)</td>
									</tr>
									<?php 
									$index = 1;
									for ($row=1; $row <= $tabledata[0]; $row++) { 
										?>
										<tr>
											<td><?php echo $row;?></td>
											<td><textarea rows="3" style="width:100%;" <?php echo "name='7_3_2/2/no".$row."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td><textarea rows="3" style="width:100%;" <?php echo "name='7_3_2/3/no".$row."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td><textarea rows="3" style="width:100%;" <?php echo "name='7_3_2/4/no".$row."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td><textarea rows="3" style="width:100%;" <?php echo "name='7_3_2/5/no".$row."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td><textarea rows="3" style="width:100%;" <?php echo "name='7_3_2/6/no".$row."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
										</tr>

										<?php
									}
									?>
									<tr>
										<td colspan="6"><button type="button" onclick="addRow32('tabel732','7.3.2/counter')"><span class="glyphicon glyphicon-plus"></span></button>
										<button type="button" onclick="removerow('tabel732','7.3.2/counter')"><span class="glyphicon glyphicon-minus"></span></button></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center" colspan="2">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="6">7.3.2</td>
									<td colspan="3">Kegiatan kerjasama dengan instansi di LN dalam tiga tahun terakhir.</td>
								</tr>
								<tr>
									<td>1</td>
									<td>Belum ada atau tidak ada kerjasama.</td>
									<td></td>
								</tr>
								<tr>
									<td>2</td>
									<td>Ada kerjasama dengan institusi di luar negeri, kurang dalam jumlah.  Sebagian besar relevan dengan bidang keahlian PS. </td>
									<td></td>
								</tr>
								<tr>
									<td>3</td>
									<td>Ada kerjasama dengan institusi di luar negeri, cukup dalam jumlah.  Sebagian besar relevan dengan bidang keahlian PS</td>
									<td></td>
								</tr>
								<tr>
									<td>4</td>
									<td>Ada kerjasama dengan institusi di luar negeri, banyak dalam jumlah.  Semuanya  relevan dengan bidang keahlian PS.</td>
									<td></td>
								</tr>
								<tr>
									<td colspan="2">Nilai</td>
									<?php 
									//retrieve Nilai
									$butir = "7.3.2";
									$tabledata = array_fill(0, 1, '');
									$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY sub_butir";
									$data = mysqli_query($db, $query);
									$count = mysqli_num_rows($data);
									if ($count>0) {
										while ($row = mysqli_fetch_assoc($data)) {
											$tabledata[0] = $row['isi'];
										}
									}
									?>
									<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="nilai/7_3_2" value=<?php echo "'".$tabledata[0]."'";?>></td>
								</tr>
							</table>
						</div>
					</div>





























				</form>
			</div>
		</div>
	</div>
</body>

<script>
	$(document).ready(function() {
		$("form input, form select, form textarea").attr('disabled',true);
		$("button").hide();
		$("#editButton").show();
	});

	$("#editButton").click(function() {
		$("form input, form select, form textarea").attr('disabled',false);
		$("button").show();
		$("#editButton").hide();
	});

	$('form').submit(function() {
		$('body').hide();
		$("header").hide();
		$("html").prepend("<div class='ball'></div><div class='ball1'></div><h1 style='margin-left:43%'>MEMPROSES<h1>");
	});
</script>

</html>