<!doctype html>
<html>
<link rel="stylesheet" type="text/css" href="../css/loading.css">
<?php
require "../Database/DatabaseConnection.php";

$query='select idPeriode from periode where aktif = 1';
$data = mysqli_query($db, $query);
$row = mysqli_fetch_assoc($data);

$periode = $row["idPeriode"];
$username= $_COOKIE['LPMKa'];

$query="select idProdi from user where username='".$username."'";
$data = mysqli_query($db, $query);
$row = mysqli_fetch_assoc($data);
$prodi = $row["idProdi"];

$standar= 4;
$nol= 0;
$nullText = "";

//insert 4.1
$butir="4.1";
$isi = $_POST['4_1'];
$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$nol."','".$nol."','".$nullText."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi_text`='".$isi."'";
mysqli_query($db, $query);

//insert 4.2
$butir="4.2";
$isi = $_POST['4_2'];
$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$nol."','".$nol."','".$nullText."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi_text`='".$isi."'";
mysqli_query($db, $query);

//insert 4.3.3 ganjil
$butir="4.3.3.gan";

//counter
$count = $_POST['4_3_3_ganjil/counter'];
$kolId = "0";
$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolId."','".$nol."','".$nol."','".$nullText."','".$count."','".$nullText."')
ON DUPLICATE KEY UPDATE `isi_float`='".$count."'";
mysqli_query($db, $query);

for ($row=1; $row <=$count ; $row++) {
	for ($kol=2; $kol <=9 ; $kol++) { 
		$name = "4_3_3_ganjil/".$kol."/no".$row;
		if ($kol==2) {
			if (isset($_POST[$name])) {
				$isiVarchar=$_POST[$name];
				$isiFloat = $nol;
			}else{
				$isiVarchar="";
				$isiFloat = $nol;
			}
		}else{
			$isiVarchar=$nullText;
			$isiFloat = $_POST[$name];
		}

		$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
		VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kol."','".$row."','".$nol."','".$isiVarchar."','".$isiFloat."','".$nullText."')
		ON DUPLICATE KEY UPDATE `isi_char`='".$isiVarchar."',`isi_float`='".$isiFloat."'";
		mysqli_query($db, $query);
		
	}
}

//insert 4.3.3 genap
$butir="4.3.3.gen";

//counter
$count = $_POST['4_3_3_genap/counter'];
$kolId = "0";
$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolId."','".$nol."','".$nol."','".$nullText."','".$count."','".$nullText."')
ON DUPLICATE KEY UPDATE `isi_float`='".$count."'";
mysqli_query($db, $query);

for ($row=1; $row <=$count ; $row++) {
	for ($kol=2; $kol <=9 ; $kol++) { 
		$name = "4_3_3_genap/".$kol."/no".$row;
		if ($kol==2) {
			if (isset($_POST[$name])) {
				$isiVarchar=$_POST[$name];
				$isiFloat = $nol;
			}else{
				$isiVarchar="";
				$isiFloat = $nol;
			}
		}else{
			$isiVarchar=$nullText;
			$isiFloat = $_POST[$name];
		}

		$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
		VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kol."','".$row."','".$nol."','".$isiVarchar."','".$isiFloat."','".$nullText."')
		ON DUPLICATE KEY UPDATE `isi_char`='".$isiVarchar."',`isi_float`='".$isiFloat."'";
		mysqli_query($db, $query);
	}
}

//insert 4.3.4 ganjil
$butir="4.3.4.gan";
//hapus untuk diinsert yang baru
$query="DELETE FROM `isi_borang` WHERE `username`='".$username."' AND `idProdi`='".$prodi."' AND `idPeriode`='".$periode."' AND `butir`='".$butir."' ";
mysqli_query($db, $query);
//counter besar
$countB = $_POST['4_3_4_ganjil/counter'];
$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$nol."','".$nol."','".$nullText."','".$countB."','".$nullText."')
ON DUPLICATE KEY UPDATE `isi_float`='".$countB."'";
mysqli_query($db, $query);
//counter kecil
for ($no=1; $no <= $countB ; $no++) { 
	$count = $_POST['4_3_4_ganjil/counter/no'.$no];
	$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
	VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$no."','".$nol."','".$nullText."','".$count."','".$nullText."')
	ON DUPLICATE KEY UPDATE `isi_float`='".$count."'";
	mysqli_query($db, $query);

	//first tr
	for ($kolom=2; $kolom <=8 ; $kolom++) { 
		$sub = 1;
		$isi = $_POST['4_3_4_ganjil/'.$kolom.'/no'.$no.'/sub1'];
		$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
		VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolom."','".$no."','".$sub."','".$isi."','".$nol."','".$nullText."')
		ON DUPLICATE KEY UPDATE `isi_char`='".$isi."'";
		mysqli_query($db, $query);
	}
	//sub tr
	for ($kolom=4; $kolom <= 8; $kolom++) { 
		for ($sub=2; $sub <= $count; $sub++) { 
			$isi = $_POST['4_3_4_ganjil/'.$kolom.'/no'.$no.'/sub'.$sub];
			$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
			VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolom."','".$no."','".$sub."','".$isi."','".$nol."','".$nullText."')
			ON DUPLICATE KEY UPDATE `isi_char`='".$isi."'";
			mysqli_query($db, $query);
		}
	}
}

//insert 4.3.4 genap
$butir="4.3.4.gen";
//hapus untuk diinsert yang baru
$query="DELETE FROM `isi_borang` WHERE `username`='".$username."' AND `idProdi`='".$prodi."' AND `idPeriode`='".$periode."' AND `butir`='".$butir."' ";
mysqli_query($db, $query);
//counter besar
$countB = $_POST['4_3_4_genap/counter'];
$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$nol."','".$nol."','".$nullText."','".$countB."','".$nullText."')
ON DUPLICATE KEY UPDATE `isi_float`='".$countB."'";
mysqli_query($db, $query);
//counter kecil
for ($no=1; $no <= $countB ; $no++) { 
	$count = $_POST['4_3_4_genap/counter/no'.$no];
	$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
	VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$no."','".$nol."','".$nullText."','".$count."','".$nullText."')
	ON DUPLICATE KEY UPDATE `isi_float`='".$count."'";
	mysqli_query($db, $query);

	//first tr
	for ($kolom=2; $kolom <=8 ; $kolom++) { 
		$sub = 1;
		$isi = $_POST['4_3_4_genap/'.$kolom.'/no'.$no.'/sub1'];
		$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
		VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolom."','".$no."','".$sub."','".$isi."','".$nol."','".$nullText."')
		ON DUPLICATE KEY UPDATE `isi_char`='".$isi."'";
		mysqli_query($db, $query);
	}
	//sub tr
	for ($kolom=4; $kolom <= 8; $kolom++) { 
		for ($sub=2; $sub <= $count; $sub++) { 
			$isi = $_POST['4_3_4_genap/'.$kolom.'/no'.$no.'/sub'.$sub];
			$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
			VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolom."','".$no."','".$sub."','".$isi."','".$nol."','".$nullText."')
			ON DUPLICATE KEY UPDATE `isi_char`='".$isi."'";
			mysqli_query($db, $query);
		}
	}
}


//insert 4.3.5 ganjil
$butir="4.3.5.gan";
//hapus untuk diinsert yang baru
$query="DELETE FROM `isi_borang` WHERE `username`='".$username."' AND `idProdi`='".$prodi."' AND `idPeriode`='".$periode."' AND `butir`='".$butir."' ";
mysqli_query($db, $query);
//counter besar
$countB = $_POST['4_3_5_ganjil/counter'];
$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$nol."','".$nol."','".$nullText."','".$countB."','".$nullText."')
ON DUPLICATE KEY UPDATE `isi_float`='".$countB."'";
mysqli_query($db, $query);
//counter kecil
for ($no=1; $no <= $countB ; $no++) { 
	$count = $_POST['4_3_5_ganjil/counter/no'.$no];
	$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
	VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$no."','".$nol."','".$nullText."','".$count."','".$nullText."')
	ON DUPLICATE KEY UPDATE `isi_float`='".$count."'";
	mysqli_query($db, $query);

	//first tr
	for ($kolom=2; $kolom <=8 ; $kolom++) { 
		$sub = 1;
		$isi = $_POST['4_3_5_ganjil/'.$kolom.'/no'.$no.'/sub1'];
		$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
		VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolom."','".$no."','".$sub."','".$isi."','".$nol."','".$nullText."')
		ON DUPLICATE KEY UPDATE `isi_char`='".$isi."'";
		mysqli_query($db, $query);
	}
	//sub tr
	for ($kolom=4; $kolom <= 8; $kolom++) { 
		for ($sub=2; $sub <= $count; $sub++) { 
			$isi = $_POST['4_3_5_ganjil/'.$kolom.'/no'.$no.'/sub'.$sub];
			$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
			VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolom."','".$no."','".$sub."','".$isi."','".$nol."','".$nullText."')
			ON DUPLICATE KEY UPDATE `isi_char`='".$isi."'";
			mysqli_query($db, $query);
		}
	}
}

//insert 4.3.5 genap
$butir="4.3.5.gen";
//hapus untuk diinsert yang baru
$query="DELETE FROM `isi_borang` WHERE `username`='".$username."' AND `idProdi`='".$prodi."' AND `idPeriode`='".$periode."' AND `butir`='".$butir."' ";
mysqli_query($db, $query);
//counter besar
$countB = $_POST['4_3_5_genap/counter'];
$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$nol."','".$nol."','".$nullText."','".$countB."','".$nullText."')
ON DUPLICATE KEY UPDATE `isi_float`='".$countB."'";
mysqli_query($db, $query);
//counter kecil
for ($no=1; $no <= $countB ; $no++) { 
	$count = $_POST['4_3_5_genap/counter/no'.$no];
	$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
	VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$no."','".$nol."','".$nullText."','".$count."','".$nullText."')
	ON DUPLICATE KEY UPDATE `isi_float`='".$count."'";
	mysqli_query($db, $query);

	//first tr
	for ($kolom=2; $kolom <=8 ; $kolom++) { 
		$sub = 1;
		$isi = $_POST['4_3_5_genap/'.$kolom.'/no'.$no.'/sub1'];
		$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
		VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolom."','".$no."','".$sub."','".$isi."','".$nol."','".$nullText."')
		ON DUPLICATE KEY UPDATE `isi_char`='".$isi."'";
		mysqli_query($db, $query);
	}
	//sub tr
	for ($kolom=4; $kolom <= 8; $kolom++) { 
		for ($sub=2; $sub <= $count; $sub++) { 
			$isi = $_POST['4_3_5_genap/'.$kolom.'/no'.$no.'/sub'.$sub];
			$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
			VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolom."','".$no."','".$sub."','".$isi."','".$nol."','".$nullText."')
			ON DUPLICATE KEY UPDATE `isi_char`='".$isi."'";
			mysqli_query($db, $query);
		}
	}
}


//insert 4.4.2 ganjil
$butir="4.4.2.gan";
//hapus untuk diinsert yang baru
$query="DELETE FROM `isi_borang` WHERE `username`='".$username."' AND `idProdi`='".$prodi."' AND `idPeriode`='".$periode."' AND `butir`='".$butir."' ";
mysqli_query($db, $query);
//counter besar
$countB = $_POST['4_4_2_ganjil/counter'];
$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$nol."','".$nol."','".$nullText."','".$countB."','".$nullText."')
ON DUPLICATE KEY UPDATE `isi_float`='".$countB."'";
mysqli_query($db, $query);
//counter kecil
for ($no=1; $no <= $countB ; $no++) { 
	$count = $_POST['4_4_2_ganjil/counter/no'.$no];
	$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
	VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$no."','".$nol."','".$nullText."','".$count."','".$nullText."')
	ON DUPLICATE KEY UPDATE `isi_float`='".$count."'";
	mysqli_query($db, $query);

	//first tr
	for ($kolom=2; $kolom <=8 ; $kolom++) { 
		$sub = 1;
		$isi = $_POST['4_4_2_ganjil/'.$kolom.'/no'.$no.'/sub1'];
		$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
		VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolom."','".$no."','".$sub."','".$isi."','".$nol."','".$nullText."')
		ON DUPLICATE KEY UPDATE `isi_char`='".$isi."'";
		mysqli_query($db, $query);
	}
	//sub tr
	for ($kolom=4; $kolom <= 8; $kolom++) { 
		for ($sub=2; $sub <= $count; $sub++) { 
			$isi = $_POST['4_4_2_ganjil/'.$kolom.'/no'.$no.'/sub'.$sub];
			$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
			VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolom."','".$no."','".$sub."','".$isi."','".$nol."','".$nullText."')
			ON DUPLICATE KEY UPDATE `isi_char`='".$isi."'";
			mysqli_query($db, $query);
		}
	}
}

//insert 4.4.2 genap
$butir="4.4.2.gen";
//hapus untuk diinsert yang baru
$query="DELETE FROM `isi_borang` WHERE `username`='".$username."' AND `idProdi`='".$prodi."' AND `idPeriode`='".$periode."' AND `butir`='".$butir."' ";
mysqli_query($db, $query);
//counter besar
$countB = $_POST['4_4_2_genap/counter'];
$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$nol."','".$nol."','".$nullText."','".$countB."','".$nullText."')
ON DUPLICATE KEY UPDATE `isi_float`='".$countB."'";
mysqli_query($db, $query);
//counter kecil
for ($no=1; $no <= $countB ; $no++) { 
	$count = $_POST['4_4_2_genap/counter/no'.$no];
	$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
	VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$no."','".$nol."','".$nullText."','".$count."','".$nullText."')
	ON DUPLICATE KEY UPDATE `isi_float`='".$count."'";
	mysqli_query($db, $query);

	//first tr
	for ($kolom=2; $kolom <=8 ; $kolom++) { 
		$sub = 1;
		$isi = $_POST['4_4_2_genap/'.$kolom.'/no'.$no.'/sub1'];
		$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
		VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolom."','".$no."','".$sub."','".$isi."','".$nol."','".$nullText."')
		ON DUPLICATE KEY UPDATE `isi_char`='".$isi."'";
		mysqli_query($db, $query);
	}
	//sub tr
	for ($kolom=4; $kolom <= 8; $kolom++) { 
		for ($sub=2; $sub <= $count; $sub++) { 
			$isi = $_POST['4_4_2_genap/'.$kolom.'/no'.$no.'/sub'.$sub];
			$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
			VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolom."','".$no."','".$sub."','".$isi."','".$nol."','".$nullText."')
			ON DUPLICATE KEY UPDATE `isi_char`='".$isi."'";
			mysqli_query($db, $query);
		}
	}
}

//insert 4.5.1
$butir="4.5.1";

//counter
$count = $_POST['4_5_1/counter'];
$kolId = "0";
$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolId."','".$nol."','".$nol."','".$nullText."','".$count."','".$nullText."')
ON DUPLICATE KEY UPDATE `isi_float`='".$count."'";
mysqli_query($db, $query);

for ($row=1; $row <=$count ; $row++) {
	for ($kol=2; $kol <=4 ; $kol++) { 
		$name = "4_5_1/".$kol."/no".$row;
		$isiVarchar=$_POST[$name];

		$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
		VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kol."','".$row."','".$nol."','".$isiVarchar."','".$nol."','".$nullText."')
		ON DUPLICATE KEY UPDATE `isi_char`='".$isiVarchar."'";
		mysqli_query($db, $query);
		
	}
}

//insert 4.5.2
$butir="4.5.2";

//counter
$count = $_POST['4_5_2/counter'];
$kolId = "0";
$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolId."','".$nol."','".$nol."','".$nullText."','".$count."','".$nullText."')
ON DUPLICATE KEY UPDATE `isi_float`='".$count."'";
mysqli_query($db, $query);

for ($row=1; $row <=$count ; $row++) {
	for ($kol=2; $kol <=7 ; $kol++) { 
		$name = "4_5_2/".$kol."/no".$row;
		$isiVarchar=$_POST[$name];

		$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
		VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kol."','".$row."','".$nol."','".$isiVarchar."','".$nol."','".$nullText."')
		ON DUPLICATE KEY UPDATE `isi_char`='".$isiVarchar."'";
		mysqli_query($db, $query);
		
	}
}

//insert 4.5.3
$butir="4.5.3";
//hapus untuk diinsert yang baru
$query="DELETE FROM `isi_borang` WHERE `username`='".$username."' AND `idProdi`='".$prodi."' AND `idPeriode`='".$periode."' AND `butir`='".$butir."' ";
mysqli_query($db, $query);
//counter besar
$countB = $_POST['4_5_3/counter'];
$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$nol."','".$nol."','".$nullText."','".$countB."','".$nullText."')
ON DUPLICATE KEY UPDATE `isi_float`='".$countB."'";
mysqli_query($db, $query);
//counter kecil
for ($no=1; $no <= $countB ; $no++) { 
	$count = $_POST['4_5_3/counter/no'.$no];
	$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
	VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$no."','".$nol."','".$nullText."','".$count."','".$nullText."')
	ON DUPLICATE KEY UPDATE `isi_float`='".$count."'";
	mysqli_query($db, $query);

	//first tr
	for ($kolom=2; $kolom <=7 ; $kolom++) { 
		$sub = 1;
		if (isset($_POST['4_5_3/'.$kolom.'/no'.$no.'/sub1'])) {
			$isi = $_POST['4_5_3/'.$kolom.'/no'.$no.'/sub1'];
		}else{
			$isi = 0;
		}
		
		$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
		VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolom."','".$no."','".$sub."','".$isi."','".$nol."','".$nullText."')
		ON DUPLICATE KEY UPDATE `isi_char`='".$isi."'";
		mysqli_query($db, $query);
	}
	//sub tr
	for ($kolom=3; $kolom <= 7; $kolom++) { 
		for ($sub=2; $sub <= $count; $sub++) { 
			if (isset($_POST['4_5_3/'.$kolom.'/no'.$no.'/sub'.$sub])) {
				$isi = $_POST['4_5_3/'.$kolom.'/no'.$no.'/sub'.$sub];
			}else{
				$isi = 0;
			}
			$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
			VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolom."','".$no."','".$sub."','".$isi."','".$nol."','".$nullText."')
			ON DUPLICATE KEY UPDATE `isi_char`='".$isi."'";
			mysqli_query($db, $query);
		}
	}
}

//insert 4.5.4
$butir="4.5.4";
//hapus untuk diinsert yang baru
$query="DELETE FROM `isi_borang` WHERE `username`='".$username."' AND `idProdi`='".$prodi."' AND `idPeriode`='".$periode."' AND `butir`='".$butir."' ";
mysqli_query($db, $query);
//counter besar
$countB = $_POST['4_5_4/counter'];
$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$nol."','".$nol."','".$nullText."','".$countB."','".$nullText."')
ON DUPLICATE KEY UPDATE `isi_float`='".$countB."'";
mysqli_query($db, $query);
//counter kecil
for ($no=1; $no <= $countB ; $no++) { 
	$count = $_POST['4_5_4/counter/no'.$no];
	$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
	VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$no."','".$nol."','".$nullText."','".$count."','".$nullText."')
	ON DUPLICATE KEY UPDATE `isi_float`='".$count."'";
	mysqli_query($db, $query);

	//first tr
	for ($kolom=2; $kolom <=5 ; $kolom++) { 
		$sub = 1;
		if (isset($_POST['4_5_4/'.$kolom.'/no'.$no.'/sub1'])) {
			$isi = $_POST['4_5_4/'.$kolom.'/no'.$no.'/sub1'];
		}else{
			$isi = 0;
		}
		
		$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
		VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolom."','".$no."','".$sub."','".$isi."','".$nol."','".$nullText."')
		ON DUPLICATE KEY UPDATE `isi_char`='".$isi."'";
		mysqli_query($db, $query);
	}
	//sub tr
	for ($kolom=3; $kolom <= 5; $kolom++) { 
		for ($sub=2; $sub <= $count; $sub++) { 
			if (isset($_POST['4_5_4/'.$kolom.'/no'.$no.'/sub'.$sub])) {
				$isi = $_POST['4_5_4/'.$kolom.'/no'.$no.'/sub'.$sub];
			}else{
				$isi = 0;
			}
			$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
			VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolom."','".$no."','".$sub."','".$isi."','".$nol."','".$nullText."')
			ON DUPLICATE KEY UPDATE `isi_char`='".$isi."'";
			mysqli_query($db, $query);
		}
	}
}

//insert 4.5.5
$butir="4.5.5";
//hapus untuk diinsert yang baru
$query="DELETE FROM `isi_borang` WHERE `username`='".$username."' AND `idProdi`='".$prodi."' AND `idPeriode`='".$periode."' AND `butir`='".$butir."' ";
mysqli_query($db, $query);
//counter besar
$countB = $_POST['4_5_5/counter'];
$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$nol."','".$nol."','".$nullText."','".$countB."','".$nullText."')
ON DUPLICATE KEY UPDATE `isi_float`='".$countB."'";
mysqli_query($db, $query);
//counter kecil
for ($no=1; $no <= $countB ; $no++) { 
	$count = $_POST['4_5_5/counter/no'.$no];
	$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
	VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$no."','".$nol."','".$nullText."','".$count."','".$nullText."')
	ON DUPLICATE KEY UPDATE `isi_float`='".$count."'";
	mysqli_query($db, $query);

	//first tr
	for ($kolom=2; $kolom <=5 ; $kolom++) { 
		$sub = 1;
		if (isset($_POST['4_5_5/'.$kolom.'/no'.$no.'/sub1'])) {
			$isi = $_POST['4_5_5/'.$kolom.'/no'.$no.'/sub1'];
		}else{
			$isi = 0;
		}
		
		$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
		VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolom."','".$no."','".$sub."','".$isi."','".$nol."','".$nullText."')
		ON DUPLICATE KEY UPDATE `isi_char`='".$isi."'";
		mysqli_query($db, $query);
	}
	//sub tr
	for ($kolom=3; $kolom <= 5; $kolom++) { 
		for ($sub=2; $sub <= $count; $sub++) { 
			if (isset($_POST['4_5_5/'.$kolom.'/no'.$no.'/sub'.$sub])) {
				$isi = $_POST['4_5_5/'.$kolom.'/no'.$no.'/sub'.$sub];
			}else{
				$isi = 0;
			}
			$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
			VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolom."','".$no."','".$sub."','".$isi."','".$nol."','".$nullText."')
			ON DUPLICATE KEY UPDATE `isi_char`='".$isi."'";
			mysqli_query($db, $query);
		}
	}
}

//insert 4.6.1
$butir="4.6.1";

//counter
$count = $_POST['4_6_1/counter'];
$kolId = "0";
$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolId."','".$nol."','".$nol."','".$nullText."','".$count."','".$nullText."')
ON DUPLICATE KEY UPDATE `isi_float`='".$count."'";
mysqli_query($db, $query);

for ($row=1; $row <=3 ; $row++){
	for ($kol=3; $kol <=10 ; $kol++){
		$name = "4_6_1/".$kol."/no".$row;
		$isiVarchar=$_POST[$name];

		$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
		VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kol."','".$row."','".$nol."','".$isiVarchar."','".$nol."','".$nullText."')
		ON DUPLICATE KEY UPDATE `isi_char`='".$isiVarchar."'";
		mysqli_query($db, $query);
	}
}

for ($row=5; $row <=$count ; $row++) {
	for ($kol=2; $kol <=10 ; $kol++) { 
		$name = "4_6_1/".$kol."/no".$row;
		$isiVarchar=$_POST[$name];

		$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
		VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kol."','".$row."','".$nol."','".$isiVarchar."','".$nol."','".$nullText."')
		ON DUPLICATE KEY UPDATE `isi_char`='".$isiVarchar."'";
		mysqli_query($db, $query);
	}
}

//insert 4.6.2
$butir="4.6.2";
$isi = $_POST['4_6_2'];
$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$nol."','".$nol."','".$nullText."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi_text`='".$isi."'";
mysqli_query($db, $query);















//Form Penilaian

//insert 4.1
$butir="4.1";
$isi = $_POST['nilai/4_1'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 4.2.1
$butir="4.2.1";
$isi = $_POST['nilai/4_2_1'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);


//insert 4.2.2
$butir="4.2.2";
$isi = $_POST['nilai/4_2_2'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 4.3.1.a
$butir="4.3.1.a";
$isi = $_POST['nilai/4_3_1_a'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 4.3.1.b
$butir="4.3.1.b";
$isi = $_POST['nilai/4_3_1_b'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 4.3.1.c
$butir="4.3.1.c";
$isi = $_POST['nilai/4_3_1_c'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 4.3.1.d
$butir="4.3.1.d";
$subButir = "2";
$isi = $_POST['nilai/4_3_1_d/2'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$subButir."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);
$isi = $_POST['nilai/4_3_1_d'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 4.3.2
$butir="4.3.2";
$isi = $_POST['nilai/4_3_2'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 4.3.3
$butir="4.3.3";
$isi = $_POST['nilai/4_3_3'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 4.3.4/5.a
$butir="4.3.4/5.a";
$isi = $_POST['nilai/4_3_4/5_a'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 4.3.4/5.b
$butir="4.3.4/5.b";
$isi = $_POST['nilai/4_3_4/5_b'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 4.4.1
$butir="4.4.1";
$isi = $_POST['nilai/4_4_1'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 4.4.2.a
$butir="4.4.2.a";
$isi = $_POST['nilai/4_4_2_a'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 4.4.2.b
$butir="4.4.2.b";
$isi = $_POST['nilai/4_4_2_b'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);


//insert 4.5.1
$butir="4.5.1";
$isi = $_POST['nilai/4_5_1'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 4.5.2
$butir="4.5.2";
$isi = $_POST['nilai/4_5_2'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 4.5.3
$butir="4.5.3";
$isi = $_POST['nilai/4_5_3'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 4.5.4
$butir="4.5.4";
$isi = $_POST['nilai/4_5_4'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 4.5.5
$butir="4.5.5";
$isi = $_POST['nilai/4_5_5'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 4.6.1.a
$butir="4.6.1.a";
$isi = $_POST['nilai/4_6_1_a'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 4.6.1.b
$butir="4.6.1.b";
$isi = $_POST['nilai/4_6_1_b'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 4.6.1.c
$butir="4.6.1.c";
$isi = $_POST['nilai/4_6_1_c'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 4.6.2
$butir="4.6.2";
$isi = $_POST['nilai/4_6_2'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);




echo "<div class='ball'></div>";
echo "<div class='ball1'></div>";
echo "<h1 style='margin-left:43%'>MEMPROSES<h1>";
echo '<script>
window.location.href="Standar4.php";
</script>';
?>
</html>