<!doctype html>
<html>
<?php
require "../CookiesKaprodi.php";
require "../Database/DatabaseConnection.php";

$query='select idPeriode,NamaPeriode from periode where aktif = 1';
$data = mysqli_query($db, $query);
$row = mysqli_fetch_assoc($data);

$namaPeriode = $row['NamaPeriode'];
$periode = $row["idPeriode"];

$username= $_COOKIE['LPMKa'];

$query="select idProdi from user where username='".$username."'";
$data = mysqli_query($db, $query);
$row = mysqli_fetch_assoc($data);
$prodi = $row["idProdi"];

?>
<head>
	<title>Kaprodi</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/style.css" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>
	<div class='wrapper'>
		<header>
			<nav class="navbar navbar-default navbar-fixed-top">
				<div id="navbar" class="navbar-collapse collapse col-sm-offset-1">
					<ul class="nav navbar-nav">
						<li><a href="HomeKaprodi.php">Home</a></li>
						<li><a href="Standar1.php">Standar 1</a></li>
						<li><a href="Standar2.php">Standar 2</a></li>
						<li><a href="Standar3.php">Standar 3</a></li>
						<li><a href="Standar4.php">Standar 4</a></li>
						<li><a href="Standar5.php">Standar 5</a></li>
						<li><a href="Standar6.php">Standar 6</a></li>
						<li><a href="Standar7.php">Standar 7</a></li>
						<li class="active"><a href="Nilai.php">Nilai</a></li>
						<li><a href="Logout.php" class="col-sm-offset-9">Log Out</a></li>
					</ul>
				</div>
			</nav>
			<div class='text-center'>
				<br>
				<br>
				<h1>Hasil perhitungan simulasi</h1>
				<h5>Periode: <?php echo $namaPeriode ?></h5>
			</div>
		</header>
	</div>
	
	<div class = "container">
		<hr class="colorgraph"><br>
		<div class="row">
			<div class="form-horizontal">
				<?php
				$hasil = 0;
				for ($standar=1; $standar <=7 ; $standar++) {

					$query="SELECT butir,bobot,isi FROM isi_form_penilaian NATURAL JOIN bobot_butir WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND sub_butir='0' ORDER BY butir";
					$data = mysqli_query($db, $query);
					$count = mysqli_num_rows($data);
					if ($count>0) {
						//ambil username Auditor
						$query="select username from isi_borang where idProdi='$prodi' and idPeriode='$periode' and username!='$username' and standar='$standar' limit 1";
						$dataUser = mysqli_query($db, $query);
						$row = mysqli_fetch_assoc($dataUser);
						$countAudit = mysqli_num_rows($dataUser);
						$revisi = "Telah Revisi";
						if ($countAudit<1){
							$revisi = "Tanpa Revisi";

							?>
							<div class="form-group">
								<label class="col-sm-8 col-sm-offset-2">Standar <?php echo $standar; ?></label>
								<div class="col-sm-8 col-sm-offset-2"><?php echo $revisi; ?></div>
							</div>
							<div class="form-group" align="center">
								<div class="col-sm-8 col-sm-offset-2 table-fixed">
									<table class="tableHasil" style="width:100%;">
										<thead>
											<tr>
												<th>Butir</th>
												<th>Nilai</th>
												<th>Bobot</th>
												<th>Nilai*Bobot</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$totalNilai = 0;

											while ($row = mysqli_fetch_assoc($data)){

											//hitung total nilai per standar
												$nilaiakhir = $row['isi']*$row['bobot'];
												$totalNilai = $totalNilai+$nilaiakhir;

												echo "<tr>";

												echo "<td>".$row['butir']."</td>";
												if ($row['isi']<=1) {
													echo "<td class='badColor'>";
												}else if ($row['isi']<=2) {
													echo "<td class='dangerColor'>";
												}else if ($row['isi']<=3){
													echo "<td class='warningColor'>";
												}else{
													echo "<td class='goodColor'>";
												}
												echo $row['isi']."</td>";
												echo "<td>".$row['bobot']."</td>";
												echo "<td>".$nilaiakhir."</td>";
												echo "</tr>";
											}
											?>
											<tr>
												<td colspan="3">Jumlah</td>
												<td><?php echo $totalNilai; ?></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>

							<?php
						}else{
							$usernameAuditor = $row['username'];

							//data Kaprodi
							$query="SELECT butir,isi FROM isi_form_penilaian NATURAL JOIN bobot_butir WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND sub_butir='0' ORDER BY butir";
							$data = mysqli_query($db, $query);

							//data Kaprodi ke array
							unset($ArrayKaprodi);
							while ($row = mysqli_fetch_assoc($data)) {
								$ArrayKaprodi[] = $row;
							}

							//data auditor
							$query="SELECT butir,bobot,isi FROM isi_form_penilaian NATURAL JOIN bobot_butir WHERE username='".$usernameAuditor."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND sub_butir='0' ORDER BY butir";
							$data = mysqli_query($db, $query);



							?>
							<div class="form-group">
								<label class="col-sm-8 col-sm-offset-2">Standar <?php echo $standar; ?></label>
								<div class="col-sm-8 col-sm-offset-2"><?php echo $revisi; ?></div>
							</div>
							<div class="form-group" align="center">
								<div class="col-sm-8 col-sm-offset-2 table-fixed">
									<table class="tableHasil" style="width:100%;">
										<thead>
											<tr>
												<th>Butir</th>
												<th>Nilai Kaprodi</th>
												<th>Nilai Auditor</th>
												<th>Bobot</th>
												<th>Nilai Auditor*Bobot</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$totalNilai = 0;
											$indexKaprodi = 0;
											while ($row = mysqli_fetch_assoc($data)){

												//hitung total nilai per standar
												$nilaiakhir = $row['isi']*$row['bobot'];
												$totalNilai = $totalNilai+$nilaiakhir;

												echo "<tr>";

												echo "<td>".$row['butir']."</td>";
												
												if ($ArrayKaprodi[$indexKaprodi]['isi']<=1) {
													echo "<td class='badColor'>";
												}else if ($ArrayKaprodi[$indexKaprodi]['isi']<=2) {
													echo "<td class='dangerColor'>";
												}else if ($ArrayKaprodi[$indexKaprodi]['isi']<=3){
													echo "<td class='warningColor'>";
												}else{
													echo "<td class='goodColor'>";
												}
												echo $ArrayKaprodi[$indexKaprodi]['isi']."</td>";

												if ($row['isi']<=1) {
													echo "<td class='badColor'>";
												}else if ($row['isi']<=2) {
													echo "<td class='dangerColor'>";
												}else if ($row['isi']<=3){
													echo "<td class='warningColor'>";
												}else{
													echo "<td class='goodColor'>";
												}
												echo $row['isi']."</td>";
												echo "<td>".$row['bobot']."</td>";
												echo "<td>".$nilaiakhir."</td>";
												echo "</tr>";

												$indexKaprodi++;
											}
											?>
											<tr>
												<td colspan="4">Jumlah</td>
												<td><?php echo $totalNilai; ?></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>

							<?php










						}
					}else{
						$totalNilai = 0;
						?>
						<div class="form-group"> 
							<label class="col-sm-8 col-sm-offset-2">Standar <?php echo $standar; ?></label>
						</div>

						<div class="form-group"> 
							<div class="col-sm-8 col-sm-offset-2">Tidak ada data nilai</div>
						</div>
						<?php
					}
					//hitung total semua standar
					$hasil = $hasil+$totalNilai;
				}				
				?>
				<div class="form-group" style="margin-top:5%;margin-bottom:5%"> 
					<div class="col-sm-8 col-sm-offset-2 text-center">
						<label>Nilai Akhir</label>
						<h3><?php echo round($hasil,2);?> dari 400</h3>
					</div>
				</div>
				
				<div class="form-group" style="margin-top:5%;margin-bottom:5%"> 
					<div class="col-sm-8 col-sm-offset-2 text-center">
						<div>Keterangan Warna:</div>
						<div style="color:#27AE60"><b>Baik = 4 - 3</b></div>
						<div style="color:#F1C40F"><b>Cukup = 3 - 2</b></div>
						<div style="color:#D35400"><b>Kurang = 2 - 1</b></div>
						<div style="color:#C0392B"><b>Buruk = 1 - 0</b></div>
					</div>
				</div>

			</div>
		</div>
	</div>
</body>

</html>