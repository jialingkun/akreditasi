<!doctype html>
<html>
<?php
require "../CookiesKaprodi.php";
require "../Database/DatabaseConnection.php";

$query='select idPeriode,NamaPeriode from periode where aktif = 1';
$data = mysqli_query($db, $query);
$row = mysqli_fetch_assoc($data);

$namaPeriode = $row['NamaPeriode'];
$periode = $row["idPeriode"];
$username= $_COOKIE['LPMKa'];

$query="select idProdi from user where username='".$username."'";
$data = mysqli_query($db, $query);
$row = mysqli_fetch_assoc($data);
$prodi = $row["idProdi"];

$standar= 0;
?>
<head>
	<title>Kaprodi</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/style.css" rel="stylesheet">
	<link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="../css/loading.css">
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script src="../js/homeKaprodi.js"></script>
	<script src="../js/form.js"></script>
	<script src="../js/jquery.min.js"></script>
</head>

<body>
	<div class='wrapper'>
		<header>
			<nav class="navbar navbar-default navbar-fixed-top">
				<div id="navbar" class="navbar-collapse collapse col-sm-offset-1">
					<ul class="nav navbar-nav">
						<li class="active"><a href="HomeKaprodi.php">Home</a></li>
						<li><a href="Standar1.php">Standar 1</a></li>
						<li><a href="Standar2.php">Standar 2</a></li>
						<li><a href="Standar3.php">Standar 3</a></li>
						<li><a href="Standar4.php">Standar 4</a></li>
						<li><a href="Standar5.php">Standar 5</a></li>
						<li><a href="Standar6.php">Standar 6</a></li>
						<li><a href="Standar7.php">Standar 7</a></li>
						<li><a href="Nilai.php">Nilai</a></li>
						<li><a href="Logout.php" class="col-sm-offset-9">Log Out</a></li>
					</ul>
				</div>
			</nav>
			<div class='text-center'>
				<br>
				<br>
				<h1>IDENTITAS PROGRAM STUDI</h1>
				<h5>Periode: <?php echo $namaPeriode ?></h5>
			</div>
		</header>
	</div>
	
	<div class = "container">
		<hr class="colorgraph"><br>
		<div class="row">
			<div class="col-md-12">
				<form action="ProsesHomeKaprodi.php" method="post" class="form-horizontal">
					<div class="fixed-button">
						<button type="submit" class="btn btn-primary">Simpan</button>
						<button type="button" class="btn btn-success" id="editButton">Edit</button>
					</div>
					<div class="form-group">
						<?php
						$butir = "Form_1_1";
						$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
						$data = mysqli_query($db, $query);
						$row = mysqli_fetch_assoc($data);
						$print = $row["isi_char"];
						?>
						<label class="col-sm-4 text-right">Program Studi (PS) :</label> 
						<div class="col-sm-6"><input type='text' class="form-control" name="Form_1_1" value="<?php echo $print;?>"></div>
					</div>
					
					<div class="form-group">
						<?php
						$butir = "Form_1_2";
						$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
						$data = mysqli_query($db, $query);
						$row = mysqli_fetch_assoc($data);
						$print = $row["isi_char"];
						?>
						<label class="col-sm-4 text-right">Jurusan/Departemen :</label> 
						<div class="col-sm-6"><input type='text' class="form-control" name="Form_1_2" value="<?php echo $print;?>"></div>
					</div>
					
					<div class="form-group">
						<?php
						$butir = "Form_1_3";
						$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
						$data = mysqli_query($db, $query);
						$row = mysqli_fetch_assoc($data);
						$print = $row["isi_char"];
						?>
						<label class="col-sm-4 text-right">Fakultas :</label> 
						<div class="col-sm-6"><input type='text' class="form-control" name="Form_1_3" value="<?php echo $print;?>"></div>
					</div>
					
					<div class="form-group">
						<?php
						$butir = "Form_1_4";
						$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
						$data = mysqli_query($db, $query);
						$row = mysqli_fetch_assoc($data);
						$print = $row["isi_char"];
						?>
						<label class="col-sm-4 text-right">Perguruan Tinggi :</label> 
						<div class="col-sm-6"><input type='text' class="form-control" name="Form_1_4" value="<?php echo $print;?>"></div>
					</div>
					
					<div class="form-group">
						<?php
						$butir = "Form_1_5";
						$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
						$data = mysqli_query($db, $query);
						$row = mysqli_fetch_assoc($data);
						$print = $row["isi_char"];
						?>
						<label class="col-sm-4 text-right">Nomor SK pendirian PS (*) :</label> 
						<div class="col-sm-6"><input type='text' class="form-control" name="Form_1_5" value="<?php echo $print;?>"></div>
					</div>
					
					<div class="form-group">
						<?php
						$butir = "Form_1_6";
						$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
						$data = mysqli_query($db, $query);
						$row = mysqli_fetch_assoc($data);
						$print = $row["isi_char"];
						?>
						<label class="col-sm-4 text-right">Tanggal SK pendirian PS :</label> 
						<div class="col-sm-6"><input type='text' class="form-control" name="Form_1_6" value="<?php echo $print;?>"></div>
					</div>
					
					<div class="form-group">
						<?php
						$butir = "Form_1_7";
						$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
						$data = mysqli_query($db, $query);
						$row = mysqli_fetch_assoc($data);
						$print = $row["isi_char"];
						?>
						<label class="col-sm-4 text-right">Pejabat Penandatangan SK Pendirian PS :</label> 
						<div class="col-sm-6"><input type='text' class="form-control" name="Form_1_7" value="<?php echo $print;?>"></div>
					</div>
					
					<div class="form-group">
						<?php
						$butir = "Form_1_8";
						$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
						$data = mysqli_query($db, $query);
						$row = mysqli_fetch_assoc($data);
						$print = $row["isi_char"];
						?>
						<label class="col-sm-4 text-right">Bulan & Tahun DimulainyaPenyelenggaraan PS :</label> 
						<div class="col-sm-6"><input type='text' class="form-control" name="Form_1_8" value="<?php echo $print;?>"></div>
					</div>
					
					<div class="form-group">
						<?php
						$butir = "Form_1_9";
						$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
						$data = mysqli_query($db, $query);
						$row = mysqli_fetch_assoc($data);
						$print = $row["isi_char"];
						?>
						<label class="col-sm-4 text-right">Nomor SK Izin Operasional (*) :</label> 
						<div class="col-sm-6"><input type='text' class="form-control" name="Form_1_9" value="<?php echo $print;?>"></div>
					</div>
					
					<div class="form-group">
						<?php
						$butir = "Form_1_10";
						$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
						$data = mysqli_query($db, $query);
						$row = mysqli_fetch_assoc($data);
						$print = $row["isi_char"];
						?>
						<label class="col-sm-4 text-right">Tanggal SK Izin Operasional :</label> 
						<div class="col-sm-6"><input type='text' class="form-control" name="Form_1_10" value="<?php echo $print;?>"></div>
					</div>
					
					<div class="form-group">
						<?php
						$butir = "Form_1_11";
						$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
						$data = mysqli_query($db, $query);
						$row = mysqli_fetch_assoc($data);
						$print = $row["isi_char"];
						?>
						<label class="col-sm-4 text-right">Peringkat (Nilai) Akreditasi Terakhir :</label> 
						<div class="col-sm-6"><input type='text' class="form-control" name="Form_1_11" value="<?php echo $print;?>"></div>
					</div>
					
					<div class="form-group">
						<?php
						$butir = "Form_1_12";
						$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
						$data = mysqli_query($db, $query);
						$row = mysqli_fetch_assoc($data);
						$print = $row["isi_char"];
						?>
						<label class="col-sm-4 text-right">Nomor SK BAN-PT :</label> 
						<div class="col-sm-6"><input type='text' class="form-control" name="Form_1_12" value="<?php echo $print;?>"></div>
					</div>
					
					<div class="form-group">
						<?php
						$butir = "Form_1_13";
						$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
						$data = mysqli_query($db, $query);
						$row = mysqli_fetch_assoc($data);
						$print = $row["isi_text"];
						?>
						<label class="col-sm-4 text-right">Alamat PS :</label> 
						<div class="col-sm-6"><textarea type='text' class="form-control" name="Form_1_13" rows="2"><?php echo $print;?></textarea></div>
					</div>
					
					<div class="form-group">
						<?php
						$butir = "Form_1_14";
						$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
						$data = mysqli_query($db, $query);
						$row = mysqli_fetch_assoc($data);
						$print = $row["isi_float"];
						?>
						<label class="col-sm-4 text-right">No. Telepon PS :</label> 
						<div class="col-sm-6"><input type='text'  pattern="[0-9]+?" class="form-control" name="Form_1_14" value="<?php echo $print;?>"></div>
					</div>
					
					<div class="form-group">
						<?php
						$butir = "Form_1_15";
						$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
						$data = mysqli_query($db, $query);
						$row = mysqli_fetch_assoc($data);
						$print = $row["isi_float"];
						?>
						<label class="col-sm-4 text-right">No. Faksimili PS :</label> 
						<div class="col-sm-6"><input type='text'  pattern="[0-9]+?" class="form-control" name="Form_1_15" value="<?php echo $print;?>"></div>
					</div>
					
					<div class="form-group">
						<?php
						$butir = "Form_1_16";
						$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
						$data = mysqli_query($db, $query);
						$row = mysqli_fetch_assoc($data);
						$print1 = $row["isi_char"];
						
						$butir = "Form_1_17";
						$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
						$data = mysqli_query($db, $query);
						$row = mysqli_fetch_assoc($data);
						$print2 = $row["isi_char"];
						?>
						<label class="col-sm-4 text-right">Homepage Email PS :</label> 
						<div class="col-sm-3"><input type='text' class="form-control" name="Form_1_16" value="<?php echo $print1;?>"></div>
						<div class="col-sm-3"><input type='text' class="form-control" name="Form_1_17" value="<?php echo $print2;?>"></div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 text-right"></label> 
						<div class="col-sm-9"><p><b>Data dosen tetap yang bidang keahliannya sesuai dengan bidang PS:</b><br>
							(Harap diisi dahulu, data ini digunakan untuk standar 4 dan standar-standar berikutnya.)</p>
						</div>
					</div>

						<div class="form-group">
							<?php
						//data retrieve for table
							$butir = "dosen_ps";
						//retrieve counter
							$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom='0' ORDER BY no ASC;";
							$data = mysqli_query($db, $query);
							$count = mysqli_num_rows($data);
							if ($count>0){
								$index = 0;
								while ($row = mysqli_fetch_assoc($data)) {
									$counterdata[$index] = $row['isi_float'];	
									$index++;
								}
							}else{
								$counterdata[0] = 1;
								$counterdata[1] = 1;
							}

						//retrieve data
							$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom!='0' ORDER BY no,sub_no,kolom ASC;";
							$data = mysqli_query($db, $query);
							$count = mysqli_num_rows($data);
							if ($count>0) {
								$index = 0;
								while ($row = mysqli_fetch_assoc($data)) {
									$tabledata[$index] = $row['isi_char'];
									$index++;
								}
							}else{
								$tabledata[0] = "";
								$tabledata[1] = "";
								$tabledata[2] = "";
								$tabledata[3] = "";
								$tabledata[4] = "";
								$tabledata[5] = "";
								$tabledata[6] = "";
								$tabledata[7] = "";
							}
							?>
							<div class="col-sm-offset-2 col-sm-9 table-wrapper">
								<input type="hidden" id="4.3.1/counter" name="4_3_1/counter" <?php echo "value='".$counterdata[0]."'";?>>
								<table class="tableBorang" id="table431" style="width:195%;">
									<thead>
										<tr>
											<th>No.</th>
											<th>Nama Dosen</th>
											<th>NIDN</th>
											<th>Tgl. Lahir</th>
											<th>Jabatatan Akademik</th>
											<th>Gelar Akademik</th>
											<th colspan="2">Pendidikan S1, S2, S3 dan Asal PT</th>
											<th>Bidang Keahlian Untuk Setiap Jenjang Pendidikan</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td style="width:3%;">(1)</td>
											<td style="width:16%;">(2)</td>
											<td style="width:9%;">(3)</td>
											<td style="width:9%;">(4)</td>
											<td style="width:12%;">(5)</td>
											<td style="width:6%;">(6)</td>
											<td style="width:6%;">(7)</td>
											<td style="width:23%;">(8)</td>
											<td>(9)</td>
										</tr>
									</tbody>

									<?php 
									$index = 0;
									for ($no=1; $no <= $counterdata[0] ; $no++) { 
										echo "<tbody>";
										echo "<input type='hidden' id='4.3.1/counter/no".$no."' value='".$counterdata[$no]."' name='4_3_1/counter/no".$no."'>";
									//first tr
										?>
										<tr>
											<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>><?php echo $no; ?></td>
											<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>><textarea rows='1' style='width:100%;' <?php echo "id='4.3.1/2/no".$no."' name='4_3_1/2/no".$no."/sub1'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>><input type='text' class='form-control' <?php echo "name='4_3_1/3/no".$no."/sub1'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
											<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>><input type='text' class='form-control' <?php echo "name='4_3_1/4/no".$no."/sub1'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
											<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>>
												<select class='form-control' <?php echo "name='4_3_1/5/no".$no."/sub1'"; ?>>
													<option <?php if ($tabledata[$index]=="-") {echo "selected";} ?> >-</option>
													<option <?php if ($tabledata[$index]=="Tenaga Pengajar") {echo "selected";} ?>>Tenaga Pengajar</option>
													<option <?php if ($tabledata[$index]=="Asisten Ahli") {echo "selected";} ?>>Asisten Ahli</option>
													<option <?php if ($tabledata[$index]=="Lektor") {echo "selected";} ?>>Lektor</option>
													<option <?php if ($tabledata[$index]=="Lektor Kepala") {echo "selected";} ?>>Lektor Kepala</option>
													<option <?php if ($tabledata[$index]=="Guru Besar") {echo "selected";} ?>>Guru Besar</option>
												</select> <?php $index++; ?>
											</td>
											<td><input type='text' class='form-control' <?php echo "name='4_3_1/6/no".$no."/sub1'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
											<td>
												<select class='form-control' <?php echo "name='4_3_1/7/no".$no."/sub1'"; ?>>
													<option <?php if ($tabledata[$index]=="-") {echo "selected";} ?>>-</option>
													<option <?php if ($tabledata[$index]=="S1") {echo "selected";} ?>>S1</option>
													<option <?php if ($tabledata[$index]=="S2") {echo "selected";} ?>>S2</option>
													<option <?php if ($tabledata[$index]=="S3") {echo "selected";} ?>>S3</option>
												</select> <?php $index++; ?>
											</td>
											<td><textarea rows='1' style='width:100%;' <?php echo "name='4_3_1/8/no".$no."/sub1'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td rowspan><textarea rows='1' style='width:100%;' <?php echo "name='4_3_1/9/no".$no."/sub1'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
										</tr>

										<?php
									//sub tr
										for ($sub=2; $sub <= $counterdata[$no]; $sub++) { 
											?>
											<tr>
												<td><input type='text' class='form-control' <?php echo "name='4_3_1/6/no".$no."/sub".$sub."'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
												<td>
													<select class='form-control' <?php echo "name='4_3_1/7/no".$no."/sub".$sub."'"; ?>>
														<option <?php if ($tabledata[$index]=="-") {echo "selected";} ?>>-</option>
														<option <?php if ($tabledata[$index]=="S1") {echo "selected";} ?>>S1</option>
														<option <?php if ($tabledata[$index]=="S2") {echo "selected";} ?>>S2</option>
														<option <?php if ($tabledata[$index]=="S3") {echo "selected";} ?>>S3</option>
													</select> <?php $index++; ?>
												</td>
												<td><textarea rows='1' style='width:100%;' <?php echo "name='4_3_1/8/no".$no."/sub".$sub."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
												<td rowspan><textarea rows='1' style='width:100%;' <?php echo "name='4_3_1/9/no".$no."/sub".$sub."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											</tr>
											<?php
										}

									//add row button
										?>
										<tr>
											<td colspan='4'><button type='button' <?php echo "onclick='addrowtbody31(\"table431\",".$no.",\"4.3.1/counter/no".$no."\")'";?>><span class="glyphicon glyphicon-plus"></span></button>
											<button type='button' <?php echo "onclick='removerowtbody(\"table431\",".$no.",\"4.3.1/counter/no".$no."\",5)'";?>><span class="glyphicon glyphicon-minus"></span></button></td>
										</tr>
										<?php
										echo "</tbody>";
									}
									?>
									<tbody>
										<tr>
											<td colspan="9"><button type="button" onclick="addtbody31('table431','4.3.1/counter')"><span class="glyphicon glyphicon-plus"></span></button>
											<button type="button" onclick="removetbody('table431','4.3.1/counter')"><span class="glyphicon glyphicon-minus"></span></button></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 text-right"></label> 
							<div class="col-sm-9"><p><b>Data dosen tetap yang bidang keahliannya di luar bidang PS:</b></p></div>
						</div>

						<div class="form-group">
							<?php
						//data retrieve for table
							$butir = "dosen_xps";
						//retrieve counter
							$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom='0' ORDER BY no ASC;";
							$data = mysqli_query($db, $query);
							$count = mysqli_num_rows($data);
							if ($count>0){
								$index = 0;
								while ($row = mysqli_fetch_assoc($data)) {
									$counterdata[$index] = $row['isi_float'];	
									$index++;
								}
							}else{
								$counterdata[0] = 1;
								$counterdata[1] = 1;
							}

						//retrieve data
							$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom!='0' ORDER BY no,sub_no,kolom ASC;";
							$data = mysqli_query($db, $query);
							$count = mysqli_num_rows($data);
							if ($count>0) {
								$index = 0;
								while ($row = mysqli_fetch_assoc($data)) {
									$tabledata[$index] = $row['isi_char'];
									$index++;
								}
							}else{
								$tabledata[0] = "";
								$tabledata[1] = "";
								$tabledata[2] = "";
								$tabledata[3] = "";
								$tabledata[4] = "";
								$tabledata[5] = "";
								$tabledata[6] = "";
								$tabledata[7] = "";
							}
							?>
							<div class="col-sm-offset-2 col-sm-9 table-wrapper">
								<input type="hidden" id="4.3.2/counter" name="4_3_2/counter" <?php echo "value='".$counterdata[0]."'";?>>
								<table class="tableBorang" id="table432" style="width:195%;">
									<thead>
										<tr>
											<th>No.</th>
											<th>Nama Dosen</th>
											<th>NIDN</th>
											<th>Tgl. Lahir</th>
											<th>Jabatatan Akademik</th>
											<th>Gelar Akademik</th>
											<th colspan="2">Pendidikan S1, S2, S3 dan Asal PT</th>
											<th>Bidang Keahlian Untuk Setiap Jenjang Pendidikan</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td style="width:3%;">(1)</td>
											<td style="width:16%;">(2)</td>
											<td style="width:9%;">(3)</td>
											<td style="width:9%;">(4)</td>
											<td style="width:12%;">(5)</td>
											<td style="width:6%;">(6)</td>
											<td style="width:6%;">(7)</td>
											<td style="width:23%;">(8)</td>
											<td>(9)</td>
										</tr>
									</tbody>

									<?php 
									$index = 0;
									for ($no=1; $no <= $counterdata[0] ; $no++) { 
										echo "<tbody>";
										echo "<input type='hidden' id='4.3.2/counter/no".$no."' value='".$counterdata[$no]."' name='4_3_2/counter/no".$no."'>";
									//first tr
										?>
										<tr>
											<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>><?php echo $no; ?></td>
											<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>><textarea rows='1' style='width:100%;' <?php echo "name='4_3_2/2/no".$no."/sub1'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>><input type='text' class='form-control' <?php echo "name='4_3_2/3/no".$no."/sub1'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
											<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>><input type='text' class='form-control' <?php echo "name='4_3_2/4/no".$no."/sub1'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
											<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>>
												<select class='form-control' <?php echo "name='4_3_2/5/no".$no."/sub1'"; ?>>
													<option <?php if ($tabledata[$index]=="-") {echo "selected";} ?> >-</option>
													<option <?php if ($tabledata[$index]=="Tenaga Pengajar") {echo "selected";} ?>>Tenaga Pengajar</option>
													<option <?php if ($tabledata[$index]=="Asisten Ahli") {echo "selected";} ?>>Asisten Ahli</option>
													<option <?php if ($tabledata[$index]=="Lektor") {echo "selected";} ?>>Lektor</option>
													<option <?php if ($tabledata[$index]=="Lektor Kepala") {echo "selected";} ?>>Lektor Kepala</option>
													<option <?php if ($tabledata[$index]=="Guru Besar") {echo "selected";} ?>>Guru Besar</option>
												</select> <?php $index++; ?>
											</td>
											<td><input type='text' class='form-control' <?php echo "name='4_3_2/6/no".$no."/sub1'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
											<td>
												<select class='form-control' <?php echo "name='4_3_2/7/no".$no."/sub1'"; ?>>
													<option <?php if ($tabledata[$index]=="-") {echo "selected";} ?>>-</option>
													<option <?php if ($tabledata[$index]=="S1") {echo "selected";} ?>>S1</option>
													<option <?php if ($tabledata[$index]=="S2") {echo "selected";} ?>>S2</option>
													<option <?php if ($tabledata[$index]=="S3") {echo "selected";} ?>>S3</option>
												</select> <?php $index++; ?>
											</td>
											<td><textarea rows='1' style='width:100%;' <?php echo "name='4_3_2/8/no".$no."/sub1'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td rowspan><textarea rows='1' style='width:100%;' <?php echo "name='4_3_2/9/no".$no."/sub1'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
										</tr>

										<?php
									//sub tr
										for ($sub=2; $sub <= $counterdata[$no]; $sub++) { 
											?>
											<tr>
												<td><input type='text' class='form-control' <?php echo "name='4_3_2/6/no".$no."/sub".$sub."'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
												<td>
													<select class='form-control' <?php echo "name='4_3_2/7/no".$no."/sub".$sub."'"; ?>>
														<option <?php if ($tabledata[$index]=="-") {echo "selected";} ?>>-</option>
														<option <?php if ($tabledata[$index]=="S1") {echo "selected";} ?>>S1</option>
														<option <?php if ($tabledata[$index]=="S2") {echo "selected";} ?>>S2</option>
														<option <?php if ($tabledata[$index]=="S3") {echo "selected";} ?>>S3</option>
													</select> <?php $index++; ?>
												</td>
												<td><textarea rows='1' style='width:100%;' <?php echo "name='4_3_2/8/no".$no."/sub".$sub."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
												<td rowspan><textarea rows='1' style='width:100%;' <?php echo "name='4_3_2/9/no".$no."/sub".$sub."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											</tr>
											<?php
										}

									//add row button
										?>
										<tr>
											<td colspan='4'><button type='button' <?php echo "onclick='addrowtbody32(\"table432\",".$no.",\"4.3.2/counter/no".$no."\")'";?>><span class="glyphicon glyphicon-plus"></span></button>
											<button type='button' <?php echo "onclick='removerowtbody(\"table432\",".$no.",\"4.3.2/counter/no".$no."\",5)'";?>><span class="glyphicon glyphicon-minus"></span></button></td>
										</tr>
										<?php
										echo "</tbody>";
									}
									?>
									<tbody>
										<tr>
											<td colspan="9"><button type="button" onclick="addtbody32('table432','4.3.2/counter')"><span class="glyphicon glyphicon-plus"></span></button>
											<button type="button" onclick="removetbody('table432','4.3.2/counter')"><span class="glyphicon glyphicon-minus"></span></button></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 text-right"></label> 
							<div class="col-sm-9"><p><b>Data dosen tidak tetap pada PS:</b></p></div>
						</div>

						<div class="form-group">
							<?php
						//data retrieve for table
							$butir = "dosen_xtp";
						//retrieve counter
							$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom='0' ORDER BY no ASC;";
							$data = mysqli_query($db, $query);
							$count = mysqli_num_rows($data);
							if ($count>0){
								$index = 0;
								while ($row = mysqli_fetch_assoc($data)) {
									$counterdata[$index] = $row['isi_float'];	
									$index++;
								}
							}else{
								$counterdata[0] = 1;
								$counterdata[1] = 1;
							}

						//retrieve data
							$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom!='0' ORDER BY no,sub_no,kolom ASC;";
							$data = mysqli_query($db, $query);
							$count = mysqli_num_rows($data);
							if ($count>0) {
								$index = 0;
								while ($row = mysqli_fetch_assoc($data)) {
									$tabledata[$index] = $row['isi_char'];
									$index++;
								}
							}else{
								$tabledata[0] = "";
								$tabledata[1] = "";
								$tabledata[2] = "";
								$tabledata[3] = "";
								$tabledata[4] = "";
								$tabledata[5] = "";
								$tabledata[6] = "";
								$tabledata[7] = "";
							}
							?>
							<div class="col-sm-offset-2 col-sm-9 table-wrapper">
								<input type="hidden" id="4.4.1/counter" name="4_4_1/counter" <?php echo "value='".$counterdata[0]."'";?>>
								<table class="tableBorang" id="table441" style="width:195%;">
									<thead>
										<tr>
											<th>No.</th>
											<th>Nama Dosen</th>
											<th>NIDN</th>
											<th>Tgl. Lahir</th>
											<th>Jabatatan Akademik</th>
											<th>Gelar Akademik</th>
											<th colspan="2">Pendidikan S1, S2, S3 dan Asal PT</th>
											<th>Bidang Keahlian Untuk Setiap Jenjang Pendidikan</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td style="width:3%;">(1)</td>
											<td style="width:16%;">(2)</td>
											<td style="width:9%;">(3)</td>
											<td style="width:9%;">(4)</td>
											<td style="width:12%;">(5)</td>
											<td style="width:6%;">(6)</td>
											<td style="width:6%;">(7)</td>
											<td style="width:23%;">(8)</td>
											<td>(9)</td>
										</tr>
									</tbody>

									<?php 
									$index = 0;
									for ($no=1; $no <= $counterdata[0] ; $no++) { 
										echo "<tbody>";
										echo "<input type='hidden' id='4.4.1/counter/no".$no."' value='".$counterdata[$no]."' name='4_4_1/counter/no".$no."'>";
									//first tr
										?>
										<tr>
											<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>><?php echo $no; ?></td>
											<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>><textarea rows='1' style='width:100%;' <?php echo "name='4_4_1/2/no".$no."/sub1'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>><input type='text' class='form-control' <?php echo "name='4_4_1/3/no".$no."/sub1'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
											<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>><input type='text' class='form-control' <?php echo "name='4_4_1/4/no".$no."/sub1'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
											<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>>
												<select class='form-control' <?php echo "name='4_4_1/5/no".$no."/sub1'"; ?>>
													<option <?php if ($tabledata[$index]=="-") {echo "selected";} ?> >-</option>
													<option <?php if ($tabledata[$index]=="Tenaga Pengajar") {echo "selected";} ?>>Tenaga Pengajar</option>
													<option <?php if ($tabledata[$index]=="Asisten Ahli") {echo "selected";} ?>>Asisten Ahli</option>
													<option <?php if ($tabledata[$index]=="Lektor") {echo "selected";} ?>>Lektor</option>
													<option <?php if ($tabledata[$index]=="Lektor Kepala") {echo "selected";} ?>>Lektor Kepala</option>
													<option <?php if ($tabledata[$index]=="Guru Besar") {echo "selected";} ?>>Guru Besar</option>
												</select> <?php $index++; ?>
											</td>
											<td><input type='text' class='form-control' <?php echo "name='4_4_1/6/no".$no."/sub1'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
											<td>
												<select class='form-control' <?php echo "name='4_4_1/7/no".$no."/sub1'"; ?>>
													<option <?php if ($tabledata[$index]=="-") {echo "selected";} ?>>-</option>
													<option <?php if ($tabledata[$index]=="S1") {echo "selected";} ?>>S1</option>
													<option <?php if ($tabledata[$index]=="S2") {echo "selected";} ?>>S2</option>
													<option <?php if ($tabledata[$index]=="S3") {echo "selected";} ?>>S3</option>
												</select> <?php $index++; ?>
											</td>
											<td><textarea rows='1' style='width:100%;' <?php echo "name='4_4_1/8/no".$no."/sub1'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td rowspan><textarea rows='1' style='width:100%;' <?php echo "name='4_4_1/9/no".$no."/sub1'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
										</tr>

										<?php
									//sub tr
										for ($sub=2; $sub <= $counterdata[$no]; $sub++) { 
											?>
											<tr>
												<td><input type='text' class='form-control' <?php echo "name='4_4_1/6/no".$no."/sub".$sub."'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
												<td>
													<select class='form-control' <?php echo "name='4_4_1/7/no".$no."/sub".$sub."'"; ?>>
														<option <?php if ($tabledata[$index]=="-") {echo "selected";} ?>>-</option>
														<option <?php if ($tabledata[$index]=="S1") {echo "selected";} ?>>S1</option>
														<option <?php if ($tabledata[$index]=="S2") {echo "selected";} ?>>S2</option>
														<option <?php if ($tabledata[$index]=="S3") {echo "selected";} ?>>S3</option>
													</select> <?php $index++; ?>
												</td>
												<td><textarea rows='1' style='width:100%;' <?php echo "name='4_4_1/8/no".$no."/sub".$sub."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
												<td rowspan><textarea rows='1' style='width:100%;' <?php echo "name='4_4_1/9/no".$no."/sub".$sub."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											</tr>
											<?php
										}

										//add row button
										?>
										<tr>
											<td colspan='4'><button type='button' <?php echo "onclick='addrowtbody41(\"table441\",".$no.",\"4.4.1/counter/no".$no."\")'";?>><span class="glyphicon glyphicon-plus"></span></button>
											<button type='button' <?php echo "onclick='removerowtbody(\"table441\",".$no.",\"4.4.1/counter/no".$no."\",5)'";?>><span class="glyphicon glyphicon-minus"></span></button></td>
										</tr>
										<?php
										echo "</tbody>";
									}
									?>
									<tbody>
										<tr>
											<td colspan="9"><button type="button" onclick="addtbody41('table441','4.4.1/counter')"><span class="glyphicon glyphicon-plus"></span></button>
											<button type="button" onclick="removetbody('table441','4.4.1/counter')"><span class="glyphicon glyphicon-minus"></span></button>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

					</form>
				</div>
			</div>
		</div>

	</body>

	<script>
		$(document).ready(function() {
			$("form input, form select, form textarea").attr('disabled',true);
			$("button").hide();
			$("#editButton").show();
		});

		$("#editButton").click(function() {
			$("form input, form select, form textarea").attr('disabled',false);
			$("button").show();
			$("#editButton").hide();
		});

		$('form').submit(function() {
			$('body').hide();
			$("header").hide();
			$("html").prepend("<div class='ball'></div><div class='ball1'></div><h1 style='margin-left:43%'>MEMPROSES<h1>");
		});
	</script>

	</html>