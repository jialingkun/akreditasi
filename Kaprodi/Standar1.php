<!doctype html>
<html>
<?php
require "../CookiesKaprodi.php";
require "../Database/DatabaseConnection.php";

$query='select idPeriode,NamaPeriode from periode where aktif = 1';
$data = mysqli_query($db, $query);
$row = mysqli_fetch_assoc($data);

$namaPeriode = $row['NamaPeriode'];
$periode = $row["idPeriode"];
$username= $_COOKIE['LPMKa'];

$query="select idProdi from user where username='".$username."'";
$data = mysqli_query($db, $query);
$row = mysqli_fetch_assoc($data);
$prodi = $row["idProdi"];

$standar= 1;
?>
<head>
	<title>Kaprodi</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/style.css" rel="stylesheet">
	<link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="../css/loading.css">
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script src="../js/jquery.min.js"></script>
</head>

<body>
	<div class='wrapper'>
		<header>
			<nav class="navbar navbar-default navbar-fixed-top">
				<div id="navbar" class="navbar-collapse collapse col-sm-offset-1">
					<ul class="nav navbar-nav">
						<li><a href="HomeKaprodi.php">Home</a></li>
						<li  class="active"><a href="Standar1.php">Standar 1</a></li>
						<li><a href="Standar2.php">Standar 2</a></li>
						<li><a href="Standar3.php">Standar 3</a></li>
						<li><a href="Standar4.php">Standar 4</a></li>
						<li><a href="Standar5.php">Standar 5</a></li>
						<li><a href="Standar6.php">Standar 6</a></li>
						<li><a href="Standar7.php">Standar 7</a></li>
						<li><a href="Nilai.php">Nilai</a></li>
						<li><a href="Logout.php" class="col-sm-offset-9">Log Out</a></li>
					</ul>
				</div>
			</nav>
			<div class='text-center'>
				<br>
				<br>
				<h1>STANDAR 1</h1>
				<h4>VISI, MISI, TUJUAN DAN SASARAN, SERTA STRATEGI PENCAPAIAN</h4>
				<h5>Periode: <?php echo $namaPeriode ?></h5>
			</div>
		</header>
	</div>
	
	<div class = "container">
		<hr class="colorgraph"><br>
		<div class="row">
			<div class="col-md-12">
				<form action="ProsesStandar1.php" method="post" class="form-horizontal">
					<div class="fixed-button">
						<button type="submit" class="btn btn-primary">Simpan</button>
						<button type="button" class="btn btn-success" id="editButton">Edit</button>
					</div>
					<div class="form-group">
						<label class="col-sm-2 text-right">1.1</label> 
						<label class="col-sm-8">Visi, Misi, Tujuan, dan Sasaran serta Strategi Pencapaian</label>
					</div>
					<div class="form-group">
						<label class="col-sm-2 text-right">1.1.1</label> 
						<label class="col-sm-8">Mekanisme penyusunan visi, misi, tujuan dan sasaran Program Studi, serta pihak-pihak yang dilibatkan :</label>
					</div>
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "1.1.1";
							$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
							$data = mysqli_query($db, $query);
							$row = mysqli_fetch_assoc($data);
							$print = $row["isi_text"];
							?>
							<textarea type="text" class="form-control" rows="20" name="1_1_1" placeholder="" maxlength="60000"><?php echo $print;?></textarea>
						</label>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 text-right">1.1.2 </label> 
						<label class="col-sm-8">Visi :</label>
					</div>
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "1.1.2";
							$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
							$data = mysqli_query($db, $query);
							$row = mysqli_fetch_assoc($data);
							$print = $row["isi_text"];
							?>
							<textarea type="text" class="form-control" rows="10" name="1_1_2" placeholder="" maxlength="60000"><?php echo $print;?></textarea>
						</label>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 text-right">1.1.3 </label> 
						<label class="col-sm-8">Misi :</label>
					</div>
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "1.1.3";
							$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
							$data = mysqli_query($db, $query);
							$row = mysqli_fetch_assoc($data);
							$print = $row["isi_text"];
							?>
							<textarea type="text" class="form-control" rows="10" name="1_1_3" placeholder="" maxlength="60000"><?php echo $print;?></textarea>
						</label>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 text-right">1.1.4 </label> 
						<label class="col-sm-8">Tujuan :</label>
					</div>
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "1.1.4";
							$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
							$data = mysqli_query($db, $query);
							$row = mysqli_fetch_assoc($data);
							$print = $row["isi_text"];
							?>
							<textarea type="text" class="form-control" rows="15" name="1_1_4" placeholder="" maxlength="60000"><?php echo $print;?></textarea>
						</label>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 text-right">1.1.5 </label> 
						<label class="col-sm-8">Sasaran dan Strategi Pencapaian :</label>
					</div>
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "1.1.5";
							$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
							$data = mysqli_query($db, $query);
							$row = mysqli_fetch_assoc($data);
							$print = $row["isi_text"];
							?>
							<textarea type="text" class="form-control" rows="25" name="1_1_5" placeholder="" maxlength="60000"><?php echo $print;?></textarea>
						</label>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "1.1.a";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="6">1.1.a</td>
										<td colspan="3">Kejelasan dan kerealistikan visi, misi, tujuan, dan sasaran program studi</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Memiliki visi, misi, tujuan, dan sasaran yang kurang jelas dan tidak realistik</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Memiliki visi, misi, tujuan, dan sasaran yang cukup jelas namun kurang realistik</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Memiliki visi, misi, tujuan, dan sasaran jelas dan  realistik</td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Memiliki visi, misi, tujuan, dan sasaran yang sangat jelas dan sangat realistik</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control"  placeholder="" value='<?php echo $print;?>' name="nilai/1_1_a"/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "1.1.b";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="6">1.1.b</td>
										<td colspan="3">Strategi pencapaian sasaran dengan rentang waktu yang jelas dan didukung oleh dokumen</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Strategi pencapaian sasaran: (1) tanpa adanya tahapan waktu yang jelas, (2) didukung dokumen yang kurang lengkap</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Strategi pencapaian sasaran: (1) dengan tahapan waktu yang jelas, dan cukup realistik, (2) didukung dokumen yang cukup lengkap</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Strategi pencapaian sasaran: (1) dengan tahapan waktu yang jelas, dan realistik, (2) didukung dokumen yang  lengkap</td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Strategi pencapaian sasaran:(1) dengan tahapan waktu yang jelas dan sangat realistik, (2) didukung dokumen yang sangat lengkap</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control"  placeholder="" value='<?php echo $print;?>' name="nilai/1_1_b"/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 text-right">1.2 </label> 
						<label class="col-sm-8">Sosialisasi :</label>
					</div>
					<div class="form-group">
						<label class="col-sm-2 text-right"></label> 
						<label class="col-sm-9">Upaya penyebaran/sosialisasi visi, misi dan tujuan Program Studi serta pemahaman sivitas akademika (dosen dan mahasiswa) dan tenaga kependidikan :</label>
					</div>
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "1.2";
							$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
							$data = mysqli_query($db, $query);
							$row = mysqli_fetch_assoc($data);
							$print = $row["isi_text"];
							?>
							<textarea type="text" class="form-control" rows="25" name="1_2" placeholder="" maxlength="60000"><?php echo $print;?></textarea>
						</label>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "1.2";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="6">1.2</td>
										<td colspan="3">Efektivitas sosialisasi visi, misi PS: tingkat pemahaman sivitas akademika</td>	
									</tr>
									<tr>
										<td>1</td>
										<td>Tidak dipahami oleh seluruh sivitas akademika dan tenaga kependidikan</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Kurang dipahami oleh  sivitas akademika  dan tenaga kependidikan</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Dipahami dengan baik oleh sebagian  sivitas akademika dan tenaga kependidikan</td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Dipahami dengan baik oleh seluruh sivitas akademika  dan tenaga kependidikan</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control" placeholder="" value='<?php echo $print;?>' name="nilai/1_2"/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>
	
</body>

<script>
	$(document).ready(function() {
		$("form input, form select, form textarea").attr('disabled',true);
		$("button").hide();
		$("#editButton").show();
	});

	$("#editButton").click(function() {
		$("form input, form select, form textarea").attr('disabled',false);
		$("button").show();
		$("#editButton").hide();
	});

	$('form').submit(function() {
		$('body').hide();
		$("header").hide();
		$("html").prepend("<div class='ball'></div><div class='ball1'></div><h1 style='margin-left:43%'>MEMPROSES<h1>");
	});
</script>

</html>