<!doctype html>
<html>
<link rel="stylesheet" type="text/css" href="../css/loading.css">
<?php
require "../Database/DatabaseConnection.php";

$query='select idPeriode from periode where aktif = 1';
$data = mysqli_query($db, $query);
$row = mysqli_fetch_assoc($data);

$periode = $row["idPeriode"];
$username= $_COOKIE['LPMKa'];

$query="select idProdi from user where username='".$username."'";
$data = mysqli_query($db, $query);
$row = mysqli_fetch_assoc($data);
$prodi = $row["idProdi"];

$standar= 7;
$nol= 0;
$nullText = "";

//insert 7.1.1
$butir="7.1.1";

for ($no=1; $no <= 5; $no++) { 
	for ($kol=2; $kol <=4 ; $kol++) { 
		$name = "7_1_1/".$kol."/no".$no;
		$isi=$_POST[$name];

		$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
		VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kol."','".$no."','".$nol."','".$nullText."','".$isi."','".$nullText."') 
		ON DUPLICATE KEY UPDATE `isi_float`='".$isi."'";
		mysqli_query($db, $query);
	}
}

//insert 7.1.2
$butir="7.1.2";

$name = "7_1_2/radio";
$isi=$_POST[$name];
$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$nol."','".$nol."','".$isi."','".$nol."','".$nullText."') 
ON DUPLICATE KEY UPDATE `isi_char`='".$isi."'";
mysqli_query($db, $query);

for ($i=1; $i <= 2; $i++) { 
	$name = "7_1_2/".$i;
	$isi=$_POST[$name];
	$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
	VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$i."','".$nol."','".$nol."','".$isi."','".$nol."','".$nullText."') 
	ON DUPLICATE KEY UPDATE `isi_char`='".$isi."'";
	mysqli_query($db, $query);
}

//insert 7.1.3
$butir="7.1.3";

//counter
$count = $_POST['7_1_3/counter'];
$kolId = "0";
$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolId."','".$nol."','".$nol."','".$nullText."','".$count."','".$nullText."')
ON DUPLICATE KEY UPDATE `isi_float`='".$count."'";
mysqli_query($db, $query);

for ($row=1; $row <=$count ; $row++) {
	for ($kol=2; $kol <=5 ; $kol++) { 
		$name = "7_1_3/".$kol."/no".$row;
		$isiVarchar=$_POST[$name];

		$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
		VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kol."','".$row."','".$nol."','".$isiVarchar."','".$nol."','".$nullText."')
		ON DUPLICATE KEY UPDATE `isi_char`='".$isiVarchar."'";
		mysqli_query($db, $query);
	}
	for ($kol=6; $kol <=8; $kol++) {
		$name = "7_1_3/".$kol."/no".$row; 
		if (isset($_POST[$name])) {
			$isiVarchar=$_POST[$name];
		}else{
			$isiVarchar=0;
		}
		$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
		VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kol."','".$row."','".$nol."','".$isiVarchar."','".$nol."','".$nullText."')
		ON DUPLICATE KEY UPDATE `isi_char`='".$isiVarchar."'";
		mysqli_query($db, $query);
	}
}

//insert 7.1.4
$butir="7.1.4";

//counter
$count = $_POST['7_1_4/counter'];
$kolId = "0";
$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolId."','".$nol."','".$nol."','".$nullText."','".$count."','".$nullText."')
ON DUPLICATE KEY UPDATE `isi_float`='".$count."'";
mysqli_query($db, $query);

for ($row=1; $row <=$count ; $row++) {
	for ($kol=2; $kol <=2 ; $kol++) { 
		$name = "7_1_4/".$kol."/no".$row;
		$isiText=$_POST[$name];

		$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
		VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kol."','".$row."','".$nol."','".$nullText."','".$nol."','".$isiText."')
		ON DUPLICATE KEY UPDATE `isi_text`='".$isiText."'";
		mysqli_query($db, $query);
	}
}

//insert 7.2.1
$butir="7.2.1";

for ($no=1; $no <= 5; $no++) { 
	for ($kol=2; $kol <=4 ; $kol++) { 
		$name = "7_2_1/".$kol."/no".$no;
		$isi=$_POST[$name];

		$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
		VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kol."','".$no."','".$nol."','".$nullText."','".$isi."','".$nullText."') 
		ON DUPLICATE KEY UPDATE `isi_float`='".$isi."'";
		mysqli_query($db, $query);
	}
}

//insert 7.2.2
$butir="7.2.2";

//radio
$isi = $_POST['7_2_2/radio'];
$kolId = "0";
$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolId."','".$nol."','".$nol."','".$isi."','".$nol."','".$nullText."')
ON DUPLICATE KEY UPDATE `isi_char`='".$isi."'";
mysqli_query($db, $query);

//penjelasan
$isi = $_POST['7_2_2/penjelasan'];
$kolId = "-1";
$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolId."','".$nol."','".$nol."','".$nullText."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi_text`='".$isi."'";
mysqli_query($db, $query);

//insert 7.3.1
$butir="7.3.1";

//counter
$count = $_POST['7_3_1/counter'];
$kolId = "0";
$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolId."','".$nol."','".$nol."','".$nullText."','".$count."','".$nullText."')
ON DUPLICATE KEY UPDATE `isi_float`='".$count."'";
mysqli_query($db, $query);

for ($row=1; $row <=$count ; $row++) {
	for ($kol=2; $kol <=6 ; $kol++) { 
		$name = "7_3_1/".$kol."/no".$row;
		$isiText=$_POST[$name];

		$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
		VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kol."','".$row."','".$nol."','".$nullText."','".$nol."','".$isiText."')
		ON DUPLICATE KEY UPDATE `isi_text`='".$isiText."'";
		mysqli_query($db, $query);
	}
}

//insert 7.3.2
$butir="7.3.2";

//counter
$count = $_POST['7_3_2/counter'];
$kolId = "0";
$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolId."','".$nol."','".$nol."','".$nullText."','".$count."','".$nullText."')
ON DUPLICATE KEY UPDATE `isi_float`='".$count."'";
mysqli_query($db, $query);

for ($row=1; $row <=$count ; $row++) {
	for ($kol=2; $kol <=6 ; $kol++) { 
		$name = "7_3_2/".$kol."/no".$row;
		$isiText=$_POST[$name];

		$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
		VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kol."','".$row."','".$nol."','".$nullText."','".$nol."','".$isiText."')
		ON DUPLICATE KEY UPDATE `isi_text`='".$isiText."'";
		mysqli_query($db, $query);
	}
}













//Form Penilaian

//insert 7.1.1
$butir="7.1.1";
$isi = $_POST['nilai/7_1_1'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 7.1.2
$butir="7.1.2";
$isi = $_POST['nilai/7_1_2'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 7.1.3
$butir="7.1.3";
$isi = $_POST['nilai/7_1_3'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 7.1.4
$butir="7.1.4";
$isi = $_POST['nilai/7_1_4'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 7.2.1
$butir="7.2.1";
$isi = $_POST['nilai/7_2_1'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 7.2.2
$butir="7.2.2";
$isi = $_POST['nilai/7_2_2'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 7.3.1
$butir="7.3.1";
$isi = $_POST['nilai/7_3_1'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 7.3.2
$butir="7.3.2";
$isi = $_POST['nilai/7_3_2'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);





echo "<div class='ball'></div>";
echo "<div class='ball1'></div>";
echo "<h1 style='margin-left:43%'>MEMPROSES<h1>";
echo '<script>
window.location.href="Standar7.php";
</script>';
?>
</html>