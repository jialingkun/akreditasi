<!doctype html>
<html>
<link rel="stylesheet" type="text/css" href="../css/loading.css">
<?php
require "../Database/DatabaseConnection.php";

$query='select idPeriode from periode where aktif = 1';
$data = mysqli_query($db, $query);
$row = mysqli_fetch_assoc($data);

$periode = $row["idPeriode"];
$username= $_COOKIE['LPMKa'];

$query="select idProdi from user where username='".$username."'";
$data = mysqli_query($db, $query);
$row = mysqli_fetch_assoc($data);
$prodi = $row["idProdi"];

$standar= 3;
$nol= 0;
$nullText = "";

//insert 3.1.1
$butir="3.1.1";

for ($TS=4; $TS >= 0; $TS--) { 
	for ($kol=2; $kol <=16 ; $kol++) { 
		$name = "3_1_1/".$kol."/TS".$TS;
		$isi=$_POST[$name];

		$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
		VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kol."','".$TS."','".$nol."','".$nullText."','".$isi."','".$nullText."') 
		ON DUPLICATE KEY UPDATE `isi_float`='".$isi."'";
		mysqli_query($db, $query);
	}
}

//insert 3.1.2
$butir="3.1.2";

for ($TS=4; $TS >= 0; $TS--) { 
	for ($kol=2; $kol <=8 ; $kol++) { 
		$name = "3_1_2/".$kol."/TS".$TS;
		$isi=$_POST[$name];

		$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
		VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kol."','".$TS."','".$nol."','".$nullText."','".$isi."','".$nullText."')
		ON DUPLICATE KEY UPDATE `isi_float`='".$isi."'";
		mysqli_query($db, $query);
	}
}


//insert 3.1.3
$butir="3.1.3";

//counter
$count = $_POST['3_1_3/counter'];
$kolId = "0";
$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolId."','".$nol."','".$nol."','".$nullText."','".$count."','".$nullText."')
ON DUPLICATE KEY UPDATE `isi_float`='".$count."'";
mysqli_query($db, $query);

for ($row=1; $row <=$count ; $row++) {
	for ($kol=2; $kol <=4 ; $kol++) { 
		$name = "3_1_3/".$kol."/no".$row;
		if ($kol==3) {
			if (isset($_POST[$name])) {
				$isiVarchar=$_POST[$name];
				$isiText = $nullText;
			}else{
				$isiVarchar="";
				$isiText = $nullText;
			}
		}else{
			$isiVarchar=$nullText;
			$isiText = $_POST[$name];
		}

		$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
		VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kol."','".$row."','".$nol."','".$isiVarchar."','".$nol."','".$isiText."')
		ON DUPLICATE KEY UPDATE `isi_char`='".$isiVarchar."',`isi_text`='".$isiText."'";
		mysqli_query($db, $query);
		
	}
}



//insert 3.1.4
$butir="3.1.4";

for ($TS=6; $TS >= 0; $TS--) { 
	for ($kol=2; $kol <=9 ; $kol++) { 
		$name = "3_1_4/".$kol."/TS".$TS;
		if (isset($_POST[$name])) {
			$isi=$_POST[$name];
			$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
			VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kol."','".$TS."','".$nol."','".$nullText."','".$isi."','".$nullText."')
			ON DUPLICATE KEY UPDATE `isi_float`='".$isi."'";
			mysqli_query($db, $query);
		}

	}
}

//insert 3.2
$butir="3.2";
$kol = 3;
for ($row=1; $row <= 6; $row++) { 
	$name = "3_2/3/no".$row;
	$isi=$_POST[$name];

	$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
	VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kol."','".$row."','".$nol."','".$nullText."','".$nol."','".$isi."')
	ON DUPLICATE KEY UPDATE `isi_text`='".$isi."'";
	mysqli_query($db, $query);	
}


//insert 3.3.1
$butir="3.3.1";

//radio
$isi = $_POST['3_3_1/radio'];
$kolId = "0";
$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolId."','".$nol."','".$nol."','".$isi."','".$nol."','".$nullText."')
ON DUPLICATE KEY UPDATE `isi_char`='".$isi."'";
mysqli_query($db, $query);

//penjelasan
$isi = $_POST['3_3_1/penjelasan'];
$kolId = "-1";
$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolId."','".$nol."','".$nol."','".$nullText."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi_text`='".$isi."'";
mysqli_query($db, $query);

//tabel
for ($row=1; $row <=7 ; $row++) {
	for ($kol=3; $kol <=7 ; $kol++) { 
		$name = "3_3_1/".$kol."/no".$row;
		if ($kol==7) {
			$isiFloat = 0;
			$isiText = $_POST[$name];
		}else{
			$isiFloat = $_POST[$name];
			$isiText = $nullText;
			
		}
		$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
		VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kol."','".$row."','".$nol."','".$nullText."','".$isiFloat."','".$isiText."')
		ON DUPLICATE KEY UPDATE `isi_float`='".$isiFloat."',`isi_text`='".$isiText."'";
		mysqli_query($db, $query);
		
	}
}


//insert 3.3.2
$butir="3.3.2";

//bulan
$isi = $_POST['3_3_2'];
$kolId = "0";
$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolId."','".$nol."','".$nol."','".$nullText."','".$isi."','".$nullText."')
ON DUPLICATE KEY UPDATE `isi_float`='".$isi."'";
mysqli_query($db, $query);

//penjelasan
$isi = $_POST['3_3_2/penjelasan'];
$kolId = "-1";
$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolId."','".$nol."','".$nol."','".$nullText."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi_text`='".$isi."'";
mysqli_query($db, $query);

//insert 3.3.3
$butir="3.3.3";

//persen
$isi = $_POST['3_3_3'];
$kolId = "0";
$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolId."','".$nol."','".$nol."','".$nullText."','".$isi."','".$nullText."')
ON DUPLICATE KEY UPDATE `isi_float`='".$isi."'";
mysqli_query($db, $query);

//penjelasan
$isi = $_POST['3_3_3/penjelasan'];
$kolId = "-1";
$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolId."','".$nol."','".$nol."','".$nullText."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi_text`='".$isi."'";
mysqli_query($db, $query);

//insert 3.4
$butir="3.4";
$isi = $_POST['3_4'];
$kolId = "0";
$query="INSERT INTO `isi_borang`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `kolom`, `no`, `sub_no`, `isi_char`, `isi_float`, `isi_text`) 
VALUES('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$kolId."','".$nol."','".$nol."','".$nullText."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi_text`='".$isi."'";
mysqli_query($db, $query);


//Form Penilaian

//insert 3.1.1.a
$butir="3.1.1.a";
$isi = $_POST['nilai/3_1_1_a'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 3.1.1.b
$butir="3.1.1.b";
$isi = $_POST['nilai/3_1_1_b'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 3.1.1.c
$butir="3.1.1.c";
$isi = $_POST['nilai/3_1_1_c'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 3.1.1.d
$butir="3.1.1.d";
$isi = $_POST['nilai/3_1_1_d'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 3.1.2
$butir="3.1.2";
$isi = $_POST['nilai/3_1_2'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 3.1.3
$butir="3.1.3";
$isi = $_POST['nilai/3_1_3'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 3.1.4.a
$butir="3.1.4.a";
$isi = $_POST['nilai/3_1_4_a'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 3.1.4.b
$butir="3.1.4.b";
$isi = $_POST['nilai/3_1_4_b'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 3.1.4.a
$butir="3.1.4.a";
$isi = $_POST['nilai/3_1_4_a'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 3.2.1
$butir="3.2.1";
$isi = $_POST['nilai/3_2_1'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 3.2.2
$butir="3.2.2";
$subButir = "1";
$isi = $_POST['nilai/3_2_2/1'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$subButir."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);
$isi = $_POST['nilai/3_2_2'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 3.3.1.a
$butir="3.3.1.a";
$isi = $_POST['nilai/3_3_1_a'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 3.3.1.b
$butir="3.3.1.b";
$isi = $_POST['nilai/3_3_1_b'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 3.3.1.c
$butir="3.3.1.c";
$isi = $_POST['nilai/3_3_1_c'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 3.3.2
$butir="3.3.2";
$isi = $_POST['nilai/3_3_2'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 3.3.3
$butir="3.3.3";
$isi = $_POST['nilai/3_3_3'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 3.4.1
$butir="3.4.1";
$isi = $_POST['nilai/3_4_1'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

//insert 3.4.2
$butir="3.4.2";
$isi = $_POST['nilai/3_4_2'];
$query="INSERT INTO `isi_form_penilaian`(`username`, `idProdi`, `idPeriode`, `standar`, `butir`, `sub_butir`, `isi`) 
VALUES ('".$username."','".$prodi."','".$periode."','".$standar."','".$butir."','".$nol."','".$isi."')
ON DUPLICATE KEY UPDATE `isi`='".$isi."'";
mysqli_query($db, $query);

echo "<div class='ball'></div>";
echo "<div class='ball1'></div>";
echo "<h1 style='margin-left:43%'>MEMPROSES<h1>";
echo '<script>
window.location.href="Standar3.php";
</script>';

?>
</html>