var dropdowndosen;
function updateNilai11(rowIndex){
	idTable = "tabel711";
	sumColumn(rowIndex,idTable);
	kolom1=parseInt(document.getElementById("7.1.1/no5").innerHTML); if (isNaN(kolom1)) {kolom1=0};
	document.getElementById("nilai/7.1.1/1").innerHTML=kolom1;
	no3 = parseInt(document.getElementById("7.1.1/no3").innerHTML); if (isNaN(no3)) {no3=0};
	no4 = parseInt(document.getElementById("7.1.1/no4").innerHTML); if (isNaN(no4)) {no4=0};
	kolom2 = no3 + no4;
	document.getElementById("nilai/7.1.1/2").innerHTML=kolom2;
	no1 = parseInt(document.getElementById("7.1.1/no1").innerHTML); if (isNaN(no1)) {no1=0};
	no2 = parseInt(document.getElementById("7.1.1/no2").innerHTML); if (isNaN(no2)) {no2=0};
	kolom3 = no1 + no2;
	document.getElementById("nilai/7.1.1/3").innerHTML=kolom3;
	jumlahDosen = parseInt(document.getElementById("nilai/7.1.1/4").innerHTML);
	NK = roundToTwo((4*kolom1+2*kolom2+kolom3)/jumlahDosen);
	document.getElementById("nilai/7.1.1/5").innerHTML = NK;

	if (NK<=0) {
		nilai = 0;
	}else if (NK<2) {
		nilai = roundToTwo(1.5*NK+1);
	}else{
		nilai=4;
	}

	document.getElementById("nilai/7.1.1/nilai").innerHTML = nilai;
	document.getElementById("nilai/7.1.1/nilaihid").value = nilai;
}

function updateNilai12(){
	kolom1 = parseInt(document.getElementById("7.1.2/2").value); if (isNaN(kolom1)) {kolom1=0;}
	document.getElementById("nilai/7.1.2/1").innerHTML = kolom1;
	kolom2 = parseInt(document.getElementById("7.1.2/1").value); if (isNaN(kolom2)) {kolom2=0;}
	document.getElementById("nilai/7.1.2/2").innerHTML = kolom2;

	if (kolom1==0) {
		persentase = 0;
	}else{
		persentase = roundToTwo(kolom2/kolom1);
	}
	document.getElementById("nilai/7.1.2/3").innerHTML = persentase*100+"%";

	if (persentase==0) {
		nilai=0;
	} else if (persentase<0.25) {
		nilai=roundToTwo(1+12*persentase);
	}else{
		nilai=4;
	}

	document.getElementById("nilai/7.1.2/nilai").innerHTML = nilai;
	document.getElementById("nilai/7.1.2/nilaihid").value = nilai;
}

function addRow13(idTable,idCounter){
	rowCount=document.getElementById(""+idCounter).value;
	nextCount = parseInt(rowCount)+1;
	htmlContent = "<td>"+nextCount+"</td>"+
	"<td><textarea rows='3' style='width:100%;' name='7_1_3/2/no"+nextCount+"'></textarea></td>"+
	"<td><select class='form-control' name='7_1_3/3/no"+nextCount+"'>"+dropdowndosen+"</select></td>"+
	"<td><textarea rows='3' style='width:100%;' name='7_1_3/4/no"+nextCount+"'></textarea></td>"+
	"<td><textarea rows='3' style='width:100%;' name='7_1_3/5/no"+nextCount+"'></textarea></td>"+
	"<td><input type='checkbox' class='form-control' value='1' onchange='updateNilai13(5)' name='7_1_3/6/no"+nextCount+"'></td>"+
	"<td><input type='checkbox' class='form-control' value='1' onchange='updateNilai13(6)' name='7_1_3/7/no"+nextCount+"'></td>"+
	"<td><input type='checkbox' class='form-control' value='1' onchange='updateNilai13(7)' name='7_1_3/8/no"+nextCount+"'></td>";
	addRow(idTable, idCounter, nextCount, htmlContent);
}

function updateNilai13(columnIndex){
	idTable = "tabel713";
	idOutput = "7.1.3/"+(columnIndex+1)+"/jumlah";
	sumRowInAddRowCheckId(columnIndex,idTable,idOutput);

	na = document.getElementById("7.1.3/8/jumlah").innerHTML;
	nb = document.getElementById("7.1.3/7/jumlah").innerHTML;
	nc = document.getElementById("7.1.3/6/jumlah").innerHTML;
	jumlahDosen = document.getElementById("nilai/7.1.3/4").innerHTML;
	document.getElementById("nilai/7.1.3/1").innerHTML = na;
	document.getElementById("nilai/7.1.3/2").innerHTML = nb;
	document.getElementById("nilai/7.1.3/3").innerHTML = nc;
	nk = roundToTwo((4*na+2*nb+nc)/jumlahDosen);
	document.getElementById("nilai/7.1.3/5").innerHTML = nk;

	if (nk<=0) {
		nilai=0;
	}else if (nk<6) {
		nilai = roundToTwo(1+nk/2);
	}else{
		nilai = 4;
	}

	document.getElementById("nilai/7.1.3/nilai").innerHTML = nilai;
	document.getElementById("nilai/7.1.3/nilaihid").value = nilai;
}

function addRow14(idTable,idCounter){
	rowCount=document.getElementById(""+idCounter).value;
	nextCount = parseInt(rowCount)+1;
	htmlContent = "<td>"+nextCount+"</td>"+
	"<td><textarea rows='2' style='width:100%;' onchange='updateNilai14()' name='7_1_4/2/no"+nextCount+"' id='7.1.4/2/no"+nextCount+"'></textarea></td>";
	addRow(idTable, idCounter, nextCount, htmlContent);
}

function updateNilai14(){
	idCounter = "7.1.4/counter";
	rowCount = parseInt(document.getElementById(""+idCounter).value);
	characterLimit = 5;
	filledCount=0;
	for(i=1;i<=rowCount;i++){
		filled = document.getElementById("7.1.4/2/no"+i).value.length;
		if (filled>characterLimit) {
			filledCount++;
		}
	}

	document.getElementById("nilai/7.1.4/1").innerHTML = filledCount;
	if (filledCount<=0) {
		nilai=2;
	}else if (filledCount==1){
		nilai = 3;
	}else{
		nilai=4;
	}
	document.getElementById("nilai/7.1.4/nilai").innerHTML = nilai;
	document.getElementById("nilai/7.1.4/nilaihid").value = nilai;
}

function updateNilai21(rowIndex){
	idTable = "tabel721";
	sumColumn(rowIndex,idTable);
	kolom1=parseInt(document.getElementById("7.2.1/no5").innerHTML); if (isNaN(kolom1)) {kolom1=0};
	document.getElementById("nilai/7.2.1/1").innerHTML=kolom1;
	no3 = parseInt(document.getElementById("7.2.1/no3").innerHTML); if (isNaN(no3)) {no3=0};
	no4 = parseInt(document.getElementById("7.2.1/no4").innerHTML); if (isNaN(no4)) {no4=0};
	kolom2 = no3 + no4;
	document.getElementById("nilai/7.2.1/2").innerHTML=kolom2;
	no1 = parseInt(document.getElementById("7.2.1/no1").innerHTML); if (isNaN(no1)) {no1=0};
	no2 = parseInt(document.getElementById("7.2.1/no2").innerHTML); if (isNaN(no2)) {no2=0};
	kolom3 = no1 + no2;
	document.getElementById("nilai/7.2.1/3").innerHTML=kolom3;
	jumlahDosen = parseInt(document.getElementById("nilai/7.2.1/4").innerHTML);
	NK = roundToTwo((4*kolom1+2*kolom2+kolom3)/jumlahDosen);
	document.getElementById("nilai/7.2.1/5").innerHTML = NK;

	if (NK<=0) {
		nilai = 0;
	}else if (NK<1) {
		nilai = roundToTwo(3*NK+1);
	}else{
		nilai=4;
	}

	document.getElementById("nilai/7.2.1/nilai").innerHTML = nilai;
	document.getElementById("nilai/7.2.1/nilaihid").value = nilai;
}

function addRow31(idTable,idCounter){
	rowCount=document.getElementById(""+idCounter).value;
	nextCount = parseInt(rowCount)+1;
	htmlContent = "<td>"+nextCount+"</td>"+
	"<td><textarea rows='3' style='width:100%;' name='7_3_1/2/no"+nextCount+"'></textarea></td>"+
	"<td><textarea rows='3' style='width:100%;' name='7_3_1/3/no"+nextCount+"'></textarea></td>"+
	"<td><textarea rows='3' style='width:100%;' name='7_3_1/4/no"+nextCount+"'></textarea></td>"+
	"<td><textarea rows='3' style='width:100%;' name='7_3_1/5/no"+nextCount+"'></textarea></td>"+
	"<td><textarea rows='3' style='width:100%;' name='7_3_1/6/no"+nextCount+"'></textarea></td>";
	addRow(idTable, idCounter, nextCount, htmlContent);
}

function addRow32(idTable,idCounter){
	rowCount=document.getElementById(""+idCounter).value;
	nextCount = parseInt(rowCount)+1;
	htmlContent = "<td>"+nextCount+"</td>"+
	"<td><textarea rows='3' style='width:100%;' name='7_3_2/2/no"+nextCount+"'></textarea></td>"+
	"<td><textarea rows='3' style='width:100%;' name='7_3_2/3/no"+nextCount+"'></textarea></td>"+
	"<td><textarea rows='3' style='width:100%;' name='7_3_2/4/no"+nextCount+"'></textarea></td>"+
	"<td><textarea rows='3' style='width:100%;' name='7_3_2/5/no"+nextCount+"'></textarea></td>"+
	"<td><textarea rows='3' style='width:100%;' name='7_3_2/6/no"+nextCount+"'></textarea></td>";
	addRow(idTable, idCounter, nextCount, htmlContent);
}

















function updateListDosen(){
	//dosen ps
	rowCount=document.getElementById("4.3.1/counter").value;
	dropdowndosen = "<option>-</option>";
	for (no = 1; no <=rowCount ; no++) {
		nama = document.getElementById("4.3.1/2/no"+no).innerHTML;
		dropdowndosen = dropdowndosen+"<option>"+nama+"</option>";
	}
}

function loadAllJs(){
	updateListDosen();
	for(i=1;i<=5;i++){
		updateNilai11(i);
	}
	updateNilai12();

	updateNilai13(5);
	updateNilai13(6);
	updateNilai13(7);

	updateNilai14();

	for(i=1;i<=5;i++){
		updateNilai21(i);
	}
}