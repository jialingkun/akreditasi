function updateNilai11(columnIndex){
	if (columnIndex>0 && columnIndex<10) {
		sumRow(columnIndex,"sumTable1");
	}

	//3.1.1.a
	row1 = document.getElementById('3.1.1/3').innerHTML;
	row2 = document.getElementById('3.1.1/2').innerHTML;
	row3 = roundToTwo(row1/row2);
	document.getElementById('nilai/3.1.1.a/1').innerHTML = row1;
	document.getElementById('nilai/3.1.1.a/2').innerHTML = row2;
	document.getElementById('nilai/3.1.1.a/3').innerHTML = row3;
	if (row3>5) {
		nilai = 4;
	}else if (row3>1) {
		nilai = roundToTwo((3+row3)/2);
	}else{
		nilai = 2*row3;
	}
	document.getElementById("nilai/3.1.1.a/nilai").innerHTML = nilai;
	document.getElementById("nilai/3.1.1.a/nilaihid").value = nilai;

	//3.1.1.b
	row1 = document.getElementById('3.1.1/5').innerHTML;
	row2 = document.getElementById('3.1.1/4').innerHTML;
	row3 = roundToTwo(row1/row2);
	document.getElementById('nilai/3.1.1.b/1').innerHTML = row1;
	document.getElementById('nilai/3.1.1.b/2').innerHTML = row2;
	document.getElementById('nilai/3.1.1.b/3').innerHTML = row3;
	if (row3>=0.95) {
		nilai = 4;
	}else if (row3>0.25) {
		nilai = roundToTwo((40*row3-10)/7);
	}else{
		nilai = 0;
	}
	document.getElementById("nilai/3.1.1.b/nilai").innerHTML = nilai;
	document.getElementById("nilai/3.1.1.b/nilaihid").value = nilai;

	//3.1.1.c
	table1data = parseFloat(document.getElementById('3.1.1/6').innerHTML);
	table2data = parseFloat(document.getElementById('3.1.2/6').innerHTML);
	if (isNaN(table2data)) {table2data=0;}
	row1 = table1data+table2data;
	table1data = parseFloat(document.getElementById('3.1.1/5').innerHTML);
	table2data = parseFloat(document.getElementById('3.1.2/5').innerHTML);
	if (isNaN(table2data)) {table2data=0;}
	row2 = table1data+table2data;
	row3 = roundToTwo(row1/row2);
	document.getElementById('nilai/3.1.1.c/1').innerHTML = row1;
	document.getElementById('nilai/3.1.1.c/2').innerHTML = row2;
	document.getElementById('nilai/3.1.1.c/3').innerHTML = row3;
	if (row3>=1.25) {
		nilai = 0;
	}else if (row3>0.25) {
		nilai = roundToTwo(5-4*row3);
	}else{
		nilai = 4;
	}
	document.getElementById("nilai/3.1.1.c/nilai").innerHTML = nilai;
	document.getElementById("nilai/3.1.1.c/nilaihid").value = nilai;

	//3.1.1.d
	AverageRow(11,"sumTable1","nilai/3.1.1.d/1");
	row1 = document.getElementById('nilai/3.1.1.d/1').innerHTML;
	if (row1>=3) {
		nilai = 4;
	}else if (row1>=2.75) {
		nilai = roundToTwo(4*row1-8);
	}else if (row1>=2) {
		nilai = roundToTwo((4*row1-2)/3);
	}else{
		nilai = 0;
	}
	document.getElementById("nilai/3.1.1.d/nilai").innerHTML = nilai;
	document.getElementById("nilai/3.1.1.d/nilaihid").value = nilai;

}

function updateNilai12(columnIndex){
	if (columnIndex>0) {
		sumRow(columnIndex,"sumTable2");
	}

	//3.1.1.c
	table1data = parseFloat(document.getElementById('3.1.1/6').innerHTML);
	table2data = parseFloat(document.getElementById('3.1.2/6').innerHTML);
	if (isNaN(table2data)) {table2data=0;}
	row1 = table1data+table2data;
	table1data = parseFloat(document.getElementById('3.1.1/5').innerHTML);
	table2data = parseFloat(document.getElementById('3.1.2/5').innerHTML);
	if (isNaN(table2data)) {table2data=0;}
	row2 = table1data+table2data;
	row3 = roundToTwo(row1/row2);
	document.getElementById('nilai/3.1.1.c/1').innerHTML = row1;
	document.getElementById('nilai/3.1.1.c/2').innerHTML = row2;
	document.getElementById('nilai/3.1.1.c/3').innerHTML = row3;
	if (row3>=1.25) {
		nilai = 0;
	}else if (row3>0.25) {
		nilai = roundToTwo(5-4*row3);
	}else{
		nilai = 4;
	}
	document.getElementById("nilai/3.1.1.c/nilai").innerHTML = nilai;
	document.getElementById("nilai/3.1.1.c/nilaihid").value = nilai;

}

function addRow13(idTable,idCounter){
	rowCount=document.getElementById(""+idCounter).value;
	nextCount = parseInt(rowCount)+1;
	htmlContent = "<td>"+nextCount+"</td><td><textarea name='3_1_3/2/no"+nextCount+"' rows='2' style='width:100%;'></textarea></td><td> <select name='3.1.3/3/no"+nextCount+"' class='form-control' onchange='updateNilai13()'><option disabled selected>--Pilih Tingkat--</option><option>Lokal</option><option>Regional</option><option>Nasional</option><option>Internasional</option><option>Kosong</option></select> </td><td><textarea name='3.1.3/4/no"+nextCount+"' rows='2' style='width:100%;'></textarea></td>";
	addRow(idTable, idCounter, nextCount, htmlContent);
}

function updateNilai13(){
	countLokal = countRowByValue(2,"addTable3","Lokal");
	countRegional = countRowByValue(2,"addTable3","Regional");
	countNasional = countRowByValue(2,"addTable3","Nasional");
	countInternasional = countRowByValue(2,"addTable3","Internasional");
	if (countInternasional>0 || countNasional>0) {
		nilai = 4;
	}else if (countRegional>0) {
		nilai = 3;
	}else if (countLokal>0) {
		nilai = 2;
	}else{
		nilai = 1;
	}
	document.getElementById("nilai/3.1.3/nilai").innerHTML = nilai;
	document.getElementById("nilai/3.1.3/nilaihid").value = nilai;
}

function updateNilai14(){

	//3.1.4.a
	row1 = parseFloat(document.getElementById('3.1.4/d').value);
	row2 = parseFloat(document.getElementById('3.1.4/f').value);
	row3 = roundToTwo(row2/row1);

	document.getElementById('nilai/3.1.4.a/1').innerHTML = row1;
	document.getElementById('nilai/3.1.4.a/2').innerHTML = row2;
	document.getElementById('nilai/3.1.4.a/3').innerHTML = (row3*100)+"%";
	if (row3>=0.5) {
		nilai = 4;
	}else if (row3>0) {
		nilai = roundToTwo(1+6*row3);
	}else{
		nilai = 0;
	}
	document.getElementById("nilai/3.1.4.a/nilai").innerHTML = nilai;
	document.getElementById("nilai/3.1.4.a/nilaihid").value = nilai;

	//3.1.4.b
	row1 = parseFloat(document.getElementById('3.1.4/a').value);
	row2 = parseFloat(document.getElementById('3.1.4/b').value);
	row3 = parseFloat(document.getElementById('3.1.4/c').value);
	row4 = roundToTwo((row1-row2-row3)/row1);

	document.getElementById('nilai/3.1.4.b/1').innerHTML = row1;
	document.getElementById('nilai/3.1.4.b/2').innerHTML = row2;
	document.getElementById('nilai/3.1.4.b/3').innerHTML = row3;
	document.getElementById('nilai/3.1.4.b/4').innerHTML = (row4*100)+"%";
	if (row4<=0.06) {
		nilai = 4;
	}else if (row4<0.45) {
		nilai = roundToTwo((180-400*row4)/39);
	}else{
		nilai = 0;
	}
	document.getElementById("nilai/3.1.4.b/nilai").innerHTML = nilai;
	document.getElementById("nilai/3.1.4.b/nilaihid").value = nilai;

}

function updateNilai2(){

	//3.2.1
	row1 = document.getElementById('3.2/3/no1').value.length;
	row2 = document.getElementById('3.2/3/no2').value.length;
	row3 = document.getElementById('3.2/3/no3').value.length;
	row4 = document.getElementById('3.2/3/no4').value.length;
	row5 = document.getElementById('3.2/3/no5').value.length;
	characterLimit = 5;

	filledCount = 0;
	if (row1>characterLimit) {
		filledCount = filledCount + 1;
	}
	if (row2>characterLimit) {
		filledCount = filledCount + 1;
	}
	if (row3>characterLimit) {
		filledCount = filledCount + 1;
	}
	if (row4>characterLimit) {
		filledCount = filledCount + 1;
	}
	if (row5>characterLimit) {
		filledCount = filledCount + 1;
	}

	if (row1>characterLimit && row2>characterLimit && row3>characterLimit && row4>characterLimit && row5>characterLimit) {
		nilai = 4;
	}else if (row1>characterLimit && row2>characterLimit && row3>characterLimit) {
		nilai = 3;
	}else if (row1>characterLimit && row2>characterLimit) {
		nilai = 2;
	}else if (filledCount>1) {
		nilai = 1;
	}else{
		nilai = 0;
	}
	document.getElementById("nilai/3.2.1/nilai").innerHTML = nilai;
	document.getElementById("nilai/3.2.1/nilaihid").value = nilai;

	//3.2.2
	nilai = parseFloat(document.getElementById('nilai/3.2.2/1').value)/5;
	document.getElementById("nilai/3.2.2/nilai").innerHTML = nilai;
	document.getElementById("nilai/3.2.2/nilaihid").value = nilai;
}

function updateNilai31(columnIndex){
	if (columnIndex==2) {
		sumRowId(columnIndex,"sumTable31","3.3.1/a");
	}else if (columnIndex==3) {
		sumRowId(columnIndex,"sumTable31","3.3.1/b");
	}else if (columnIndex==4) {
		sumRowId(columnIndex,"sumTable31","3.3.1/c");
	}else if (columnIndex==5) {
		sumRowId(columnIndex,"sumTable31","3.3.1/d");
	}

	//3.3.1.c
	row1 = parseFloat(document.getElementById('3.3.1/a').innerHTML);
	row2 = parseFloat(document.getElementById('3.3.1/b').innerHTML);
	row3 = parseFloat(document.getElementById('3.3.1/c').innerHTML);
	row4 = parseFloat(document.getElementById('3.3.1/d').innerHTML);
	document.getElementById('nilai/3.3.1.c/1').innerHTML = row1+"%";
	document.getElementById('nilai/3.3.1.c/2').innerHTML = row2+"%";
	document.getElementById('nilai/3.3.1.c/3').innerHTML = row3+"%";
	document.getElementById('nilai/3.3.1.c/4').innerHTML = row4+"%";
	nilai = roundToTwo(((4*row1)+(3*row2)+(2*row3)+(row4))/7/100);
	document.getElementById("nilai/3.3.1.c/5").innerHTML = nilai;
	document.getElementById("nilai/3.3.1.c/nilai").innerHTML = nilai;
	document.getElementById("nilai/3.3.1.c/nilaihid").value = nilai;

}

function updateNilai32(){
	row1 = parseFloat(document.getElementById('3.3.2').value);
	document.getElementById('nilai/3.3.2/1').innerHTML = row1+" bulan";
	if (row1==0) {
		nilai = 0;
	}else if (row1<=3) {
		nilai = 4;
	}else if (row1<18) {
		nilai = roundToTwo((72-4*row1)/15);
	}else{
		nilai = 0;
	}
	document.getElementById("nilai/3.3.2/nilai").innerHTML = nilai;
	document.getElementById("nilai/3.3.2/nilaihid").value = nilai;
}


function updateNilai33(){
	row1 = parseFloat(document.getElementById('3.3.3').value);
	document.getElementById('nilai/3.3.3/1').innerHTML = row1+" %";
	if (row1>80) {
		nilai = 4;
	}else{
		nilai = roundToTwo(5*(row1/100));
	}
	document.getElementById("nilai/3.3.3/nilai").innerHTML = nilai;
	document.getElementById("nilai/3.3.3/nilaihid").value = nilai;
}

function loadAllJs(){
	for(i = 1; i<12;i++){
		updateNilai11(i);
	}
	for(i = 1; i<8;i++){
		updateNilai12(i);
	}

	updateNilai13();
	updateNilai14();
	updateNilai2();

	for(i = 2; i<6;i++){
		updateNilai31(i);
	}

	updateNilai32();
	updateNilai33();
}