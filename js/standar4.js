var dropdowndosen;
var dropdowndosenxps;
var dropdowndosenxtp;

function updateNilai31(){
	idTable = "table431";
	idCounter = "4.3.1/counter";
	//4.3.1.a , 4.3.1.b, dan 4.3.1.c
	tbodyCount=document.getElementById(""+idCounter).value;
	jumlahdosen = 0;
	jumlahjabatantinggi = 0;
	jumlahS1 = 0;
	jumlahS2 = 0;
	jumlahS3 = 0;
	for (no=1; no<=tbodyCount; no++){
		rowCount = document.getElementById("4.3.1/counter/no"+no).value;
		pendidikanterakhir = "";
		//tr pertama
		jabatan = retrieveCellInnerHTML(4,idTable,no,0);
		pendidikan = retrieveCellInnerHTML(6,idTable,no,0);
		pendidikanterakhir = pendidikan;

		if (jabatan!="-") {
			jumlahdosen++;
		}

		if (jabatan=="Lektor Kepala" || jabatan=="Guru Besar") {
			jumlahjabatantinggi++;
		}

		//tr lainnya
		for (sub=1; sub<rowCount; sub++){
			pendidikan = retrieveCellInnerHTML(1,idTable,no,sub);
			if (pendidikan=="S3") {
				pendidikanterakhir=pendidikan;
			}else if(pendidikan=="S2" && pendidikanterakhir=="S1"){
				pendidikanterakhir=pendidikan;
			}
		}
		if (pendidikanterakhir=="S3") {
			jumlahS3++;
		} else if(pendidikanterakhir=="S2"){
			jumlahS2++;
		} else if(pendidikanterakhir=="S1"){
			jumlahS1++;
		}
	}
	
	//4.3.1.a
	document.getElementById("nilai/4.3.1.a/1").innerHTML = jumlahdosen;
	document.getElementById("nilai/4.3.1.a/2").innerHTML = jumlahS1;
	document.getElementById("nilai/4.3.1.a/3").innerHTML = jumlahS2;
	document.getElementById("nilai/4.3.1.a/4").innerHTML = jumlahS3;
	document.getElementById("nilai/4.3.1.a/5").innerHTML = jumlahS2 + jumlahS3;
	persentase = roundToTwo((jumlahS2 + jumlahS3)/jumlahdosen);
	document.getElementById("nilai/4.3.1.a/6").innerHTML = (persentase*100)+"%";
	if (persentase>=0.9) {
		nilai=4;
	} else if (persentase>0.3) {
		nilai=roundToTwo(20*persentase/3-2);
	} else{
		nilai=0;
	}
	document.getElementById("nilai/4.3.1.a/nilai").innerHTML = nilai;
	document.getElementById("nilai/4.3.1.a/nilaihid").value = nilai;

	//4.3.1.b
	document.getElementById("nilai/4.3.1.b/1").innerHTML = jumlahdosen;
	document.getElementById("nilai/4.3.1.b/2").innerHTML = jumlahS3;
	persentase = roundToTwo(jumlahS3/jumlahdosen);
	document.getElementById("nilai/4.3.1.b/3").innerHTML = (persentase*100)+"%";
	if (persentase>=0.4) {
		nilai=4;
	} else{
		nilai=roundToTwo(2+5*persentase);
	}
	document.getElementById("nilai/4.3.1.b/nilai").innerHTML = nilai;
	document.getElementById("nilai/4.3.1.b/nilaihid").value = nilai;

	//4.3.1.c
	document.getElementById("nilai/4.3.1.c/1").innerHTML = jumlahdosen;
	document.getElementById("nilai/4.3.1.c/2").innerHTML = jumlahjabatantinggi;
	persentase = roundToTwo(jumlahjabatantinggi/jumlahdosen);
	document.getElementById("nilai/4.3.1.c/3").innerHTML = (persentase*100)+"%";
	if (persentase>=0.4) {
		nilai=4;
	} else{
		nilai=roundToTwo(1+7.5*persentase);
	}
	document.getElementById("nilai/4.3.1.c/nilai").innerHTML = nilai;
	document.getElementById("nilai/4.3.1.c/nilaihid").value = nilai;

	//4.3.1.d
	document.getElementById("nilai/4.3.1.d/1").innerHTML = jumlahdosen;
	jumlahsertifikat = parseInt(document.getElementById("nilai/4.3.1.d/2").value);
	persentase = roundToTwo(jumlahsertifikat/jumlahdosen);
	document.getElementById("nilai/4.3.1.d/3").innerHTML = (persentase*100)+"%";
	if (persentase>=0.4) {
		nilai=4;
	} else{
		nilai=roundToTwo(1+7.5*persentase);
	}
	document.getElementById("nilai/4.3.1.d/nilai").innerHTML = nilai;
	document.getElementById("nilai/4.3.1.d/nilaihid").value = nilai;

	//4.3.2
	document.getElementById("nilai/4.3.2/3").innerHTML = jumlahdosen;
	statusprodi = document.getElementById("nilai/4.3.2/1").value;
	jumlahmahasiswa = document.getElementById("nilai/4.3.2/2").innerHTML;
	rasio = roundToTwo(jumlahmahasiswa/jumlahdosen);
	document.getElementById("nilai/4.3.2/4").innerHTML = rasio;

	if (statusprodi==0) {
		if (rasio<17) {
			nilai = roundToTwo(4*rasio/17);
		}else if(rasio<=23){
			nilai = 4;
		}else if (rasio<60) {
			nilai = roundToTwo(4*(60-rasio)/37);
		}else{
			nilai = 0;
		}
	} else if (statusprodi==1){
		if (rasio<=5) {
			nilai= 0;
		}else if (rasio<27) {
			nilai = roundToTwo(2*(rasio-5)/11);
		}else if (rasio<=33) {
			nilai = 4;
		}else if (rasio<70) {
			nilai = roundToTwo(4*(70-rasio)/37);
		}else{
			nilai = 0;
		}
	}
	document.getElementById("nilai/4.3.2/nilai").innerHTML = nilai;
	document.getElementById("nilai/4.3.2/nilaihid").value = nilai;
}

function addRow33ganjil(idTable,idCounter){
	rowCount=document.getElementById(""+idCounter).value;
	nextCount = parseInt(rowCount)+1;
	htmlContent = "<td>"+nextCount+"</td>"+
	"<td><select class='form-control' name='4_3_3_ganjil/2/no"+nextCount+"' id='4_3_3/2/no"+nextCount+"'>"+dropdowndosen+"</select></td>"+
	"<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_3_3_ganjil/3/no"+nextCount+"' onchange='updateNilai33ganjil("+nextCount+",2,\"tabel433ganjil\",\"4.3.3.ganjil/3/jumlah\",\"4.3.3.ganjil/3/rata\")'></td>"+
	"<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_3_3_ganjil/4/no"+nextCount+"' onchange='updateNilai33ganjil("+nextCount+",3,\"tabel433ganjil\",\"4.3.3.ganjil/4/jumlah\",\"4.3.3.ganjil/4/rata\")'></td>"+
	"<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_3_3_ganjil/5/no"+nextCount+"' onchange='updateNilai33ganjil("+nextCount+",4,\"tabel433ganjil\",\"4.3.3.ganjil/5/jumlah\",\"4.3.3.ganjil/5/rata\")'></td>"+
	"<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_3_3_ganjil/6/no"+nextCount+"' onchange='updateNilai33ganjil("+nextCount+",5,\"tabel433ganjil\",\"4.3.3.ganjil/6/jumlah\",\"4.3.3.ganjil/6/rata\")'></td>"+
	"<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_3_3_ganjil/7/no"+nextCount+"' onchange='updateNilai33ganjil("+nextCount+",6,\"tabel433ganjil\",\"4.3.3.ganjil/7/jumlah\",\"4.3.3.ganjil/7/rata\")'></td>"+
	"<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_3_3_ganjil/8/no"+nextCount+"' onchange='updateNilai33ganjil("+nextCount+",7,\"tabel433ganjil\",\"4.3.3.ganjil/8/jumlah\",\"4.3.3.ganjil/8/rata\")'></td>"+
	"<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_3_3_ganjil/9/no"+nextCount+"' onchange='updateNilai33ganjil("+nextCount+",8,\"tabel433ganjil\",\"4.3.3.ganjil/9/jumlah\",\"4.3.3.ganjil/9/rata\")'></td>"+
	"<td></td>";
	addRow(idTable, idCounter, nextCount, htmlContent);
}

function addRow33genap(idTable,idCounter){
	rowCount=document.getElementById(""+idCounter).value;
	nextCount = parseInt(rowCount)+1;
	htmlContent = "<td>"+nextCount+"</td>"+
	"<td><select class='form-control' name='4_3_3_genap/2/no"+nextCount+"' id='4_3_3/2/no"+nextCount+"'>"+dropdowndosen+"</select></td>"+
	"<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_3_3_genap/3/no"+nextCount+"' onchange='updateNilai33genap("+nextCount+",2,\"tabel433genap\",\"4.3.3.genap/3/jumlah\",\"4.3.3.genap/3/rata\")'></td>"+
	"<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_3_3_genap/4/no"+nextCount+"' onchange='updateNilai33genap("+nextCount+",3,\"tabel433genap\",\"4.3.3.genap/4/jumlah\",\"4.3.3.genap/4/rata\")'></td>"+
	"<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_3_3_genap/5/no"+nextCount+"' onchange='updateNilai33genap("+nextCount+",4,\"tabel433genap\",\"4.3.3.genap/5/jumlah\",\"4.3.3.genap/5/rata\")'></td>"+
	"<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_3_3_genap/6/no"+nextCount+"' onchange='updateNilai33genap("+nextCount+",5,\"tabel433genap\",\"4.3.3.genap/6/jumlah\",\"4.3.3.genap/6/rata\")'></td>"+
	"<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_3_3_genap/7/no"+nextCount+"' onchange='updateNilai33genap("+nextCount+",6,\"tabel433genap\",\"4.3.3.genap/7/jumlah\",\"4.3.3.genap/7/rata\")'></td>"+
	"<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_3_3_genap/8/no"+nextCount+"' onchange='updateNilai33genap("+nextCount+",7,\"tabel433genap\",\"4.3.3.genap/8/jumlah\",\"4.3.3.genap/8/rata\")'></td>"+
	"<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_3_3_genap/9/no"+nextCount+"' onchange='updateNilai33genap("+nextCount+",8,\"tabel433genap\",\"4.3.3.genap/9/jumlah\",\"4.3.3.genap/9/rata\")'></td>"+
	"<td></td>";
	addRow(idTable, idCounter, nextCount, htmlContent);
}


function updateNilai33ganjil(rowIndex,columnIndex,idTable,idOutputJumlah,idOutputRata){
	sumColumn(rowIndex,idTable);
	sumRowInAddRowId(columnIndex,idTable,idOutputJumlah);
	sumRowInAddRowIdInnerHTML(9,idTable,"4.3.3.ganjil/10/jumlah");
	AverageRowInAddRow(columnIndex,idTable,idOutputRata);
	AverageRowInAddRowInnerHTML(9,idTable,"4.3.3.ganjil/10/rata");
	updateNilai33();
}

function updateNilai33genap(rowIndex,columnIndex,idTable,idOutputJumlah,idOutputRata){
	sumColumn(rowIndex,idTable);
	sumRowInAddRowId(columnIndex,idTable,idOutputJumlah);
	sumRowInAddRowIdInnerHTML(9,idTable,"4.3.3.genap/10/jumlah");
	AverageRowInAddRow(columnIndex,idTable,idOutputRata);
	AverageRowInAddRowInnerHTML(9,idTable,"4.3.3.genap/10/rata");
	updateNilai33();
}

function updateNilai33(){
	//4.3.3
	rataganjil=parseFloat(document.getElementById("4.3.3.ganjil/10/rata").innerHTML);
	ratagenap=parseFloat(document.getElementById("4.3.3.genap/10/rata").innerHTML);
	fte = roundToTwo((rataganjil+ratagenap)/2);
	document.getElementById("nilai/4.3.3/1").innerHTML = fte;

	if (fte<=5 || fte>=21) {
		nilai = 1;
	}else if(fte<11){
		nilai = roundToTwo((fte-3)/2);
	}else if (fte<=13) {
		nilai = 4;
	}else{
		nilai = roundToTwo((71-3*fte)/8);
	}
	document.getElementById("nilai/4.3.3/nilai").innerHTML = nilai;
	document.getElementById("nilai/4.3.3/nilaihid").value = nilai;
}

function addtbody34ganjil(idTable,idCounter){
	rowCount=document.getElementById(""+idCounter).value;
	nextCount = parseInt(rowCount)+1;
	htmlContent = "<tbody>"+
	"<input type='hidden' id='4.3.4.ganjil/counter/no"+nextCount+"' value='1' name='4_3_4_ganjil/counter/no"+nextCount+"'>"+
	"<tr>"+
	"<td rowspan='2'>"+nextCount+"</td>"+
	"<td rowspan='2'><select class='form-control' name='4_3_4_ganjil/2/no"+nextCount+"/sub1' id='4.3.4.ganjil/2/no"+nextCount+"'>"+dropdowndosen+"</select></td>"+
	"<td rowspan='2'><textarea rows='1' style='width:100%;' name='4_3_4_ganjil/3/no"+nextCount+"/sub1'></textarea></td>"+
	"<td><input type='text' class='form-control' name='4_3_4_ganjil/4/no"+nextCount+"/sub1'></td>"+
	"<td><textarea rows='1' style='width:100%;' name='4_3_4_ganjil/5/no"+nextCount+"/sub1'></textarea></td>"+
	"<td><input type='text' class='form-control' name='4_3_4_ganjil/6/no"+nextCount+"/sub1'></td>"+
	"<td><input type='text' class='form-control' onchange='updateNilai3435(6,\"table434ganjil\",\"4.3.4.ganjil/counter\",\"4.3.4.ganjil/7/jumlah\")' pattern='[0-9]+([\.][0-9]+)?' name='4_3_4_ganjil/7/no"+nextCount+"/sub1'></td>"+
	"<td><input type='text' class='form-control' onchange='updateNilai3435(7,\"table434ganjil\",\"4.3.4.ganjil/counter\",\"4.3.4.ganjil/8/jumlah\")' pattern='[0-9]+([\.][0-9]+)?' name='4_3_4_ganjil/8/no"+nextCount+"/sub1'></td>"+
	"</tr>"+
	"<tr>"+
	"<td colspan='5'><button type='button' onclick='addrowtbody34ganjil(\"table434ganjil\","+nextCount+",\"4.3.4.ganjil/counter/no"+nextCount+"\")'><span class='glyphicon glyphicon-plus'></span></button>"+
	"<button type='button' onclick='removerowtbody(\"table434ganjil\","+nextCount+",\"4.3.4.ganjil/counter/no"+nextCount+"\",3)'><span class='glyphicon glyphicon-minus'></span></button></td>"+
	"</tr>"+
	"</tbody>";
	addTbody(idTable, idCounter, nextCount, htmlContent);
}

function addrowtbody34ganjil(idTable, tbodyNode, idCounter){
	rowCount=document.getElementById(""+idCounter).value;
	nextCount = parseInt(rowCount)+1;
	htmlContent ="<td><input type='text' class='form-control' name='4_3_4_ganjil/4/no"+tbodyNode+"/sub"+nextCount+"'></td>"+
	"<td><textarea rows='1' style='width:100%;' name='4_3_4_ganjil/5/no"+tbodyNode+"/sub"+nextCount+"'></textarea></td>"+
	"<td><input type='text' class='form-control' name='4_3_4_ganjil/6/no"+tbodyNode+"/sub"+nextCount+"'></td>"+
	"<td><input type='text' class='form-control' onchange='updateNilai3435(6,\"table434ganjil\",\"4.3.4.ganjil/counter\",\"4.3.4.ganjil/7/jumlah\")' pattern='[0-9]+([\.][0-9]+)?' name='4_3_4_ganjil/7/no"+tbodyNode+"/sub"+nextCount+"'></td>"+
	"<td><input type='text' class='form-control' onchange='updateNilai3435(7,\"table434ganjil\",\"4.3.4.ganjil/counter\",\"4.3.4.ganjil/8/jumlah\")' pattern='[0-9]+([\.][0-9]+)?' name='4_3_4_ganjil/8/no"+tbodyNode+"/sub"+nextCount+"'></td>";
	addRowTbody(idTable, idCounter, nextCount, htmlContent, tbodyNode);
	var tableElem = window.document.getElementById(""+idTable);
	var tableBody = tableElem.getElementsByTagName("tbody").item(tbodyNode);
	var trElem = tableBody.getElementsByTagName("tr").item(0);
	var tdElem1 = trElem.getElementsByTagName("td").item(0);
	var tdElem2 = trElem.getElementsByTagName("td").item(1);
	var tdElem3 = trElem.getElementsByTagName("td").item(2);
	tdElem1.rowSpan = nextCount+1;
	tdElem2.rowSpan = nextCount+1;
	tdElem3.rowSpan = nextCount+1;
}

function addtbody34genap(idTable,idCounter){
	rowCount=document.getElementById(""+idCounter).value;
	nextCount = parseInt(rowCount)+1;
	htmlContent = "<tbody>"+
	"<input type='hidden' id='4.3.4.genap/counter/no"+nextCount+"' value='1' name='4_3_4_genap/counter/no"+nextCount+"'>"+
	"<tr>"+
	"<td rowspan='2'>"+nextCount+"</td>"+
	"<td rowspan='2'><select class='form-control' name='4_3_4_genap/2/no"+nextCount+"/sub1' id='4.3.4.genap/2/no"+nextCount+"'>"+dropdowndosen+"</select></td>"+
	"<td rowspan='2'><textarea rows='1' style='width:100%;' name='4_3_4_genap/3/no"+nextCount+"/sub1'></textarea></td>"+
	"<td><input type='text' class='form-control' name='4_3_4_genap/4/no"+nextCount+"/sub1'></td>"+
	"<td><textarea rows='1' style='width:100%;' name='4_3_4_genap/5/no"+nextCount+"/sub1'></textarea></td>"+
	"<td><input type='text' class='form-control' name='4_3_4_genap/6/no"+nextCount+"/sub1'></td>"+
	"<td><input type='text' class='form-control' onchange='updateNilai3435(6,\"table434genap\",\"4.3.4.genap/counter\",\"4.3.4.genap/7/jumlah\")' pattern='[0-9]+([\.][0-9]+)?' name='4_3_4_genap/7/no"+nextCount+"/sub1'></td>"+
	"<td><input type='text' class='form-control' onchange='updateNilai3435(7,\"table434genap\",\"4.3.4.genap/counter\",\"4.3.4.genap/8/jumlah\")' pattern='[0-9]+([\.][0-9]+)?' name='4_3_4_genap/8/no"+nextCount+"/sub1'></td>"+
	"</tr>"+
	"<tr>"+
	"<td colspan='5'><button type='button' onclick='addrowtbody34genap(\"table434genap\","+nextCount+",\"4.3.4.genap/counter/no"+nextCount+"\")'><span class='glyphicon glyphicon-plus'></span></button>"+
	"<button type='button' onclick='removerowtbody(\"table434genap\","+nextCount+",\"4.3.4.genap/counter/no"+nextCount+"\",3)'><span class='glyphicon glyphicon-minus'></span></button></td>"+
	"</tr>"+
	"</tbody>";
	addTbody(idTable, idCounter, nextCount, htmlContent);
}

function addrowtbody34genap(idTable, tbodyNode, idCounter){
	rowCount=document.getElementById(""+idCounter).value;
	nextCount = parseInt(rowCount)+1;
	htmlContent ="<td><input type='text' class='form-control' name='4_3_4_genap/4/no"+tbodyNode+"/sub"+nextCount+"'></td>"+
	"<td><textarea rows='1' style='width:100%;' name='4_3_4_genap/5/no"+tbodyNode+"/sub"+nextCount+"'></textarea></td>"+
	"<td><input type='text' class='form-control' name='4_3_4_genap/6/no"+tbodyNode+"/sub"+nextCount+"'></td>"+
	"<td><input type='text' class='form-control' onchange='updateNilai3435(6,\"table434genap\",\"4.3.4.genap/counter\",\"4.3.4.genap/7/jumlah\")' pattern='[0-9]+([\.][0-9]+)?' name='4_3_4_genap/7/no"+tbodyNode+"/sub"+nextCount+"'></td>"+
	"<td><input type='text' class='form-control' onchange='updateNilai3435(7,\"table434genap\",\"4.3.4.genap/counter\",\"4.3.4.genap/8/jumlah\")' pattern='[0-9]+([\.][0-9]+)?' name='4_3_4_genap/8/no"+tbodyNode+"/sub"+nextCount+"'></td>";
	addRowTbody(idTable, idCounter, nextCount, htmlContent, tbodyNode);
	var tableElem = window.document.getElementById(""+idTable);
	var tableBody = tableElem.getElementsByTagName("tbody").item(tbodyNode);
	var trElem = tableBody.getElementsByTagName("tr").item(0);
	var tdElem1 = trElem.getElementsByTagName("td").item(0);
	var tdElem2 = trElem.getElementsByTagName("td").item(1);
	var tdElem3 = trElem.getElementsByTagName("td").item(2);
	tdElem1.rowSpan = nextCount+1;
	tdElem2.rowSpan = nextCount+1;
	tdElem3.rowSpan = nextCount+1;
}

function addtbody35ganjil(idTable,idCounter){
	rowCount=document.getElementById(""+idCounter).value;
	nextCount = parseInt(rowCount)+1;
	htmlContent = "<tbody>"+
	"<input type='hidden' id='4.3.5.ganjil/counter/no"+nextCount+"' value='1' name='4_3_5_ganjil/counter/no"+nextCount+"'>"+
	"<tr>"+
	"<td rowspan='2'>"+nextCount+"</td>"+
	"<td rowspan='2'><select class='form-control' name='4_3_5_ganjil/2/no"+nextCount+"/sub1' id='4.3.5.ganjil/2/no"+nextCount+"'>"+dropdowndosenxps+"</select></td>"+
	"<td rowspan='2'><textarea rows='1' style='width:100%;' name='4_3_5_ganjil/3/no"+nextCount+"/sub1'></textarea></td>"+
	"<td><input type='text' class='form-control' name='4_3_5_ganjil/4/no"+nextCount+"/sub1'></td>"+
	"<td><textarea rows='1' style='width:100%;' name='4_3_5_ganjil/5/no"+nextCount+"/sub1'></textarea></td>"+
	"<td><input type='text' class='form-control' name='4_3_5_ganjil/6/no"+nextCount+"/sub1'></td>"+
	"<td><input type='text' class='form-control' onchange='updateNilai3435(6,\"table435ganjil\",\"4.3.5.ganjil/counter\",\"4.3.5.ganjil/7/jumlah\")' pattern='[0-9]+([\.][0-9]+)?' name='4_3_5_ganjil/7/no"+nextCount+"/sub1'></td>"+
	"<td><input type='text' class='form-control' onchange='updateNilai3435(7,\"table435ganjil\",\"4.3.5.ganjil/counter\",\"4.3.5.ganjil/8/jumlah\")' pattern='[0-9]+([\.][0-9]+)?' name='4_3_5_ganjil/8/no"+nextCount+"/sub1'></td>"+
	"</tr>"+
	"<tr>"+
	"<td colspan='5'><button type='button' onclick='addrowtbody35ganjil(\"table435ganjil\","+nextCount+",\"4.3.5.ganjil/counter/no"+nextCount+"\")'><span class='glyphicon glyphicon-plus'></span></button>"+
	"<button type='button' onclick='removerowtbody(\"table435ganjil\","+nextCount+",\"4.3.5.ganjil/counter/no"+nextCount+"\",3)'><span class='glyphicon glyphicon-minus'></span></button></td>"+
	"</tr>"+
	"</tbody>";
	addTbody(idTable, idCounter, nextCount, htmlContent);
}

function addrowtbody35ganjil(idTable, tbodyNode, idCounter){
	rowCount=document.getElementById(""+idCounter).value;
	nextCount = parseInt(rowCount)+1;
	htmlContent ="<td><input type='text' class='form-control' name='4_3_5_ganjil/4/no"+tbodyNode+"/sub"+nextCount+"'></td>"+
	"<td><textarea rows='1' style='width:100%;' name='4_3_5_ganjil/5/no"+tbodyNode+"/sub"+nextCount+"'></textarea></td>"+
	"<td><input type='text' class='form-control' name='4_3_5_ganjil/6/no"+tbodyNode+"/sub"+nextCount+"'></td>"+
	"<td><input type='text' class='form-control' onchange='updateNilai3435(6,\"table435ganjil\",\"4.3.5.ganjil/counter\",\"4.3.5.ganjil/7/jumlah\")' pattern='[0-9]+([\.][0-9]+)?' name='4_3_5_ganjil/7/no"+tbodyNode+"/sub"+nextCount+"'></td>"+
	"<td><input type='text' class='form-control' onchange='updateNilai3435(7,\"table435ganjil\",\"4.3.5.ganjil/counter\",\"4.3.5.ganjil/8/jumlah\")' pattern='[0-9]+([\.][0-9]+)?' name='4_3_5_ganjil/8/no"+tbodyNode+"/sub"+nextCount+"'></td>";
	addRowTbody(idTable, idCounter, nextCount, htmlContent, tbodyNode);
	var tableElem = window.document.getElementById(""+idTable);
	var tableBody = tableElem.getElementsByTagName("tbody").item(tbodyNode);
	var trElem = tableBody.getElementsByTagName("tr").item(0);
	var tdElem1 = trElem.getElementsByTagName("td").item(0);
	var tdElem2 = trElem.getElementsByTagName("td").item(1);
	var tdElem3 = trElem.getElementsByTagName("td").item(2);
	tdElem1.rowSpan = nextCount+1;
	tdElem2.rowSpan = nextCount+1;
	tdElem3.rowSpan = nextCount+1;
}

function addtbody35genap(idTable,idCounter){
	rowCount=document.getElementById(""+idCounter).value;
	nextCount = parseInt(rowCount)+1;
	htmlContent = "<tbody>"+
	"<input type='hidden' id='4.3.5.genap/counter/no"+nextCount+"' value='1' name='4_3_5_genap/counter/no"+nextCount+"'>"+
	"<tr>"+
	"<td rowspan='2'>"+nextCount+"</td>"+
	"<td rowspan='2'><select class='form-control' name='4_3_5_genap/2/no"+nextCount+"/sub1' id='4.3.5.genap/2/no"+nextCount+"'>"+dropdowndosenxps+"</select></td>"+
	"<td rowspan='2'><textarea rows='1' style='width:100%;' name='4_3_5_genap/3/no"+nextCount+"/sub1'></textarea></td>"+
	"<td><input type='text' class='form-control' name='4_3_5_genap/4/no"+nextCount+"/sub1'></td>"+
	"<td><textarea rows='1' style='width:100%;' name='4_3_5_genap/5/no"+nextCount+"/sub1'></textarea></td>"+
	"<td><input type='text' class='form-control' name='4_3_5_genap/6/no"+nextCount+"/sub1'></td>"+
	"<td><input type='text' class='form-control' onchange='updateNilai3435(6,\"table435genap\",\"4.3.5.genap/counter\",\"4.3.5.genap/7/jumlah\")' pattern='[0-9]+([\.][0-9]+)?' name='4_3_5_genap/7/no"+nextCount+"/sub1'></td>"+
	"<td><input type='text' class='form-control' onchange='updateNilai3435(7,\"table435genap\",\"4.3.5.genap/counter\",\"4.3.5.genap/8/jumlah\")' pattern='[0-9]+([\.][0-9]+)?' name='4_3_5_genap/8/no"+nextCount+"/sub1'></td>"+
	"</tr>"+
	"<tr>"+
	"<td colspan='5'><button type='button' onclick='addrowtbody35genap(\"table435genap\","+nextCount+",\"4.3.5.genap/counter/no"+nextCount+"\")'><span class='glyphicon glyphicon-plus'></span></button>"+
	"<button type='button' onclick='removerowtbody(\"table435genap\","+nextCount+",\"4.3.5.genap/counter/no"+nextCount+"\",3)'><span class='glyphicon glyphicon-minus'></span></button></td>"+
	"</tr>"+
	"</tbody>";
	addTbody(idTable, idCounter, nextCount, htmlContent);
}

function addrowtbody35genap(idTable, tbodyNode, idCounter){
	rowCount=document.getElementById(""+idCounter).value;
	nextCount = parseInt(rowCount)+1;
	htmlContent ="<td><input type='text' class='form-control' name='4_3_5_genap/4/no"+tbodyNode+"/sub"+nextCount+"'></td>"+
	"<td><textarea rows='1' style='width:100%;' name='4_3_5_genap/5/no"+tbodyNode+"/sub"+nextCount+"'></textarea></td>"+
	"<td><input type='text' class='form-control' name='4_3_5_genap/6/no"+tbodyNode+"/sub"+nextCount+"'></td>"+
	"<td><input type='text' class='form-control' onchange='updateNilai3435(6,\"table435genap\",\"4.3.5.genap/counter\",\"4.3.5.genap/7/jumlah\")' pattern='[0-9]+([\.][0-9]+)?' name='4_3_5_genap/7/no"+tbodyNode+"/sub"+nextCount+"'></td>"+
	"<td><input type='text' class='form-control' onchange='updateNilai3435(7,\"table435genap\",\"4.3.5.genap/counter\",\"4.3.5.genap/8/jumlah\")' pattern='[0-9]+([\.][0-9]+)?' name='4_3_5_genap/8/no"+tbodyNode+"/sub"+nextCount+"'></td>";
	addRowTbody(idTable, idCounter, nextCount, htmlContent, tbodyNode);
	var tableElem = window.document.getElementById(""+idTable);
	var tableBody = tableElem.getElementsByTagName("tbody").item(tbodyNode);
	var trElem = tableBody.getElementsByTagName("tr").item(0);
	var tdElem1 = trElem.getElementsByTagName("td").item(0);
	var tdElem2 = trElem.getElementsByTagName("td").item(1);
	var tdElem3 = trElem.getElementsByTagName("td").item(2);
	tdElem1.rowSpan = nextCount+1;
	tdElem2.rowSpan = nextCount+1;
	tdElem3.rowSpan = nextCount+1;
}

function addtbody42ganjil(idTable,idCounter){
	rowCount=document.getElementById(""+idCounter).value;
	nextCount = parseInt(rowCount)+1;
	htmlContent = "<tbody>"+
	"<input type='hidden' id='4.4.2.ganjil/counter/no"+nextCount+"' value='1' name='4_4_2_ganjil/counter/no"+nextCount+"'>"+
	"<tr>"+
	"<td rowspan='2'>"+nextCount+"</td>"+
	"<td rowspan='2'><select class='form-control' name='4_4_2_ganjil/2/no"+nextCount+"/sub1' id='4.4.2.ganjil/2/no"+nextCount+"'>"+dropdowndosenxtp+"</select></td>"+
	"<td rowspan='2'><textarea rows='1' style='width:100%;' name='4_4_2_ganjil/3/no"+nextCount+"/sub1'></textarea></td>"+
	"<td><input type='text' class='form-control' name='4_4_2_ganjil/4/no"+nextCount+"/sub1'></td>"+
	"<td><textarea rows='1' style='width:100%;' name='4_4_2_ganjil/5/no"+nextCount+"/sub1'></textarea></td>"+
	"<td><input type='text' class='form-control' name='4_4_2_ganjil/6/no"+nextCount+"/sub1'></td>"+
	"<td><input type='text' class='form-control' onchange='updateNilai3442(6,\"table442ganjil\",\"4.4.2.ganjil/counter\",\"4.4.2.ganjil/7/jumlah\")' pattern='[0-9]+([\.][0-9]+)?' name='4_4_2_ganjil/7/no"+nextCount+"/sub1'></td>"+
	"<td><input type='text' class='form-control' onchange='updateNilai3442(7,\"table442ganjil\",\"4.4.2.ganjil/counter\",\"4.4.2.ganjil/8/jumlah\")' pattern='[0-9]+([\.][0-9]+)?' name='4_4_2_ganjil/8/no"+nextCount+"/sub1'></td>"+
	"</tr>"+
	"<tr>"+
	"<td colspan='5'><button type='button' onclick='addrowtbody42ganjil(\"table442ganjil\","+nextCount+",\"4.4.2.ganjil/counter/no"+nextCount+"\")'><span class='glyphicon glyphicon-plus'></span></button>"+
	"<button type='button' onclick='removerowtbody(\"table442ganjil\","+nextCount+",\"4.4.2.ganjil/counter/no"+nextCount+"\",3)'><span class='glyphicon glyphicon-minus'></span></button></td>"+
	"</tr>"+
	"</tbody>";
	addTbody(idTable, idCounter, nextCount, htmlContent);
}

function addrowtbody42ganjil(idTable, tbodyNode, idCounter){
	rowCount=document.getElementById(""+idCounter).value;
	nextCount = parseInt(rowCount)+1;
	htmlContent ="<td><input type='text' class='form-control' name='4_4_2_ganjil/4/no"+tbodyNode+"/sub"+nextCount+"'></td>"+
	"<td><textarea rows='1' style='width:100%;' name='4_4_2_ganjil/5/no"+tbodyNode+"/sub"+nextCount+"'></textarea></td>"+
	"<td><input type='text' class='form-control' name='4_4_2_ganjil/6/no"+tbodyNode+"/sub"+nextCount+"'></td>"+
	"<td><input type='text' class='form-control' onchange='updateNilai3442(6,\"table442ganjil\",\"4.4.2.ganjil/counter\",\"4.4.2.ganjil/7/jumlah\")' pattern='[0-9]+([\.][0-9]+)?' name='4_4_2_ganjil/7/no"+tbodyNode+"/sub"+nextCount+"'></td>"+
	"<td><input type='text' class='form-control' onchange='updateNilai3442(7,\"table442ganjil\",\"4.4.2.ganjil/counter\",\"4.4.2.ganjil/8/jumlah\")' pattern='[0-9]+([\.][0-9]+)?' name='4_4_2_ganjil/8/no"+tbodyNode+"/sub"+nextCount+"'></td>";
	addRowTbody(idTable, idCounter, nextCount, htmlContent, tbodyNode);
	var tableElem = window.document.getElementById(""+idTable);
	var tableBody = tableElem.getElementsByTagName("tbody").item(tbodyNode);
	var trElem = tableBody.getElementsByTagName("tr").item(0);
	var tdElem1 = trElem.getElementsByTagName("td").item(0);
	var tdElem2 = trElem.getElementsByTagName("td").item(1);
	var tdElem3 = trElem.getElementsByTagName("td").item(2);
	tdElem1.rowSpan = nextCount+1;
	tdElem2.rowSpan = nextCount+1;
	tdElem3.rowSpan = nextCount+1;
}

function addtbody42genap(idTable,idCounter){
	rowCount=document.getElementById(""+idCounter).value;
	nextCount = parseInt(rowCount)+1;
	htmlContent = "<tbody>"+
	"<input type='hidden' id='4.4.2.genap/counter/no"+nextCount+"' value='1' name='4_4_2_genap/counter/no"+nextCount+"'>"+
	"<tr>"+
	"<td rowspan='2'>"+nextCount+"</td>"+
	"<td rowspan='2'><select class='form-control' name='4_4_2_genap/2/no"+nextCount+"/sub1' id='4.4.2.genap/2/no"+nextCount+"'>"+dropdowndosenxps+"</select></td>"+
	"<td rowspan='2'><textarea rows='1' style='width:100%;' name='4_4_2_genap/3/no"+nextCount+"/sub1'></textarea></td>"+
	"<td><input type='text' class='form-control' name='4_4_2_genap/4/no"+nextCount+"/sub1'></td>"+
	"<td><textarea rows='1' style='width:100%;' name='4_4_2_genap/5/no"+nextCount+"/sub1'></textarea></td>"+
	"<td><input type='text' class='form-control' name='4_4_2_genap/6/no"+nextCount+"/sub1'></td>"+
	"<td><input type='text' class='form-control' onchange='updateNilai3442(6,\"table442genap\",\"4.4.2.genap/counter\",\"4.4.2.genap/7/jumlah\")' pattern='[0-9]+([\.][0-9]+)?' name='4_4_2_genap/7/no"+nextCount+"/sub1'></td>"+
	"<td><input type='text' class='form-control' onchange='updateNilai3442(7,\"table442genap\",\"4.4.2.genap/counter\",\"4.4.2.genap/8/jumlah\")' pattern='[0-9]+([\.][0-9]+)?' name='4_4_2_genap/8/no"+nextCount+"/sub1'></td>"+
	"</tr>"+
	"<tr>"+
	"<td colspan='5'><button type='button' onclick='addrowtbody42genap(\"table442genap\","+nextCount+",\"4.4.2.genap/counter/no"+nextCount+"\")'><span class='glyphicon glyphicon-plus'></span></button>"+
	"<button type='button' onclick='removerowtbody(\"table442genap\","+nextCount+",\"4.4.2.genap/counter/no"+nextCount+"\",3)'><span class='glyphicon glyphicon-minus'></span></button></td>"+
	"</tr>"+
	"</tbody>";
	addTbody(idTable, idCounter, nextCount, htmlContent);
}

function addrowtbody42genap(idTable, tbodyNode, idCounter){
	rowCount=document.getElementById(""+idCounter).value;
	nextCount = parseInt(rowCount)+1;
	htmlContent ="<td><input type='text' class='form-control' name='4_4_2_genap/4/no"+tbodyNode+"/sub"+nextCount+"'></td>"+
	"<td><textarea rows='1' style='width:100%;' name='4_4_2_genap/5/no"+tbodyNode+"/sub"+nextCount+"'></textarea></td>"+
	"<td><input type='text' class='form-control' name='4_4_2_genap/6/no"+tbodyNode+"/sub"+nextCount+"'></td>"+
	"<td><input type='text' class='form-control' onchange='updateNilai3442(6,\"table442genap\",\"4.4.2.genap/counter\",\"4.4.2.genap/7/jumlah\")' pattern='[0-9]+([\.][0-9]+)?' name='4_4_2_genap/7/no"+tbodyNode+"/sub"+nextCount+"'></td>"+
	"<td><input type='text' class='form-control' onchange='updateNilai3442(7,\"table442genap\",\"4.4.2.genap/counter\",\"4.4.2.genap/8/jumlah\")' pattern='[0-9]+([\.][0-9]+)?' name='4_4_2_genap/8/no"+tbodyNode+"/sub"+nextCount+"'></td>";
	addRowTbody(idTable, idCounter, nextCount, htmlContent, tbodyNode);
	var tableElem = window.document.getElementById(""+idTable);
	var tableBody = tableElem.getElementsByTagName("tbody").item(tbodyNode);
	var trElem = tableBody.getElementsByTagName("tr").item(0);
	var tdElem1 = trElem.getElementsByTagName("td").item(0);
	var tdElem2 = trElem.getElementsByTagName("td").item(1);
	var tdElem3 = trElem.getElementsByTagName("td").item(2);
	tdElem1.rowSpan = nextCount+1;
	tdElem2.rowSpan = nextCount+1;
	tdElem3.rowSpan = nextCount+1;
}

function updateNilai3435(columnIndex,idTable,idCounter,idOutput){
	sumRowInAddRowIdSubTbody(columnIndex,idTable,idCounter,idOutput, 3);

//4.3.4/5.b
jumlahRencanaPSganjil = parseFloat(document.getElementById("4.3.4.ganjil/7/jumlah").innerHTML);
jumlahLaksanaPSganjil = parseFloat(document.getElementById("4.3.4.ganjil/8/jumlah").innerHTML);
jumlahRencanaPSgenap = parseFloat(document.getElementById("4.3.4.genap/7/jumlah").innerHTML);
jumlahLaksanaPSgenap = parseFloat(document.getElementById("4.3.4.genap/8/jumlah").innerHTML);

jumlahRencanaXPSganjil = parseFloat(document.getElementById("4.3.5.ganjil/7/jumlah").innerHTML);
jumlahLaksanaXPSganjil = parseFloat(document.getElementById("4.3.5.ganjil/8/jumlah").innerHTML);
jumlahRencanaXPSgenap = parseFloat(document.getElementById("4.3.5.genap/7/jumlah").innerHTML);
jumlahLaksanaXPSgenap = parseFloat(document.getElementById("4.3.5.genap/8/jumlah").innerHTML);

hadirRencana = jumlahRencanaPSganjil+jumlahRencanaPSgenap+jumlahRencanaXPSganjil+jumlahRencanaXPSgenap;
document.getElementById("nilai/4.3.4/5.b/1").innerHTML=hadirRencana;
hadirLaksana = jumlahLaksanaPSganjil+jumlahLaksanaPSgenap+jumlahLaksanaXPSganjil+jumlahLaksanaXPSgenap;
document.getElementById("nilai/4.3.4/5.b/2").innerHTML=hadirLaksana;

presentase = roundToTwo(hadirLaksana/hadirRencana);
document.getElementById("nilai/4.3.4/5.b/3").innerHTML=presentase*100+"%";

if (presentase>=0.95) {
	nilai=4;
}else if (presentase>0.6){
	nilai=roundToTwo(((80*presentase)-48)/7);
}else{
	nilai=0;
}

document.getElementById("nilai/4.3.4/5.b/nilai").innerHTML = nilai;
document.getElementById("nilai/4.3.4/5.b/nilaihid").value = nilai;
}

function updateNilai42b(columnIndex,idTable,idCounter,idOutput){
sumRowInAddRowIdSubTbody(columnIndex,idTable,idCounter,idOutput, 3);

//4.4.2.b
jumlahRencanaPSganjil = parseFloat(document.getElementById("4.4.2.ganjil/7/jumlah").innerHTML);
jumlahLaksanaPSganjil = parseFloat(document.getElementById("4.4.2.ganjil/8/jumlah").innerHTML);
jumlahRencanaPSgenap = parseFloat(document.getElementById("4.4.2.genap/7/jumlah").innerHTML);
jumlahLaksanaPSgenap = parseFloat(document.getElementById("4.4.2.genap/8/jumlah").innerHTML);

hadirRencana = jumlahRencanaPSganjil+jumlahRencanaPSgenap;
document.getElementById("nilai/4.4.2.b/1").innerHTML=hadirRencana;
hadirLaksana = jumlahLaksanaPSganjil+jumlahLaksanaPSgenap;
document.getElementById("nilai/4.4.2.b/2").innerHTML=hadirLaksana;

presentase = roundToTwo(hadirLaksana/hadirRencana);
document.getElementById("nilai/4.4.2.b/3").innerHTML=presentase*100+"%";

if (presentase>=0.95 || isNaN(presentase)) {
	nilai=4;
}else if (presentase>0.6){
	nilai=roundToTwo(((80*presentase)-48)/7);
}else{
	nilai=0;
}

document.getElementById("nilai/4.4.2.b/nilai").innerHTML = nilai;
document.getElementById("nilai/4.4.2.b/nilaihid").value = nilai;
}

function addRow51(idTable,idCounter){
	rowCount=document.getElementById(""+idCounter).value;
	nextCount = parseInt(rowCount)+1;
	htmlContent = "<td>"+nextCount+"</td>"+
	"<td><textarea rows='2' style='width:100%;' onchange='updateNilai51()' name='4_5_1/2/no"+nextCount+"' id='4.5.1/2/no"+nextCount+"'></textarea></td>"+
	"<td><textarea rows='2' style='width:100%;' name='4_5_1/3/no"+nextCount+"'></textarea></td>"+
	"<td><textarea rows='2' style='width:100%;' name='4_5_1/4/no"+nextCount+"'></textarea></td>";
	addRow(idTable, idCounter, nextCount, htmlContent);
}

function updateNilai51(){
	idCounter = "4.5.1/counter";
	rowCount = parseInt(document.getElementById(""+idCounter).value);
	characterLimit = 5;
	filledCount=0;
	for(i=1;i<=rowCount;i++){
		filled = document.getElementById("4.5.1/2/no"+i).value.length;
		if (filled>characterLimit) {
			filledCount++;
		}
	}
	document.getElementById("nilai/4.5.1/1").innerHTML = filledCount;

	if (filledCount>=12) {
		nilai=4;
	}else{
		nilai=roundToTwo(1+filledCount/4);
	}

	document.getElementById("nilai/4.5.1/nilai").innerHTML = nilai;
	document.getElementById("nilai/4.5.1/nilaihid").value = nilai;
}

function addRow52(idTable,idCounter){
	rowCount=document.getElementById(""+idCounter).value;
	nextCount = parseInt(rowCount)+1;
	htmlContent = "<td>"+nextCount+"</td>"+
	"<td><select class='form-control' name='4_5_2/2/no"+nextCount+"'>"+dropdowndosen+"</select></td>"+
	"<td>"+
	"<select class='form-control' onchange='updateNilai52()' name='4_5_2/3/no"+nextCount+"'>"+
	"<option>-</option>"+
	"<option>S1</option>"+
	"<option>S2</option>"+
	"<option>S3</option>"+
	"</select>"+
	"</td>"+
	"<td><textarea rows='1' style='width:100%;' name='4_5_2/4/no"+nextCount+"'></textarea></td>"+
	"<td><textarea rows='1' style='width:100%;' name='4_5_2/5/no"+nextCount+"'></textarea></td>"+
	"<td><textarea rows='1' style='width:100%;' name='4_5_2/6/no"+nextCount+"'></textarea></td>"+
	"<td><textarea rows='1' style='width:100%;' name='4_5_2/7/no"+nextCount+"'></textarea></td>";
	addRow(idTable, idCounter, nextCount, htmlContent);
}

function updateNilai52(){
	idCounter = "4.5.2/counter";
	idTable = "tabel452";
	rowCount = parseInt(document.getElementById(""+idCounter).value);

	persentaseS2S3 = parseFloat(document.getElementById("nilai/4.3.1.a/6").innerHTML);
	persentaseS3 = parseFloat(document.getElementById("nilai/4.3.1.b/3").innerHTML);

	document.getElementById("nilai/4.5.2/1").innerHTML = persentaseS2S3+"%";
	document.getElementById("nilai/4.5.2/2").innerHTML = persentaseS3+"%";

	countS2 = 0;
	countS3 = 0;
	for(i=1;i<=rowCount;i++){
		jenjang=retrieveCellInputValue(2,idTable,0,i);
		if (jenjang=="S2") {
			countS2++;
		}else if (jenjang=="S3") {
			countS3++;
		}
	}

	document.getElementById("nilai/4.5.2/3").innerHTML = countS2;
	document.getElementById("nilai/4.5.2/4").innerHTML = countS3;

	SD = roundToTwo(0.75*countS2+1.25*countS3);
	document.getElementById("nilai/4.5.2/5").innerHTML = SD;

	if (persentaseS2S3>90 || persentaseS3>40 || SD>=4) {
		nilai=4;
	}else{
		nilai=SD;
	}

	document.getElementById("nilai/4.5.2/nilai").innerHTML = nilai;
	document.getElementById("nilai/4.5.2/nilaihid").value = nilai;

}

function addtbody53(idTable,idCounter){
	rowCount=document.getElementById(""+idCounter).value;
	nextCount = parseInt(rowCount)+1;
	htmlContent = "<tbody>"+
	"<input type='hidden' id='4.5.3/counter/no"+nextCount+"' value='1' name='4_5_3/counter/no"+nextCount+"'>"+
	"<tr>"+
	"<td rowspan='2'>"+nextCount+"</td>"+
	"<td rowspan='2'><select class='form-control' name='4_5_3/2/no"+nextCount+"/sub1' id='4.5.3/2/no"+nextCount+"'>"+dropdowndosen+"</select></td>"+
	"<td><textarea rows='2' style='width:100%;' name='4_5_3/3/no"+nextCount+"/sub1'></textarea></td>"+
	"<td><textarea rows='2' style='width:100%;' name='4_5_3/4/no"+nextCount+"/sub1'></textarea></td>"+
	"<td><textarea rows='2' style='width:100%;' name='4_5_3/5/no"+nextCount+"/sub1'></textarea></td>"+
	"<td><input type='checkbox' class='form-control' value='1' onchange='updateNilai53(5)' name='4_5_3/6/no"+nextCount+"/sub1'></td>"+
	"<td><input type='checkbox' class='form-control' value='1' onchange='updateNilai53(6)' name='4_5_3/7/no"+nextCount+"/sub1'></td>"+
	"</tr>"+
	"<tr>"+
	"<td colspan='6'><button type='button' onclick='addrowtbody53(\"table453\","+nextCount+",\"4.5.3/counter/no"+nextCount+"\")'><span class='glyphicon glyphicon-plus'></span></button>"+
	"<button type='button' onclick='removerowtbody(\"table453\","+nextCount+",\"4.5.3/counter/no"+nextCount+"\",2)'><span class='glyphicon glyphicon-minus'></span></button></td>"+
	"</tr>"+
	"</tbody>";
	addTbody(idTable, idCounter, nextCount, htmlContent);
}

function addrowtbody53(idTable, tbodyNode, idCounter){
	rowCount=document.getElementById(""+idCounter).value;
	nextCount = parseInt(rowCount)+1;
	htmlContent ="<td><textarea rows='2' style='width:100%;' name='4_5_3/3/no"+tbodyNode+"/sub"+nextCount+"'></textarea></td>"+
	"<td><textarea rows='2' style='width:100%;' name='4_5_3/4/no"+tbodyNode+"/sub"+nextCount+"'></textarea></td>"+
	"<td><textarea rows='2' style='width:100%;' name='4_5_3/5/no"+tbodyNode+"/sub"+nextCount+"'></textarea></td>"+
	"<td><input type='checkbox' class='form-control' value='1' onchange='updateNilai53(5)' name='4_5_3/6/no"+tbodyNode+"/sub"+nextCount+"'></td>"+
	"<td><input type='checkbox' class='form-control' value='1' onchange='updateNilai53(6)' name='4_5_3/7/no"+tbodyNode+"/sub"+nextCount+"'></td>";

	addRowTbody(idTable, idCounter, nextCount, htmlContent, tbodyNode);
	var tableElem = window.document.getElementById(""+idTable);
	var tableBody = tableElem.getElementsByTagName("tbody").item(tbodyNode);
	var trElem = tableBody.getElementsByTagName("tr").item(0);
	var tdElem1 = trElem.getElementsByTagName("td").item(0);
	var tdElem2 = trElem.getElementsByTagName("td").item(1);
	var tdElem3 = trElem.getElementsByTagName("td").item(2);
	tdElem1.rowSpan = nextCount+1;
	tdElem2.rowSpan = nextCount+1;
}

function updateNilai53(columnIndex){
	idCounter = "4.5.3/counter";
	idTable = "table453";
	column = columnIndex+1;
	idOutput = "4.5.3/"+column+"/jumlah";
	rowCount = parseInt(document.getElementById(""+idCounter).value);
	sumRowInAddRowSubTbodyCheckId(columnIndex,idTable,idCounter,idOutput, 2);

	//form penilaian
	jumlahPenyaji=parseInt(document.getElementById("4.5.3/6/jumlah").innerHTML);
	document.getElementById("nilai/4.5.3/1").innerHTML = jumlahPenyaji;
	jumlahPeserta=parseInt(document.getElementById("4.5.3/7/jumlah").innerHTML);
	document.getElementById("nilai/4.5.3/2").innerHTML = jumlahPeserta;
	jumlahDosen=parseInt(document.getElementById("4.3.1/counter").value);
	document.getElementById("nilai/4.5.3/3").innerHTML = jumlahDosen;
	SP = roundToTwo((jumlahPenyaji+jumlahPeserta/4)/jumlahDosen);
	document.getElementById("nilai/4.5.3/4").innerHTML = SP;

	if (SP==0) {
		nilai=0;
	}else if(SP<3){
		nilai=roundToTwo(1+SP);
	}else{
		nilai=4;
	}
	document.getElementById("nilai/4.5.3/nilai").innerHTML = nilai;
	document.getElementById("nilai/4.5.3/nilaihid").value = nilai;
}

function addtbody54(idTable,idCounter){
	rowCount=document.getElementById(""+idCounter).value;
	nextCount = parseInt(rowCount)+1;
	htmlContent = "<tbody>"+
	"<input type='hidden' id='4.5.4/counter/no"+nextCount+"' value='1' name='4_5_4/counter/no"+nextCount+"'>"+
	"<tr>"+
	"<td rowspan='2'>"+nextCount+"</td>"+
	"<td rowspan='2'><select class='form-control' name='4_5_4/2/no"+nextCount+"/sub1' id='4.5.4/2/no"+nextCount+"'>"+dropdowndosen+"</select></td>"+
	"<td><textarea rows='2' style='width:100%;' name='4_5_4/3/no"+nextCount+"/sub1'></textarea></td>"+
	"<td><textarea rows='2' style='width:100%;' name='4_5_4/4/no"+nextCount+"/sub1'></textarea></td>"+
	"<td><select class='form-control' name='4_5_4/5/no"+nextCount+"/sub1'>"+
	"<option>-</option>"+
	"<option>Lokal</option>"+
	"<option>Nasional</option>"+
	"<option>Internasional</option>"+			
	"</select></td>"+
	"</tr>"+
	"<tr>"+
	"<td colspan='4'><button type='button' onclick='addrowtbody54(\"table454\","+nextCount+",\"4.5.4/counter/no"+nextCount+"\")'><span class='glyphicon glyphicon-plus'></span></button>"+
	"<button type='button' onclick='removerowtbody(\"table454\","+nextCount+",\"4.5.4/counter/no"+nextCount+"\",2)'><span class='glyphicon glyphicon-minus'></span></button></td>"+
	"</tr>"+
	"</tbody>";
	addTbody(idTable, idCounter, nextCount, htmlContent);
}

function addrowtbody54(idTable, tbodyNode, idCounter){
	rowCount=document.getElementById(""+idCounter).value;
	nextCount = parseInt(rowCount)+1;
	htmlContent ="<td><textarea rows='2' style='width:100%;' name='4_5_4/3/no"+tbodyNode+"/sub"+nextCount+"'></textarea></td>"+
	"<td><textarea rows='2' style='width:100%;' name='4_5_4/4/no"+tbodyNode+"/sub"+nextCount+"'></textarea></td>"+
	"<td><select class='form-control' name='4_5_4/5/no"+tbodyNode+"/sub"+nextCount+"'>"+
	"<option>-</option>"+
	"<option>Lokal</option>"+
	"<option>Nasional</option>"+
	"<option>Internasional</option>"+			
	"</select></td>";
	addRowTbody(idTable, idCounter, nextCount, htmlContent, tbodyNode);
	var tableElem = window.document.getElementById(""+idTable);
	var tableBody = tableElem.getElementsByTagName("tbody").item(tbodyNode);
	var trElem = tableBody.getElementsByTagName("tr").item(0);
	var tdElem1 = trElem.getElementsByTagName("td").item(0);
	var tdElem2 = trElem.getElementsByTagName("td").item(1);
	var tdElem3 = trElem.getElementsByTagName("td").item(2);
	tdElem1.rowSpan = nextCount+1;
	tdElem2.rowSpan = nextCount+1;
}

function addtbody55(idTable,idCounter){
	rowCount=document.getElementById(""+idCounter).value;
	nextCount = parseInt(rowCount)+1;
	htmlContent = "<tbody>"+
	"<input type='hidden' id='4.5.5/counter/no"+nextCount+"' value='1' name='4_5_5/counter/no"+nextCount+"'>"+
	"<tr>"+
	"<td rowspan='2'>"+nextCount+"</td>"+
	"<td rowspan='2'><select class='form-control' name='4_5_5/2/no"+nextCount+"/sub1' id='4.5.5/2/no"+nextCount+"'>"+dropdowndosen+"</select></td>"+
	"<td><textarea rows='2' style='width:100%;' name='4_5_5/3/no"+nextCount+"/sub1'></textarea></td>"+
	"<td><textarea rows='2' style='width:100%;' name='4_5_5/4/no"+nextCount+"/sub1'></textarea></td>"+
	"<td><select class='form-control' onchange='updateNilai55()' name='4_5_5/5/no"+nextCount+"/sub1'>"+
	"<option>-</option>"+
	"<option>Lokal</option>"+
	"<option>Nasional</option>"+
	"<option>Internasional</option>"+			
	"</select></td>"+
	"</tr>"+
	"<tr>"+
	"<td colspan='4'><button type='button' onclick='addrowtbody55(\"table455\","+nextCount+",\"4.5.5/counter/no"+nextCount+"\")'><span class='glyphicon glyphicon-plus'></span></button>"+
	"<button type='button' onclick='removerowtbody(\"table455\","+nextCount+",\"4.5.5/counter/no"+nextCount+"\")'><span class='glyphicon glyphicon-minus'></span></button></td>"+
	"</tr>"+
	"</tbody>";
	addTbody(idTable, idCounter, nextCount, htmlContent);
}

function addrowtbody55(idTable, tbodyNode, idCounter){
	rowCount=document.getElementById(""+idCounter).value;
	nextCount = parseInt(rowCount)+1;
	htmlContent ="<td><textarea rows='2' style='width:100%;' name='4_5_5/3/no"+tbodyNode+"/sub"+nextCount+"'></textarea></td>"+
	"<td><textarea rows='2' style='width:100%;' name='4_5_5/4/no"+tbodyNode+"/sub"+nextCount+"'></textarea></td>"+
	"<td><select class='form-control' onchange='updateNilai55()' name='4_5_5/5/no"+tbodyNode+"/sub"+nextCount+"'>"+
	"<option>-</option>"+
	"<option>Lokal</option>"+
	"<option>Nasional</option>"+
	"<option>Internasional</option>"+			
	"</select></td>";
	addRowTbody(idTable, idCounter, nextCount, htmlContent, tbodyNode);
	var tableElem = window.document.getElementById(""+idTable);
	var tableBody = tableElem.getElementsByTagName("tbody").item(tbodyNode);
	var trElem = tableBody.getElementsByTagName("tr").item(0);
	var tdElem1 = trElem.getElementsByTagName("td").item(0);
	var tdElem2 = trElem.getElementsByTagName("td").item(1);
	var tdElem3 = trElem.getElementsByTagName("td").item(2);
	tdElem1.rowSpan = nextCount+1;
	tdElem2.rowSpan = nextCount+1;
}

function updateNilai55(){
	idTable = "table455";
	idCounter = "4.5.5/counter";
	jumlahDosen=parseInt(document.getElementById("4.3.1/counter").value);
	document.getElementById("nilai/4.5.5/1").innerHTML = jumlahDosen;
	jumlahInternasional = 0;
	jumlahNasional = 0;
	tbodyCount=document.getElementById(""+idCounter).value;
	for (no=1; no<=tbodyCount; no++){
		rowCount = document.getElementById("4.5.5/counter/no"+no).value;
		tingkatTertinggi = "";
		//tr pertama
		tingkat = retrieveCellInputValue(4,idTable,no,0);
		tingkatTertinggi = tingkat;

		//tr lainnya
		for (sub=1; sub<rowCount; sub++){
			tingkat = retrieveCellInputValue(2,idTable,no,sub);
			if (tingkat=="Internasional") {
				tingkatTertinggi=tingkat;
			}else if(tingkat=="Nasional" && tingkatTertinggi!="Internasional"){
				tingkatTertinggi=tingkat;
			}
		}
		if (tingkatTertinggi=="Internasional") {
			jumlahInternasional++;
		} else if(tingkatTertinggi=="Nasional"){
			jumlahNasional++;
		}
	}

	document.getElementById("nilai/4.5.5/2").innerHTML = jumlahInternasional;
	document.getElementById("nilai/4.5.5/3").innerHTML = jumlahNasional;
	persenInternasional = roundToTwo((jumlahInternasional/jumlahdosen)*100);
	persenNasional = roundToTwo((jumlahNasional/jumlahdosen)*100);
	document.getElementById("nilai/4.5.5/4").innerHTML = persenInternasional+"%";
	document.getElementById("nilai/4.5.5/5").innerHTML = persenNasional+"%";

	if (persenInternasional>30) {
		nilai=4;
	}else if ((persenNasional+persenInternasional)>30) {
		nilai=3;
	}else if ((persenNasional+persenInternasional)>=15) {
		nilai=2;
	}else if ((persenNasional+persenInternasional)<=0) {
		nilai=0;
	}else{
		nilai=1;
	}

	document.getElementById("nilai/4.5.5/nilai").innerHTML = nilai;
	document.getElementById("nilai/4.5.5/nilaihid").value = nilai;
}

function addRow61(idTable,idCounter){
	rowCount=document.getElementById(""+idCounter).value;
	nextCount = parseInt(rowCount)+1;
	htmlContent = "<td></td>"+
	"<td><textarea rows='1' style='width:100%;' name='4_6_1/2/no"+nextCount+"'></textarea></td>"+
	"<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_6_1/3/no"+nextCount+"' onchange='updateNilai61(2)'></td>"+
	"<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_6_1/4/no"+nextCount+"' onchange='updateNilai61(3)'></td>"+
	"<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_6_1/5/no"+nextCount+"' onchange='updateNilai61(4)'></td>"+
	"<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_6_1/6/no"+nextCount+"' onchange='updateNilai61(5)'></td>"+
	"<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_6_1/7/no"+nextCount+"' onchange='updateNilai61(6)'></td>"+
	"<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_6_1/8/no"+nextCount+"' onchange='updateNilai61(7)'></td>"+
	"<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_6_1/9/no"+nextCount+"' onchange='updateNilai61(8)'></td>"+
	"<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_6_1/10/no"+nextCount+"' onchange='updateNilai61(9)'></td>";
	addRow(idTable, idCounter, nextCount, htmlContent);
}

function updateNilai61(columnIndex){
	idTable="tabel461";
	idOutput="4.6.1/"+(columnIndex+1);
	sumRowInAddRowId(columnIndex,idTable,idOutput);

	//4.6.1.a
	S3 = parseInt(retrieveCellInputValue(2,idTable,0,1)); if (isNaN(S3)) {S3=0}
	S2 = parseInt(retrieveCellInputValue(3,idTable,0,1)); if (isNaN(S2)) {S2=0}
	X1 = S3 + S2;
	document.getElementById("nilai/4.6.1.a/1").innerHTML = X1;
	S1 = parseInt(retrieveCellInputValue(4,idTable,0,1)); if (isNaN(S1)) {S1=0}
	D4 = parseInt(retrieveCellInputValue(5,idTable,0,1)); if (isNaN(D4)) {D4=0}
	X2 = S1 + D4;
	document.getElementById("nilai/4.6.1.a/2").innerHTML = X2;
	D3 = parseInt(retrieveCellInputValue(6,idTable,0,1)); if (isNaN(D3)) {D3=0}
	D2 = parseInt(retrieveCellInputValue(7,idTable,0,1)); if (isNaN(D2)) {D2=0}
	D1 = parseInt(retrieveCellInputValue(8,idTable,0,1)); if (isNaN(D1)) {D1=0}
	X3 = D3 + D2 + D1;
	document.getElementById("nilai/4.6.1.a/3").innerHTML = X3;
	A = roundToTwo((4*X1+3*X2+2*X3)/4);
	document.getElementById("nilai/4.6.1.a/4").innerHTML = A;
	if (A>=4) {
		nilai=4;
	}else{
		nilai = A;
	}
	document.getElementById("nilai/4.6.1.a/nilai").innerHTML = nilai;
	document.getElementById("nilai/4.6.1.a/nilaihid").value = nilai;


	//4.6.1.c
	S1 = parseInt(retrieveCellInputValue(4,idTable,0,3)); if (isNaN(S1)) {S1=0}
	D4 = parseInt(retrieveCellInputValue(5,idTable,0,3)); if (isNaN(D4)) {D4=0}
	X1 = S1 + D4;
	document.getElementById("nilai/4.6.1.c/1").innerHTML = X1;
	D3 = parseInt(retrieveCellInputValue(6,idTable,0,3)); if (isNaN(D3)) {D3=0}
	X2 = D3;
	document.getElementById("nilai/4.6.1.c/2").innerHTML = X2;
	D2 = parseInt(retrieveCellInputValue(7,idTable,0,3)); if (isNaN(D2)) {D2=0}
	D1 = parseInt(retrieveCellInputValue(8,idTable,0,3)); if (isNaN(D1)) {D1=0}
	X3 = D2 + D1;
	document.getElementById("nilai/4.6.1.c/3").innerHTML = X3;
	SMU = parseInt(retrieveCellInputValue(9,idTable,0,3)); if (isNaN(SMU)) {SMU=0}
	X4 = SMU;
	document.getElementById("nilai/4.6.1.c/4").innerHTML = X4;
	D = roundToTwo((4*X1+3*X2+2*X3+X4)/4);
	document.getElementById("nilai/4.6.1.c/5").innerHTML = D;

	if (D>=4) {
		nilai=4;
	}else{
		nilai = D;
	}
	document.getElementById("nilai/4.6.1.c/nilai").innerHTML = nilai;
	document.getElementById("nilai/4.6.1.c/nilaihid").value = nilai;


}


function updateListDosen(){
	//dosen ps
	rowCount=document.getElementById("4.3.1/counter").value;
	dropdowndosen = "<option>-</option>";
	for (no = 1; no <=rowCount ; no++) {
		nama = document.getElementById("4.3.1/2/no"+no).innerHTML;
		dropdowndosen = dropdowndosen+"<option>"+nama+"</option>";
	}

	//dosen xps
	rowCount=document.getElementById("4.3.2/counter").value;
	dropdowndosenxps = "<option>-</option>";
	for (no = 1; no <=rowCount ; no++) {
		nama = document.getElementById("4.3.2/2/no"+no).innerHTML;
		dropdowndosenxps = dropdowndosenxps+"<option>"+nama+"</option>";
	}

	//dosen xtp
	rowCount=document.getElementById("4.4.1/counter").value;
	dropdowndosenxtp = "<option>-</option>";
	for (no = 1; no <=rowCount ; no++) {
		nama = document.getElementById("4.4.1/2/no"+no).innerHTML;
		dropdowndosenxtp = dropdowndosenxtp+"<option>"+nama+"</option>";
	}
}

function loadAllJs(){
	updateListDosen();
	updateNilai31();
	//433 ganjil
	var idTable = "tabel433ganjil";
	var tableElem = window.document.getElementById(""+idTable);
	var tableBody = tableElem.getElementsByTagName("tbody").item(0);
	var howManyRows = tableBody.rows.length;
	for (row=1; row<(howManyRows-2); row++){
		for(column=3;column<=9;column++){
			updateNilai33ganjil(row,column-1,idTable,"4.3.3.ganjil/"+column+"/jumlah","4.3.3.ganjil/"+column+"/rata");
		}
	}
	//433 genap
	var idTable = "tabel433genap";
	var tableElem = window.document.getElementById(""+idTable);
	var tableBody = tableElem.getElementsByTagName("tbody").item(0);
	var howManyRows = tableBody.rows.length;
	for (row=1; row<(howManyRows-2); row++){
		for(column=3;column<=9;column++){
			updateNilai33genap(row,column-1,idTable,"4.3.3.genap/"+column+"/jumlah","4.3.3.genap/"+column+"/rata");
		}
	}

	//434
	updateNilai3435(6,"table434ganjil","4.3.4.ganjil/counter","4.3.4.ganjil/7/jumlah");
	updateNilai3435(7,"table434ganjil","4.3.4.ganjil/counter","4.3.4.ganjil/8/jumlah");
	updateNilai3435(6,"table434genap","4.3.4.genap/counter","4.3.4.genap/7/jumlah");
	updateNilai3435(7,"table434genap","4.3.4.genap/counter","4.3.4.genap/8/jumlah");

	//435
	updateNilai3435(6,"table435ganjil","4.3.5.ganjil/counter","4.3.5.ganjil/7/jumlah");
	updateNilai3435(7,"table435ganjil","4.3.5.ganjil/counter","4.3.5.ganjil/8/jumlah");
	updateNilai3435(6,"table435genap","4.3.5.genap/counter","4.3.5.genap/7/jumlah");
	updateNilai3435(7,"table435genap","4.3.5.genap/counter","4.3.5.genap/8/jumlah");

	//42b
	updateNilai42b(6,"table442ganjil","4.4.2.ganjil/counter","4.4.2.ganjil/7/jumlah");
	updateNilai42b(7,"table442ganjil","4.4.2.ganjil/counter","4.4.2.ganjil/8/jumlah");
	updateNilai42b(6,"table442genap","4.4.2.genap/counter","4.4.2.genap/7/jumlah");
	updateNilai42b(7,"table442genap","4.4.2.genap/counter","4.4.2.genap/8/jumlah");

	updateNilai51();
	updateNilai52();
	updateNilai53(5);
	updateNilai53(6);
	updateNilai55();

	for(i=2;i<=9;i++){
		updateNilai61(i);
	}

}