function roundToTwo(num) {    
	return +(Math.round(num + "e+2")  + "e-2");
}

function sumRow(columnIndex,idTable){
	try{
		var tableElem = window.document.getElementById(""+idTable);
		var tableBody = tableElem.getElementsByTagName("tbody").item(0);
		var i;
		var total=0;
		var whichColumn = columnIndex; // which column in the table has what we want
		var howManyRows = tableBody.rows.length;
		for (i=1; i<(howManyRows-1); i++){ // skip first and last row (hence i=1, and howManyRows-1)
			var thisTrElem = tableBody.rows[i];
			var thisTdElem = thisTrElem.cells[whichColumn];			
			var thisTextNode = thisTdElem.children[0];
			var thisNumber = parseFloat(thisTextNode.value);
       		// if you didn't get back the value NaN (i.e. not a number), add into result
       		if (!isNaN(thisNumber)){
       			total = total + thisNumber;
       		}
		} // end for
		total = roundToTwo(total);
		totalNode=tableBody.rows[howManyRows-1].cells[whichColumn];
		totalNode.innerHTML = total; 		
	}catch (ex){
		window.alert("Error counting Sum of row in table:\n" + ex);
	}
}

function sumRowInAddRowId(columnIndex,idTable,idOutput){
	try{
		var tableElem = window.document.getElementById(""+idTable);
		var tableBody = tableElem.getElementsByTagName("tbody").item(0);
		var i;
		var total=0;
		var whichColumn = columnIndex; // which column in the table has what we want
		var howManyRows = tableBody.rows.length;
		for (i=1; i<(howManyRows-2); i++){ // skip first and last row (hence i=1, and howManyRows-1)
			var thisTrElem = tableBody.rows[i];
			var thisTdElem = thisTrElem.cells[whichColumn];			
			var thisTextNode = thisTdElem.children[0];
			var thisNumber = parseFloat(thisTextNode.value);
       		// if you didn't get back the value NaN (i.e. not a number), add into result
       		if (!isNaN(thisNumber)){
       			total = total + thisNumber;
       		}
		} // end for
		total = roundToTwo(total);
		totalNode=document.getElementById(""+idOutput);
		totalNode.innerHTML = total; 		
	}catch (ex){
		window.alert("Error counting Sum of row in table:\n" + ex);
	}
}

function sumRowInAddRowIdInnerHTML(columnIndex,idTable,idOutput){
	try{
		var tableElem = window.document.getElementById(""+idTable);
		var tableBody = tableElem.getElementsByTagName("tbody").item(0);
		var i;
		var total=0;
		var whichColumn = columnIndex; // which column in the table has what we want
		var howManyRows = tableBody.rows.length;
		for (i=1; i<(howManyRows-2); i++){ // skip first and last row (hence i=1, and howManyRows-1)
			var thisTrElem = tableBody.rows[i];
			var thisTdElem = thisTrElem.cells[whichColumn];			
			var thisTextNode = thisTdElem;
			var thisNumber = parseFloat(thisTextNode.innerHTML);
       		// if you didn't get back the value NaN (i.e. not a number), add into result
       		if (!isNaN(thisNumber)){
       			total = total + thisNumber;
       		}
		} // end for
		total = roundToTwo(total);
		totalNode=document.getElementById(""+idOutput);
		totalNode.innerHTML = total; 		
	}catch (ex){
		window.alert("Error counting Sum of row in table:\n" + ex);
	}
}

function sumRowInAddRowIdSubTbody(columnIndex,idTable,idCounter,idOutput, difference){
	try{
		var tableElem = window.document.getElementById(""+idTable);
		var countNode = window.document.getElementById(""+idCounter).value;
		var total = 0;
		var whichColumn = columnIndex;
		for(no = 1; no<=countNode; no++){
			var tableBody = tableElem.getElementsByTagName("tbody").item(no);
			var howManyRows = tableBody.rows.length;
			//first tr
			var thisTrElem = tableBody.rows[0];
			var thisTdElem = thisTrElem.cells[whichColumn];			
			var thisTextNode = thisTdElem.children[0];
			var thisNumber = parseFloat(thisTextNode.value);
			if (!isNaN(thisNumber)){
				total = total + thisNumber;
			}
			//sub tr
			for (i=1; i<(howManyRows-1); i++){
				thisTrElem = tableBody.rows[i];
				thisTdElem = thisTrElem.cells[whichColumn-difference];			
				thisTextNode = thisTdElem.children[0];
				thisNumber = parseFloat(thisTextNode.value);
				if (!isNaN(thisNumber)){
					total = total + thisNumber;
				}
			}
			total = roundToTwo(total);
			totalNode=document.getElementById(""+idOutput);
			totalNode.innerHTML = total; 
		}
	}catch (ex){
		window.alert("Error counting Sum of row in table:\n" + ex);
	}
}

function sumRowInAddRowCheckId(columnIndex,idTable,idOutput){
	try{
		var tableElem = window.document.getElementById(""+idTable);
		var tableBody = tableElem.getElementsByTagName("tbody").item(0);
		var i;
		var total=0;
		var count=0;
		var whichColumn = columnIndex; // which column in the table has what we want
		var howManyRows = tableBody.rows.length;
		for (i=1; i<(howManyRows-2); i++){ // skip first and last row (hence i=1, and howManyRows-1)
			var thisTrElem = tableBody.rows[i];
			var thisTdElem = thisTrElem.cells[whichColumn];			
			var thisTextNode = thisTdElem.children[0];
			if(thisTextNode.checked){
				count = count + 1;
			}
		} // end for
		totalNode=document.getElementById(""+idOutput);
		totalNode.innerHTML = count;
	}catch (ex){
		window.alert("Error counting Sum of row in table:\n" + ex);
	}
}

function sumRowInAddRowCheckId3(columnIndex1,columnIndex2,columnIndex3,idTable,idOutput){
	try{
		var tableElem = window.document.getElementById(""+idTable);
		var tableBody = tableElem.getElementsByTagName("tbody").item(0);
		var howManyRows = tableBody.rows.length;		
		
		var whichColumn1 = columnIndex1;
		var whichColumn2 = columnIndex2;
		var whichColumn3 = columnIndex3;
		var count=0;
		
		for (i=1; i<(howManyRows-2); i++){
			var thisTrElem = tableBody.rows[i];
			var thisTdElemi = thisTrElem.cells[whichColumn1];
			var thisTdElemx = thisTrElem.cells[whichColumn2];
			var thisTdElemy = thisTrElem.cells[whichColumn3];

			var thisTextNode1 = thisTdElemi.children[0];
			var thisTextNode2 = thisTdElemx.children[0];
			var thisTextNode3 = thisTdElemy.children[0];

			if(thisTextNode1.checked && thisTextNode2.checked && thisTextNode3.checked){
				count = count + 1;
			}
		} 
		totalNode=document.getElementById(""+idOutput);
		totalNode.innerHTML = count; 		
	}catch (ex){
		window.alert("Error counting Sum of row in table:\n" + ex);
	}
}

function sumRowInAddRowSubTbodyCheckId(columnIndex,idTable,idCounter,idOutput, difference){
	try{
		var tableElem = window.document.getElementById(""+idTable);
		var countNode = window.document.getElementById(""+idCounter).value;
		var total = 0;
		var whichColumn = columnIndex;
		for(no = 1; no<=countNode; no++){
			var tableBody = tableElem.getElementsByTagName("tbody").item(no);
			var howManyRows = tableBody.rows.length;
			//first tr
			var thisTrElem = tableBody.rows[0];
			var thisTdElem = thisTrElem.cells[whichColumn];			
			var thisTextNode = thisTdElem.children[0];
			if(thisTextNode.checked){
				total++;
			}
			//sub tr
			for (i=1; i<(howManyRows-1); i++){
				thisTrElem = tableBody.rows[i];
				thisTdElem = thisTrElem.cells[whichColumn-difference];			
				thisTextNode = thisTdElem.children[0];
				if(thisTextNode.checked){
					total++;
				}
			}
			total = roundToTwo(total);
			totalNode=document.getElementById(""+idOutput);
			totalNode.innerHTML = total; 
		}
	}catch (ex){
		window.alert("Error counting Sum of row in table:\n" + ex);
	}
}

function sumRowId(columnIndex,idTable,idOutput){
	try{
		var tableElem = window.document.getElementById(""+idTable);
		var tableBody = tableElem.getElementsByTagName("tbody").item(0);
		var i;
		var total=0;
		var whichColumn = columnIndex; // which column in the table has what we want
		var howManyRows = tableBody.rows.length;
		for (i=1; i<(howManyRows-1); i++){ // skip first and last row (hence i=1, and howManyRows-1)
			var thisTrElem = tableBody.rows[i];
			var thisTdElem = thisTrElem.cells[whichColumn];			
			var thisTextNode = thisTdElem.children[0];
			var thisNumber = parseFloat(thisTextNode.value);
       		// if you didn't get back the value NaN (i.e. not a number), add into result
       		if (!isNaN(thisNumber)){
       			total = total + thisNumber;
       		}
		} // end for
		total = roundToTwo(total);
		totalNode=document.getElementById(""+idOutput);
		totalNode.innerHTML = total; 		
	}catch (ex){
		window.alert("Error counting Sum of row in table:\n" + ex);
	}
}

function sumColumn(rowIndex,idTable){
	try{
		var tableElem = window.document.getElementById(""+idTable);
		var tableBody = tableElem.getElementsByTagName("tbody").item(0);
		var i;
		var total=0;
		var whichRow = rowIndex; // which column in the table has what we want
		var thisTrElem = tableBody.rows[whichRow];
		var howManyColumns = thisTrElem.cells.length;
		for (i=1; i<(howManyColumns-1); i++){ // skip first, second and last column
			var thisTdElem = thisTrElem.cells[i];			
			var thisTextNode = thisTdElem.children[0];
			var thisNumber = parseFloat(thisTextNode.value);
       		// if you didn't get back the value NaN (i.e. not a number), add into result
       		if (!isNaN(thisNumber)){
       			total = total + thisNumber;
       		}
		} // end for
		total = roundToTwo(total);
		totalNode=tableBody.rows[whichRow].cells[howManyColumns-1];
		totalNode.innerHTML = total; 		
	}catch (ex){
		window.alert("Error counting Sum of row in table:\n" + ex);
	}
}

function AverageRow(columnIndex,idTable,idOutput){
	try{
		var tableElem = window.document.getElementById(""+idTable);
		var tableBody = tableElem.getElementsByTagName("tbody").item(0);
		var i;
		var total=0;
		var whichColumn = columnIndex; // which column in the table has what we want
		var howManyRows = tableBody.rows.length;
		for (i=1; i<(howManyRows-1); i++){ // skip first and last row (hence i=1, and howManyRows-1)
			var thisTrElem = tableBody.rows[i];
			var thisTdElem = thisTrElem.cells[whichColumn];			
			var thisTextNode = thisTdElem.children[0];
			var thisNumber = parseFloat(thisTextNode.value);
       		// if you didn't get back the value NaN (i.e. not a number), add into result
       		if (!isNaN(thisNumber)){
       			total = total + thisNumber;
       		}
		} // end for
		total = roundToTwo(total/(howManyRows-2));
		totalNode=document.getElementById(""+idOutput);
		totalNode.innerHTML = total;		
	}catch (ex){
		window.alert("Error counting Sum of row in table:\n" + ex);
	}
}

function AverageRowInAddRow(columnIndex,idTable,idOutput){
	try{
		var tableElem = window.document.getElementById(""+idTable);
		var tableBody = tableElem.getElementsByTagName("tbody").item(0);
		var i;
		var total=0;
		var whichColumn = columnIndex; // which column in the table has what we want
		var howManyRows = tableBody.rows.length;
		for (i=1; i<(howManyRows-2); i++){ // skip first and last row (hence i=1, and howManyRows-1)
			var thisTrElem = tableBody.rows[i];
			var thisTdElem = thisTrElem.cells[whichColumn];			
			var thisTextNode = thisTdElem.children[0];
			var thisNumber = parseFloat(thisTextNode.value);
       		// if you didn't get back the value NaN (i.e. not a number), add into result
       		if (!isNaN(thisNumber)){
       			total = total + thisNumber;
       		}
		} // end for
		total = roundToTwo(total/(howManyRows-3));
		totalNode=document.getElementById(""+idOutput);
		totalNode.innerHTML = total;		
	}catch (ex){
		window.alert("Error counting Sum of row in table:\n" + ex);
	}
}

function AverageRowInAddRowInnerHTML(columnIndex,idTable,idOutput){
	try{
		var tableElem = window.document.getElementById(""+idTable);
		var tableBody = tableElem.getElementsByTagName("tbody").item(0);
		var i;
		var total=0;
		var whichColumn = columnIndex; // which column in the table has what we want
		var howManyRows = tableBody.rows.length;
		for (i=1; i<(howManyRows-2); i++){ // skip first and last row (hence i=1, and howManyRows-1)
			var thisTrElem = tableBody.rows[i];
			var thisTdElem = thisTrElem.cells[whichColumn];			
			var thisTextNode = thisTdElem;
			var thisNumber = parseFloat(thisTextNode.innerHTML);
       		// if you didn't get back the value NaN (i.e. not a number), add into result
       		if (!isNaN(thisNumber)){
       			total = total + thisNumber;
       		}
		} // end for
		total = roundToTwo(total/(howManyRows-3));
		totalNode=document.getElementById(""+idOutput);
		totalNode.innerHTML = total;		
	}catch (ex){
		window.alert("Error counting Sum of row in table:\n" + ex);
	}
}

function addRow(idTable,idCounter,nextCount,htmlContent){
	var tableElem = window.document.getElementById(""+idTable);
	var tableBody = tableElem.getElementsByTagName("tbody").item(0);
	row = tableBody.insertRow(nextCount);
	row.innerHTML = htmlContent;
	document.getElementById(""+idCounter).value = nextCount;
}

function addRowTbody(idTable,idCounter,nextCount,htmlContent, tbodynode){
	var tableElem = window.document.getElementById(""+idTable);
	var tableBody = tableElem.getElementsByTagName("tbody").item(tbodynode);
	nextNode = nextCount - 1;
	row = tableBody.insertRow(nextNode);
	row.innerHTML = htmlContent;
	document.getElementById(""+idCounter).value = nextCount;
}

function addTbody(idTable,idCounter,nextCount,htmlContent){
	var tableElem = window.document.getElementById(""+idTable);
	var newTableBody = document.createElement("tbody");
	newTableBody.innerHTML = htmlContent;
	tableElem.insertBefore(newTableBody,tableElem.getElementsByTagName("tbody").item(nextCount));
	document.getElementById(""+idCounter).value = nextCount;
}

function countRowByValue(columnIndex,idTable,value){
	try{
		var tableElem = window.document.getElementById(""+idTable);
		var tableBody = tableElem.getElementsByTagName("tbody").item(0);
		var i;
		var total=0;
		var whichColumn = columnIndex; // which column in the table has what we want
		var howManyRows = tableBody.rows.length;
		for (i=1; i<(howManyRows-1); i++){ // skip first and last row (hence i=1, and howManyRows-1)
			var thisTrElem = tableBody.rows[i];
			var thisTdElem = thisTrElem.cells[whichColumn];			
			var thisTextNode = thisTdElem.children[0];
			var thisValue = thisTextNode.value;
			if (thisValue==value){
				total = total + 1;
			}
		} // end for
		
	}catch (ex){
		window.alert("Error count Row by category in table:\n" + ex);
	}
	return total;
}

function retrieveCellInputValue(columnIndex,idTable,tbodynode,trnode){
	try{
		var tableElem = window.document.getElementById(""+idTable);
		var tableBody = tableElem.getElementsByTagName("tbody").item(tbodynode);
		var trElem = tableBody.getElementsByTagName("tr").item(trnode);
		var tdElem = trElem.getElementsByTagName("td").item(columnIndex);
		var thisTextNode = tdElem.children[0];
		var thisValue = thisTextNode.value;
		
	}catch (ex){
		window.alert("Error Retrieve Value:\n" + ex);
	}
	return thisValue;
}

function retrieveCellInnerHTML(columnIndex,idTable,tbodynode,trnode){
	try{
		var tableElem = window.document.getElementById(""+idTable);
		var tableBody = tableElem.getElementsByTagName("tbody").item(tbodynode);
		var trElem = tableBody.getElementsByTagName("tr").item(trnode);
		var tdElem = trElem.getElementsByTagName("td").item(columnIndex);
		var thisValue = tdElem.innerHTML;
		
	}catch (ex){
		window.alert("Error Retrieve Value:\n" + ex);
	}
	return thisValue;
}

function removetbody(idTable,idCounter){
	rowCount=document.getElementById(""+idCounter).value;
	if (rowCount>0) {
		nextCount = parseInt(rowCount)-1;
		tableElem = window.document.getElementById(""+idTable);
		tableBody = tableElem.getElementsByTagName("tbody").item(rowCount);
		tableBody.parentNode.removeChild(tableBody);
		document.getElementById(""+idCounter).value = nextCount;
	}
	
}

function removerowtbody(idTable, tbodyNode, idCounter, lastrowspan){
	rowCount=document.getElementById(""+idCounter).value;
	if (rowCount>1) {
		nextCount = parseInt(rowCount)-1;
		tableElem = window.document.getElementById(""+idTable);
		tableBody = tableElem.getElementsByTagName("tbody").item(tbodyNode);
		thisTrElem = tableBody.rows[nextCount];
		thisTrElem.parentNode.removeChild(thisTrElem);
		document.getElementById(""+idCounter).value = nextCount;

		firsttrElem = tableBody.getElementsByTagName("tr").item(0);
		for(i=0;i<lastrowspan;i++){
			tdElem = firsttrElem.getElementsByTagName("td").item(i);
			tdElem.rowSpan = nextCount+1;
		}
	}
}

function removerow(idTable, idCounter){
	rowCount=document.getElementById(""+idCounter).value;
	if (rowCount>0) {
		nextCount = parseInt(rowCount)-1;
		tableElem = window.document.getElementById(""+idTable);
		tableBody = tableElem.getElementsByTagName("tbody").item(0);
		thisTrElem = tableBody.rows[rowCount];
		thisTrElem.parentNode.removeChild(thisTrElem);
		document.getElementById(""+idCounter).value = nextCount;
	}
}

function removerowstatictable(idTable,idCounter,startnode){
	rowCount=document.getElementById(""+idCounter).value;
	if (rowCount>startnode) {
		nextCount = parseInt(rowCount)-1;
		tableElem = window.document.getElementById(""+idTable);
		tableBody = tableElem.getElementsByTagName("tbody").item(0);
		thisTrElem = tableBody.rows[rowCount];
		thisTrElem.parentNode.removeChild(thisTrElem);
		document.getElementById(""+idCounter).value = nextCount;
	}	
}