<!doctype html>
<html>
<link rel="stylesheet" type="text/css" href="css/loading.css">
<?php
require "Database/DatabaseConnection.php";

$statuslogin = 2;
$idprodi = 0;

$query='select aktif from periode where aktif=1';
$data = mysqli_query($db, $query);
$count = mysqli_num_rows($data);

$query='select username,password,status from user where aktif=1';
$data = $db->query($query);
foreach($data as $row){
	if($_POST['username'] == $row['username'] && $_POST['password'] == $row['password']){
		if ($row['status']=="Admin") {
			$statuslogin = -1;
			if(isset($_POST['remember'])){
				$cookie_username=$row['username'];
				setcookie("LPMGo", $cookie_username, time() +(3600 * 48), "/");
			}else{
				$cookie_username=$row['username'];
				setcookie("LPMGo", $cookie_username, 0, "/");
			}
		} else if($count>0){
			if ($row['status']=="Auditor") {
				$statuslogin = 0;
				if(isset($_POST['remember'])){
					$cookie_username=$row['username'];
					setcookie("LPMAu", $cookie_username, time() +(3600 * 48), "/");
				}else{
					$cookie_username=$row['username'];
					setcookie("LPMAu", $cookie_username, 0, "/");					
				}
			} else if ($row['status']=="Kaprodi") {
				$statuslogin = 1;
				if(isset($_POST['remember'])){
					$cookie_username=$row['username'];
					setcookie("LPMKa", $cookie_username, time() +(3600 * 48), "/");
				}else{
					$cookie_username=$row['username'];
					setcookie("LPMKa", $cookie_username,0, "/");					
				}
			}
		} 
	}
}
if($statuslogin==-1){
	echo "<div class='ball'></div>";
	echo "<div class='ball1'></div>";
	echo "<h1 style='margin-left:43%'>MEMPROSES<h1>";
	echo '<script>
	window.location.href="Admin/HomeAdmin.php";
</script>';
} else if($statuslogin==0){
	echo "<div class='ball'></div>";
	echo "<div class='ball1'></div>";
	echo "<h1 style='margin-left:43%'>MEMPROSES<h1>";
	echo '<script>
	window.location.href="Auditor/HomeAuditor.php";
</script>';
} else if($statuslogin==1){
	echo "<div class='ball'></div>";
	echo "<div class='ball1'></div>";
	echo "<h1 style='margin-left:43%'>MEMPROSES<h1>";
	echo '<script>
	window.location.href="Kaprodi/HomeKaprodi.php?idprodi='.$idprodi.'";
</script>';
} else{
	echo '<script>
	alert("Login gagal.");
	window.location.href="Index.php";
</script>';
}
?>
</html>