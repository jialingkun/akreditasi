<!doctype html>
<html>
	<?php
		require "../Database/DatabaseConnection.php";
		require "../CookiesAuditor.php";

		$null=0;
		$kolom1=1;
		$kolom2=2;
		$kolom3=3;
		$kolom4=4;
		$kolom5=5;
		$kolom6=6;
		$kolom7=7;
		$kolom8=8;
		$kolom9=9;
		$kolom10=10;
		$kolom11=11;
		$subno1=1;
		$subno2=2;
		$subno3=3;
		$subno4=4;
		$subno5=5;
		$subno6=6;
		$subno7=7;
		$subno8=8;
		$no1=1;
		$no2=2;
		$no3=3;
		$no4=4;
		$no5=5;
		$no6=6;
		$no7=7;
		$no8=8;
		
		$query='select idPeriode,NamaPeriode from periode where aktif = 1';
		$data = mysqli_query($db, $query);
		$row = mysqli_fetch_assoc($data);

		$namaPeriode = $row['NamaPeriode'];
		$periode = $row["idPeriode"];

		$prodi = $_GET['prodi'];

		$query="select * from prodi where idProdi='$prodi'";
		$data = mysqli_query($db, $query);
		$row = mysqli_fetch_assoc($data);

		$namaProdi = $row['namaProdi'];

		$standar= 5;

		//cek data auditor
		$revisi="Telah direvisi";
		$username= $_COOKIE['LPMAu'];
		$query="select username from isi_borang where idProdi='$prodi' and idPeriode='$periode' and username='$username' and standar='$standar' limit 1";
		$data = mysqli_query($db, $query);
		$count = mysqli_num_rows($data);
		if ($count<1){
			//ambil username kaprodi
			$query="select username from isi_borang where idProdi='$prodi' and idPeriode='$periode' and username!='$username' and standar='$standar' limit 1";
			$data = mysqli_query($db, $query);
			$row = mysqli_fetch_assoc($data);
			$username = $row['username'];
			$revisi="Tanpa Revisi";
		}

		$standarProfil= 0;

		//cek data dosen di halaman profil
		$usernameProfil= $_COOKIE['LPMAu'];
		$query="select username from isi_borang where idProdi='$prodi' and idPeriode='$periode' and username='$usernameProfil' and standar='$standarProfil' limit 1";
		$data = mysqli_query($db, $query);
		$count = mysqli_num_rows($data);
		if ($count<1){
			//ambil username kaprodi
			$query="select username from isi_borang where idProdi='$prodi' and idPeriode='$periode' and username!='$usernameProfil' and standar='$standarProfil' limit 1";
			$data = mysqli_query($db, $query);
			$row = mysqli_fetch_assoc($data);
			$usernameProfil = $row['username'];
		}

		//retrieve data dosen PS
		$butir = "dosen_ps";
		$query="SELECT isi_char FROM isi_borang WHERE username='".$usernameProfil."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom='2' ORDER BY no,sub_no,kolom ASC;";
		$data = mysqli_query($db, $query);
		$countDosenPS = mysqli_num_rows($data);
		$dosenPS[0] = "-";
		if ($countDosenPS>0) {
			$index = 1;
			while ($row = mysqli_fetch_assoc($data)) {
				$dosenPS[$index] = $row['isi_char'];
				$index++;
			}
		}
?>
	<head>
		<title>Kaprodi</title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<link href="../css/bootstrap.min.css" rel="stylesheet">
		<link href="../css/style.css" rel="stylesheet">
		<link href="../font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="../css/loading.css">
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		<script src="../js/jquery.min.js"></script>
		<script src="../js/form.js"></script>
		<script src="../js/standar5.js"></script>
	</head>
	<body onload='TotalJsFUll()'>
		<div class='wrapper'>
			<header>
				<nav class="navbar navbar-default navbar-fixed-top">
					<div id="navbar" class="navbar-collapse collapse col-sm-offset-1">
						<ul class="nav navbar-nav">
							<?php echo "
							<li><a href='HomeKaprodi.php?prodi=$prodi'>Profil</a></li>
							<li><a href='Standar1.php?prodi=$prodi'>Standar 1</a></li>
							<li><a href='Standar2.php?prodi=$prodi'>Standar 2</a></li>
							<li><a href='Standar3.php?prodi=$prodi'>Standar 3</a></li>
							<li><a href='Standar4.php?prodi=$prodi'>Standar 4</a></li>
							<li class='active'><a href='Standar5.php?prodi=$prodi'>Standar 5</a></li>
							<li><a href='Standar6.php?prodi=$prodi'>Standar 6</a></li>
							<li><a href='Standar7.php?prodi=$prodi'>Standar 7</a></li>
							<li><a href='Nilai.php?prodi=$prodi'>Nilai</a></li>
							"; ?>
							<li><a href="HomeAuditor.php" class="col-md-offset-8">Kembali</a></li>	
							<li><a href="Logout.php">Log Out</a></li>
						</ul>
					</div>
				</nav>
				<div class='text-center'>
					<br>
					<br>
					<h1>STANDAR 5</h1>
					<h4>KURIKULUM, PEMBELAJARAN, DAN SUASANA AKADEMIK</h4>
					<h5><?php echo $namaProdi ?></h5>
					<h5>Periode: <?php echo $namaPeriode ?></h5>
					<h5><b><?php echo $revisi?></b></h5>
				</div>
			</header>
		</div>
		<div class = "container">
		<hr class="colorgraph"><br>
		<div class="row">
			<div class="col-md-12">
				<form action="ProsesStandar5.php" method="post" class="form-horizontal">
					
					<?php 
					// default id prodi yang disubmit khusus auditor
					echo "
					<input type='hidden' name='prodi' value='".$prodi."'>";
					?>
					
					<div class="fixed-button" style="left:89%">
						<button type="submit" class="btn btn-primary">Simpan</button>
						<button type="button" class="btn btn-success" id="editButton">Edit</button>
						<button type="button" class="btn btn-default" id="hideButton">Hide</button>
						<button type="button" class="btn btn-default" id="showButton">Show</button>
					</div>
					
					<div class="form-group" style='visibility:collapse;'>
						<?php
						//data retrieve for table
						$butir = "dosen_ps";
						//retrieve counter
						$query="SELECT isi_float FROM isi_borang WHERE username='".$usernameProfil."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom='0' ORDER BY no ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0){
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$counterdata[$index] = $row['isi_float'];	
								$index++;
							}
						}else{
							$counterdata[0] = 1;
							$counterdata[1] = 1;
						}

						//retrieve data
						$query="SELECT isi_char FROM isi_borang WHERE username='".$usernameProfil."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom!='0' ORDER BY no,sub_no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$tabledata[$index] = $row['isi_char'];
								$index++;
							}
						}else{
							$tabledata[0] = "";
							$tabledata[1] = "";
							$tabledata[2] = "";
							$tabledata[3] = "";
							$tabledata[4] = "";
							$tabledata[5] = "";
							$tabledata[6] = "";
							$tabledata[7] = "";
						}
						?>
						<div class="col-sm-offset-2 col-sm-9 table-wrapper">
							<input type="hidden" id="4.3.1/counter" name="4_3_1/counter" <?php echo "value='".$counterdata[0]."'";?>>
							<table class="tableBorang" id="table431" style="width:195%;">
								<thead>
									<tr>
										<th>No.</th>
										<th>Nama Dosen</th>
										<th>NIDN</th>
										<th>Tgl. Lahir</th>
										<th>Jabatatan Akademik</th>
										<th>Gelar Akademik</th>
										<th colspan="2">Pendidikan S1, S2, S3 dan Asal PT</th>
										<th>Bidang Keahlian Untuk Setiap Jenjang Pendidikan</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="width:3%;">(1)</td>
										<td style="width:16%;">(2)</td>
										<td style="width:9%;">(3)</td>
										<td style="width:9%;">(4)</td>
										<td style="width:12%;">(5)</td>
										<td style="width:6%;">(6)</td>
										<td style="width:6%;">(7)</td>
										<td style="width:23%;">(8)</td>
										<td>(9)</td>
									</tr>
								</tbody>

								<?php 
								$index = 0;
								for ($no=1; $no <= $counterdata[0] ; $no++) { 
									echo "<tbody>";
									echo "<input type='hidden' id='4.3.1/counter/no".$no."' value='".$counterdata[$no]."'>";
									//first tr
									?>
									<tr>
										<td <?php echo "rowspan='".$counterdata[$no]."'" ?>><?php echo $no; ?></td>
										<td <?php echo "rowspan='".$counterdata[$no]."' id='4.3.1/2/no".$no."'";?>><?php echo $tabledata[$index]; $index++;?></td>
										<td <?php echo "rowspan='".$counterdata[$no]."'" ?>><?php echo $tabledata[$index]; $index++;?></td>
										<td <?php echo "rowspan='".$counterdata[$no]."'" ?>><?php echo $tabledata[$index]; $index++;?></td>
										<td <?php echo "rowspan='".$counterdata[$no]."'" ?>><?php echo $tabledata[$index]; $index++;?></td>
										<td><?php echo $tabledata[$index]; $index++;?></td>
										<td><?php echo $tabledata[$index]; $index++;?></td>
										<td><?php echo $tabledata[$index]; $index++;?></td>
										<td><?php echo $tabledata[$index]; $index++;?></td>
									</tr>

									<?php
									//sub tr
									for ($sub=2; $sub <= $counterdata[$no]; $sub++) { 
										?>
										<tr>
											<td><?php echo $tabledata[$index]; $index++;?></td>
											<td><?php echo $tabledata[$index]; $index++;?></td>
											<td><?php echo $tabledata[$index]; $index++;?></td>
											<td><?php echo $tabledata[$index]; $index++;?></td>
										</tr>
										<?php
									}
									echo "</tbody>";
								}
								?>
							</table>
						</div>
					</div>

<!--51-->					
					<div class="form-group">
						<label class="col-sm-2 text-right">5.1</label> 
						<label class="col-sm-8">Kurikulum</label>
					</div>
					
					<div class="form-group"> 
						<div class="col-sm-offset-2 col-sm-8">
							<p style="text-indent:5em;">Kurikulum pendidikan tinggi adalah seperangkat rencana dan pengaturan mengenai isi, bahan kajian, maupun bahan pelajaran serta cara penyampaiannya, dan penilaian yang digunakan sebagai pedoman penyelenggaraan kegiatan pembelajaran di perguruan tinggi.
							</p>
							<p style="text-indent:5em;">Kurikulum seharusnyamemuat standar kompetensi lulusan yang terstruktur dalam kompetensi utama, pendukung dan lainnyayang mendukung  tercapainya tujuan, terlaksananya misi, dan terwujudnya visiProgram Studi. Kurikulum memuat mata kuliah/modul/blok yang mendukung pencapaian kompetensi lulusan dan memberikan keleluasaan pada mahasiswa untuk memperluas wawasan dan memperdalam keahlian sesuai dengan minatnya, serta dilengkapi dengan deskripsi mata kuliah/modul/blok, silabus, rencana pembelajaran dan evaluasi. 
							</p>
							<p style="text-indent:5em;">Kurikulum harus dirancang berdasarkan relevansinya dengan tujuan, cakupan dan kedalaman materi, pengorganisasian yang mendorong terbentuknya hard skills danketerampilan kepribadian dan perilaku (soft skills)yangdapat diterapkan dalam berbagai situasi dan kondisi.
							</p>
						</div>
					</div>
<!--511-->
					<div class="form-group">
						<label class="col-sm-2 text-right">5.1.1</label> 
						<label class="col-sm-8">Kompetensi</label>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 text-right">5.1.1.1</label> 
						<label class="col-sm-8">Uraian secara ringkas kompetensi utama lulusan</label>
					</div>
<!--5111-->					
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "5.1.1.1";
							$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
							$data = mysqli_query($db, $query);
							$row = mysqli_fetch_assoc($data);
							$print = $row["isi_text"];
							?>
							<textarea type="text" class="form-control" rows="10" name="5_1_1_1" placeholder="" maxlength="60000"><?php echo $print; ?></textarea>
						</label>
					</div>
<!--5112-->
					<div class="form-group">
						<label class="col-sm-2 text-right">5.1.1.2</label> 
						<label class="col-sm-8">Uraian secara ringkas kompetensi pendukung lulusan</label>
					</div>
					
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "5.1.1.2";
							$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
							$data = mysqli_query($db, $query);
							$row = mysqli_fetch_assoc($data);
							$print = $row["isi_text"];
							?>
							<textarea type="text" class="form-control" rows="10" name="5_1_1_2" placeholder="" maxlength="60000"><?php echo $print; ?></textarea>
						</label>
					</div>
<!--5113-->
					<div class="form-group">
						<label class="col-sm-2 text-right">5.1.1.3</label> 
						<label class="col-sm-8">Uraian secara ringkas kompetensi pendukung lulusan</label>
					</div>
					
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "5.1.1.3";
							$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
							$data = mysqli_query($db, $query);
							$row = mysqli_fetch_assoc($data);
							$print = $row["isi_text"];
							?>
							<textarea type="text" class="form-control" rows="10" name="5_1_1_3" placeholder="" maxlength="60000"><?php echo $print; ?></textarea>
						</label>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "5.1.1.a";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="7">5.1.1.a</td>
										<td colspan="3">Kelengkapan dan perumusan kompetensi dalam kurikulum.</td>
									</tr>
									<tr>
										<td>0</td>
										<td>Kurikulum tidak memuat kompetensi lulusan secara lengkap.</td>
										<td></td>
									</tr>
									<tr>
										<td>1</td>
										<td>Kurikulum memuat kompetensi lulusan secara lengkap (utama, pendukung, lainnya), namun rumusannya kurang jelas.</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Kurikulum memuat kompetensi lulusan secara lengkap (utama, pendukung, lainnya) yang terumuskan secara cukup jelas.</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Kurikulum memuat kompetensi lulusan secara lengkap (utama, pendukung, lainnya) yang terumuskan secara jelas.</td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Kurikulum memuat kompetensi lulusan secara lengkap (utama, pendukung, lainnya) yang terumuskan secara sangat jelas.</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control"  placeholder="" value='<?php echo $print;?>' name="nilai/5_1_1_a"/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "5.1.1.b";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="7">5.1.1.b</td>
										<td colspan="3">Orientasi dan kesesuaian kurikulum dengan visi dan misi PS.</td>
									</tr>
									<tr>
										<td>0</td>
										<td>Tidak sesuai dengan visi-misi serta tidak jelas orientasinya, atau tidak memuat memuat standar kompetensi.</td>
										<td></td>
									</tr>
									<tr>
										<td>1</td>
										<td>Tidak sesuai dengan visi-misi.</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Sesuai dengan visi-misi, tetapi masih berorientasi ke masa lalu.</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Sesuai dengan visi-misi, berorientasi ke masa kini.</td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Sesuai dengan visi-misi, sudah berorientasi  ke masa depan.</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control"  placeholder="" value='<?php echo $print;?>' name="nilai/5_1_1_b"/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
<!--512-->
					<div class="form-group">
						<label class="col-sm-2 text-right">5.2</label> 
						<label class="col-sm-8">Struktur Kurikulum</label>
					</div>
<!--5121-->
					<div class="form-group">
						<?php
						$butir = "5.1.2.1";
						$null=0;
						$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'  AND kolom ='".$null."'  AND no ='".$null."'";
						$data = mysqli_query($db, $query);
						$row = mysqli_fetch_assoc($data);
						$print1 = $row["isi_float"];
						?>
						<label class="col-sm-2 text-right">5.1.2.1</label> 
						<label class="col-sm-8">Jumlah sks PS (minimum untuk kelulusan) : <input style='width:4%' maxlength="3" type="text" pattern="[0-9]+([\.][0-9]+)?"  <?php echo "value='".$print1."'"; ?> name="5_1_2_1"/>SKS</label>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered" id="table121">
								<?php
								$butir = "5.1.2.1";
								$no1=1;
								$no2=2;
								$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'  AND kolom ='".$kolom2."'  AND no ='".$no1."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print1 = $row["isi_float"];

								$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'  AND kolom ='".$kolom3."'  AND no ='".$no1."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print2 = $row["isi_text"];

								$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'  AND kolom ='".$kolom2."'  AND no ='".$no2."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print3 = $row["isi_float"];

								$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'  AND kolom ='".$kolom3."'  AND no ='".$no2."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print4 = $row["isi_text"];
								?>
								<thead>
									<tr>
										<th width="30%" class="text-center">Jenis Mata Kuliah</th>
										<th class="text-center">sks</th>
										<th class="text-center">Keterangan</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="text-center">(1)</td>
										<td class="text-center">(2)</td>
										<td class="text-center">(3)</td>
									</tr>
									<tr>
										<td>Mata Kuliah Wajib</td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="5_1_2_1/1/1" onchange="update121(1)" <?php echo "value='".$print1."'"; ?> id="MataKuliahWajib"></td>
										<td><textarea type="text" class="form-control" name="5_1_2_1/2/1" placeholder="" maxlength="60000"><?php echo $print2; ?></textarea></td>
									</tr>
									<tr>
										<td>Mata Kuliah Pilihan</td>
										<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" id='mkp' name="5_1_2_1/1/2" onchange="update121(1)" <?php echo "value='".$print3."'"; ?> id="MataKuliahPilihan"></td>
										<td><textarea type="text" class="form-control" name="5_1_2_1/2/2" placeholder="" maxlength="60000"><?php echo $print4; ?></textarea></td>
									</tr>
									<tr>
										<td>Jumlah total</td>
										<td id="JumlahTotal"></td>
										<td class="voidColor"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
<!--5122-->
					<div class="form-group">
						<label class="col-sm-2 text-right">5.1.2.2</label> 
						<label class="col-sm-8">struktur kurikulum berdasarkan urutan mata kuliah (MK) semester demi semester, dengan mengikuti format tabel berikut:</label>
					</div>
<!--Semester1-->					
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">Semester 1</label>
						<div class="col-sm-offset-2 col-sm-9 table-wrapper">
							<?php
								$butir = "5.1.2.2";
								$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$null."' AND no='".$null."' AND sub_no='".$no1."'";
								$data = mysqli_query($db, $query);
								$count = mysqli_num_rows($data);
								if ($count>0) {
									$row = mysqli_fetch_assoc($data);
									$ValueCounter = $row["isi_float"];
								}else{
									$ValueCounter = 0;
								}
								echo "<input type='hidden' id='5_1_2_2/1/counter' name='5_1_2_2/1/counter' value='".$ValueCounter."'>";
							?>
							<table class="tableBorang" id="addTable21" style="width:180%;">						
								<thead>
									<tr>
										<th rowspan="2">Semester</th>
										<th rowspan="2">Kode Mata Kuliah</th>
										<th rowspan="2">Nama Mata Kuliah</th>
										<th rowspan="2">Bobot SKS</th>
										<th colspan="2">SKS MK</th>
										<th rowspan="2">Tugas</th>
										<th colspan="3">Kelengkapan</th>
										<th rowspan="2">Unit/Jur/Fak Penyelenggara</th>
									</tr>
									<tr>
										<th width="3%">Inti</th>
										<th>Institusi</th>
										<th>Deskripsi</th>
										<th>Silabus</th>
										<th width="3%">SAP</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
										<td>(4)</td>
										<td>(5)</td>
										<td>(6)</td>
										<td>(7)</td>
										<td>(8)</td>
										<td>(9)</td>
										<td>(10)</td>
										<td>(11)</td>
									</tr>
									
										<?php
											for($i=1;$i<=$ValueCounter;$i++){
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$i."' AND sub_no='".$no1."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print1 = $row["isi_char"];
												
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$i."' AND sub_no='".$no1."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print2 = $row["isi_char"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom4."' AND no='".$i."' AND sub_no='".$no1."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print3 = $row["isi_float"];

												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom5."' AND no='".$i."' AND sub_no='".$no1."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print4 = $row["isi_float"];

												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom6."' AND no='".$i."' AND sub_no='".$no1."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print5 = $row["isi_float"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom7."' AND no='".$i."' AND sub_no='".$no1."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print6 = $row["isi_float"];

												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom8."' AND no='".$i."' AND sub_no='".$no1."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print7 = $row["isi_float"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom9."' AND no='".$i."' AND sub_no='".$no1."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print8 = $row["isi_float"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom10."' AND no='".$i."' AND sub_no='".$no1."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print9 = $row["isi_float"];
												
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom11."' AND no='".$i."' AND sub_no='".$no1."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print10 = $row["isi_char"];
												
												$check1="";
												$check2="";
												$check3="";
												$check4="";
												$check5="";
												$check6="";
												$choice="";
												if($print4 == 1){
													$check1 = "checked";
												}
												if($print5 == 1){
													$check2 = "checked";
												}
												if($print6 == 1){
													$check3 = "checked";
												}
												if($print7 == 1){
													$check4 = "checked";
												}
												if($print8 == 1){
													$check5 = "checked";
												}
												if($print9 == 1){
													$check6 = "checked";
												}
												if($print10 == "Universitas"){
													$choice ="<select name='5_1_2_2/11/1/".$i."' style='width:80%;'>
														<option value='Program Studi'>Program Studi</option>
														<option value='Universitas' selected>Universitas</option>
													</select>";
												}else{
													$choice ="<select name='5_1_2_2/11/1/".$i."' style='width:80%;'>
														<option value='Program Studi' selected>Program Studi</option>
														<option value='Universitas'>Universitas</option>
													</select>";
												}
												
												echo "<tr>";
												echo "<td>I</td>";
												echo "<td><input type='text' class='form-control' name='5_1_2_2/2/1/".$i."' value='".$print1."'></td>";
												echo "<td><input type='text' class='form-control' name='5_1_2_2/3/1/".$i."' value='".$print2."'></td>";
												echo "<td><input type='text' pattern='[0-9]+([\-][0-9]+)?' class='form-control' name='5_1_2_2/4/1/".$i."' value='".$print3."' onchange='update122a(3)'></td>";
												echo "<td><input type='checkbox' name='5_1_2_2/5/1/".$i."' value='1' ".$check1." onchange='check51(4)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/6/1/".$i."' value='1' ".$check2." onchange='check61(5)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/7/1/".$i."' value='1' ".$check3." onchange='check71(6)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/8/1/".$i."' value='1' ".$check4." onchange='check81(7)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/9/1/".$i."' value='1' ".$check5." onchange='check91(8)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/10/1/".$i."' value='1' ".$check6." onchange='check101(9)'>";
												echo "<td>".$choice."</td>";
												echo "</tr>";
											}
										?>
									<tr>
										<td colspan="11"><button type="button" onclick="addRow121('addTable21','5_1_2_2/1/counter')"><span class="glyphicon glyphicon-plus"></span></button></td>
									</tr>
									<tr>
										<td colspan="3">TOTAL SKS MINIMUM</td>
										<td id="5.1.2.2/1"></td>
										<td id="5.1.2.2/1/inti"></td>
										<td id="5.1.2.2/1/instusi"></td>
										<td id="5.1.2.2/1/Tugas"></td>
										<td id="5.1.2.2/1/Check/1"></td>
										<td id="5.1.2.2/1/Check/2"></td>
										<td id="5.1.2.2/1/Check/3"></td>
										<td class="voidColor"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
<!--Semester2-->
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">Semester 2</label>
						<div class="col-sm-offset-2 col-sm-9 table-wrapper">
							<?php
								$butir = "5.1.2.2";
								$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$null."' AND no='".$null."' AND sub_no='".$no2."'";
								$data = mysqli_query($db, $query);
								$count = mysqli_num_rows($data);
								if ($count>0) {
									$row = mysqli_fetch_assoc($data);
									$ValueCounter = $row["isi_float"];
								}else{
									$ValueCounter = 0;
								}
								echo "<input type='hidden' id='5_1_2_2/2/counter' name='5_1_2_2/2/counter' value='".$ValueCounter."'>";
							?>
							<table class="tableBorang" id="addTable22" style="width:180%;">						
								<thead>
									<tr>
										<th rowspan="2">Semester</th>
										<th rowspan="2">Kode Mata Kuliah</th>
										<th rowspan="2">Nama Mata Kuliah</th>
										<th rowspan="2">Bobot SKS</th>
										<th colspan="2">SKS MK</th>
										<th rowspan="2">Tugas</th>
										<th colspan="3">Kelengkapan</th>
										<th rowspan="2">Unit/Jur/Fak Penyelenggara</th>
									</tr>
									<tr>
										<th width="3%">Inti</th>
										<th>Institusi</th>
										<th>Deskripsi</th>
										<th>Silabus</th>
										<th width="3%">SAP</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
										<td>(4)</td>
										<td>(5)</td>
										<td>(6)</td>
										<td>(7)</td>
										<td>(8)</td>
										<td>(9)</td>
										<td>(10)</td>
										<td>(11)</td>
									</tr>
										<?php
											for($i=1;$i<=$ValueCounter;$i++){
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$i."' AND sub_no='".$no2."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print1 = $row["isi_char"];
												
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$i."' AND sub_no='".$no2."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print2 = $row["isi_char"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom4."' AND no='".$i."' AND sub_no='".$no2."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print3 = $row["isi_float"];

												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom5."' AND no='".$i."' AND sub_no='".$no2."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print4 = $row["isi_float"];

												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom6."' AND no='".$i."' AND sub_no='".$no2."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print5 = $row["isi_float"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom7."' AND no='".$i."' AND sub_no='".$no2."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print6 = $row["isi_float"];

												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom8."' AND no='".$i."' AND sub_no='".$no2."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print7 = $row["isi_float"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom9."' AND no='".$i."' AND sub_no='".$no2."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print8 = $row["isi_float"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom10."' AND no='".$i."' AND sub_no='".$no2."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print9 = $row["isi_float"];
												
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom11."' AND no='".$i."' AND sub_no='".$no2."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print10 = $row["isi_char"];
												
												$check1="";
												$check2="";
												$check3="";
												$check4="";
												$check5="";
												$check6="";
												$choice="";
												if($print4 == 1){
													$check1 = "checked";
												}
												if($print5 == 1){
													$check2 = "checked";
												}
												if($print6 == 1){
													$check3 = "checked";
												}
												if($print7 == 1){
													$check4 = "checked";
												}
												if($print8 == 1){
													$check5 = "checked";
												}
												if($print9 == 1){
													$check6 = "checked";
												}
												if($print10 == "Universitas"){
													$choice ="<select name='5_1_2_2/11/2/".$i."' style='width:80%;'>
														<option value='Program Studi'>Program Studi</option>
														<option value='Universitas' selected>Universitas</option>
													</select>";
												}else{
													$choice ="<select name='5_1_2_2/11/2/".$i."' style='width:80%;'>
														<option value='Program Studi' selected>Program Studi</option>
														<option value='Universitas'>Universitas</option>
													</select>";
												}
												
												echo "<tr>";
												echo "<td>II</td>";
												echo "<td><input type='text' class='form-control' name='5_1_2_2/2/2/".$i."' value='".$print1."'></td>";
												echo "<td><input type='text' class='form-control' name='5_1_2_2/3/2/".$i."' value='".$print2."'></td>";
												echo "<td><input type='text' pattern='[0-9]+([\-][0-9]+)?' class='form-control' name='5_1_2_2/4/2/".$i."' value='".$print3."' onchange='update122b(3)'></td>";
												echo "<td><input type='checkbox' name='5_1_2_2/5/2/".$i."' value='1' ".$check1." onchange='check52(4)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/6/2/".$i."' value='1' ".$check2." onchange='check62(5)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/7/2/".$i."' value='1' ".$check3." onchange='check72(6)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/8/2/".$i."' value='1' ".$check4." onchange='check82(7)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/9/2/".$i."' value='1' ".$check5." onchange='check92(8)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/10/2/".$i."' value='1' ".$check6." onchange='check102(9)'>";
												echo "<td>".$choice."</td>";
												echo "</tr>";
											}
										?>
									<tr>
										<td colspan="11"><button type="button" onclick="addRow122('addTable22','5_1_2_2/2/counter')"><span class="glyphicon glyphicon-plus"></span></button></td>
									</tr>
									<tr>
										<td colspan="3">TOTAL SKS MINIMUM</td>
										<td id="5.1.2.2/2"></td>
										<td id="5.1.2.2/2/inti"></td>
										<td id="5.1.2.2/2/instusi"></td>
										<td id="5.1.2.2/2/Tugas"></td>
										<td id="5.1.2.2/2/Check/1"></td>
										<td id="5.1.2.2/2/Check/2"></td>
										<td id="5.1.2.2/2/Check/3"></td>
										<td class="voidColor"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
<!--Semester3-->
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">Semester 3</label>
						<div class="col-sm-offset-2 col-sm-9 table-wrapper">
							<?php
								$butir = "5.1.2.2";
								$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$null."' AND no='".$null."' AND sub_no='".$no3."'";
								$data = mysqli_query($db, $query);
								$count = mysqli_num_rows($data);
								if ($count>0) {
									$row = mysqli_fetch_assoc($data);
									$ValueCounter = $row["isi_float"];
								}else{
									$ValueCounter = 0;
								}
								echo "<input type='hidden' id='5_1_2_2/3/counter' name='5_1_2_2/3/counter' value='".$ValueCounter."'>";
							?>
							<table class="tableBorang" id="addTable23" style="width:180%;">						
								<thead>
									<tr>
										<th rowspan="2">Semester</th>
										<th rowspan="2">Kode Mata Kuliah</th>
										<th rowspan="2">Nama Mata Kuliah</th>
										<th rowspan="2">Bobot SKS</th>
										<th colspan="2">SKS MK</th>
										<th rowspan="2">Tugas</th>
										<th colspan="3">Kelengkapan</th>
										<th rowspan="2">Unit/Jur/Fak Penyelenggara</th>
									</tr>
									<tr>
										<th width="3%">Inti</th>
										<th>Institusi</th>
										<th>Deskripsi</th>
										<th>Silabus</th>
										<th width="3%">SAP</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
										<td>(4)</td>
										<td>(5)</td>
										<td>(6)</td>
										<td>(7)</td>
										<td>(8)</td>
										<td>(9)</td>
										<td>(10)</td>
										<td>(11)</td>
									</tr>
										<?php
											for($i=1;$i<=$ValueCounter;$i++){
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$i."' AND sub_no='".$no3."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print1 = $row["isi_char"];
												
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$i."' AND sub_no='".$no3."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print2 = $row["isi_char"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom4."' AND no='".$i."' AND sub_no='".$no3."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print3 = $row["isi_float"];

												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom5."' AND no='".$i."' AND sub_no='".$no3."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print4 = $row["isi_float"];

												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom6."' AND no='".$i."' AND sub_no='".$no3."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print5 = $row["isi_float"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom7."' AND no='".$i."' AND sub_no='".$no3."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print6 = $row["isi_float"];

												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom8."' AND no='".$i."' AND sub_no='".$no3."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print7 = $row["isi_float"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom9."' AND no='".$i."' AND sub_no='".$no3."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print8 = $row["isi_float"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom10."' AND no='".$i."' AND sub_no='".$no3."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print9 = $row["isi_float"];
												
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom11."' AND no='".$i."' AND sub_no='".$no3."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print10 = $row["isi_char"];
												
												$check1="";
												$check2="";
												$check3="";
												$check4="";
												$check5="";
												$check6="";
												$choice="";
												if($print4 == 1){
													$check1 = "checked";
												}
												if($print5 == 1){
													$check2 = "checked";
												}
												if($print6 == 1){
													$check3 = "checked";
												}
												if($print7 == 1){
													$check4 = "checked";
												}
												if($print8 == 1){
													$check5 = "checked";
												}
												if($print9 == 1){
													$check6 = "checked";
												}
												if($print10 == "Universitas"){
													$choice ="<select name='5_1_2_2/11/3/".$i."' style='width:80%;'>
														<option value='Program Studi'>Program Studi</option>
														<option value='Universitas' selected>Universitas</option>
													</select>";
												}else{
													$choice ="<select name='5_1_2_2/11/3/".$i."' style='width:80%;'>
														<option value='Program Studi' selected>Program Studi</option>
														<option value='Universitas'>Universitas</option>
													</select>";
												}
												
												echo "<tr>";
												echo "<td>III</td>";
												echo "<td><input type='text' class='form-control' name='5_1_2_2/2/3/".$i."' value='".$print1."'></td>";
												echo "<td><input type='text' class='form-control' name='5_1_2_2/3/3/".$i."' value='".$print2."'></td>";
												echo "<td><input type='text' pattern='[0-9]+([\-][0-9]+)?' class='form-control' name='5_1_2_2/4/3/".$i."' value='".$print3."' onchange='update122c(3)'></td>";
												echo "<td><input type='checkbox' name='5_1_2_2/5/3/".$i."' value='1' ".$check1." onchange='check53(4)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/6/3/".$i."' value='1' ".$check2." onchange='check63(5)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/7/3/".$i."' value='1' ".$check3." onchange='check73(6)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/8/3/".$i."' value='1' ".$check4." onchange='check83(7)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/9/3/".$i."' value='1' ".$check5." onchange='check93(8)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/10/3/".$i."' value='1' ".$check6." onchange='check103(9)'>";
												echo "<td>".$choice."</td>";
												echo "</tr>";
											}
										?>
									<tr>
										<td colspan="11"><button type="button" onclick="addRow123('addTable23','5_1_2_2/3/counter')"><span class="glyphicon glyphicon-plus"></span></button></td>
									</tr>
									<tr>
										<td colspan="3">TOTAL SKS MINIMUM</td>
										<td id="5.1.2.2/3"></td>
										<td id="5.1.2.2/3/inti"></td>
										<td id="5.1.2.2/3/instusi"></td>
										<td id="5.1.2.2/3/Tugas"></td>
										<td id="5.1.2.2/3/Check/1"></td>
										<td id="5.1.2.2/3/Check/2"></td>
										<td id="5.1.2.2/3/Check/3"></td>
										<td class="voidColor"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
<!--Semester4-->
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">Semester 4</label>
						<div class="col-sm-offset-2 col-sm-9 table-wrapper">
							<?php
								$butir = "5.1.2.2";
								$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$null."' AND no='".$null."' AND sub_no='".$no4."'";
								$data = mysqli_query($db, $query);
								$count = mysqli_num_rows($data);
								if ($count>0) {
									$row = mysqli_fetch_assoc($data);
									$ValueCounter = $row["isi_float"];
								}else{
									$ValueCounter = 0;
								}
								echo "<input type='hidden' id='5_1_2_2/4/counter' name='5_1_2_2/4/counter' value='".$ValueCounter."'>";
							?>
							<table class="tableBorang" id="addTable24" style="width:180%;">						
								<thead>
									<tr>
										<th rowspan="2">Semester</th>
										<th rowspan="2">Kode Mata Kuliah</th>
										<th rowspan="2">Nama Mata Kuliah</th>
										<th rowspan="2">Bobot SKS</th>
										<th colspan="2">SKS MK</th>
										<th rowspan="2">Tugas</th>
										<th colspan="3">Kelengkapan</th>
										<th rowspan="2">Unit/Jur/Fak Penyelenggara</th>
									</tr>
									<tr>
										<th width="3%">Inti</th>
										<th>Institusi</th>
										<th>Deskripsi</th>
										<th>Silabus</th>
										<th width="3%">SAP</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
										<td>(4)</td>
										<td>(5)</td>
										<td>(6)</td>
										<td>(7)</td>
										<td>(8)</td>
										<td>(9)</td>
										<td>(10)</td>
										<td>(11)</td>
									</tr>
										<?php
											for($i=1;$i<=$ValueCounter;$i++){
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$i."' AND sub_no='".$no4."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print1 = $row["isi_char"];
												
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$i."' AND sub_no='".$no4."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print2 = $row["isi_char"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom4."' AND no='".$i."' AND sub_no='".$no4."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print3 = $row["isi_float"];

												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom5."' AND no='".$i."' AND sub_no='".$no4."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print4 = $row["isi_float"];

												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom6."' AND no='".$i."' AND sub_no='".$no4."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print5 = $row["isi_float"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom7."' AND no='".$i."' AND sub_no='".$no4."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print6 = $row["isi_float"];

												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom8."' AND no='".$i."' AND sub_no='".$no4."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print7 = $row["isi_float"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom9."' AND no='".$i."' AND sub_no='".$no4."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print8 = $row["isi_float"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom10."' AND no='".$i."' AND sub_no='".$no4."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print9 = $row["isi_float"];
												
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom11."' AND no='".$i."' AND sub_no='".$no4."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print10 = $row["isi_char"];
												
												$check1="";
												$check2="";
												$check3="";
												$check4="";
												$check5="";
												$check6="";
												$choice="";
												if($print4 == 1){
													$check1 = "checked";
												}
												if($print5 == 1){
													$check2 = "checked";
												}
												if($print6 == 1){
													$check3 = "checked";
												}
												if($print7 == 1){
													$check4 = "checked";
												}
												if($print8 == 1){
													$check5 = "checked";
												}
												if($print9 == 1){
													$check6 = "checked";
												}
												if($print10 == "Universitas"){
													$choice ="<select name='5_1_2_2/11/4/".$i."' style='width:80%;'>
														<option value='Program Studi'>Program Studi</option>
														<option value='Universitas' selected>Universitas</option>
													</select>";
												}else{
													$choice ="<select name='5_1_2_2/11/4/".$i."' style='width:80%;'>
														<option value='Program Studi' selected>Program Studi</option>
														<option value='Universitas'>Universitas</option>
													</select>";
												}
												
												echo "<tr>";
												echo "<td>IV</td>";
												echo "<td><input type='text' class='form-control' name='5_1_2_2/2/4/".$i."' value='".$print1."'></td>";
												echo "<td><input type='text' class='form-control' name='5_1_2_2/3/4/".$i."' value='".$print2."'></td>";
												echo "<td><input type='text' pattern='[0-9]+([\-][0-9]+)?' class='form-control' name='5_1_2_2/4/4/".$i."' value='".$print3."' onchange='update122d(3)'></td>";
												echo "<td><input type='checkbox' name='5_1_2_2/5/4/".$i."' value='1' ".$check1." onchange='check54(4)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/6/4/".$i."' value='1' ".$check2." onchange='check64(5)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/7/4/".$i."' value='1' ".$check3." onchange='check74(6)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/8/4/".$i."' value='1' ".$check4." onchange='check84(7)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/9/4/".$i."' value='1' ".$check5." onchange='check94(8)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/10/4/".$i."' value='1' ".$check6." onchange='check104(9)'>";
												echo "<td>".$choice."</td>";
												echo "</tr>";
											}
										?>
									<tr>
										<td colspan="11"><button type="button" onclick="addRow124('addTable24','5_1_2_2/4/counter')"><span class="glyphicon glyphicon-plus"></span></button></td>
									</tr>
									<tr>
										<td colspan="3">TOTAL SKS MINIMUM</td>
										<td id="5.1.2.2/4"></td>
										<td id="5.1.2.2/4/inti"></td>
										<td id="5.1.2.2/4/instusi"></td>
										<td id="5.1.2.2/4/Tugas"></td>
										<td id="5.1.2.2/4/Check/1"></td>
										<td id="5.1.2.2/4/Check/2"></td>
										<td id="5.1.2.2/4/Check/3"></td>
										<td class="voidColor"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
<!--Semester5-->
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">Semester 5</label>
						<div class="col-sm-offset-2 col-sm-9 table-wrapper">
							<?php
								$butir = "5.1.2.2";
								$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$null."' AND no='".$null."' AND sub_no='".$no5."'";
								$data = mysqli_query($db, $query);
								$count = mysqli_num_rows($data);
								if ($count>0) {
									$row = mysqli_fetch_assoc($data);
									$ValueCounter = $row["isi_float"];
								}else{
									$ValueCounter = 0;
								}
								echo "<input type='hidden' id='5_1_2_2/5/counter' name='5_1_2_2/5/counter' value='".$ValueCounter."'>";
							?>
							<table class="tableBorang" id="addTable25" style="width:180%;">						
								<thead>
									<tr>
										<th rowspan="2">Semester</th>
										<th rowspan="2">Kode Mata Kuliah</th>
										<th rowspan="2">Nama Mata Kuliah</th>
										<th rowspan="2">Bobot SKS</th>
										<th colspan="2">SKS MK</th>
										<th rowspan="2">Tugas</th>
										<th colspan="3">Kelengkapan</th>
										<th rowspan="2">Unit/Jur/Fak Penyelenggara</th>
									</tr>
									<tr>
										<th width="3%">Inti</th>
										<th>Institusi</th>
										<th>Deskripsi</th>
										<th>Silabus</th>
										<th width="3%">SAP</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
										<td>(4)</td>
										<td>(5)</td>
										<td>(6)</td>
										<td>(7)</td>
										<td>(8)</td>
										<td>(9)</td>
										<td>(10)</td>
										<td>(11)</td>
									</tr>
										<?php
											for($i=1;$i<=$ValueCounter;$i++){
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$i."' AND sub_no='".$no5."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print1 = $row["isi_char"];
												
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$i."' AND sub_no='".$no5."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print2 = $row["isi_char"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom4."' AND no='".$i."' AND sub_no='".$no5."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print3 = $row["isi_float"];

												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom5."' AND no='".$i."' AND sub_no='".$no5."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print4 = $row["isi_float"];

												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom6."' AND no='".$i."' AND sub_no='".$no5."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print5 = $row["isi_float"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom7."' AND no='".$i."' AND sub_no='".$no5."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print6 = $row["isi_float"];

												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom8."' AND no='".$i."' AND sub_no='".$no5."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print7 = $row["isi_float"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom9."' AND no='".$i."' AND sub_no='".$no5."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print8 = $row["isi_float"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom10."' AND no='".$i."' AND sub_no='".$no5."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print9 = $row["isi_float"];
												
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom11."' AND no='".$i."' AND sub_no='".$no5."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print10 = $row["isi_char"];
												
												$check1="";
												$check2="";
												$check3="";
												$check4="";
												$check5="";
												$check6="";
												$choice="";
												if($print4 == 1){
													$check1 = "checked";
												}
												if($print5 == 1){
													$check2 = "checked";
												}
												if($print6 == 1){
													$check3 = "checked";
												}
												if($print7 == 1){
													$check4 = "checked";
												}
												if($print8 == 1){
													$check5 = "checked";
												}
												if($print9 == 1){
													$check6 = "checked";
												}
												if($print10 == "Universitas"){
													$choice ="<select name='5_1_2_2/11/5/".$i."' style='width:80%;'>
														<option value='Program Studi'>Program Studi</option>
														<option value='Universitas' selected>Universitas</option>
													</select>";
												}else{
													$choice ="<select name='5_1_2_2/11/5/".$i."' style='width:80%;'>
														<option value='Program Studi' selected>Program Studi</option>
														<option value='Universitas'>Universitas</option>
													</select>";
												}
												
												echo "<tr>";
												echo "<td>V</td>";
												echo "<td><input type='text' class='form-control' name='5_1_2_2/2/5/".$i."' value='".$print1."'></td>";
												echo "<td><input type='text' class='form-control' name='5_1_2_2/3/5/".$i."' value='".$print2."'></td>";
												echo "<td><input type='text' pattern='[0-9]+([\-][0-9]+)?' class='form-control' name='5_1_2_2/4/5/".$i."' value='".$print3."' onchange='update122e(3)'></td>";
												echo "<td><input type='checkbox' name='5_1_2_2/5/5/".$i."' value='1' ".$check1." onchange='check55(4)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/6/5/".$i."' value='1' ".$check2." onchange='check65(5)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/7/5/".$i."' value='1' ".$check3." onchange='check75(6)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/8/5/".$i."' value='1' ".$check4." onchange='check85(7)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/9/5/".$i."' value='1' ".$check5." onchange='check95(8)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/10/5/".$i."' value='1' ".$check6." onchange='check105(9)'>";
												echo "<td>".$choice."</td>";
												echo "</tr>";
											}
										?>
									<tr>
										<td colspan="11"><button type="button" onclick="addRow125('addTable25','5_1_2_2/5/counter')"><span class="glyphicon glyphicon-plus"></span></button></td>
									</tr>
									<tr>
										<td colspan="3">TOTAL SKS MINIMUM</td>
										<td id="5.1.2.2/5"></td>
										<td id="5.1.2.2/5/inti"></td>
										<td id="5.1.2.2/5/instusi"></td>
										<td id="5.1.2.2/5/Tugas"></td>
										<td id="5.1.2.2/5/Check/1"></td>
										<td id="5.1.2.2/5/Check/2"></td>
										<td id="5.1.2.2/5/Check/3"></td>
										<td class="voidColor"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
<!--Semester6-->
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">Semester 6</label>
						<div class="col-sm-offset-2 col-sm-9 table-wrapper">
							<?php
								$butir = "5.1.2.2";
								$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$null."' AND no='".$null."' AND sub_no='".$no6."'";
								$data = mysqli_query($db, $query);
								$count = mysqli_num_rows($data);
								if ($count>0) {
									$row = mysqli_fetch_assoc($data);
									$ValueCounter = $row["isi_float"];
								}else{
									$ValueCounter = 0;
								}
								echo "<input type='hidden' id='5_1_2_2/6/counter' name='5_1_2_2/6/counter' value='".$ValueCounter."'>";
							?>
							<table class="tableBorang" id="addTable26" style="width:180%;">						
								<thead>
									<tr>
										<th rowspan="2">Semester</th>
										<th rowspan="2">Kode Mata Kuliah</th>
										<th rowspan="2">Nama Mata Kuliah</th>
										<th rowspan="2">Bobot SKS</th>
										<th colspan="2">SKS MK</th>
										<th rowspan="2">Tugas</th>
										<th colspan="3">Kelengkapan</th>
										<th rowspan="2">Unit/Jur/Fak Penyelenggara</th>
									</tr>
									<tr>
										<th width="3%">Inti</th>
										<th>Institusi</th>
										<th>Deskripsi</th>
										<th>Silabus</th>
										<th width="3%">SAP</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
										<td>(4)</td>
										<td>(5)</td>
										<td>(6)</td>
										<td>(7)</td>
										<td>(8)</td>
										<td>(9)</td>
										<td>(10)</td>
										<td>(11)</td>
									</tr>
										<?php
											for($i=1;$i<=$ValueCounter;$i++){
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$i."' AND sub_no='".$no6."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print1 = $row["isi_char"];
												
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$i."' AND sub_no='".$no6."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print2 = $row["isi_char"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom4."' AND no='".$i."' AND sub_no='".$no6."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print3 = $row["isi_float"];

												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom5."' AND no='".$i."' AND sub_no='".$no6."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print4 = $row["isi_float"];

												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom6."' AND no='".$i."' AND sub_no='".$no6."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print5 = $row["isi_float"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom7."' AND no='".$i."' AND sub_no='".$no6."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print6 = $row["isi_float"];

												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom8."' AND no='".$i."' AND sub_no='".$no6."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print7 = $row["isi_float"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom9."' AND no='".$i."' AND sub_no='".$no6."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print8 = $row["isi_float"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom10."' AND no='".$i."' AND sub_no='".$no6."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print9 = $row["isi_float"];
												
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom11."' AND no='".$i."' AND sub_no='".$no6."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print10 = $row["isi_char"];
												
												$check1="";
												$check2="";
												$check3="";
												$check4="";
												$check5="";
												$check6="";
												$choice="";
												if($print4 == 1){
													$check1 = "checked";
												}
												if($print5 == 1){
													$check2 = "checked";
												}
												if($print6 == 1){
													$check3 = "checked";
												}
												if($print7 == 1){
													$check4 = "checked";
												}
												if($print8 == 1){
													$check5 = "checked";
												}
												if($print9 == 1){
													$check6 = "checked";
												}
												if($print10 == "Universitas"){
													$choice ="<select name='5_1_2_2/11/6/".$i."' style='width:80%;'>
														<option value='Program Studi'>Program Studi</option>
														<option value='Universitas' selected>Universitas</option>
													</select>";
												}else{
													$choice ="<select name='5_1_2_2/11/6/".$i."' style='width:80%;'>
														<option value='Program Studi' selected>Program Studi</option>
														<option value='Universitas'>Universitas</option>
													</select>";
												}
												
												echo "<tr>";
												echo "<td>VI</td>";
												echo "<td><input type='text' class='form-control' name='5_1_2_2/2/6/".$i."' value='".$print1."'></td>";
												echo "<td><input type='text' class='form-control' name='5_1_2_2/3/6/".$i."' value='".$print2."'></td>";
												echo "<td><input type='text' pattern='[0-9]+([\-][0-9]+)?' class='form-control' name='5_1_2_2/4/6/".$i."' value='".$print3."' onchange='update122f(3)'></td>";
												echo "<td><input type='checkbox' name='5_1_2_2/5/6/".$i."' value='1' ".$check1." onchange='check56(4)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/6/6/".$i."' value='1' ".$check2." onchange='check66(5)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/7/6/".$i."' value='1' ".$check3." onchange='check76(6)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/8/6/".$i."' value='1' ".$check4." onchange='check86(7)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/9/6/".$i."' value='1' ".$check5." onchange='check96(8)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/10/6/".$i."' value='1' ".$check6." onchange='check106(9)'>";
												echo "<td>".$choice."</td>";
												echo "</tr>";
											}
										?>
									<tr>
										<td colspan="11"><button type="button" onclick="addRow126('addTable26','5_1_2_2/6/counter')"><span class="glyphicon glyphicon-plus"></span></button></td>
									</tr>
									<tr>
										<td colspan="3">TOTAL SKS MINIMUM</td>
										<td id="5.1.2.2/6"></td>
										<td id="5.1.2.2/6/inti"></td>
										<td id="5.1.2.2/6/instusi"></td>
										<td id="5.1.2.2/6/Tugas"></td>
										<td id="5.1.2.2/6/Check/1"></td>
										<td id="5.1.2.2/6/Check/2"></td>
										<td id="5.1.2.2/6/Check/3"></td>
										<td class="voidColor"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
<!--Semester7-->
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">Semester 7</label>
						<div class="col-sm-offset-2 col-sm-9 table-wrapper">
							<?php
								$butir = "5.1.2.2";
								$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$null."' AND no='".$null."' AND sub_no='".$no7."'";
								$data = mysqli_query($db, $query);
								$count = mysqli_num_rows($data);
								if ($count>0) {
									$row = mysqli_fetch_assoc($data);
									$ValueCounter = $row["isi_float"];
								}else{
									$ValueCounter = 0;
								}
								echo "<input type='hidden' id='5_1_2_2/7/counter' name='5_1_2_2/7/counter' value='".$ValueCounter."'>";
							?>
							<table class="tableBorang" id="addTable27" style="width:180%;">						
								<thead>
									<tr>
										<th rowspan="2">Semester</th>
										<th rowspan="2">Kode Mata Kuliah</th>
										<th rowspan="2">Nama Mata Kuliah</th>
										<th rowspan="2">Bobot SKS</th>
										<th colspan="2">SKS MK</th>
										<th rowspan="2">Tugas</th>
										<th colspan="3">Kelengkapan</th>
										<th rowspan="2">Unit/Jur/Fak Penyelenggara</th>
									</tr>
									<tr>
										<th width="3%">Inti</th>
										<th>Institusi</th>
										<th>Deskripsi</th>
										<th>Silabus</th>
										<th width="3%">SAP</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
										<td>(4)</td>
										<td>(5)</td>
										<td>(6)</td>
										<td>(7)</td>
										<td>(8)</td>
										<td>(9)</td>
										<td>(10)</td>
										<td>(11)</td>
									</tr>
										<?php
											for($i=1;$i<=$ValueCounter;$i++){
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$i."' AND sub_no='".$no7."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print1 = $row["isi_char"];
												
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$i."' AND sub_no='".$no7."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print2 = $row["isi_char"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom4."' AND no='".$i."' AND sub_no='".$no7."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print3 = $row["isi_float"];

												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom5."' AND no='".$i."' AND sub_no='".$no7."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print4 = $row["isi_float"];

												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom6."' AND no='".$i."' AND sub_no='".$no7."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print5 = $row["isi_float"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom7."' AND no='".$i."' AND sub_no='".$no7."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print6 = $row["isi_float"];

												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom8."' AND no='".$i."' AND sub_no='".$no7."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print7 = $row["isi_float"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom9."' AND no='".$i."' AND sub_no='".$no7."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print8 = $row["isi_float"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom10."' AND no='".$i."' AND sub_no='".$no7."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print9 = $row["isi_float"];
												
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom11."' AND no='".$i."' AND sub_no='".$no7."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print10 = $row["isi_char"];
												
												$check1="";
												$check2="";
												$check3="";
												$check4="";
												$check5="";
												$check6="";
												$choice="";
												if($print4 == 1){
													$check1 = "checked";
												}
												if($print5 == 1){
													$check2 = "checked";
												}
												if($print6 == 1){
													$check3 = "checked";
												}
												if($print7 == 1){
													$check4 = "checked";
												}
												if($print8 == 1){
													$check5 = "checked";
												}
												if($print9 == 1){
													$check6 = "checked";
												}
												if($print10 == "Universitas"){
													$choice ="<select name='5_1_2_2/11/7/".$i."' style='width:80%;'>
														<option value='Program Studi'>Program Studi</option>
														<option value='Universitas' selected>Universitas</option>
													</select>";
												}else{
													$choice ="<select name='5_1_2_2/11/7/".$i."' style='width:80%;'>
														<option value='Program Studi' selected>Program Studi</option>
														<option value='Universitas'>Universitas</option>
													</select>";
												}
												
												echo "<tr>";
												echo "<td>VII</td>";
												echo "<td><input type='text' class='form-control' name='5_1_2_2/2/7/".$i."' value='".$print1."'></td>";
												echo "<td><input type='text' class='form-control' name='5_1_2_2/3/7/".$i."' value='".$print2."'></td>";
												echo "<td><input type='text' pattern='[0-9]+([\-][0-9]+)?' class='form-control' name='5_1_2_2/4/7/".$i."' value='".$print3."' onchange='update122g(3)'></td>";
												echo "<td><input type='checkbox' name='5_1_2_2/5/7/".$i."' value='1' ".$check1." onchange='check57(4)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/6/7/".$i."' value='1' ".$check2." onchange='check67(5)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/7/7/".$i."' value='1' ".$check3." onchange='check77(6)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/8/7/".$i."' value='1' ".$check4." onchange='check87(7)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/9/7/".$i."' value='1' ".$check5." onchange='check97(8)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/10/7/".$i."' value='1' ".$check6." onchange='check107(9)'>";
												echo "<td>".$choice."</td>";
												echo "</tr>";
											}
										?>
									<tr>
										<td colspan="11"><button type="button" onclick="addRow127('addTable27','5_1_2_2/7/counter')"><span class="glyphicon glyphicon-plus"></span></button></td>
									</tr>
									<tr>
										<td colspan="3">TOTAL SKS MINIMUM</td>
										<td id="5.1.2.2/7"></td>
										<td id="5.1.2.2/7/inti"></td>
										<td id="5.1.2.2/7/instusi"></td>
										<td id="5.1.2.2/7/Tugas"></td>
										<td id="5.1.2.2/7/Check/1"></td>
										<td id="5.1.2.2/7/Check/2"></td>
										<td id="5.1.2.2/7/Check/3"></td>
										<td class="voidColor"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
<!--Semester8-->
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">Semester 8</label>
						<div class="col-sm-offset-2 col-sm-9 table-wrapper">
							<?php
								$butir = "5.1.2.2";
								$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$null."' AND no='".$null."' AND sub_no='".$no8."'";
								$data = mysqli_query($db, $query);
								$count = mysqli_num_rows($data);
								if ($count>0) {
									$row = mysqli_fetch_assoc($data);
									$ValueCounter = $row["isi_float"];
								}else{
									$ValueCounter = 0;
								}
								echo "<input type='hidden' id='5_1_2_2/8/counter' name='5_1_2_2/8/counter' value='".$ValueCounter."'>";
							?>
							<table class="tableBorang" id="addTable28" style="width:180%;">						
								<thead>
									<tr>
										<th rowspan="2">Semester</th>
										<th rowspan="2">Kode Mata Kuliah</th>
										<th rowspan="2">Nama Mata Kuliah</th>
										<th rowspan="2">Bobot SKS</th>
										<th colspan="2">SKS MK</th>
										<th rowspan="2">Tugas</th>
										<th colspan="3">Kelengkapan</th>
										<th rowspan="2">Unit/Jur/Fak Penyelenggara</th>
									</tr>
									<tr>
										<th width="3%">Inti</th>
										<th>Institusi</th>
										<th>Deskripsi</th>
										<th>Silabus</th>
										<th width="3%">SAP</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
										<td>(4)</td>
										<td>(5)</td>
										<td>(6)</td>
										<td>(7)</td>
										<td>(8)</td>
										<td>(9)</td>
										<td>(10)</td>
										<td>(11)</td>
									</tr>
										<?php
											for($i=1;$i<=$ValueCounter;$i++){
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$i."' AND sub_no='".$no8."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print1 = $row["isi_char"];
												
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$i."' AND sub_no='".$no8."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print2 = $row["isi_char"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom4."' AND no='".$i."' AND sub_no='".$no8."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print3 = $row["isi_float"];

												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom5."' AND no='".$i."' AND sub_no='".$no8."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print4 = $row["isi_float"];

												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom6."' AND no='".$i."' AND sub_no='".$no8."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print5 = $row["isi_float"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom7."' AND no='".$i."' AND sub_no='".$no8."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print6 = $row["isi_float"];

												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom8."' AND no='".$i."' AND sub_no='".$no8."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print7 = $row["isi_float"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom9."' AND no='".$i."' AND sub_no='".$no8."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print8 = $row["isi_float"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom10."' AND no='".$i."' AND sub_no='".$no8."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print9 = $row["isi_float"];
												
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom11."' AND no='".$i."' AND sub_no='".$no8."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print10 = $row["isi_char"];
												
												$check1="";
												$check2="";
												$check3="";
												$check4="";
												$check5="";
												$check6="";
												$choice="";
												if($print4 == 1){
													$check1 = "checked";
												}
												if($print5 == 1){
													$check2 = "checked";
												}
												if($print6 == 1){
													$check3 = "checked";
												}
												if($print7 == 1){
													$check4 = "checked";
												}
												if($print8 == 1){
													$check5 = "checked";
												}
												if($print9 == 1){
													$check6 = "checked";
												}
												if($print10 == "Universitas"){
													$choice ="<select name='5_1_2_2/11/8/".$i."' style='width:80%;'>
														<option value='Program Studi'>Program Studi</option>
														<option value='Universitas' selected>Universitas</option>
													</select>";
												}else{
													$choice ="<select name='5_1_2_2/11/8/".$i."' style='width:80%;'>
														<option value='Program Studi' selected>Program Studi</option>
														<option value='Universitas'>Universitas</option>
													</select>";
												}
												
												echo "<tr>";
												echo "<td>VIII</td>";
												echo "<td><input type='text' class='form-control' name='5_1_2_2/2/8/".$i."' value='".$print1."'></td>";
												echo "<td><input type='text' class='form-control' name='5_1_2_2/3/8/".$i."' value='".$print2."'></td>";
												echo "<td><input type='text' pattern='[0-9]+([\-][0-9]+)?' class='form-control' name='5_1_2_2/4/8/".$i."' value='".$print3."' onchange='update122h(3)'></td>";
												echo "<td><input type='checkbox' name='5_1_2_2/5/8/".$i."' value='1' ".$check1." onchange='check58(4)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/6/8/".$i."' value='1' ".$check2." onchange='check68(5)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/7/8/".$i."' value='1' ".$check3." onchange='check78(6)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/8/8/".$i."' value='1' ".$check4." onchange='check88(7)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/9/8/".$i."' value='1' ".$check5." onchange='check98(8)'>";
												echo "<td><input type='checkbox' name='5_1_2_2/10/8/".$i."' value='1' ".$check6."  onchange='check108(9)'>";
												echo "<td>".$choice."</td>";
												echo "</tr>";
											}
										?>
									<tr>
										<td colspan="11"><button type="button" onclick="addRow128('addTable28','5_1_2_2/8/counter')"><span class="glyphicon glyphicon-plus"></span></button></td>
									</tr>
									<tr>
										<td colspan="3">TOTAL SKS MINIMUM</td>
										<td id="5.1.2.2/8"></td>
										<td id="5.1.2.2/8/inti"></td>
										<td id="5.1.2.2/8/instusi"></td>
										<td id="5.1.2.2/8/Tugas"></td>
										<td id="5.1.2.2/8/Check/1"></td>
										<td id="5.1.2.2/8/Check/2"></td>
										<td id="5.1.2.2/8/Check/3"></td>
										<td class="voidColor"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered" id="totalTable121">
								<thead>
									<tr>
										<th rowspan='2'>Total</th>
										<th rowspan='2' width="30%" class="text-center">Bobot SKS</th>
										<th colspan='2' class="text-center">SKS MK</th>
										<th rowspan='2' class="text-center">Tugas</th>
										<th colspan='3' class="text-center">Kelengkapan</th>
									</tr>
									<tr>
										<th class="text-center">inti</th>
										<th class="text-center">instusi</th>
										<th class="text-center">Deskripsi</th>
										<th class="text-center">Silabus</th>
										<th class="text-center">SAP</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<td id="total1"></td>
										<td id="total2"></td>
										<td id="total3"></td>
										<td id="total4"></td>
										<td id="total5"></td>
										<td id="total6"></td>
										<td id="total7"></td>
										<input type="hidden" id="3box1">
										<input type="hidden" id="3box2">
										<input type="hidden" id="3box3">
										<input type="hidden" id="3box4">
										<input type="hidden" id="3box5">
										<input type="hidden" id="3box6">
										<input type="hidden" id="3box7">
										<input type="hidden" id="3box8">
										<input type="hidden" id="total3box">
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "5.1.2.a";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="7">5.1.2.a</td>
										<td colspan="3">Kesesuaian matakuliah dan urutannya dengan standar kompetensi PS.</td>
									</tr>
									<tr>
										<td>0</td>
										<td>Tidak tidak memiliki standar kompetensi.</td>
										<td></td>
									</tr>
									<tr>
										<td>1</td>
										<td>Tidak sesuai dengan standar kompetensi.</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Sesuai dengan standar kompetensi, tetapi masih berorientasi ke masa lalu.</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Sesuai dengan standar kompetensi, berorientasi ke masa kini.</td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Sesuai dengan standar kompetensi, sudah berorientasi  ke masa depan.</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control"  placeholder="" value='<?php echo $print;?>' name="nilai/5_1_2_a"/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="5">5.1.2.b</td>
										<td colspan="3">Persentase mata kuliah  yang dalam penentuan nilai akhirnya memberikan bobot pada tugas-tugas (praktikum/praktek, PR atau makalah) ≥ 20%.</td>
									</tr>
									<tr>
										<td colspan="2">Jumlah MK pada kolom 7 yg ada tanda centang (v)</td>
										<td id="totalkolom7"></td>
									</tr>
									<tr>
										<td colspan="2">Jumlah MK wajib +  pilihan</td>
										<td id="totalmkwj"></td>
									</tr>
									<tr>
										<td>PTGS</td>
										<td>Persentase MK dengan tugas >=20%</td>
										<td id="nilaiPTGS"></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td id="nilai/5_1_2_b"></td>
										<input type="hidden" name="nilai/5_1_2_b" id="nilai/512b">
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="5">5.1.2.c</td>
										<td colspan="3">Mata kuliah dilengkapi dengan deskripsi mata kuliah, silabus dan SAP.</td>
									</tr>
									<tr>
										<td colspan="2">Jumlah MK dengan deskripsi, silabus, dan SAP</td>
										<td id="total3boxnilai"></td>
									</tr>
									<tr>
										<td colspan="2">Jumlah seluruh MK</td>
										<td id="smk"></td>
									</tr>
									<tr>
										<td colspan="2">Persentase mata kuliah yang memiliki deskripsi, silabus dan SAP</td>
										<td id="pts"></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td id='nilai/5_1_2_c'></td>
										<input type="hidden" name="nilai/5_1_2_c" id="nilai/512c">
									</tr>
								</tbody>
							</table>
						</div>
					</div>
<!--513-->
					<div class="form-group">
						<label class="col-sm-2 text-right">5.1.3</label> 
						<label class="col-sm-8">Mata kuliah pilihan Program Studi Teknik Informatika yang dilaksanakan tiga tahun terakhir:</label>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-9 table-wrapper">
							<?php
								$butir = "5.1.3";
								$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$null."' AND no='".$null."'";
								$data = mysqli_query($db, $query);
								$count = mysqli_num_rows($data);
								if ($count>0) {
									$row = mysqli_fetch_assoc($data);
									$ValueCounter = $row["isi_float"];
								}else{
									$ValueCounter = 0;
								}
								echo "<input type='hidden' id='5_1_3/counter' name='5_1_3/counter' value='".$ValueCounter."'>";
							?>
							<table class="tableBorang" id="addTable13" style="width:180%;">						
								<thead>
									<tr>
										<th>Semester</th>
										<th>Kode</th>
										<th>Mata Kuliah</th>
										<th>Bobot SKS</th>
										<th>Tugas</th>
										<th>Unit/ Jur/ Fak Penyelenggara</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
										<td>(4)</td>
										<td>(5)</td>
										<td>(6)</td>
									</tr>
										<?php
											for($i=1;$i<=$ValueCounter;$i++){
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$i."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print1 = $row["isi_char"];
												
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$i."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print2 = $row["isi_char"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom4."' AND no='".$i."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print3 = $row["isi_float"];

												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom5."' AND no='".$i."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print4 = $row["isi_float"];
												
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom6."' AND no='".$i."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print5 = $row["isi_char"];
												
												$check1="";
												if($print4 == 1){
													$check1 = "checked";
												}
												if($print5 == "Universitas"){
													$choice ="<select name='5_1_3/6/".$i."' style='width:80%;'>
														<option value='Program Studi'>Program Studi</option>
														<option value='Universitas' selected>Universitas</option>
													</select>";
												}else{
													$choice ="<select name='5_1_3/6/".$i."' style='width:80%;'>
														<option value='Program Studi' selected>Program Studi</option>
														<option value='Universitas'>Universitas</option>
													</select>";
												}
												
												echo "<tr>";
												echo "<td>Genap dan Ganjil</td>";
												echo "<td><input type='text' class='form-control' name='5_1_3/2/".$i."' value='".$print1."'></td>";
												echo "<td><input type='text' class='form-control' name='5_1_3/3/".$i."' value='".$print2."'></td>";
												echo "<td><input type='text' pattern='[0-9]+([\-][0-9]+)?' class='form-control' name='5_1_3/4/".$i."' value='".$print3."' onchange='totalbobotpilihan(3)'></td>";
												echo "<td><input type='checkbox' name='5_1_3/5/".$i."' value='1' ".$check1.">";
												echo "<td>".$choice."</td>";
												echo "</tr>";
											}
										?>
									<tr>
										<td colspan="6"><button type="button" onclick="addRow13('addTable13','5_1_3/counter')"><span class="glyphicon glyphicon-plus"></span></button></td>
									</tr>
									<tr>
										<td colspan="3">TOTAL SKS PILIHAN</td>
										<td id="bobotskspilihan"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="5">5.1.3</td>
										<td colspan="3">Fleksibilitas mata kuliah pilihan</td>
									</tr>
									<tr>
										<td colspan="2">Bobot MK pilihan yang harus diambil</td>
										<td id="mkpnilai"></td>
									</tr>
									<tr>
										<td colspan="2">MK pilihan yang disediakan</td>
										<td id="mkpdisediakan"></td>
									</tr>
									<tr>
										<td colspan="2">Rasio sks mata kuliah pilihan yang disediakan terhadap jumlah sks yang diwajibkan</td>
										<td id="rasionilai513"></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td id='nilai/5_1_3'></td>
										<input type="hidden" name="nilai/5_1_3" id="nilai/513">
									</tr>
								</tbody>
							</table>
						</div>
					</div>
<!--514-->
					<div class="form-group">
						<label class="col-sm-2 text-right">5.1.4</label> 
						<label class="col-sm-8">Mata kuliah pilihan Program Studi Teknik Informatika yang dilaksanakan tiga tahun terakhir:</label>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-9 table-wrapper">
							<?php
								$butir = "5.1.4";
								$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$null."' AND no='".$null."'";
								$data = mysqli_query($db, $query);
								$count = mysqli_num_rows($data);
								if ($count>0) {
									$row = mysqli_fetch_assoc($data);
									$ValueCounter = $row["isi_float"];
								}else{
									$ValueCounter = 0;
								}
								echo "<input type='hidden' id='5_1_4/counter' name='5_1_4/counter' value='".$ValueCounter."'>";
							?>
							<table class="tableBorang" id="addTable14" style="width:180%;">						
								<thead>
									<tr>
										<th>No</th>
										<th>Mata kuliah</th>
										<th>Isi Pratikum</th>
										<th>Jam Pelaksanaan / Menit</th>
										<th>Tempat Pratikum</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
										<td>(4)</td>
										<td>(5)</td>
									</tr>
										<?php
											for($i=1;$i<=$ValueCounter;$i++){
												$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$i."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print1 = $row["isi_text"];
												
												$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$i."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print2 = $row["isi_text"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom4."' AND no='".$i."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print3 = $row["isi_float"];
												
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom5."' AND no='".$i."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print4 = $row["isi_char"];
												
												$choice="";
												
												if($print4 == "Kelas"){
													$choice ="<select name='5_1_4/5/".$i."' style='width:80%;'>
														<option value='Kelas' selected>Kelas</option>
														<option value='Laboratorium Komputer'>Laboratorium Komputer</option>
													</select>";
												}else{
													$choice ="<select name='5_1_4/5/".$i."' style='width:80%;'>
														<option value='Kelas'>Kelas</option>
														<option value='Laboratorium Komputer' selected>Laboratorium Komputer</option>
													</select>";
												}
												
												echo "<tr>";
												echo "<td>".$i."</td>";
												echo "<td><textarea type='text' rows='3' class='form-control' name='5_1_4/2/".$i."'>".$print1."</textarea></td>";
												echo "<td><textarea type='text' rows='3' class='form-control' name='5_1_4/3/".$i."'>".$print2."</textarea></td>";
												echo "<td><input type='text' pattern='[0-9]+([\-][0-9]+)?' class='form-control' name='5_1_4/4/".$i."' value='".$print3."'></td>";
												echo "<td>".$choice."</td>";
												echo "</tr>";
											}
										?>
									<tr>
										<td colspan="6"><button type="button" onclick="addRow14('addTable14','5_1_4/counter')"><span class="glyphicon glyphicon-plus"></span></button></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "5.1.4";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="6">5.1.4</td>
										<td colspan="3">Substansi praktikum dan pelaksanaan praktikum.</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Pelaksanaan modul praktikum kurang dari minimum.</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Pelaksanaan modul praktikum cukup, tetapi dilaksanakan di PT lain.</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Pelaksanaan modul praktikum cukup, dilaksanakan di PT sendiri.</td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Pelaksanaan modul praktikum lebih dari cukup  (ditambah dengan demonstrasi di laboratorium ) di PT sendiri.</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control"  placeholder="" value='<?php echo $print;?>' name="nilai/5_1_4"/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
<!--52-->
					<div class="form-group">
						<label class="col-sm-2 text-right">5.2</label> 
						<label class="col-sm-8">Peninjauan Kurikulum dalam 5 Tahun Terakhir</label>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "5.2";
							$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
							$data = mysqli_query($db, $query);
							$row = mysqli_fetch_assoc($data);
							$print = $row["isi_text"];
							?>
							<textarea type="text" class="form-control" rows="10" name="5_2" placeholder="" maxlength="60000"><?php echo $print; ?></textarea>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">Hasil peninjauan kurikulum:</label> 
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-87 table-wrapper">
							<?php
								$butir = "5.2";
								$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$null."' AND no='".$null."' AND sub_no='".$no1."'";
								$data = mysqli_query($db, $query);
								$count = mysqli_num_rows($data);
								if ($count>0) {
									$row = mysqli_fetch_assoc($data);
									$ValueCounter = $row["isi_float"];
								}else{
									$ValueCounter = 0;
								}
								echo "<input type='hidden' id='5_2/counter' name='5_2/counter' value='".$ValueCounter."'>";
							?>
							<table class="tableBorang" id="addTable02" style="width:180%;">						
								<thead>
									<tr>
										<th rowspan="2">No.</th>
										<th rowspan="2">Kode Mata Kuliah</th>
										<th rowspan="2">Nama Mata Kuliah</th>
										<th rowspan="2">MK Baru/Lama/Hapus</th>
										<th colspan="2">Perubahan</th>
										<th rowspan="2">Alasan</th>
										<th rowspan="2">Atas Dasar</th>
										<th rowspan="2">Berlaku Tahun</th>
									</tr>
									<tr>
										<th>Silabus</th>
										<th>Buku Dasar</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
										<td>(4)</td>
										<td>(5)</td>
										<td>(6)</td>
										<td>(7)</td>
										<td>(8)</td>
										<td>(9)</td>
									</tr>
										<?php
											for($i=1;$i<=$ValueCounter;$i++){
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$i."' AND sub_no='".$no1."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print1 = $row["isi_char"];
												
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$i."' AND sub_no='".$no1."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print2 = $row["isi_char"];
												
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom4."' AND no='".$i."' AND sub_no='".$no1."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print3 = $row["isi_char"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom5."' AND no='".$i."' AND sub_no='".$no1."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print4 = $row["isi_float"];

												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom6."' AND no='".$i."' AND sub_no='".$no1."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print5 = $row["isi_float"];

												$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom7."' AND no='".$i."' AND sub_no='".$no1."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print6 = $row["isi_text"];
												
												$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom8."' AND no='".$i."' AND sub_no='".$no1."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print7 = $row["isi_text"];

												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom9."' AND no='".$i."' AND sub_no='".$no1."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print8 = $row["isi_float"];
												
												$check1="";
												$check2="";
												$choice="";
												if($print4 == 1){
													$check1 = "checked";
												}
												if($print5 == 1){
													$check2 = "checked";
												}
											
												if($print3 == "Baru"){
													$choice ="<select name='5_2/4/1/".$i."' style='width:80%;'>
														<option value='Baru' selected>Baru</option>
														<option value='Lama'>Lama</option>
														<option value='Hapus'>Hapus</option>
													</select>";
												}else if($print3 == "Lama"){
													$choice ="<select name='5_2/4/1/".$i."' style='width:80%;'>
														<option value='Baru'>Baru</option>
														<option value='Lama'>Lama</option>
														<option value='Hapus' selected>Hapus</option>
													</select>";
												}else {
													$choice ="<select name='5_2/4/1/".$i."' style='width:80%;'>
														<option value='Baru'>Baru</option>
														<option value='Lama'>Lama</option>
														<option value='Hapus' selected>Hapus</option>
													</select>";
												}
												
												echo "<tr>";
												echo "<td>".$i."</td>";
												echo "<td><input type='text' class='form-control' name='5_2/2/1/".$i."' value='".$print1."'></td>";
												echo "<td><input type='text' class='form-control' name='5_2/3/1/".$i."' value='".$print2."'></td>";
												echo "<td>".$choice."</td>";
												echo "<td><input type='checkbox' name='5_2/5/1/".$i."' value='1' ".$check1.">";
												echo "<td><input type='checkbox' name='5_2/6/1/".$i."' value='1' ".$check2.">";
												echo "<td><textarea type='text' rows='3' class='form-control' name='5_2/7/1/".$i."'>".$print6."</textarea></td>";	
												echo "<td><textarea type='text' rows='3' class='form-control' name='5_2/8/1/".$i."'>".$print7."</textarea></td>";
												echo "<td><input type='text' pattern='[0-9]+([\-][0-9]+)?' class='form-control' name='5_2/9/1/".$i."' value='".$print8."'></td>";
												echo "</tr>";
											}
										?>
									<tr>
										<td colspan="9"><button type="button" onclick="addRow02('addTable02','5_2/counter')"><span class="glyphicon glyphicon-plus"></span></button></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "5.2.a";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="7">5.2.a</td>
										<td colspan="3">Pelaksanaan peninjauan kurikulum dalam kurun waktu 5 tahun terakhir.</td>
									</tr>
									<tr>
										<td>0</td>
										<td>Dalam 5 tahun terakhir, tidak pernah melakukan peninjauan ulang (Khusus untuk PS yang berdiri 5 tahun atau lebih).</td>
										<td></td>
									</tr>
									<tr>
										<td>1</td>
										<td>Pengembangan mengikuti perubahan di perguruan tinggi lain tanpa penyesuaian.</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Pengembangan mengikuti perubahan di perguruan tinggi lain yang disesuaikan dengan visi, misi, dan umpan balik.</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Pengembangan dilakukan bekerjasama dengan perguruan tinggi lain tetapi tidak melibatkan pemangku kepentingan eksternal lainnya walaupun menyesuaikan dengan visi, misi, dan umpan balik.</td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Pengembangan dilakukan secara mandiri dengan melibatkan pemangku kepentingan internal dan eksternal dan memperhatikan visi, misi, dan umpan balik program studi.</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control"  placeholder="" value='<?php echo $print;?>' name="nilai/5_2_a"/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "5.2.b";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="7">5.2.b</td>
										<td colspan="3">Penyesuaian kurikulum dengan perkembangan Ipteks dan kebutuhan.</td>
									</tr>
									<tr>
										<td>0</td>
										<td>Tidak ada pembaharuan kurikulum selama 5 tahun terakhir.</td>
										<td></td>
									</tr>
									<tr>
										<td>1</td>
										<td>(Tidak ada skor 1)</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Pembaharuan hanya menata ulang kurikulum yang sudah ada, tanpa disesuaikan dengan perkembangan.</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Pembaharuan kurikulum dilakukan sesuai dengan perkembangan ilmu di bidangnya, tetapi kurang memperhatikan kebutuhan pemangku kepentingan.</td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Pembaharuan kurikulum dilakukan sesuai dengan perkembangan ilmu di bidangnya dan kebutuhan pemangku kepentingan.</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control"  placeholder="" value='<?php echo $print;?>' name="nilai/5_2_b"/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
<!--53-->
					<div class="form-group">
						<label class="col-sm-2 text-right">5.3</label> 
						<label class="col-sm-8">Pelaksanaan Proses Pembelajaran</label>
					</div>
<!--531-->
					<div class="form-group">
						<label class="col-sm-2 text-right">5.3.1</label> 
						<p class="col-sm-8">Mekanisme untuk memonitor pelaksanaan perkuliahan, khususnya mengenai kehadiran dosen, mahasiswa, serta materi perkuliahan</p>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "5.3.1";
							$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
							$data = mysqli_query($db, $query);
							$row = mysqli_fetch_assoc($data);
							$print = $row["isi_text"];
							?>
							<textarea type="text" class="form-control" rows="10" name="5_3_1" placeholder="" maxlength="60000"><?php echo $print; ?></textarea>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "5.3.1.a";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND sub_butir='".$no1."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="4">5.3.1.a</td>
										<td colspan="3">Pelaksanaan pembelajaran memiliki mekanisme untuk memonitor, mengkaji, dan memperbaiki setiap semester tentang:(a) kehadiran mahasiswa, (b) kehadiran dosen, (c) materi kuliah.</td>
									</tr>
									<tr>
										<td colspan='2'>Jumlah skor untuk tiga aspek (Rentang 3 - 12)</td>
										<td><input type="text" pattern='[0-9]+([\-][0-9]+)?' pattern='[0-9]+([\-][0-9]+)?' class="form-control"  id="jumlahskor" value='<?php echo $print;?>' name="nilai/5_3_1_a/1" onchange="borangnilai531a()"></td>
									</tr>
									<tr>
										<td colspan='2'>Skor Akhir</td>
										<td id="skorakhir"></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td id="nilai/5_3_1"></td>
										<input type="hidden" name="nilai/5_3_1_a" id="nilai/531a">
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "5.3.1.b";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="7">5.3.1.b</td>
										<td colspan="3">Mekanisme penyusunan materi perkuliahan.</td>
									</tr>
									<tr>
										<td>0</td>
										<td>Tidak ada mekanisme monitoring.</td>
										<td></td>
									</tr>
									<tr>
										<td>1</td>
										<td>Materi kuliah hanya disusun oleh dosen pengajar tanpa melibatkan dosen lain.</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Materi kuliah disusun oleh kelompok dosen dalam satu bidang ilmu.</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Materi kuliah disusun oleh kelompok dosen dalam satu bidang ilmu, dengan memperhatikan masukan dari dosen lain.</td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Materi kuliah disusun oleh kelompok dosen dalam satu bidang ilmu, dengan memperhatikan masukan dari dosen lain atau dari pengguna lulusan.</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control"  placeholder="" value='<?php echo $print;?>' name="nilai/5_3_1_b"/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
<!--532-->
					<div class="form-group">
						<label class="col-sm-2 text-right">5.3.2</label> 
						<p class="col-sm-8">Contoh soal ujian dalam 1 tahun terakhir untuk 5 mata kuliah keahlian berikut silabusnya ada pada Lampiran. Adapun mata kuliah yang mewakili adalah :</p>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<?php
								$butir = "5.3.2";
								$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$null."' AND no='".$null."' AND sub_no='".$no1."'";
								$data = mysqli_query($db, $query);
								$count = mysqli_num_rows($data);
								if ($count>0) {
									$row = mysqli_fetch_assoc($data);
									$ValueCounter = $row["isi_float"];
								}else{
									$ValueCounter = 0;
								}
								echo "<input type='hidden' id='5_3_2/counter' name='5_3_2/counter' value='".$ValueCounter."'>";
							?>
							<table class="tableBorang" id="addTable32" style="width:100%;">						
								<thead>
									<tr>
										<th>No.</th>
										<th>Mata Kuliah</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
									</tr>
										<?php
											for($i=1;$i<=$ValueCounter;$i++){
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$i."' AND sub_no='".$no1."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print1 = $row["isi_char"];
												
												echo "<tr>";
												echo "<td>".$i."</td>";
												echo "<td><input type='text' class='form-control' name='5_3_2/2/2/".$i."' value='".$print1."'></td>";
												echo "</tr>";
											}
										?>
									<tr>
										<td colspan="2"><button type="button" onclick="addRow32('addTable32','5_3_2/counter')"><span class="glyphicon glyphicon-plus"></span></button></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "5.3.2";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="7">5.3.2</td>
										<td colspan="3">Mutu soal ujian.</td>
									</tr>
									<tr>
										<td>0</td>
										<td>Semua soal ujian tidak bermutu atau tidak sesuai dengan GBPP/SAP.</td>
										<td></td>
									</tr>
									<tr>
										<td>1</td>
										<td>Hanya satu contoh soal ujian yang mutunya baik, dan sesuai dengan GBPP/SAP.</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Dua s.d. tiga contoh soal ujian yang mutunya baik, dan sesuai dengan GBPP/SAP.</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Empat dari lima contoh soal ujian yang mutunya baik, dan sesuai dengan GBPP/SAP.</td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Mutu soal ujian untuk lima mata kuliah yang diberikan semuanya bermutu baik, dan sesuai dengan GBPP/SAP.</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control"  placeholder="" value='<?php echo $print;?>' name="nilai/5_3_2"/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
<!--54-->
					<div class="form-group">
						<label class="col-sm-2 text-right">5.4</label> 
						<label class="col-sm-8">Sistem Pembimbingan Akademik</label>
					</div>
<!--541-->
					<div class="form-group">
						<label class="col-sm-2 text-right">5.4.1</label> 
						<p class="col-sm-8">Pembimbingan Akademik </p>
					</div>
									
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<?php
								$butir = "5.4.1";
								$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$null."' AND no='".$null."'";
								$data = mysqli_query($db, $query);
								$count = mysqli_num_rows($data);
								if ($count>0) {
									$row = mysqli_fetch_assoc($data);
									$ValueCounter = $row["isi_float"];
								}else{
									$ValueCounter = 0;
								}
								echo "<input type='hidden' id='5_4_1/counter' name='5_4_1/counter' value='".$ValueCounter."'>";
							?>
							<table class="tableBorang" id="addTable41" style="width:100%;">						
								<thead>
									<tr>
										<th>No.</th>
										<th>Nama Dosen Pembimbing Akademik</th>
										<th>Jumlah Mahasiswa Bimbingan</th>
										<th>Rata-rata Banyaknya Pertemuan/ mhs/semester</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
										<td>(4)</td>
									</tr>
										<?php
											for($i=1;$i<=$ValueCounter;$i++){
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$i."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print1 = $row["isi_char"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$i."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print2 = $row["isi_float"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom4."' AND no='".$i."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print3 = $row["isi_float"];
												
												echo "<tr>";
												echo "<td>".$i."</td>";
												echo "<td><select class='form-control' name='5_4_1/2/".$i."'>
														<option>-</option>";
														for ($y=1; $y <= $countDosenPS; $y++) {
															if ($print1==$dosenPS[$y]) {
																echo "<option selected>".$dosenPS[$y]."</option>";
															}
															else{
																echo "<option>".$dosenPS[$y]."</option>";	
															}
															
														}
														echo "</select></td>";
												echo "<td><input type='text' pattern='[0-9]+([\-][0-9]+)?' class='form-control' name='5_4_1/3/".$i."' value='".$print2."' onchange='update41a(2)'></td>";
												echo "<td><input type='text' pattern='[0-9]+([\-][0-9]+)?' class='form-control' name='5_4_1/4/".$i."' value='".$print3."'></td>";
												echo "</tr>";
											}
										?>
									<tr>
										<td colspan="4"><button type="button" onclick="addRow41('addTable41','5_4_1/counter')"><span class="glyphicon glyphicon-plus"></span></button></td>
									</tr>
									<tr>
										<td colspan='2'>Total</td>
										<td id="totalMB"></td>
										<td></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="col-sm-offset-2 col-sm-8">
						<?php
							$butir="5.4.1";
							$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$null."' AND no='".$null."'  AND sub_no='".$no1."'";
							$data = mysqli_query($db, $query);
							$row = mysqli_fetch_assoc($data);
							$print4 = $row["isi_float"];
						?>
						<p>Rata-rata banyaknya pertemuan per mahasiswa per semester = <input type="text" name="5_4_1/pertemuan" id="ratapertemuandosen" onchange='nilaiborang541c()' <?php echo "value=".$print4; ?>> kali.</p>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="5">5.4.1.a</td>
										<td colspan="3">Rata-rata banyaknya mahasiswa per dosen Pembimbing Akademik (PA) per semester.</td>
									</tr>
									<tr>
										<td colspan='2'>Banyaknya dosen PA</td>
										<td id="dosenPA"></td>
									</tr>
									<tr>
										<td colspan='2'>Banyaknya mahasiswa bimbingan PA</td>
										<td id="BMB"></td>
									</tr>
									<tr>
										<td colspan='2'>Rasio mahasiswa/dosen</td>
										<td id="RMD"></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td id="nilai/5_4_1_a"></td>
										<input type='hidden' name='nilai/5_4_1_a' id='nilai541a'>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
									$butir = "5.4.1.b";
									$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="7">5.4.1.b</td>
										<td colspan="3">Pelaksanaan kegiatan pembimbingan akademik.</td>
									</tr>
									<tr>
										<td>0</td>
										<td>Tidak ada pembimbingan, hanya ada pengesahan dokumen akademik oleh pegawai administratif</td>
										<td></td>
									</tr>
									<tr>
										<td>1</td>
										<td>Perwalian tidak dilakukan oleh dosen PA tetapi oleh Tenaga Administrasi.</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Perwalian dilakukan oleh sebagian dosen PA dan sebagian oleh Tenaga Administrasi.</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Perwalian dilakukan oleh seluruh dosen PA tetapi tidak seluruhnya menurut panduan tertulis.</td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Dilakukan oleh seluruh dosen PA dengan baik sesuai panduan tertulis.</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control"  placeholder="" value='<?php echo $print;?>' name="nilai/5_4_1_b"/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="2">5.4.1.c</td>
										<td colspan="2">Jumlah rata-rata pertemuan pembimbingan per mahasiswa per semester</td>
										<td id="rataP"></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td id="nilai/5_4_1_c"></td>
										<input type='hidden' name='nilai/5_4_1_c' id='nilai541c'>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
<!--542-->
					<div class="form-group">
						<label class="col-sm-2 text-right">5.4.2</label> 
						<p class="col-sm-8">Proses pembimbingan akademik yang diterapkan pada Program Studi</p>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<?php
								$butir = "5.4.2";
								$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$null."' AND no='".$null."'";
								$data = mysqli_query($db, $query);
								$count = mysqli_num_rows($data);
								if ($count>0) {
									$row = mysqli_fetch_assoc($data);
									$ValueCounter = $row["isi_float"];
								}else{
									$ValueCounter = 0;
								}
								echo "<input type='hidden' id='5_4_2/counter' name='5_4_2/counter' value='".$ValueCounter."'>";
							?>
							<table class="tableBorang" id="addTable42" style="width:100%;">						
								<thead>
									<tr>
										<th>No.</th>
										<th>Hal</th>
										<th>Penjelasan</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
									</tr>
										<?php
											for($i=1;$i<=$ValueCounter;$i++){
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$i."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print1 = $row["isi_char"];
												
												$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$i."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print2 = $row["isi_text"];
												
												echo "<tr>";
												echo "<td>".$i."</td>";
												echo "<td><input type='text' class='form-control' name='5_4_2/2/".$i."' value='".$print1."'></td>";
												echo "<td><textarea rows='3' type='text' class='form-control' name='5_4_2/3/".$i."'>".$print2."</textarea></td>";
												echo "</tr>";
											}
										?>
									<tr>
										<td colspan="3"><button type="button" onclick="addRow42('addTable42','5_4_2/counter')"><span class="glyphicon glyphicon-plus"></span></button></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
									$butir = "5.4.2";
									$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="7">5.4.2</td>
										<td colspan="3">Efektivitas kegiatan perwalian/pembimbingan akademik.</td>
									</tr>
									<tr>
										<td>0</td>
										<td>Sistem bantuan dan bimbingan akademik tidak jalan, atau tidak ada pembimbingan.</td>
										<td></td>
									</tr>
									<tr>
										<td>1</td>
										<td>Sistem bantuan dan bimbingan akademik tidak efektif.</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Sistem bantuan dan bimbingan akademik cukup efektif.</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Sistem bimbingan akademik efektif.</td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Sistem bimbingan akademik sangat efektif.</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control"  placeholder="" value='<?php echo $print;?>' name="nilai/5_4_2"/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
<!--55-->
					<div class="form-group">
						<label class="col-sm-2 text-right">5.5</label> 
						<label class="col-sm-8">Pembimbingan Tugas Akhir/ Skripsi</label>
					</div>
<!--551-->
					<div class="form-group">
						<label class="col-sm-2 text-right">5.5.1</label> 
						<p class="col-sm-8">Pelaksanaan pembimbingan Tugas Akhir (TA) atau skripsi</p>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "5.5.1";
							$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
							$data = mysqli_query($db, $query);
							$row = mysqli_fetch_assoc($data);
							$print = $row["isi_text"];
							?>
							<textarea type="text" class="form-control" rows="10" name="5_5_1" placeholder="" maxlength="60000"><?php echo $print; ?></textarea>
						</div>
					</div>
					
					<div class="form-group">
						<p class="col-sm-offset-2 col-sm-8">Nama-nama dosen yang menjadi pembimbing tugas akhir, dan jumlah mahasiswa yang bimbingan dengan mengikuti format tabel berikut:</p>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<?php
								$butir = "5.5.1";
								$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$null."' AND no='".$null."'AND sub_no='".$no1."'";
								$data = mysqli_query($db, $query);
								$count = mysqli_num_rows($data);
								if ($count>0) {
									$row = mysqli_fetch_assoc($data);
									$ValueCounter = $row["isi_float"];
								}else{
									$ValueCounter = 0;
								}
								echo "<input type='hidden' id='5_5_1/counter' name='5_5_1/counter' value='".$ValueCounter."'>";
							?>
							<table class="tableBorang" id="addTable51" style="width:100%;">						
								<thead>
									<tr>
										<th>No.</th>
										<th>Nama Dosen Pembimbing Akademik</th>
										<th>Jumlah Mahasiswa</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
									</tr>
										<?php
											for($i=1;$i<=$ValueCounter;$i++){
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$i."'AND sub_no='".$no1."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print1 = $row["isi_char"];
												
												$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$i."'AND sub_no='".$no1."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print2 = $row["isi_float"];
												
												echo "<tr>";
												echo "<td>".$i."</td>";
												echo "<td><select class='form-control' name='5_5_1/2/".$i."'>
														<option>-</option>";
														for ($y=1; $y <= $countDosenPS; $y++) {
															if ($print1==$dosenPS[$y]) {
																echo "<option selected>".$dosenPS[$y]."</option>";
															}
															else{
																echo "<option>".$dosenPS[$y]."</option>";	
															}
															
														}
														echo "</select></td>";
												echo "<td><input type='text' pattern='[0-9]+([\-][0-9]+)?' class='form-control' name='5_5_1/3/".$i."' value='".$print2."' onchange='update51(2)'></td>";
												echo "</tr>";
											}
										?>
									<tr>
										<td colspan="4"><button type="button" onclick="addRow51('addTable51','5_5_1/counter')"><span class="glyphicon glyphicon-plus"></span></button></td>
									</tr>
									<tr>
										<td colspan="2">Total</td>
										<td id="TM"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<p class="col-sm-offset-2 col-sm-8">Ketersediaan panduan pembimbingan tugas akhir (Beri tanda √ pada pilihan yang sesuai):</p>
					</div>
					
					<div class="form-group">
						<div class="radio col-sm-offset-2 col-sm-8">
							<?php
								$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$null."' AND no='".$null."' AND sub_no='".$no2."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print1 = $row["isi_char"];
							?>
							<label><input type="radio" name="5_5_1/2" value="Ya" <?php if ($print1 == "Ya") {echo "checked";} ?>>Ya</label>
							<label><input type="radio" name="5_5_1/2" value="Tidak" <?php if ($print1 != "Ya") {echo "checked";} ?>>Tidak</label>
						</div>
					</div>
					
					<div class="form-group">
						<p class="col-sm-offset-2 col-sm-8">Cara sosialisasi dan pelaksanaannya.</p>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "5.5.1";
							$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'  AND sub_no='".$no3."'";
							$data = mysqli_query($db, $query);
							$row = mysqli_fetch_assoc($data);
							$print = $row["isi_text"];
							?>
							<textarea type="text" class="form-control" rows="10" name="5_5_1/3" placeholder="" maxlength="60000"><?php echo $print; ?></textarea>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
									$butir = "5.5.1.a";
									$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
									$data = mysqli_query($db, $query);
									$row = mysqli_fetch_assoc($data);
									$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="7">5.5.1.a</td>
										<td colspan="3">Ketersediaan panduan tugas akhir, sosialisasi,  dan penggunaan.</td>
									</tr>
									<tr>
										<td>0</td>
										<td>Tidak ada panduan tertulis.</td>
										<td></td>
									</tr>
									<tr>
										<td>1</td>
										<td>(Tidak ada nilai 1)</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Ada panduan tertulis tetapi tidak disosialisasikan dengan baik, serta tidak dilaksanakan secara konsisten.</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Ada panduan tertulis dan disosialisasikan dengan baik, tetapi tidak dilaksanakan secara konsisten.</td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Ada panduan tertulis yang disosialisasikan dan dilaksanakan dengan konsisten.</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control"  placeholder="" value='<?php echo $print;?>' name="nilai/5_5_1_a"/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="5">5.5.1.b</td>
										<td colspan="3">Rata-rata mahasiswa per dosen pembimbing tugas akhir</td>
									</tr>
									<tr>
										<td colspan="2">Jumlah dosen pembimbing tugas akhir</td>
										<td id="TDP"></td>
									</tr>
									<tr>
										<td colspan="2">Jumlah mahasiswa tugas akhir</td>
										<td id="JMTA"></td>
									</tr>
									<tr>
										<td colspan="2">Rasio mahasiswa/dosen</td>
										<td id="RasioMD"></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td id="nilai/5_5_1_b"></td>
										<input type="hidden" name="nilai/5_5_1_b" id="nilai551b">
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<?php
										$butir = "5.5.1.c";
										$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND sub_butir='".$no1."'";
										$data = mysqli_query($db, $query);
										$row = mysqli_fetch_assoc($data);
										$print = $row["isi"];
									?>
									<tr>
										<td rowspan="2">5.5.1.c</td>
										<td colspan="2">Rata-rata mahasiswa per dosen pembimbing tugas akhir</td>
										<td><input type="text" pattern='[0-9]+([\-][0-9]+)?' class="form-control" id="rataMDP" onchange='nilaiborang551c()' placeholder="" value='<?php echo $print;?>' name="nilai/5_5_1_c/1"/></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td id="nilai/5_5_1_c"></td>
										<input type="hidden" name="nilai/5_5_1_c" id="nilai551c">
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "5.5.1.d";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="6">5.5.1.d</td>
										<td colspan="3">Kualifikasi akademik dosen pembimbing tugas akhir.</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Sebagian besar dosen pembimbing belum berpendidikan minimal S2 dan  tidak sesuai dengan bidang keahliannya.</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Sebagian besar dosen pembimbing berpendidikan minimal S2, tetapi sebagian kecil tidak sesuai dengan bidang keahliannya.</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Seluruh dosen pembimbing berpendidikan minimal S2, tetapi sebagian kecil tidak sesuai dengan bidang keahliannya.</td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Seluruh dosen pembimbing berpendidikan minimal S2 dan sesuai dengan bidang keahliannya.</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control"  placeholder="" value='<?php echo $print;?>' name="nilai/5_5_1_d"/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
<!--552-->
					<div class="form-group">
						<label class="col-sm-2 text-right">5.5.2</label> 
						<?php
							$butir = "5.5.2";
							$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'  AND kolom ='".$null."'  AND no ='".$null."'";
							$data = mysqli_query($db, $query);
							$row = mysqli_fetch_assoc($data);
							$print1 = $row["isi_float"];
						?>
						<p class="col-sm-8">Rata-rata lama penyelesaian tugas akhir/skripsi pada tiga tahun terakhir :
						<input type="text" name="5_5_2" pattern='[0-9]+([\-][0-9]+)?' id="rataselesaita" <?php echo "value='".$print1."'"; ?> onchange="nilaiborang552()"> 
						bulan. (Menurut kurikulum, Tugas Akhir direncanakan satu semester). Lama penyelesaian dihitung sejak ditetapkannya dosen pembimbing sampai dengan dinyatakan lulus pada ujian komprehensif</p>
					</div>
					
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">Kolom isian yang tidak ada datanya, harus diisi dengan nol.</label>
						<label class="col-sm-offset-2 col-sm-8">Untuk PS dengan waktu penyelesaian TA selama 1 semester</label>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "5.5.2";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								
								$butir = "5.5.2";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND sub_butir='".$no1."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print1 = $row["isi"]; 
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="6">5.5.2</td>
										<td colspan="2">Rata-rata waktu penyelesaian TA</td>
										<td id="RataWTA"></td>
									</tr>
									<tr>
										<td colspan='2'>Nilai TA terjadwal 1 semester</td>
										<td id="NTA1S"></td>
									</tr>
									<tr>
										<td colspan='3'>Untuk PS dengan waktu penyelesaian TA selama 2 semester</td>
									</tr>
									<tr>
										<td colspan='2'>Rata-rata waktu penyelesaian TA</td>
										<td><input type="text" name="nilai/5_5_2/1" pattern='[0-9]+([\-][0-9]+)?' id="RPTA" <?php echo "value='".$print1."'"; ?> onchange="nilaiborang552()"> </td>
									</tr>
									<tr>
										<td colspan='2'>Nilai TA terjadwal 2 semester</td>
										<td id="NTA2S"></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td id="nilai/5_5_2"></td>
										<input type="hidden" name="nilai/5_5_2" id="nilai552">
									</tr>
								</tbody>
							</table>
						</div>
					</div>
<!--56-->
					<div class="form-group">
						<label class="col-sm-2 text-right">5.6</label> 
						<label class="col-sm-8">Upaya perbaikan pembelajaran</label>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<?php
								$butir = "5.6";
								$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$null."' AND no='".$null."'";
								$data = mysqli_query($db, $query);
								$count = mysqli_num_rows($data);
								if ($count>0) {
									$row = mysqli_fetch_assoc($data);
									$ValueCounter = $row["isi_float"];
								}else{
									$ValueCounter = 0;
								}
								echo "<input type='hidden' id='5_6/counter' name='5_6/counter' value='".$ValueCounter."'>";
							?>
							<table class="tableBorang" id="addTable6" style="width:100%;">						
								<thead>
									<tr>
										<th rowspan='2'>Butir</th>
										<th colspan='2'>Upaya Perbaikan</th>
									</tr>
									<tr>
										<th>Tindakan</th>
										<th>Hasil</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
									</tr>
										<?php
											for($i=1;$i<=$ValueCounter;$i++){
												$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom1."' AND no='".$i."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print1 = $row["isi_char"];
												
												$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no='".$i."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print2 = $row["isi_text"];
												
												$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no='".$i."'";
												$data = mysqli_query($db, $query);
												$row = mysqli_fetch_assoc($data);
												$print3 = $row["isi_text"];
												
												echo "<tr>";
												echo "<td><input type='text' class='form-control' name='5_6/1/".$i."' value='".$print1."'></td>";
												echo "<td><textarea rows='3' type='text' class='form-control' name='5_6/2/".$i."'>".$print2."</textarea></td>";
												echo "<td><textarea rows='3' type='text' class='form-control' name='5_6/3/".$i."'>".$print3."</textarea></td>";
												echo "</tr>";
											}
										?>
									<tr>
										<td colspan="3"><button type="button" onclick="addRow6('addTable6','5_6/counter')"><span class="glyphicon glyphicon-plus"></span></button></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "5.6";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="7">5.6</td>
										<td colspan="3">Upaya perbaikan sistem pembelajaran yang telah dilakukan selama tiga tahun terakhir berkaitan dengan:  (a) Materi, (b) Metode pembelajaran, (c) Penggunaan teknologi pembelajaran, dan (d) Cara-cara evaluasi.</td>
									</tr>
									<tr>
										<td>0</td>
										<td>Tidak ada upaya perbaikan.</td>
										<td></td>
									</tr>
									<tr>
										<td>1</td>
										<td>Upaya perbaikan dilakukan untuk 1 dari yang seharusnya diperbaiki/ ditingkatkan.</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Upaya perbaikan dilakukan untuk 2 dari 4 yang seharusnya diperbaiki/ ditingkatkan.</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Upaya perbaikan dilakukan untuk 3 dari 4  yang seharusnya diperbaiki/ ditingkatkan.</td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Upaya perbaikan dilakukan untuk  semua dari yang seharusnya diperbaiki/ ditingkatkan.</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control"  placeholder="" value='<?php echo $print;?>' name="nilai/5_6"/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
<!--57-->
					<div class="form-group">
						<label class="col-sm-2 text-right">5.7</label> 
						<label class="col-sm-8">Upaya Peningkatan Suasana Akademik</label>
					</div>
<!--571-->
					<div class="form-group">
						<label class="col-sm-2 text-right">5.7.1</label> 
						<p class="col-sm-8">Kebijakan tentang suasana akademik (otonomi keilmuan, kebebasan akademik, kebebasan mimbar akademik).</p>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "5.7.1";
							$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
							$data = mysqli_query($db, $query);
							$row = mysqli_fetch_assoc($data);
							$print = $row["isi_text"];
							?>
							<textarea type="text" class="form-control" rows="10" name="5_7_1" placeholder="" maxlength="60000"><?php echo $print; ?></textarea>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "5.7.1";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="6">5.7.1</td>
										<td colspan="3">Kebijakan tertulis tentang suasana akademik (otonomi keilmuan, kebebasan akademik, kebebasan mimbar akademik, kemitraan dosen-mahasiswa).</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Tidak ada kebijakan tertulis tentang otonomi keilmuan, kebebasan akademik, kebebasan mimbar akademik, dan kemitraan dosen-mahasiswa.</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Kebijakan tertulis kurang lengkap.</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Kebijakan lengkap mencakup informasi tentang otonomi keilmuan, kebebasan akademik, kebebasan mimbar akademik, dan kemitraan dosen-mahasiswa, namun tidak dilaksanakan secara konsisten.</td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Kebijakan lengkap mencakup informasi tentang otonomi keilmuan, kebebasan akademik, kebebasan mimbar akademik, dan kemitraan dosen-mahasiswa, serta dilaksanakan secara konsisten.</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control"  placeholder="" value='<?php echo $print;?>' name="nilai/5_7_1"/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

<!--572-->
					<div class="form-group">
						<label class="col-sm-2 text-right">5.7.2</label> 
						<p class="col-sm-8">Ketersediaan dan jenis prasarana, sarana dan dana yang memungkinkan terciptanya interaksi akademik antara sivitas akademika.</p>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "5.7.2";
							$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
							$data = mysqli_query($db, $query);
							$row = mysqli_fetch_assoc($data);
							$print = $row["isi_text"];
							?>
							<textarea type="text" class="form-control" rows="10" name="5_7_2" placeholder="" maxlength="60000"><?php echo $print; ?></textarea>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "5.7.2";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="6">5.7.2</td>
										<td colspan="3">Ketersediaan dan kelengkapan jenis prasarana, sarana serta dana yang memungkinkan terciptanya interaksi akademik antara sivitas akademika.</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Prasarana utama masih kurang, demikian pula dengan dukungan dana.</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Tersedia, cukup lengkap, milik sendiri atau sewa, dan dana yang cukup memadai.</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Tersedia, milik sendiri,   lengkap, dan dana yang memadai.</td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Tersedia, milik sendiri,  sangat lengkap dan dana yang sangat memadai.</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control"  placeholder="" value='<?php echo $print;?>' name="nilai/5_7_2"/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
<!--573-->
					<div class="form-group">
						<label class="col-sm-2 text-right">5.7.3</label> 
						<p class="col-sm-8">Kebijakan tentang suasana akademik (otonomi keilmuan, kebebasan akademik, kebebasan mimbar akademik).</p>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "5.7.3";
							$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
							$data = mysqli_query($db, $query);
							$row = mysqli_fetch_assoc($data);
							$print = $row["isi_text"];
							?>
							<textarea type="text" class="form-control" rows="10" name="5_7_3" placeholder="" maxlength="60000"><?php echo $print; ?></textarea>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "5.7.3";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="6">5.7.3</td>
										<td colspan="3">Interaksi akademik berupa program dan kegiatan akademik, selain perkuliahan dan tugas-tugas khusus, untuk menciptakan suasana akademik (seminar, simposium, lokakarya, bedah buku dll).</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Kegiatan ilmiah yang terjadwal dilaksanakan lebih dari enam bulan sekali.</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Kegiatan ilmiah yang terjadwal dilaksanakan empat s.d. enam bulan sekali.</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Kegiatan ilmiah yang terjadwal dilaksanakan dua s.d tiga bulan sekali.</td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Kegiatan ilmiah yang terjadwal dilaksanakan setiap bulan.</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control"  placeholder="" value='<?php echo $print;?>' name="nilai/5_7_3"/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
<!--574-->
					<div class="form-group">
						<label class="col-sm-2 text-right">5.7.4</label> 
						<p class="col-sm-8">Kebijakan tentang suasana akademik (otonomi keilmuan, kebebasan akademik, kebebasan mimbar akademik).</p>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "5.7.4";
							$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
							$data = mysqli_query($db, $query);
							$row = mysqli_fetch_assoc($data);
							$print = $row["isi_text"];
							?>
							<textarea type="text" class="form-control" rows="10" name="5_7_4" placeholder="" maxlength="60000"><?php echo $print; ?></textarea>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "5.7.4";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="6">5.7.4</td>
										<td colspan="3">Interaksi akademik antara dosen-mahasiswa.</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Upaya dinilai kurang dan hasilnya tidak nampak, atau tidak ada upaya.</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Cukup dalam upaya dan hasilnya.</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Upaya baik, namun hasilnya baru cukup</td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Upaya baik dan hasilnya suasana kondusif untuk meningkatkan suasana akademik yang baik.</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control"  placeholder="" value='<?php echo $print;?>' name="nilai/5_7_4"/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
<!--575-->			
					<div class="form-group">
						<label class="col-sm-2 text-right">5.7.5</label> 
						<p class="col-sm-8">Kebijakan tentang suasana akademik (otonomi keilmuan, kebebasan akademik, kebebasan mimbar akademik).</p>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "5.7.5";
							$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
							$data = mysqli_query($db, $query);
							$row = mysqli_fetch_assoc($data);
							$print = $row["isi_text"];
							?>
							<textarea type="text" class="form-control" rows="10" name="5_7_5" placeholder="" maxlength="60000"><?php echo $print; ?></textarea>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "5.7.5";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="6">5.7.5</td>
										<td colspan="3">Pengembangan perilaku kecendekiawanan.Bentuk kegiatan antara lain dapat berupa: (a) Kegiatan penanggulangan kemiskinan, (b) Pelestarian lingkungan, (c) Peningkatan kesejahteraan masyarakat, (d) Kegiatan penanggulangan masalah  ekonomi, politik, sosial, budaya, dan lingkungan lainnya.</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Kegiatan yang dilakukan tidak menunjang pengembangan perilaku kecendekiawanan.</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Kegiatan yang dilakukan cukup menunjang pengembangan perilaku kecendekiawanan.</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Kegiatan yang dilakukan menunjang pengembangan perilaku kecendekiawanan.</td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Kegiatan yang dilakukan sangat menunjang pengembangan perilaku kecendekiawanan.</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control"  placeholder="" value='<?php echo $print;?>' name="nilai/5_7_5"/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
	</body>
	<script>
		$(document).ready(function() {
			$("form input, form select, form textarea").attr('disabled',true);
			$("button").hide();
			$("#editButton").show();
			$("#hideButton").show();
		});

		$("#editButton").click(function() {
			$("form input, form select, form textarea").attr('disabled',false);
			$("button").not($("#hideButton,#showButton")).show();
			$("#editButton").hide();
		});

		$("#hideButton").click(function() {
			$(".form-group").not($(".tableNilai").parent().parent()).hide();
			$("#showButton").show();
			$("#hideButton").hide();
		});

		$("#showButton").click(function() {
			$(".form-group").show();
			$("#showButton").hide();
			$("#hideButton").show();
		});

		$('form').submit(function() {
			$('body').hide();
			$("header").hide();
			$("html").prepend("<div class='ball'></div><div class='ball1'></div><h1 style='margin-left:43%'>MEMPROSES<h1>");
		});
	</script>
</html>