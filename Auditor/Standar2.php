<!doctype html>
<html>
<?php
require "../CookiesAuditor.php";
require "../Database/DatabaseConnection.php";

$query='select idPeriode,NamaPeriode from periode where aktif = 1';
$data = mysqli_query($db, $query);
$row = mysqli_fetch_assoc($data);

$namaPeriode = $row['NamaPeriode'];
$periode = $row["idPeriode"];
$username= $_COOKIE['LPMAu'];
$prodi = $_GET['prodi'];

$query="select * from prodi where idProdi='$prodi'";
$data = mysqli_query($db, $query);
$row = mysqli_fetch_assoc($data);

$namaProdi = $row['namaProdi'];
$standar= 2;

//cek data auditor
$revisi="Telah direvisi";
$username= $_COOKIE['LPMAu'];
$query="select username from isi_borang where idProdi='$prodi' and idPeriode='$periode' and username='$username' and standar='$standar' limit 1";
$data = mysqli_query($db, $query);
$count = mysqli_num_rows($data);
if ($count<1){
	//ambil username kaprodi
	$query="select username from isi_borang where idProdi='$prodi' and idPeriode='$periode' and username!='$username' and standar='$standar' limit 1";
	$data = mysqli_query($db, $query);
	$row = mysqli_fetch_assoc($data);
	$username = $row['username'];
	$revisi="Tanpa Revisi";
}
?>
<head>
	<title>Kaprodi</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/style.css" rel="stylesheet">
	<link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="../css/loading.css">
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script src="../js/jquery.min.js"></script>
</head>

<body>
	<div class='wrapper'>
		<header>
			<nav class="navbar navbar-default navbar-fixed-top">
				<div id="navbar" class="navbar-collapse collapse col-sm-offset-1">
					<ul class="nav navbar-nav">
						<?php echo "
						<li><a href='HomeKaprodi.php?prodi=$prodi'>Profil</a></li>
						<li><a href='Standar1.php?prodi=$prodi'>Standar 1</a></li>
						<li class='active'><a href='Standar2.php?prodi=$prodi'>Standar 2</a></li>
						<li><a href='Standar3.php?prodi=$prodi'>Standar 3</a></li>
						<li><a href='Standar4.php?prodi=$prodi'>Standar 4</a></li>
						<li><a href='Standar5.php?prodi=$prodi'>Standar 5</a></li>
						<li><a href='Standar6.php?prodi=$prodi'>Standar 6</a></li>
						<li><a href='Standar7.php?prodi=$prodi'>Standar 7</a></li>
						<li><a href='Nilai.php?prodi=$prodi'>Nilai</a></li>
						"; ?>
						<li><a href="HomeAuditor.php" class="col-md-offset-8">Kembali</a></li>	
						<li><a href="Logout.php">Log Out</a></li>
					</ul>
				</div>
			</nav>
			<div class='text-center'>
				<br>
				<br>
				<h1>STANDAR 2</h1>
				<h4>TATA  PAMONG, KEPEMIMPINAN, SISTEM  PENGELOLAAN, DAN PENJAMINAN MUTU</h4>
				<h5><?php echo $namaProdi ?></h5>
				<h5>Periode: <?php echo $namaPeriode ?></h5>
				<h5><b><?php echo $revisi?></b></h5>
			</div>
		</header>
	</div>
	
	<div class = "container">
		<hr class="colorgraph"><br>
		<div class="row">
			<div class="col-md-12">
				<form action="ProsesStandar2.php" method="post" class="form-horizontal">
					<?php 
					// default id prodi yang disubmit khusus auditor
					echo "
					<input type='hidden' name='prodi' value='".$prodi."'>";
					?>
					<div class="fixed-button" style="left:89%">
						<button type="submit" class="btn btn-primary">Simpan</button>
						<button type="button" class="btn btn-success" id="editButton">Edit</button>
						<button type="button" class="btn btn-default" id="hideButton">Hide</button>
						<button type="button" class="btn btn-default" id="showButton">Show</button>
					</div>
					<div class="form-group">
						<label class="col-sm-2 text-right">2.1</label> 
						<label class="col-sm-8">Sistem Tata Pamong</label>
					</div>
					<div class="form-group"> 
						<div class="col-sm-offset-2 col-sm-8">
							<p style="text-indent:5em;">Sistem tata pamong berjalan secara efektif melalui mekanisme yang disepakati bersama, serta dapat memelihara dan mengakomodasi semua unsur, fungsi, dan peran dalam Program Studi. Tata pamong didukung dengan budaya organisasi yang dicerminkan dengan ada dan tegaknya aturan, tatacara pemilihan pimpinan, etika dosen, etika mahasiswa, etika tenaga kependidikan, sistem penghargaan dan sanksi serta pedoman dan prosedur pelayanan (administrasi, perpustakaan, laboratorium, dan studio). Sistem tata pamong (input, proses, output dan outcome serta lingkungan eksternal yang menjamin terlaksananya tata pamong yang baik) harus diformulasikan, disosialisasikan, dilaksanakan, dipantau dan dievaluasi dengan peraturan dan prosedur yang jelas.
							</p>
							<p style="text-indent:5em;">Uraian ringkas sistem dan pelaksanaan tata pamong di Program Studi untuk  membangun sistem tata pamong yang kredibel, transparan, akuntabel, bertanggung jawab dan adil.
							</p>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "2.1";
							$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
							$data = mysqli_query($db, $query);
							$row = mysqli_fetch_assoc($data);
							$print = $row["isi_text"];
							?>
							<textarea type="text" class="form-control" rows="35" name="2_1" placeholder="" maxlength="60000"><?php echo $print; ?></textarea>
						</label>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "2.1";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="6">2.1</td>
										<td colspan="3">Tata pamong menjamin terwujudnya visi, terlaksananya misi, tercapainya tujuan, berhasilnya strategi yang digunakan secara kredibel, transparan, akuntabel, bertanggung jawab, dan adil</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Program studi memiliki  tatapamong, namun hanya memenuhi 1 s.d. 2 dari 5 aspek berikut: kredibel, transparan,akuntabel,bertanggung jawab,adil</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Program studi memiliki  tatapamong yang memungkinkan terlaksananya secara cukup konsisten prinsip tatapamong, dan menjamin penyelenggaraan program studi yang memenuhi  3 dari 5 aspek berikut: kredibel, transparan, akuntabel, bertanggung jawab, dan adil</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Program studi memiliki  tatapamong yang memungkinkan terlaksananya secara konsisten prinsip tatapamong, dan menjamin penyelenggaraan program studi yang memenuhi 4 dari 5 aspek berikut: kredibel, transparan, akuntabel, bertanggung jawab, dan adil</td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Program studi memiliki  tatapamong yang memungkinkan terlaksananya secara konsisten prinsip tatapamong, dan menjamin penyelenggaraan program studi yang memenuhi 5 aspek berikut:kredibel, transparan, akuntabel, bertanggung jawab, dan adil</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control" placeholder="" value='<?php echo $print; ?>' name="nilai/2.1"/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 text-right">2.2</label> 
						<label class="col-sm-8">Kepemimpinan</label>
					</div>
					
					<div class="form-group"> 
						<div class="col-sm-offset-2 col-sm-8">
							<p style="text-indent:5em;">Kepemimpinan efektif mengarahkan dan mempengaruhi perilaku semua unsur dalam Program Studi, mengikuti nilai, norma, etika, dan budaya organisasi yang disepakati bersama, serta mampu membuat keputusan yang tepat dan cepat.
							</p>
							<p style="text-indent:5em;">Kepemimpinan mampu memprediksi masa depan, merumuskan dan mengartikulasi visi yang realistik, kredibel, serta mengkomunikasikan visi kedepan, yang menekankan pada keharmonisan hubungan manusia dan mampu menstimulasi secara intelektual dan arif bagi anggota untuk mewujudkan visi organisasi, serta mampu memberikan arahan, tujuan, peran, dan tugas kepada seluruh unsur dalam perguruan tinggi.
							</p>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "2.2";
							$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
							$data = mysqli_query($db, $query);
							$row = mysqli_fetch_assoc($data);
							$print = $row["isi_text"];
							?>
							<textarea type="text" class="form-control" rows="15" name="2_2" placeholder="" maxlength="60000"><?php echo $print; ?></textarea>
						</label>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "2.2";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="6">2.2</td>
										<td colspan="3">Karakteristik kepemimpinan: operasional, organisasi, dan publik</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Kepemimpinan program studi lemah dalam karakteristik berikut: (1) kepemim-pinan operasional, (2) kepemim-pinan organisasi, (3) kepemim-pinan publik</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Kepemimpinan program studi memiliki karakter kepemimpinan yang kuat dalam salah satu dari karakteristik berikut: (1) kepemimpinan operasional, (2) kepemimpinan organisasi, (3) kepemimpinan publik</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Kepemimpinan program studi memiliki karakter kepemimpinan yang kuat dalam dua dari karakteristik berikut: (1) kepemimpinan operasional, (2) kepemimpinan organisasi, (3) kepemimpinan publik</td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Kepemimpinan program studi memiliki karakteristik yang kuat dalam: (1) kepemimpinan operasional, (2) kepemimpinan organisasi, (3) kepemimpinan publik</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control" placeholder="" value="<?php echo $print; ?>" name="nilai/2_2"/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 text-right">2.3</label> 
						<label class="col-sm-8">Sistem Pengelolaan</label>
					</div>
					
					<div class="form-group"> 
						<div class="col-sm-offset-2 col-sm-8">
							<p style="text-indent:5em;">Sistem pengelolaan fungsional dan operasional Program Studi mencakup planning, organizing, staffing, leading, controlling dalam kegiatan  internal maupun eksternal.
							</p>
							<p style="text-indent:5em;">Penjelasana sistem pengelolaan Program Studi serta dokumen pendukungnya.
							</p>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "2.3";
							$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
							$data = mysqli_query($db, $query);
							$row = mysqli_fetch_assoc($data);
							$print = $row["isi_text"];
							?>
							<textarea type="text" class="form-control" rows="15" name="2_3" placeholder="<?php echo $print; ?>" maxlength="60000"><?php echo $print; ?></textarea>
						</label>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "2.3";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="7">2.3</td>
										<td colspan="3">Sistem pengelolaan fungsional dan operasional program studi mencakup: planning, organizing, staffing, leading, controlling yang efektif dilaksanakan</td>
									</tr>
									<tr>
										<td>0</td>
										<td>Tidak ada sistem pengelolaan</td>
										<td></td>
									</tr>
									<tr>
										<td>1</td>
										<td>Sistem pengelolaan fungsional dan operasional program studi dilakukan tidak sesuai dengan SOP</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Sistem pengelolaan fungsional dan operasional program studi dilakukan hanya sebagian sesuai dengan SOP dan dokumen kurang lengkap</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Sistem pengelolaan fungsional dan operasional program studi dilakukan dengan cukup baik, sesuai dengan SOP, namun dokumen kurang lengkap</td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Sistem pengelolaan fungsional dan operasional program studi berjalan sesuai dengan SOP, yang didukung dokumen yang lengkap</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control" placeholder="" value='<?php echo $print; ?>' name="nilai/2_3"/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 text-right">2.4</label> 
						<label class="col-sm-8">Penjaminan Mutu</label>
					</div>
					
					<div class="form-group"> 
						<div class="col-sm-offset-2 col-sm-8">
							<p style="text-indent:5em;">Penjelasan pelaksanaan penjaminan mutu pada Program Studi.
							</p>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "2.4";
							$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
							$data = mysqli_query($db, $query);
							$row = mysqli_fetch_assoc($data);
							$print = $row["isi_text"];
							?>
							<textarea type="text" class="form-control" rows="15" name="2_4" placeholder="" maxlength="60000"><?php echo $print; ?></textarea>
						</label>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "2.4";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="7">2.4</td>
										<td colspan="3">Pelaksanaan penjaminan mutu di PS</td>
									</tr>
									<tr>
										<td>0</td>
										<td>Tidak ada sistem penjaminan mutu</td>
										<td></td>
									</tr>
									<tr>
										<td>1</td>
										<td>Ada sistem penjaminan mutu, tetapi tidak berfungsi</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Sistem penjaminan mutu berfungsi sebagian namun  tidak ada umpan balik dan dokumen kurang lengkap</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Sistem penjaminan mutu berjalan sesuai dengan standar penjaminan mutu, umpan balik tersedia tetapi tidak ada tindak lanjut</td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Sistem penjaminan mutu berjalan sesuai dengan standar penjaminan mutu, ada  umpan balik dan tindak lanjutnya, yang didukung dokumen yang lengkap</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control" placeholder="" value='<?php echo $print; ?>' name="nilai/2_4"/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					
					<div class="form-group">
						<label class="col-sm-2 text-right">2.5</label> 
						<label class="col-sm-8">Umpan Balik</label>
					</div>
					
					<div class="form-group"> 
						<div class="col-sm-offset-2 col-sm-8">
							<p style="text-indent:5em;">Kajian Program Studi tentang proses pembelajaran melalui umpan balik dari dosen, mahasiswa, alumni, dan pengguna lulusan mengenai harapan dan persepsi.
							</p>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered">
								<?php
								$kolom2=2;
								$kolom3=3;
								$no1=1;
								$no2=2;
								$no3=3;
								$no4=4;
								$butir = "2.5";
								$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no ='".$no1."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print11 = $row["isi_text"];

								$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no ='".$no1."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print12 = $row["isi_text"];

								$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no ='".$no2."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print21 = $row["isi_text"];

								$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no ='".$no2."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print22 = $row["isi_text"];

								$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no ='".$no3."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print31 = $row["isi_text"];

								$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no ='".$no3."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print32 = $row["isi_text"];

								$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom2."' AND no ='".$no4."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print41 = $row["isi_text"];

								$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."' AND kolom='".$kolom3."' AND no ='".$no4."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print42 = $row["isi_text"];
								?>
								<tr>
									<th class="text-center">Umpan Balik Dari</th>
									<th class="text-center">Isi Umpan Balik</th>
									<th class="text-center">Tindak Lanjut</th>
								</tr>
								<tr>
									<td class="text-center">(1)</td>
									<td class="text-center">(2)</td>
									<td class="text-center">(3)</td>
								</tr>
								<tr>
									<td>Dosen</td>
									<td><textarea type="text" class="form-control" rows="10" name="2_5/2/Dosen" placeholder="" maxlength="60000"><?php echo $print11; ?></textarea></td>
									<td><textarea type="text" class="form-control" rows="10" name="2_5/3/Dosen" placeholder="" maxlength="60000"><?php echo $print12; ?></textarea></td>
								</tr>
								<tr>
									<td>Mahasiswa</td>
									<td><textarea type="text" class="form-control" rows="10" name="2_5/2/Mahasiswa" placeholder="" maxlength="60000"><?php echo $print21; ?></textarea></td>
									<td><textarea type="text" class="form-control" rows="10" name="2_5/3/Mahasiswa" placeholder="" maxlength="60000"><?php echo $print22; ?></textarea></td>
								</tr>
								<tr>
									<td>Alumni</td>
									<td><textarea type="text" class="form-control" rows="10" name="2_5/2/Alumni" placeholder="" maxlength="60000"><?php echo $print31; ?></textarea></td>
									<td><textarea type="text" class="form-control" rows="10" name="2_5/3/Alumni" placeholder="" maxlength="60000"><?php echo $print32; ?></textarea></td>
								</tr>
								<tr>
									<td>Pengguna Lulusan</td>
									<td><textarea type="text" class="form-control" rows="10" name="2_5/2/PenggunaLulusan" placeholder="" maxlength="60000"><?php echo $print41; ?></textarea></td>
									<td><textarea type="text" class="form-control" rows="10" name="2_5/3/PenggunaLulusan" placeholder="" maxlength="60000"><?php echo $print42; ?></textarea></td>
								</tr>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "2.5";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="7">2.5</td>
										<td colspan="3">Penjaringan umpan balik dan tindak lanjutnya. Sumber umpan balik antara lain dari: (1) dosen, (2) mahasiswa, (3) alumni, (4) pengguna lulusan</td>
									</tr>
									<tr>
										<td>0</td>
										<td>Tidak ada umpan balik</td>
										<td></td>
									</tr>
									<tr>
										<td>1</td>
										<td>Umpan balik hanya diperoleh dari sebagian dan tidak ada tindak lanjut</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Umpan balik hanya diperoleh dari sebagian dan ada tindak lanjut secara insidental</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Umpan balik diperoleh dari dosen, mahasiswa, alumni dan pengguna serta ditindaklanjuti secara insidental</td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Umpan balik diperoleh dari dosen, mahasiswa, alumni dan pengguna serta ditindaklanjuti secara berkelanjutan</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control" placeholder="" value='<?php echo $print; ?>' name="nilai/2_5"/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					
					<div class="form-group">
						<label class="col-sm-2 text-right">2.6</label> 
						<label class="col-sm-8">Keberlanjutan</label>
					</div>
					
					<div class="form-group"> 
						<div class="col-sm-offset-2 col-sm-8">
							<p style="text-indent:5em;">Upaya untuk menjamin keberlanjutan (sustainability) Program Studi, khususnya dalam hal:
							</p>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-offset-2 col-sm-8">
							<?php
							$butir = "2.6";
							$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
							$data = mysqli_query($db, $query);
							$row = mysqli_fetch_assoc($data);
							$print = $row["isi_text"];
							?>
							<textarea type="text" class="form-control" rows="15" name="2.6" placeholder="" maxlength="60000"><?php echo $print; ?></textarea>
						</label>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<?php
								$butir = "2.6";
								$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND standar='".$standar."' AND butir ='".$butir."'";
								$data = mysqli_query($db, $query);
								$row = mysqli_fetch_assoc($data);
								$print = $row["isi"];
								?>
								<thead>
									<tr>
										<th>NO. Item</th>
										<th class="text-center" colspan="2">Keterangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td rowspan="7">2.6</td>
										<td colspan="3">Upaya untuk menjamin keberlanjutan PS, mencakup: (a) Upaya untuk peningkatan animo calon mahasiswa, (b) Upaya peningkatan mutu manajemen, (c) Upaya untuk peningkatan mutu lulusan, (d) Upaya untuk pelaksanaan dan hasil kerjasama kemitraan, (e) Upaya dan prestasi dalam memperoleh dana hibah kompetitif</td>
									</tr>
									<tr>
										<td>0</td>
										<td>Tidak ada usaha</td>
										<td></td>
									</tr>
									<tr>
										<td>1</td>
										<td>Ada bukti hanya 1 usaha yang dilakukan</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>Ada bukti hanya sebagian kecil usaha (2-3) yang dilakukan</td>
										<td></td>
									</tr>
									<tr>
										<td>3</td>
										<td>Ada bukti sebagian usaha ( > 3) dilakukan</td>
										<td></td>
									</tr>
									<tr>
										<td>4</td>
										<td>Ada bukti semua usaha dilakukan berikut hasilnya</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">Nilai</td>
										<td><input type="text" pattern="[0-4]([\.][0-9]{1,2})?" class="form-control" placeholder="" value='<?php echo $print; ?>' name="nilai/2_6"/></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
				</form>
			</div>
		</div>
	</div>
</body>

<script>
	$(document).ready(function() {
		$("form input, form select, form textarea").attr('disabled',true);
		$("button").hide();
		$("#editButton").show();
		$("#hideButton").show();
	});

	$("#editButton").click(function() {
		$("form input, form select, form textarea").attr('disabled',false);
		$("button").not($("#hideButton,#showButton")).show();
		$("#editButton").hide();
	});

	$("#hideButton").click(function() {
		$(".form-group").not($(".tableNilai").parent().parent()).hide();
		$("#showButton").show();
		$("#hideButton").hide();
	});

	$("#showButton").click(function() {
		$(".form-group").show();
		$("#showButton").hide();
		$("#hideButton").show();
	});

	$('form').submit(function() {
		$('body').hide();
		$("header").hide();
		$("html").prepend("<div class='ball'></div><div class='ball1'></div><h1 style='margin-left:43%'>MEMPROSES<h1>");
	});
</script>

</html>