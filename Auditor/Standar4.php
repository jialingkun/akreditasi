<!doctype html>
<html>
<?php
require "../CookiesAuditor.php";
require "../Database/DatabaseConnection.php";

$query='select idPeriode,NamaPeriode from periode where aktif = 1';
$data = mysqli_query($db, $query);
$row = mysqli_fetch_assoc($data);

$namaPeriode = $row['NamaPeriode'];
$periode = $row["idPeriode"];

$prodi = $_GET['prodi'];

$query="select * from prodi where idProdi='$prodi'";
$data = mysqli_query($db, $query);
$row = mysqli_fetch_assoc($data);

$namaProdi = $row['namaProdi'];

$standar= 4;

//cek data auditor
$revisi="Telah direvisi";
$username= $_COOKIE['LPMAu'];
$query="select username from isi_borang where idProdi='$prodi' and idPeriode='$periode' and username='$username' and standar='$standar' limit 1";
$data = mysqli_query($db, $query);
$count = mysqli_num_rows($data);
if ($count<1){
	//ambil username kaprodi
	$query="select username from isi_borang where idProdi='$prodi' and idPeriode='$periode' and username!='$username' and standar='$standar' limit 1";
	$data = mysqli_query($db, $query);
	$row = mysqli_fetch_assoc($data);
	$username = $row['username'];
	$revisi="Tanpa Revisi";
}

$standarProfil= 0;

//cek data dosen di halaman profil
$usernameProfil= $_COOKIE['LPMAu'];
$query="select username from isi_borang where idProdi='$prodi' and idPeriode='$periode' and username='$usernameProfil' and standar='$standarProfil' limit 1";
$data = mysqli_query($db, $query);
$count = mysqli_num_rows($data);
if ($count<1){
	//ambil username kaprodi
	$query="select username from isi_borang where idProdi='$prodi' and idPeriode='$periode' and username!='$usernameProfil' and standar='$standarProfil' limit 1";
	$data = mysqli_query($db, $query);
	$row = mysqli_fetch_assoc($data);
	$usernameProfil = $row['username'];
}

//retrieve data dosen PS
$butir = "dosen_ps";
$query="SELECT isi_char FROM isi_borang WHERE username='".$usernameProfil."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom='2' ORDER BY no,sub_no,kolom ASC;";
$data = mysqli_query($db, $query);
$countDosenPS = mysqli_num_rows($data);
$dosenPS[0] = "-";
if ($countDosenPS>0) {
	$index = 1;
	while ($row = mysqli_fetch_assoc($data)) {
		$dosenPS[$index] = $row['isi_char'];
		$index++;
	}
}

//retrieve data dosen bukan PS
$butir = "dosen_xps";
$query="SELECT isi_char FROM isi_borang WHERE username='".$usernameProfil."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom='2' ORDER BY no,sub_no,kolom ASC;";
$data = mysqli_query($db, $query);
$countDosenXPS = mysqli_num_rows($data);
$dosenXPS[0] = "-";
if ($countDosenXPS>0) {
	$index = 1;
	while ($row = mysqli_fetch_assoc($data)) {
		$dosenXPS[$index] = $row['isi_char'];
		$index++;
	}
}

//retrieve data dosen tidak tetap
$butir = "dosen_xtp";
$query="SELECT isi_char FROM isi_borang WHERE username='".$usernameProfil."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom='2' ORDER BY no,sub_no,kolom ASC;";
$data = mysqli_query($db, $query);
$countDosenXTP = mysqli_num_rows($data);
$dosenXTP[0] = "-";
if ($countDosenXTP>0) {
	$index = 1;
	while ($row = mysqli_fetch_assoc($data)) {
		$dosenXTP[$index] = $row['isi_char'];
		$index++;
	}
}

?>
<head>
	<title>Kaprodi</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/style.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../css/loading.css">
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script src="../js/jquery.min.js"></script>
	<script src="../js/form.js"></script>
	<script src="../js/standar4.js"></script>
</head>

<body onload="loadAllJs()">
	<div class='wrapper'>
		<header>
			<nav class="navbar navbar-default navbar-fixed-top">
				<div id="navbar" class="navbar-collapse collapse col-sm-offset-1">
					<ul class="nav navbar-nav">
						<?php echo "
						<li><a href='HomeKaprodi.php?prodi=$prodi'>Profil</a></li>
						<li><a href='Standar1.php?prodi=$prodi'>Standar 1</a></li>
						<li><a href='Standar2.php?prodi=$prodi'>Standar 2</a></li>
						<li><a href='Standar3.php?prodi=$prodi'>Standar 3</a></li>
						<li class='active'><a href='Standar4.php?prodi=$prodi'>Standar 4</a></li>
						<li><a href='Standar5.php?prodi=$prodi'>Standar 5</a></li>
						<li><a href='Standar6.php?prodi=$prodi'>Standar 6</a></li>
						<li><a href='Standar7.php?prodi=$prodi'>Standar 7</a></li>
						<li><a href='Nilai.php?prodi=$prodi'>Nilai</a></li>
						"; ?>
						<li><a href="HomeAuditor.php" class="col-md-offset-8">Kembali</a></li>	
						<li><a href="Logout.php">Log Out</a></li>
					</ul>
				</div>
			</nav>
			<div class='text-center'>
				<br>
				<br>
				<h1>STANDAR 4</h1>
				<h4>SUMBER DAYA MANUSIA</h4>
				<h5><?php echo $namaProdi ?></h5>
				<h5>Periode: <?php echo $namaPeriode ?></h5>
				<h5><b><?php echo $revisi?></b></h5>
			</div>
		</header>
	</div>
	
	<div class = "container">
		<hr class="colorgraph"><br>
		<div class="row">
			<div class="col-md-12">
				<form action="ProsesStandar4.php" method="post" class="form-horizontal">

					<?php 
					// default id prodi yang disubmit khusus auditor
					echo "
					<input type='hidden' name='prodi' value='".$prodi."'>";
					?>

					<div class="fixed-button" style="left:89%">
						<button type="submit" class="btn btn-primary">Simpan</button>
						<button type="button" class="btn btn-success" id="editButton">Edit</button>
						<button type="button" class="btn btn-default" id="hideButton">Hide</button>
						<button type="button" class="btn btn-default" id="showButton">Show</button>
					</div>

					<div class="form-group">
						<label class="col-sm-2 text-right">4.1</label> 
						<label class="col-sm-9">Sistem Seleksi dan Pengembangan</label>
					</div>
					<div class="form-group"> 
						<div class="col-sm-offset-2 col-sm-8">
							<p style="text-indent:5em;">Sistem seleksi / perekrutan, penempatan, pengembangan, retensi, dan pemberhentian dosen dan tenaga kependidikan untuk menjamin mutu penyelenggaraan program akademik
							</p>
						</div>
					</div>
					
					<div class="form-group">
						<?php
						//data retrieve
						$butir = "4.1";
						$tabledata = array_fill(0, 1, '');
						$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							while ($row = mysqli_fetch_assoc($data)) {
								$tabledata[0] = $row['isi_text'];
							}
						}
						?>
						<div class="col-sm-offset-2 col-sm-8">
							<textarea type="text" class="form-control" rows="15" name="4_1" placeholder=""><?php echo $tabledata[0]; ?></textarea>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center" colspan="2">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="7">4.1</td>
									<td colspan="3">Pedoman tertulis tentang sistem seleksi, perekrutan, penempatan, pengembangan, retensi, dan pemberhentian dosen dan tenaga kependidikan</td>
								</tr>
								<tr>
									<td>0</td>
									<td>Tidak ada pedoman tertulis.</td>
									<td></td>
								</tr>
								<tr>
									<td>1</td>
									<td>Ada pedoman tertulis, tidak lengkap dan tidak dilaksanakan.</td>
									<td></td>
								</tr>
								<tr>
									<td>2</td>
									<td>Ada pedoman tertulis yang lengkap; tetapi tidak dilaksanakan.</td>
									<td></td>
								</tr>
								<tr>
									<td>3</td>
									<td>Ada pedoman tertulis yang lengkap; dan tidak ada bukti dilaksanakan secara konsisten.</td>
									<td></td>
								</tr>
								<tr>
									<td>4</td>
									<td>Ada pedoman tertulis yang lengkap; dan ada bukti dilaksanakan secara konsisten.</td>
									<td></td>
								</tr>
								<tr>
									<td colspan="2">Nilai</td>
									<?php 
									//retrieve Nilai
									$butir = "4.1";
									$tabledata = array_fill(0, 1, '');
									$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY sub_butir";
									$data = mysqli_query($db, $query);
									$count = mysqli_num_rows($data);
									if ($count>0) {
										while ($row = mysqli_fetch_assoc($data)) {
											$tabledata[0] = $row['isi'];
										}
									}
									?>
									<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="nilai/4_1" value=<?php echo "'".$tabledata[0]."'";?>></td>
								</tr>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<?php
						//data retrieve
						$butir = "4.2";
						$tabledata = array_fill(0, 1, '');
						$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							while ($row = mysqli_fetch_assoc($data)) {
								$tabledata[0] = $row['isi_text'];
							}
						}
						?>
						<label class="col-sm-2 text-right">4.2</label> 
						<label class="col-sm-9">Monitoring dan Evaluasi</label>
					</div>
					<div class="form-group"> 
						<div class="col-sm-offset-2 col-sm-8">
							<p style="text-indent:5em;">Sistem monitoring dan evaluasi, serta rekam jejak kinerja akademik dosen dan kinerja tenaga kependidikan
							</p>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<textarea type="text" class="form-control" rows="15" name="4_2" placeholder=""><?php echo $tabledata[0]; ?></textarea>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center" colspan="2">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="7">4.2.1</td>
									<td colspan="3">Pedoman tertulis tentang sistem monitoring dan evaluasi, serta rekam jejak kinerja dosen dan tenaga kependidikan.</td>
								</tr>
								<tr>
									<td>0</td>
									<td>Tidak ada pedoman tertulis.</td>
									<td></td>
								</tr>
								<tr>
									<td>1</td>
									<td>Ada pedoman tertulis, tidak lengkap dan tidak dilaksanakan.</td>
									<td></td>
								</tr>
								<tr>
									<td>2</td>
									<td>Ada pedoman tertulis yang lengkap; tetapi tidak dilaksanakan.</td>
									<td></td>
								</tr>
								<tr>
									<td>3</td>
									<td>Ada pedoman tertulis yang lengkap; dan tidak ada bukti dilaksanakan secara konsisten.</td>
									<td></td>
								</tr>
								<tr>
									<td>4</td>
									<td>Ada pedoman tertulis yang lengkap; dan ada bukti dilaksanakan secara konsisten.</td>
									<td></td>
								</tr>
								<tr>
									<td colspan="2">Nilai</td>
									<?php 
									//retrieve Nilai
									$butir = "4.2.1";
									$tabledata = array_fill(0, 1, '');
									$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY sub_butir";
									$data = mysqli_query($db, $query);
									$count = mysqli_num_rows($data);
									if ($count>0) {
										while ($row = mysqli_fetch_assoc($data)) {
											$tabledata[0] = $row['isi'];
										}
									}
									?>
									<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="nilai/4_2_1" value=<?php echo "'".$tabledata[0]."'";?>></td>
								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center" colspan="2">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="7">4.2.2</td>
									<td colspan="3">Pelaksanaan monitoring dan evaluasi kinerja dosen di bidang tridarma.</td>
								</tr>
								<tr>
									<td>0</td>
									<td>Tidak ada bukti tentang kinerja dosen yang terdokumentasikan.</td>
									<td></td>
								</tr>
								<tr>
									<td>1</td>
									<td>Ada bukti tentang kinerja dosen di bidang  pendidikan tetapi tidak terdokumentasikan dengan baik serta tidak ada di bidang penelitian  atau pelayanan/ pengabdian kepada masyarakat.</td>
									<td></td>
								</tr>
								<tr>
									<td>2</td>
									<td>Ada bukti tentang kinerja dosen di bidang pendidikan yang terdokumentasikan dengan baik tetapi tidak ada di bidang penelitian  atau pelayanan/ pengabdian kepada masyarakat.</td>
									<td></td>
								</tr>
								<tr>
									<td>3</td>
									<td>Ada bukti tentang kinerja dosen di bidang: (1) pendidikan, (2) penelitian, (3) pelayanan/ pengabdian kepada masyarakat tetapi tidak terdokumentasi dengan baik.</td>
									<td></td>
								</tr>
								<tr>
									<td>4</td>
									<td>Ada bukti tentang kinerja dosen di bidang: (1) pendidikan, (2) penelitian, (3) pelayanan/ pengabdian kepada masyarakat yang terdokumentasi dengan baik.</td>
									<td></td>
								</tr>
								<tr>
									<td colspan="2">Nilai</td>
									<?php 
									//retrieve Nilai
									$butir = "4.2.2";
									$tabledata = array_fill(0, 1, '');
									$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY sub_butir";
									$data = mysqli_query($db, $query);
									$count = mysqli_num_rows($data);
									if ($count>0) {
										while ($row = mysqli_fetch_assoc($data)) {
											$tabledata[0] = $row['isi'];
										}
									}
									?>
									<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="nilai/4_2_2" value=<?php echo "'".$tabledata[0]."'";?>></td>
								</tr>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 text-right">4.3</label> 
						<label class="col-sm-9">Dosen Tetap</label>
					</div>
					<div class="form-group"> 
						<div class="col-sm-offset-2 col-sm-8">
							<p style="text-indent:5em;">Dosen tetap dalam borang akreditasi BAN-PT adalah dosen yang diangkat dan ditempatkan sebagai tenaga tetap pada PT yang bersangkutan; termasuk dosen penugasan Kopertis, dan dosen yayasan pada PTS dalam bidang yang relevan dengan keahlian bidang studinya. Seorang dosen hanya dapat menjadi dosen tetap pada satu perguruan tinggi, dan mempunyai penugasan kerja minimum 20 jam/minggu
							</p>
							<p>Dosen tetap dipilah dalam 2 kelompok, yaitu:
								<ol>
									<li>Dosen tetap yang bidang keahliannya sesuai dengan PS</li>
									<li>Dosen tetap yang bidang keahliannya di luar PS</li>
								</ol>
							</p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 text-right">4.3.1</label> 
						<div class="col-sm-8"><p>Data dosen tetap yang bidang keahliannya sesuai dengan bidang PS:</p></div>
					</div>

					<div class="form-group">
						<?php
						//data retrieve for table
						$butir = "dosen_ps";
						//retrieve counter
						$query="SELECT isi_float FROM isi_borang WHERE username='".$usernameProfil."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom='0' ORDER BY no ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0){
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$counterdata[$index] = $row['isi_float'];	
								$index++;
							}
						}else{
							$counterdata[0] = 1;
							$counterdata[1] = 1;
						}

						//retrieve data
						$query="SELECT isi_char FROM isi_borang WHERE username='".$usernameProfil."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom!='0' ORDER BY no,sub_no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$tabledata[$index] = $row['isi_char'];
								$index++;
							}
						}else{
							$tabledata[0] = "";
							$tabledata[1] = "";
							$tabledata[2] = "";
							$tabledata[3] = "";
							$tabledata[4] = "";
							$tabledata[5] = "";
							$tabledata[6] = "";
							$tabledata[7] = "";
						}
						?>
						<div class="col-sm-offset-2 col-sm-9 table-wrapper">
							<input type="hidden" id="4.3.1/counter" name="4_3_1/counter" <?php echo "value='".$counterdata[0]."'";?>>
							<table class="tableBorang" id="table431" style="width:195%;">
								<thead>
									<tr>
										<th>No.</th>
										<th>Nama Dosen</th>
										<th>NIDN</th>
										<th>Tgl. Lahir</th>
										<th>Jabatatan Akademik</th>
										<th>Gelar Akademik</th>
										<th colspan="2">Pendidikan S1, S2, S3 dan Asal PT</th>
										<th>Bidang Keahlian Untuk Setiap Jenjang Pendidikan</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="width:3%;">(1)</td>
										<td style="width:16%;">(2)</td>
										<td style="width:9%;">(3)</td>
										<td style="width:9%;">(4)</td>
										<td style="width:12%;">(5)</td>
										<td style="width:6%;">(6)</td>
										<td style="width:6%;">(7)</td>
										<td style="width:23%;">(8)</td>
										<td>(9)</td>
									</tr>
								</tbody>

								<?php 
								$index = 0;
								for ($no=1; $no <= $counterdata[0] ; $no++) { 
									echo "<tbody>";
									echo "<input type='hidden' id='4.3.1/counter/no".$no."' value='".$counterdata[$no]."'>";
									//first tr
									?>
									<tr>
										<td <?php echo "rowspan='".$counterdata[$no]."'" ?>><?php echo $no; ?></td>
										<td <?php echo "rowspan='".$counterdata[$no]."' id='4.3.1/2/no".$no."'";?>><?php echo $tabledata[$index]; $index++;?></td>
										<td <?php echo "rowspan='".$counterdata[$no]."'" ?>><?php echo $tabledata[$index]; $index++;?></td>
										<td <?php echo "rowspan='".$counterdata[$no]."'" ?>><?php echo $tabledata[$index]; $index++;?></td>
										<td <?php echo "rowspan='".$counterdata[$no]."'" ?>><?php echo $tabledata[$index]; $index++;?></td>
										<td><?php echo $tabledata[$index]; $index++;?></td>
										<td><?php echo $tabledata[$index]; $index++;?></td>
										<td><?php echo $tabledata[$index]; $index++;?></td>
										<td><?php echo $tabledata[$index]; $index++;?></td>
									</tr>

									<?php
									//sub tr
									for ($sub=2; $sub <= $counterdata[$no]; $sub++) { 
										?>
										<tr>
											<td><?php echo $tabledata[$index]; $index++;?></td>
											<td><?php echo $tabledata[$index]; $index++;?></td>
											<td><?php echo $tabledata[$index]; $index++;?></td>
											<td><?php echo $tabledata[$index]; $index++;?></td>
										</tr>
										<?php
									}
									echo "</tbody>";
								}
								?>
							</table>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="8">4.3.1.a</td>
									<td colspan="2">Dosen tetap berpendidikan (terakhir) S2 dan S3 yang bidang keahliannya sesuai dengan kompetensi PS.</td>
								</tr>
								<tr>
									<td>Jumlah Dosen Tetap</td>
									<td id="nilai/4.3.1.a/1"></td>
								</tr>
								<tr>
									<td>Dosen tetap berpendidikan terakhir S1</td>
									<td id="nilai/4.3.1.a/2"></td>
								</tr>
								<tr>
									<td>Dosen tetap berpendidikan terakhir S2</td>
									<td id="nilai/4.3.1.a/3"></td>
								</tr>
								<tr>
									<td>Dosen tetap berpendidikan terakhir S3</td>
									<td id="nilai/4.3.1.a/4"></td>
								</tr>
								<tr>
									<td>Jumlah Dosen bergelar S2 dan S3</td>
									<td id="nilai/4.3.1.a/5"></td>
								</tr>
								<tr>
									<td>Persentase dosen tetap berpendidikan (terakhir) S2 dan S3 yang bidang keahliannya sesuai dengan kompetensi PS</td>
									<td id="nilai/4.3.1.a/6"></td>
								</tr>
								<tr>
									<td>Nilai</td>
									<td id="nilai/4.3.1.a/nilai"></td>
									<input type="hidden" id="nilai/4.3.1.a/nilaihid" name="nilai/4_3_1_a" value="1">
								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="5">4.3.1.b</td>
									<td colspan="2">Dosen tetap yang berpendidikan S3 yang bidang keahliannya sesuai dengan kompetensi PS</td>
								</tr>
								<tr>
									<td>Jumlah Dosen Tetap</td>
									<td id="nilai/4.3.1.b/1"></td>
								</tr>
								<tr>
									<td>Jumlah dosen bergelar S3</td>
									<td id="nilai/4.3.1.b/2"></td>
								</tr>
								<tr>
									<td>Persentase dosen tetap yang berpendidikan S3 yang bidang keahliannya sesuai dengan kompetensi PS</td>
									<td id="nilai/4.3.1.b/3"></td>
								</tr>
								<tr>
									<td>Nilai</td>
									<td id="nilai/4.3.1.b/nilai"></td>
									<input type="hidden" id="nilai/4.3.1.b/nilaihid" name="nilai/4_3_1_b" value="1">
								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="5">4.3.1.c</td>
									<td colspan="2">Dosen tetap yang memiliki jabatan Lektor Kepala dan Guru Besar yang bidang keahliannya sesuai dengan kompetensi PS</td>
								</tr>
								<tr>
									<td>Jumlah Dosen Tetap</td>
									<td id="nilai/4.3.1.c/1"></td>
								</tr>
								<tr>
									<td>Jumlah dosen dengan jabatan lektor kepala dan guru besar</td>
									<td id="nilai/4.3.1.c/2"></td>
								</tr>
								<tr>
									<td>Persentase dosen tetap yang memiliki jabatan lektor kepala dan guru besar yang bidang keahliannya sesuai dengan kompetensi PS</td>
									<td id="nilai/4.3.1.c/3"></td>
								</tr>
								<tr>
									<td>Nilai</td>
									<td id="nilai/4.3.1.c/nilai"></td>
									<input type="hidden" id="nilai/4.3.1.c/nilaihid" name="nilai/4_3_1_c" value="1">
								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="5">4.3.1.d</td>
									<td colspan="2">Dosen tetap yang bidang keahliannya sesuai dengan kompetensi PS, yang memiliki Sertifikat Pendidik Profesional</td>
								</tr>
								<tr>
									<td>Jumlah Dosen Tetap</td>
									<td id="nilai/4.3.1.d/1"></td>
								</tr>
								<tr>
									<td>Dosen yang memiliki sertifikat pendidik profesional</td>
									<?php 
									//retrieve Nilai
									$butir = "4.3.1.d";
									$subButir = "2";
									$tabledata = array_fill(0, 1, '');
									$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND sub_butir='".$subButir."' ORDER BY sub_butir";
									$data = mysqli_query($db, $query);
									$count = mysqli_num_rows($data);
									if ($count>0) {
										while ($row = mysqli_fetch_assoc($data)) {
											$tabledata[0] = $row['isi'];
										}
									}
									?>
									<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="nilai/4_3_1_d/2" id="nilai/4.3.1.d/2" onchange="updateNilai31();" value=<?php echo "'".$tabledata[0]."'";?>></td>
								</tr>
								<tr>
									<td>Persentase dosen yang memiliki Sertifikat Pendidik Profesional</td>
									<td id="nilai/4.3.1.d/3"></td>
								</tr>
								<tr>
									<td>Nilai</td>
									<td id="nilai/4.3.1.d/nilai"></td>
									<input type="hidden" id="nilai/4.3.1.d/nilaihid" name="nilai/4_3_1_d" value="1">
								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 text-right">4.3.2</label> 
						<div class="col-sm-8"><p>Data dosen tetap yang bidang keahliannya di luar bidang PS:</p></div>
					</div>

					<div class="form-group">
						<?php
						//data retrieve for table
						$butir = "dosen_xps";
						//retrieve counter
						$query="SELECT isi_float FROM isi_borang WHERE username='".$usernameProfil."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom='0' ORDER BY no ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0){
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$counterdata[$index] = $row['isi_float'];	
								$index++;
							}
						}else{
							$counterdata[0] = 1;
							$counterdata[1] = 1;
						}

						//retrieve data
						$query="SELECT isi_char FROM isi_borang WHERE username='".$usernameProfil."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom!='0' ORDER BY no,sub_no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$tabledata[$index] = $row['isi_char'];
								$index++;
							}
						}else{
							$tabledata[0] = "";
							$tabledata[1] = "";
							$tabledata[2] = "";
							$tabledata[3] = "";
							$tabledata[4] = "";
							$tabledata[5] = "";
							$tabledata[6] = "";
							$tabledata[7] = "";
						}
						?>
						<div class="col-sm-offset-2 col-sm-9 table-wrapper">
							<input type="hidden" id="4.3.2/counter" name="4_3_2/counter" <?php echo "value='".$counterdata[0]."'";?>>
							<table class="tableBorang" id="table432" style="width:195%;">
								<thead>
									<tr>
										<th>No.</th>
										<th>Nama Dosen</th>
										<th>NIDN</th>
										<th>Tgl. Lahir</th>
										<th>Jabatatan Akademik</th>
										<th>Gelar Akademik</th>
										<th colspan="2">Pendidikan S1, S2, S3 dan Asal PT</th>
										<th>Bidang Keahlian Untuk Setiap Jenjang Pendidikan</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="width:3%;">(1)</td>
										<td style="width:16%;">(2)</td>
										<td style="width:9%;">(3)</td>
										<td style="width:9%;">(4)</td>
										<td style="width:12%;">(5)</td>
										<td style="width:6%;">(6)</td>
										<td style="width:6%;">(7)</td>
										<td style="width:23%;">(8)</td>
										<td>(9)</td>
									</tr>
								</tbody>

								<?php 
								$index = 0;
								for ($no=1; $no <= $counterdata[0] ; $no++) { 
									echo "<tbody>";
									echo "<input type='hidden' id='4.3.2/counter/no".$no."' value='".$counterdata[$no]."'>";
									//first tr
									?>
									<tr>
										<td <?php echo "rowspan='".$counterdata[$no]."'" ?>><?php echo $no; ?></td>
										<td <?php echo "rowspan='".$counterdata[$no]."' id='4.3.2/2/no".$no."'";?>><?php echo $tabledata[$index]; $index++;?></td>
										<td <?php echo "rowspan='".$counterdata[$no]."'" ?>><?php echo $tabledata[$index]; $index++;?></td>
										<td <?php echo "rowspan='".$counterdata[$no]."'" ?>><?php echo $tabledata[$index]; $index++;?></td>
										<td <?php echo "rowspan='".$counterdata[$no]."'" ?>><?php echo $tabledata[$index]; $index++;?></td>
										<td><?php echo $tabledata[$index]; $index++;?></td>
										<td><?php echo $tabledata[$index]; $index++;?></td>
										<td><?php echo $tabledata[$index]; $index++;?></td>
										<td><?php echo $tabledata[$index]; $index++;?></td>
									</tr>

									<?php
									//sub tr
									for ($sub=2; $sub <= $counterdata[$no]; $sub++) { 
										?>
										<tr>
											<td><?php echo $tabledata[$index]; $index++;?></td>
											<td><?php echo $tabledata[$index]; $index++;?></td>
											<td><?php echo $tabledata[$index]; $index++;?></td>
											<td><?php echo $tabledata[$index]; $index++;?></td>
										</tr>
										<?php
									}
									echo "</tbody>";
								}
								?>
								
							</table>
						</div>
					</div>

					<div class="form-group">
						<?php 
						//retrieve eksakta/sosial
						$butir = "4.3.2";
						$subButir = "1";
						$tabledata = array_fill(0, 2, '0');
						$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND sub_butir='".$subButir."' ORDER BY sub_butir";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							while ($row = mysqli_fetch_assoc($data)) {
								$tabledata[0] = $row['isi'];
							}
						}

						//retrieve jumlah mahasiswa
						$butir1 = "3.1.1";
						$butir2 = "3.1.2";
						$kolom1 = "7";
						$kolom2 = "8";
						$nobaris = "0";
						$tabledata = array_fill(0, 1, '');
						$query="SELECT SUM(isi_float) FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND (butir ='".$butir1."' OR butir='".$butir2."') AND (kolom='".$kolom1."' OR kolom='".$kolom2."') AND no='".$nobaris."' ORDER BY no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							while ($row = mysqli_fetch_assoc($data)) {
								$tabledata[1] = $row['SUM(isi_float)'];
							}
						}
						
						?>
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center" style="width:72%;>Keterangan</th>
									<th">Nilai</th>
								</tr>
								<tr>
									<td rowspan="6">4.3.2</td>
									<td colspan="2">Rasio mahasiswa terhadap dosen tetap yang bidang keahliannya sesuai dengan bidang PS</td>
								</tr>
								<tr>
									<td>Prodi Eksakta/Sosial</td>
									<td> <select class='form-control' id="nilai/4.3.2/1" name="nilai/4_3_2/1" onchange="updateNilai31()">
										<option value="0" <?php if ($tabledata[0]=="0") {echo "selected";} ?> >Eksakta</option>
										<option value="1" <?php if ($tabledata[0]=="1") {echo "selected";} ?> >Sosial</option>
									</select> </td>
								</tr>
								<tr>
									<td>Jumlah mahasiswa (Dari tabel Standar 3 baris TS kolom 7 dan 8)</td>
									<td id="nilai/4.3.2/2"><?php echo $tabledata[1]; ?></td>
								</tr>
								<tr>
									<td>Jumlah dosen</td>
									<td id="nilai/4.3.2/3"></td>
								</tr>
								<tr>
									<td>Rasio</td>
									<td id="nilai/4.3.2/4"></td>
								</tr>
								<tr>
									<td>Nilai</td>
									<td id="nilai/4.3.2/nilai"></td>
									<input type="hidden" id="nilai/4.3.2/nilaihid" name="nilai/4.3.2" value="">
								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 text-right">4.3.3</label> 
						<div class="col-sm-8"><p>Aktivitas dosen tetap yang bidang keahliannya sesuai dengan PS dinyatakan dalam sks rata-rata per semester pada satu tahun akademik terakhir, diisi dengan perhitungan sesuai SK Dirjen DIKTI no. 48 tahun 1983 (12 sks setara dengan 36 jam kerja per minggu) <br><br>Jika pilihan 'nama dosen' masih kosong, anda mungkin belum melengkapi data dosen di halaman utama (HOME)</p></div>
					</div>

					<div class="form-group">
						<?php
						//data retrieve for table
						$butir = "4.3.3.gan";
						$query="SELECT isi_char, isi_float, isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							$categoryIndex = 1;
							while ($row = mysqli_fetch_assoc($data)) {
								if ($index == $categoryIndex) {
									$tabledata[$index] = $row['isi_char'];
									$categoryIndex = $categoryIndex + 8;
								}else{
									$tabledata[$index] = $row['isi_float'];
								}
								
								$index++;
							}
						}else{
							$tabledata[0] = 1;
							$tabledata[1] = "-";
							$tabledata[2] = "";
							$tabledata[3] = "";
							$tabledata[4] = "";
							$tabledata[5] = "";
							$tabledata[6] = "";
							$tabledata[7] = "";
							$tabledata[8] = "";
							
						}
						?>
						<label class="col-sm-offset-2 col-sm-8">Semester Ganjil</label>
						<div class="col-sm-8 col-sm-offset-2 table-wrapper">
							<?php 
							echo "<input type='hidden' id='4.3.3.ganjil/counter' name='4_3_3_ganjil/counter' value=".$tabledata[0].">";
							?>
							<table class="tableBorang" style="width:150%;" id="tabel433ganjil">
								<thead>
									<tr>
										<th rowspan="2">No</th>
										<th rowspan="2">Nama Dosen</th>
										<th colspan="3">sks Pengajaran pada</th>
										<th rowspan="2">sks Penelitian</th>
										<th rowspan="2">sks Pengabdian kepada Masyarakat</th>
										<th colspan="2">sks Manajemen</th>
										<th rowspan="2">Jumlah sks</th>
									</tr>
									<tr>
										<th>PS Sendiri</th>
										<th>PS lain PT sendiri</th>
										<th>PT lain</th>
										<th>PT Sendiri</th>
										<th>PT lain</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="width:4%;">(1)</td>
										<td>(2)</td>
										<td style="width:8%;">(3)</td>
										<td style="width:8%;">(4)</td>
										<td style="width:8%;">(5)</td>
										<td style="width:8%;">(6)</td>
										<td style="width:9%;">(7)</td>
										<td style="width:8%;">(8)</td>
										<td style="width:8%;">(9)</td>
										<td style="width:8%;">(10)</td>
									</tr>
									<?php 
									$index = 1;
									for ($row=1; $row <= $tabledata[0]; $row++){
										echo "<tr>
										<td>".$row."</td>
										<td><select class='form-control' name='4_3_3_ganjil/2/no".$row."' id='4_3_3/2/no".$row."'>
											<option>-</option>";
											for ($i=1; $i <= $countDosenPS; $i++) {
												if ($tabledata[$index]==$dosenPS[$i]) {
													echo "<option selected>".$dosenPS[$i]."</option>";
												}
												else{
													echo "<option>".$dosenPS[$i]."</option>";	
												}
												
											}
											$index++;
											echo "</select></td>";
											echo "<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_3_3_ganjil/3/no".$row."' onchange='updateNilai33ganjil(".$row.",2,\"tabel433ganjil\",\"4.3.3.ganjil/3/jumlah\",\"4.3.3.ganjil/3/rata\")' value='".$tabledata[$index]."'></td>"; $index++;
											echo "<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_3_3_ganjil/4/no".$row."' onchange='updateNilai33ganjil(".$row.",3,\"tabel433ganjil\",\"4.3.3.ganjil/4/jumlah\",\"4.3.3.ganjil/4/rata\")' value='".$tabledata[$index]."'></td>"; $index++;
											echo "<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_3_3_ganjil/5/no".$row."' onchange='updateNilai33ganjil(".$row.",4,\"tabel433ganjil\",\"4.3.3.ganjil/5/jumlah\",\"4.3.3.ganjil/5/rata\")' value='".$tabledata[$index]."'></td>"; $index++;
											echo "<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_3_3_ganjil/6/no".$row."' onchange='updateNilai33ganjil(".$row.",5,\"tabel433ganjil\",\"4.3.3.ganjil/6/jumlah\",\"4.3.3.ganjil/6/rata\")' value='".$tabledata[$index]."'></td>"; $index++;
											echo "<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_3_3_ganjil/7/no".$row."' onchange='updateNilai33ganjil(".$row.",6,\"tabel433ganjil\",\"4.3.3.ganjil/7/jumlah\",\"4.3.3.ganjil/7/rata\")' value='".$tabledata[$index]."'></td>"; $index++;
											echo "<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_3_3_ganjil/8/no".$row."' onchange='updateNilai33ganjil(".$row.",7,\"tabel433ganjil\",\"4.3.3.ganjil/8/jumlah\",\"4.3.3.ganjil/8/rata\")' value='".$tabledata[$index]."'></td>"; $index++;
											echo "<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_3_3_ganjil/9/no".$row."' onchange='updateNilai33ganjil(".$row.",8,\"tabel433ganjil\",\"4.3.3.ganjil/9/jumlah\",\"4.3.3.ganjil/9/rata\")' value='".$tabledata[$index]."'></td>"; $index++;
											echo "<td></td>
										</tr>";
									}
									?>


									<tr>
										<td colspan="10"><button type="button" onclick="addRow33ganjil('tabel433ganjil','4.3.3.ganjil/counter')"><span class="glyphicon glyphicon-plus"></span></button>
										<button type="button" onclick="removerow('tabel433ganjil','4.3.3.ganjil/counter')"><span class="glyphicon glyphicon-minus"></span></button></td>
									</tr>

									<tr>
										<td colspan="2">Jumlah</td>
										<td id="4.3.3.ganjil/3/jumlah"></td>
										<td id="4.3.3.ganjil/4/jumlah"></td>
										<td id="4.3.3.ganjil/5/jumlah"></td>
										<td id="4.3.3.ganjil/6/jumlah"></td>
										<td id="4.3.3.ganjil/7/jumlah"></td>
										<td id="4.3.3.ganjil/8/jumlah"></td>
										<td id="4.3.3.ganjil/9/jumlah"></td>
										<td id="4.3.3.ganjil/10/jumlah"></td>
									</tr>
								</tbody>
								<tbody>
									<tr>
										<td colspan="2">Rata-rata</td>
										<td id="4.3.3.ganjil/3/rata"></td>
										<td id="4.3.3.ganjil/4/rata"></td>
										<td id="4.3.3.ganjil/5/rata"></td>
										<td id="4.3.3.ganjil/6/rata"></td>
										<td id="4.3.3.ganjil/7/rata"></td>
										<td id="4.3.3.ganjil/8/rata"></td>
										<td id="4.3.3.ganjil/9/rata"></td>
										<td id="4.3.3.ganjil/10/rata"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="form-group">
						<?php
						//data retrieve for table
						$butir = "4.3.3.gen";
						$query="SELECT isi_char, isi_float, isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							$categoryIndex = 1;
							while ($row = mysqli_fetch_assoc($data)) {
								if ($index == $categoryIndex) {
									$tabledata[$index] = $row['isi_char'];
									$categoryIndex = $categoryIndex + 8;
								}else{
									$tabledata[$index] = $row['isi_float'];
								}
								
								$index++;
							}
						}else{
							$tabledata[0] = 1;
							$tabledata[1] = "-";
							$tabledata[2] = "";
							$tabledata[3] = "";
							$tabledata[4] = "";
							$tabledata[5] = "";
							$tabledata[6] = "";
							$tabledata[7] = "";
							$tabledata[8] = "";
							
						}
						?>
						<label class="col-sm-offset-2 col-sm-8">Semester genap</label>
						<div class="col-sm-8 col-sm-offset-2 table-wrapper">
							<?php 
							echo "<input type='hidden' id='4.3.3.genap/counter' name='4_3_3_genap/counter' value=".$tabledata[0].">";
							?>
							<table class="tableBorang" style="width:150%;" id="tabel433genap">
								<thead>
									<tr>
										<th rowspan="2">No</th>
										<th rowspan="2">Nama Dosen</th>
										<th colspan="3">sks Pengajaran pada</th>
										<th rowspan="2">sks Penelitian</th>
										<th rowspan="2">sks Pengabdian kepada Masyarakat</th>
										<th colspan="2">sks Manajemen</th>
										<th rowspan="2">Jumlah sks</th>
									</tr>
									<tr>
										<th>PS Sendiri</th>
										<th>PS lain PT sendiri</th>
										<th>PT lain</th>
										<th>PT Sendiri</th>
										<th>PT lain</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="width:4%;">(1)</td>
										<td>(2)</td>
										<td style="width:8%;">(3)</td>
										<td style="width:8%;">(4)</td>
										<td style="width:8%;">(5)</td>
										<td style="width:8%;">(6)</td>
										<td style="width:9%;">(7)</td>
										<td style="width:8%;">(8)</td>
										<td style="width:8%;">(9)</td>
										<td style="width:8%;">(10)</td>
									</tr>
									<?php 
									$index = 1;
									for ($row=1; $row <= $tabledata[0]; $row++){
										echo "<tr>
										<td>".$row."</td>
										<td><select class='form-control' name='4_3_3_genap/2/no".$row."' id='4_3_3/2/no".$row."'>
											<option>-</option>";
											for ($i=1; $i <= $countDosenPS; $i++) {
												if ($tabledata[$index]==$dosenPS[$i]) {
													echo "<option selected>".$dosenPS[$i]."</option>";
												}
												else{
													echo "<option>".$dosenPS[$i]."</option>";	
												}
												
											}
											$index++;
											echo "</select></td>";
											echo "<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_3_3_genap/3/no".$row."' onchange='updateNilai33genap(".$row.",2,\"tabel433genap\",\"4.3.3.genap/3/jumlah\",\"4.3.3.genap/3/rata\")' value='".$tabledata[$index]."'></td>"; $index++;
											echo "<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_3_3_genap/4/no".$row."' onchange='updateNilai33genap(".$row.",3,\"tabel433genap\",\"4.3.3.genap/4/jumlah\",\"4.3.3.genap/4/rata\")' value='".$tabledata[$index]."'></td>"; $index++;
											echo "<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_3_3_genap/5/no".$row."' onchange='updateNilai33genap(".$row.",4,\"tabel433genap\",\"4.3.3.genap/5/jumlah\",\"4.3.3.genap/5/rata\")' value='".$tabledata[$index]."'></td>"; $index++;
											echo "<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_3_3_genap/6/no".$row."' onchange='updateNilai33genap(".$row.",5,\"tabel433genap\",\"4.3.3.genap/6/jumlah\",\"4.3.3.genap/6/rata\")' value='".$tabledata[$index]."'></td>"; $index++;
											echo "<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_3_3_genap/7/no".$row."' onchange='updateNilai33genap(".$row.",6,\"tabel433genap\",\"4.3.3.genap/7/jumlah\",\"4.3.3.genap/7/rata\")' value='".$tabledata[$index]."'></td>"; $index++;
											echo "<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_3_3_genap/8/no".$row."' onchange='updateNilai33genap(".$row.",7,\"tabel433genap\",\"4.3.3.genap/8/jumlah\",\"4.3.3.genap/8/rata\")' value='".$tabledata[$index]."'></td>"; $index++;
											echo "<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_3_3_genap/9/no".$row."' onchange='updateNilai33genap(".$row.",8,\"tabel433genap\",\"4.3.3.genap/9/jumlah\",\"4.3.3.genap/9/rata\")' value='".$tabledata[$index]."'></td>"; $index++;
											echo "<td></td>
										</tr>";
									}



									?>


									<tr>
										<td colspan="10"><button type="button" onclick="addRow33genap('tabel433genap','4.3.3.genap/counter')"><span class="glyphicon glyphicon-plus"></span></button>
										<button type="button" onclick="removerow('tabel433genap','4.3.3.genap/counter')"><span class="glyphicon glyphicon-minus"></span></button></td>
									</tr>

									<tr>
										<td colspan="2">Jumlah</td>
										<td id="4.3.3.genap/3/jumlah"></td>
										<td id="4.3.3.genap/4/jumlah"></td>
										<td id="4.3.3.genap/5/jumlah"></td>
										<td id="4.3.3.genap/6/jumlah"></td>
										<td id="4.3.3.genap/7/jumlah"></td>
										<td id="4.3.3.genap/8/jumlah"></td>
										<td id="4.3.3.genap/9/jumlah"></td>
										<td id="4.3.3.genap/10/jumlah"></td>
									</tr>
								</tbody>
								<tbody>
									<tr>
										<td colspan="2">Rata-rata</td>
										<td id="4.3.3.genap/3/rata"></td>
										<td id="4.3.3.genap/4/rata"></td>
										<td id="4.3.3.genap/5/rata"></td>
										<td id="4.3.3.genap/6/rata"></td>
										<td id="4.3.3.genap/7/rata"></td>
										<td id="4.3.3.genap/8/rata"></td>
										<td id="4.3.3.genap/9/rata"></td>
										<td id="4.3.3.genap/10/rata"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="2">4.3.3</td>
									<td>Rata-rata beban dosen per semester, atau rata-rata FTE (Fulltime Teaching Equivalent)</td>
									<td id="nilai/4.3.3/1"></td>
								</tr>
								<tr>
									<td>Nilai</td>
									<td id="nilai/4.3.3/nilai"></td>
									<input type="hidden" id="nilai/4.3.3/nilaihid" name="nilai/4_3_3" value="">
								</tr>
							</table>
						</div>
					</div>


					<div class="form-group">
						<label class="col-sm-2 text-right">4.3.4</label> 
						<div class="col-sm-8"><p>Data aktivitas mengajar dosen tetap yang bidang keahliannya sesuai dengan PS, dalam satu tahun akademik terakhir di PS ini:</p></div>
					</div>

					<div class="form-group">
						<?php
						//data retrieve for table
						$butir = "4.3.4.gan";
						//retrieve counter
						$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom='0' ORDER BY no ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0){
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$counterdata[$index] = $row['isi_float'];	
								$index++;
							}
						}else{
							$counterdata[0] = 1;
							$counterdata[1] = 1;
						}

						//retrieve data
						$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom!='0' ORDER BY no,sub_no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$tabledata[$index] = $row['isi_char'];
								$index++;
							}
						}else{
							$tabledata[0] = "";
							$tabledata[1] = "";
							$tabledata[2] = "";
							$tabledata[3] = "";
							$tabledata[4] = "";
							$tabledata[5] = "";
							$tabledata[6] = "";
						}
						?>

						<label class="col-sm-offset-2 col-sm-8">Semester ganjil</label>
						<div class="col-sm-offset-2 col-sm-9 table-wrapper">
							<input type="hidden" id="4.3.4.ganjil/counter" name="4_3_4_ganjil/counter" <?php echo "value='".$counterdata[0]."'";?>>
							<table class="tableBorang" id="table434ganjil" style="width:150%;">
								<thead>
									<tr>
										<th>No.</th>
										<th>Nama Dosen</th>
										<th>Bidang Keahlian</th>
										<th>Kode Mata Kuliah</th>
										<th>Nama Mata Kuliah</th>
										<th>Jumlah Kelas</th>
										<th>Jumlah Pertemuan yang direncanakan</th>
										<th>Jumlah Pertemuan yang dilaksanakan</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="width:4%;">(1)</td>
										<td>(2)</td>
										<td style="width:15%;">(3)</td>
										<td style="width:9%;">(4)</td>
										<td style="width:17%;">(5)</td>
										<td style="width:6%;">(6)</td>
										<td style="width:10%;">(7)</td>
										<td style="width:10%;">(8)</td>
									</tr>
								</tbody>

								<?php 
								$index = 0;
								for ($no=1; $no <= $counterdata[0] ; $no++) { 
									echo "<tbody>";
									echo "<input type='hidden' id='4.3.4.ganjil/counter/no".$no."' value='".$counterdata[$no]."' name='4_3_4_ganjil/counter/no".$no."'>";
									//first tr
									?>
									<tr>
										<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>><?php echo $no; ?></td>
										<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>>
											<?php
											echo "<select class='form-control' name='4_3_4_ganjil/2/no".$no."/sub1' id='4.3.4.ganjil/2/no".$no."'>
											<option>-</option>";
											for ($i=1; $i <= $countDosenPS; $i++) {
												if ($tabledata[$index]==$dosenPS[$i]) {
													echo "<option selected>".$dosenPS[$i]."</option>";
												}
												else{
													echo "<option>".$dosenPS[$i]."</option>";	
												}
												
											}
											$index++;
											echo "</select>";
											?> 
										</td>
										<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>><textarea rows='1' style='width:100%;' <?php echo "name='4_3_4_ganjil/3/no".$no."/sub1'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
										<td><input type='text' class='form-control' <?php echo "name='4_3_4_ganjil/4/no".$no."/sub1'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
										<td><textarea rows='1' style='width:100%;' <?php echo "name='4_3_4_ganjil/5/no".$no."/sub1'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
										<td><input type='text' class='form-control' <?php echo "name='4_3_4_ganjil/6/no".$no."/sub1'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
										<td><input type='text' class='form-control' onchange='updateNilai3435(6,"table434ganjil","4.3.4.ganjil/counter","4.3.4.ganjil/7/jumlah")' pattern='[0-9]+([\.][0-9]+)?' <?php echo "name='4_3_4_ganjil/7/no".$no."/sub1'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
										<td><input type='text' class='form-control' onchange='updateNilai3435(7,"table434ganjil","4.3.4.ganjil/counter","4.3.4.ganjil/8/jumlah")' pattern='[0-9]+([\.][0-9]+)?' <?php echo "name='4_3_4_ganjil/8/no".$no."/sub1'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
										
									</tr>

									<?php
									//sub tr
									for ($sub=2; $sub <= $counterdata[$no]; $sub++) { 
										?>
										<tr>
											<td><input type='text' class='form-control' <?php echo "name='4_3_4_ganjil/4/no".$no."/sub".$sub."'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
											<td><textarea rows='1' style='width:100%;' <?php echo "name='4_3_4_ganjil/5/no".$no."/sub".$sub."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td><input type='text' class='form-control' <?php echo "name='4_3_4_ganjil/6/no".$no."/sub".$sub."'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
											<td><input type='text' class='form-control' onchange='updateNilai3435(6,"table434ganjil","4.3.4.ganjil/counter","4.3.4.ganjil/7/jumlah")' pattern='[0-9]+([\.][0-9]+)?' <?php echo "name='4_3_4_ganjil/7/no".$no."/sub".$sub."'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
											<td><input type='text' class='form-control' onchange='updateNilai3435(7,"table434ganjil","4.3.4.ganjil/counter","4.3.4.ganjil/8/jumlah")' pattern='[0-9]+([\.][0-9]+)?' <?php echo "name='4_3_4_ganjil/8/no".$no."/sub".$sub."'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
										</tr>
										<?php
									}

									//add row button
									?>
									<tr>
										<td colspan='5'><button type='button' <?php echo "onclick='addrowtbody34ganjil(\"table434ganjil\",".$no.",\"4.3.4.ganjil/counter/no".$no."\")'";?>><span class="glyphicon glyphicon-plus"></span></button>
										<button type='button' <?php echo "onclick='removerowtbody(\"table434ganjil\",".$no.",\"4.3.4.ganjil/counter/no".$no."\",3)'";?>><span class="glyphicon glyphicon-minus"></span></button></td>
									</tr>
									<?php
									echo "</tbody>";
								}
								?>
								<tbody>
									<tr>
										<td colspan="8"><button type="button" onclick="addtbody34ganjil('table434ganjil','4.3.4.ganjil/counter')"><span class="glyphicon glyphicon-plus"></span></button>
										<button type="button" onclick="removetbody('table434ganjil','4.3.4.ganjil/counter')"><span class="glyphicon glyphicon-minus"></span></button></td>
									</tr>
								</tbody>
								<tbody>
									<tr>
										<td colspan="6">Jumlah</td>
										<td id="4.3.4.ganjil/7/jumlah"></td>
										<td id="4.3.4.ganjil/8/jumlah"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="form-group">
						<?php
						//data retrieve for table
						$butir = "4.3.4.gen";
						//retrieve counter
						$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom='0' ORDER BY no ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0){
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$counterdata[$index] = $row['isi_float'];	
								$index++;
							}
						}else{
							$counterdata[0] = 1;
							$counterdata[1] = 1;
						}

						//retrieve data
						$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom!='0' ORDER BY no,sub_no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$tabledata[$index] = $row['isi_char'];
								$index++;
							}
						}else{
							$tabledata[0] = "";
							$tabledata[1] = "";
							$tabledata[2] = "";
							$tabledata[3] = "";
							$tabledata[4] = "";
							$tabledata[5] = "";
							$tabledata[6] = "";
						}
						?>

						<label class="col-sm-offset-2 col-sm-8">Semester genap</label>
						<div class="col-sm-offset-2 col-sm-9 table-wrapper">
							<input type="hidden" id="4.3.4.genap/counter" name="4_3_4_genap/counter" <?php echo "value='".$counterdata[0]."'";?>>
							<table class="tableBorang" id="table434genap" style="width:150%;">
								<thead>
									<tr>
										<th>No.</th>
										<th>Nama Dosen</th>
										<th>Bidang Keahlian</th>
										<th>Kode Mata Kuliah</th>
										<th>Nama Mata Kuliah</th>
										<th>Jumlah Kelas</th>
										<th>Jumlah Pertemuan yang direncanakan</th>
										<th>Jumlah Pertemuan yang dilaksanakan</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="width:4%;">(1)</td>
										<td>(2)</td>
										<td style="width:15%;">(3)</td>
										<td style="width:9%;">(4)</td>
										<td style="width:17%;">(5)</td>
										<td style="width:6%;">(6)</td>
										<td style="width:10%;">(7)</td>
										<td style="width:10%;">(8)</td>
									</tr>
								</tbody>

								<?php 
								$index = 0;
								for ($no=1; $no <= $counterdata[0] ; $no++) { 
									echo "<tbody>";
									echo "<input type='hidden' id='4.3.4.genap/counter/no".$no."' value='".$counterdata[$no]."' name='4_3_4_genap/counter/no".$no."'>";
									//first tr
									?>
									<tr>
										<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>><?php echo $no; ?></td>
										<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>>
											<?php
											echo "<select class='form-control' name='4_3_4_genap/2/no".$no."/sub1' id='4.3.4.genap/2/no".$no."'>
											<option>-</option>";
											for ($i=1; $i <= $countDosenPS; $i++) {
												if ($tabledata[$index]==$dosenPS[$i]) {
													echo "<option selected>".$dosenPS[$i]."</option>";
												}
												else{
													echo "<option>".$dosenPS[$i]."</option>";	
												}
												
											}
											$index++;
											echo "</select>";
											?> 
										</td>
										<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>><textarea rows='1' style='width:100%;' <?php echo "name='4_3_4_genap/3/no".$no."/sub1'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
										<td><input type='text' class='form-control' <?php echo "name='4_3_4_genap/4/no".$no."/sub1'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
										<td><textarea rows='1' style='width:100%;' <?php echo "name='4_3_4_genap/5/no".$no."/sub1'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
										<td><input type='text' class='form-control' <?php echo "name='4_3_4_genap/6/no".$no."/sub1'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
										<td><input type='text' class='form-control' onchange='updateNilai3435(6,"table434genap","4.3.4.genap/counter","4.3.4.genap/7/jumlah")' pattern='[0-9]+([\.][0-9]+)?' <?php echo "name='4_3_4_genap/7/no".$no."/sub1'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
										<td><input type='text' class='form-control' onchange='updateNilai3435(7,"table434genap","4.3.4.genap/counter","4.3.4.genap/8/jumlah")' pattern='[0-9]+([\.][0-9]+)?' <?php echo "name='4_3_4_genap/8/no".$no."/sub1'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
										
									</tr>

									<?php
									//sub tr
									for ($sub=2; $sub <= $counterdata[$no]; $sub++) { 
										?>
										<tr>
											<td><input type='text' class='form-control' <?php echo "name='4_3_4_genap/4/no".$no."/sub".$sub."'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
											<td><textarea rows='1' style='width:100%;' <?php echo "name='4_3_4_genap/5/no".$no."/sub".$sub."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td><input type='text' class='form-control' <?php echo "name='4_3_4_genap/6/no".$no."/sub".$sub."'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
											<td><input type='text' class='form-control' onchange='updateNilai3435(6,"table434genap","4.3.4.genap/counter","4.3.4.genap/7/jumlah")' pattern='[0-9]+([\.][0-9]+)?' <?php echo "name='4_3_4_genap/7/no".$no."/sub".$sub."'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
											<td><input type='text' class='form-control' onchange='updateNilai3435(7,"table434genap","4.3.4.genap/counter","4.3.4.genap/8/jumlah")' pattern='[0-9]+([\.][0-9]+)?' <?php echo "name='4_3_4_genap/8/no".$no."/sub".$sub."'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
										</tr>
										<?php
									}

									//add row button
									?>
									<tr>
										<td colspan='5'><button type='button' <?php echo "onclick='addrowtbody34genap(\"table434genap\",".$no.",\"4.3.4.genap/counter/no".$no."\")'";?>><span class="glyphicon glyphicon-plus"></span></button>
										<button type='button' <?php echo "onclick='removerowtbody(\"table434genap\",".$no.",\"4.3.4.genap/counter/no".$no."\",3)'";?>><span class="glyphicon glyphicon-minus"></span></button></td>
									</tr>
									<?php
									echo "</tbody>";
								}
								?>
								<tbody>
									<tr>
										<td colspan="8"><button type="button" onclick="addtbody34genap('table434genap','4.3.4.genap/counter')"><span class="glyphicon glyphicon-plus"></span></button>
										<button type="button" onclick="removetbody('table434genap','4.3.4.genap/counter')"><span class="glyphicon glyphicon-minus"></span></button></td>
									</tr>
								</tbody>
								<tbody>
									<tr>
										<td colspan="6">Jumlah</td>
										<td id="4.3.4.genap/7/jumlah"></td>
										<td id="4.3.4.genap/8/jumlah"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 text-right">4.3.5</label>
						<div class="col-sm-8"><p>Data aktivitas mengajar dosen tetap yang bidang keahliannya di luar PS, dalam satu tahun akademik terakhir di PS ini:</p></div>
					</div>

					<div class="form-group">
						<?php
						//data retrieve for table
						$butir = "4.3.5.gan";
						//retrieve counter
						$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom='0' ORDER BY no ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0){
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$counterdata[$index] = $row['isi_float'];	
								$index++;
							}
						}else{
							$counterdata[0] = 1;
							$counterdata[1] = 1;
						}

						//retrieve data
						$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom!='0' ORDER BY no,sub_no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$tabledata[$index] = $row['isi_char'];
								$index++;
							}
						}else{
							$tabledata[0] = "";
							$tabledata[1] = "";
							$tabledata[2] = "";
							$tabledata[3] = "";
							$tabledata[4] = "";
							$tabledata[5] = "";
							$tabledata[6] = "";
						}
						?>

						<label class="col-sm-offset-2 col-sm-8">Semester ganjil</label>
						<div class="col-sm-offset-2 col-sm-9 table-wrapper">
							<input type="hidden" id="4.3.5.ganjil/counter" name="4_3_5_ganjil/counter" <?php echo "value='".$counterdata[0]."'";?>>
							<table class="tableBorang" id="table435ganjil" style="width:150%;">
								<thead>
									<tr>
										<th>No.</th>
										<th>Nama Dosen</th>
										<th>Bidang Keahlian</th>
										<th>Kode Mata Kuliah</th>
										<th>Nama Mata Kuliah</th>
										<th>Jumlah Kelas</th>
										<th>Jumlah Pertemuan yang direncanakan</th>
										<th>Jumlah Pertemuan yang dilaksanakan</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="width:4%;">(1)</td>
										<td>(2)</td>
										<td style="width:15%;">(3)</td>
										<td style="width:9%;">(4)</td>
										<td style="width:17%;">(5)</td>
										<td style="width:6%;">(6)</td>
										<td style="width:10%;">(7)</td>
										<td style="width:10%;">(8)</td>
									</tr>
								</tbody>

								<?php 
								$index = 0;
								for ($no=1; $no <= $counterdata[0] ; $no++) { 
									echo "<tbody>";
									echo "<input type='hidden' id='4.3.5.ganjil/counter/no".$no."' value='".$counterdata[$no]."' name='4_3_5_ganjil/counter/no".$no."'>";
									//first tr
									?>
									<tr>
										<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>><?php echo $no; ?></td>
										<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>>
											<?php
											echo "<select class='form-control' name='4_3_5_ganjil/2/no".$no."/sub1' id='4.3.5.ganjil/2/no".$no."'>
											<option>-</option>";
											for ($i=1; $i <= $countDosenXPS; $i++) {
												if ($tabledata[$index]==$dosenXPS[$i]) {
													echo "<option selected>".$dosenXPS[$i]."</option>";
												}
												else{
													echo "<option>".$dosenXPS[$i]."</option>";	
												}
												
											}
											$index++;
											echo "</select>";
											?> 
										</td>
										<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>><textarea rows='1' style='width:100%;' <?php echo "name='4_3_5_ganjil/3/no".$no."/sub1'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
										<td><input type='text' class='form-control' <?php echo "name='4_3_5_ganjil/4/no".$no."/sub1'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
										<td><textarea rows='1' style='width:100%;' <?php echo "name='4_3_5_ganjil/5/no".$no."/sub1'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
										<td><input type='text' class='form-control' <?php echo "name='4_3_5_ganjil/6/no".$no."/sub1'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
										<td><input type='text' class='form-control' onchange='updateNilai3435(6,"table435ganjil","4.3.5.ganjil/counter","4.3.5.ganjil/7/jumlah")' pattern='[0-9]+([\.][0-9]+)?' <?php echo "name='4_3_5_ganjil/7/no".$no."/sub1'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
										<td><input type='text' class='form-control' onchange='updateNilai3435(7,"table435ganjil","4.3.5.ganjil/counter","4.3.5.ganjil/8/jumlah")' pattern='[0-9]+([\.][0-9]+)?' <?php echo "name='4_3_5_ganjil/8/no".$no."/sub1'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
										
									</tr>

									<?php
									//sub tr
									for ($sub=2; $sub <= $counterdata[$no]; $sub++) { 
										?>
										<tr>
											<td><input type='text' class='form-control' <?php echo "name='4_3_5_ganjil/4/no".$no."/sub".$sub."'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
											<td><textarea rows='1' style='width:100%;' <?php echo "name='4_3_5_ganjil/5/no".$no."/sub".$sub."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td><input type='text' class='form-control' <?php echo "name='4_3_5_ganjil/6/no".$no."/sub".$sub."'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
											<td><input type='text' class='form-control' onchange='updateNilai3435(6,"table435ganjil","4.3.5.ganjil/counter","4.3.5.ganjil/7/jumlah")' pattern='[0-9]+([\.][0-9]+)?' <?php echo "name='4_3_5_ganjil/7/no".$no."/sub".$sub."'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
											<td><input type='text' class='form-control' onchange='updateNilai3435(7,"table435ganjil","4.3.5.ganjil/counter","4.3.5.ganjil/8/jumlah")' pattern='[0-9]+([\.][0-9]+)?' <?php echo "name='4_3_5_ganjil/8/no".$no."/sub".$sub."'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
										</tr>
										<?php
									}

									//add row button
									?>
									<tr>
										<td colspan='5'><button type='button' <?php echo "onclick='addrowtbody35ganjil(\"table435ganjil\",".$no.",\"4.3.5.ganjil/counter/no".$no."\")'";?>><span class="glyphicon glyphicon-plus"></span></button>
										<button type='button' <?php echo "onclick='removerowtbody(\"table435ganjil\",".$no.",\"4.3.5.ganjil/counter/no".$no."\",3)'";?>><span class="glyphicon glyphicon-minus"></span></button></td>
									</tr>
									<?php
									echo "</tbody>";
								}
								?>
								<tbody>
									<tr>
										<td colspan="8"><button type="button" onclick="addtbody35ganjil('table435ganjil','4.3.5.ganjil/counter')"><span class="glyphicon glyphicon-plus"></span></button>
										<button type="button" onclick="removetbody('table435ganjil','4.3.5.ganjil/counter')"><span class="glyphicon glyphicon-minus"></span></button></td>
									</tr>
								</tbody>
								<tbody>
									<tr>
										<td colspan="6">Jumlah</td>
										<td id="4.3.5.ganjil/7/jumlah"></td>
										<td id="4.3.5.ganjil/8/jumlah"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="form-group">
						<?php
						//data retrieve for table
						$butir = "4.3.5.gen";
						//retrieve counter
						$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom='0' ORDER BY no ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0){
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$counterdata[$index] = $row['isi_float'];	
								$index++;
							}
						}else{
							$counterdata[0] = 1;
							$counterdata[1] = 1;
						}

						//retrieve data
						$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom!='0' ORDER BY no,sub_no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$tabledata[$index] = $row['isi_char'];
								$index++;
							}
						}else{
							$tabledata[0] = "";
							$tabledata[1] = "";
							$tabledata[2] = "";
							$tabledata[3] = "";
							$tabledata[4] = "";
							$tabledata[5] = "";
							$tabledata[6] = "";
						}
						?>

						<label class="col-sm-offset-2 col-sm-8">Semester genap</label>
						<div class="col-sm-offset-2 col-sm-9 table-wrapper">
							<input type="hidden" id="4.3.5.genap/counter" name="4_3_5_genap/counter" <?php echo "value='".$counterdata[0]."'";?>>
							<table class="tableBorang" id="table435genap" style="width:150%;">
								<thead>
									<tr>
										<th>No.</th>
										<th>Nama Dosen</th>
										<th>Bidang Keahlian</th>
										<th>Kode Mata Kuliah</th>
										<th>Nama Mata Kuliah</th>
										<th>Jumlah Kelas</th>
										<th>Jumlah Pertemuan yang direncanakan</th>
										<th>Jumlah Pertemuan yang dilaksanakan</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="width:4%;">(1)</td>
										<td>(2)</td>
										<td style="width:15%;">(3)</td>
										<td style="width:9%;">(4)</td>
										<td style="width:17%;">(5)</td>
										<td style="width:6%;">(6)</td>
										<td style="width:10%;">(7)</td>
										<td style="width:10%;">(8)</td>
									</tr>
								</tbody>

								<?php 
								$index = 0;
								for ($no=1; $no <= $counterdata[0] ; $no++) { 
									echo "<tbody>";
									echo "<input type='hidden' id='4.3.5.genap/counter/no".$no."' value='".$counterdata[$no]."' name='4_3_5_genap/counter/no".$no."'>";
									//first tr
									?>
									<tr>
										<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>><?php echo $no; ?></td>
										<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>>
											<?php
											echo "<select class='form-control' name='4_3_5_genap/2/no".$no."/sub1' id='4.3.5.genap/2/no".$no."'>
											<option>-</option>";
											for ($i=1; $i <= $countDosenXPS; $i++) {
												if ($tabledata[$index]==$dosenXPS[$i]) {
													echo "<option selected>".$dosenXPS[$i]."</option>";
												}
												else{
													echo "<option>".$dosenXPS[$i]."</option>";	
												}
												
											}
											$index++;
											echo "</select>";
											?> 
										</td>
										<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>><textarea rows='1' style='width:100%;' <?php echo "name='4_3_5_genap/3/no".$no."/sub1'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
										<td><input type='text' class='form-control' <?php echo "name='4_3_5_genap/4/no".$no."/sub1'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
										<td><textarea rows='1' style='width:100%;' <?php echo "name='4_3_5_genap/5/no".$no."/sub1'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
										<td><input type='text' class='form-control' <?php echo "name='4_3_5_genap/6/no".$no."/sub1'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
										<td><input type='text' class='form-control' onchange='updateNilai3435(6,"table435genap","4.3.5.genap/counter","4.3.5.genap/7/jumlah")' pattern='[0-9]+([\.][0-9]+)?' <?php echo "name='4_3_5_genap/7/no".$no."/sub1'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
										<td><input type='text' class='form-control' onchange='updateNilai3435(7,"table435genap","4.3.5.genap/counter","4.3.5.genap/8/jumlah")' pattern='[0-9]+([\.][0-9]+)?' <?php echo "name='4_3_5_genap/8/no".$no."/sub1'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
										
									</tr>

									<?php
									//sub tr
									for ($sub=2; $sub <= $counterdata[$no]; $sub++) { 
										?>
										<tr>
											<td><input type='text' class='form-control' <?php echo "name='4_3_5_genap/4/no".$no."/sub".$sub."'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
											<td><textarea rows='1' style='width:100%;' <?php echo "name='4_3_5_genap/5/no".$no."/sub".$sub."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td><input type='text' class='form-control' <?php echo "name='4_3_5_genap/6/no".$no."/sub".$sub."'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
											<td><input type='text' class='form-control' onchange='updateNilai3435(6,"table435genap","4.3.5.genap/counter","4.3.5.genap/7/jumlah")' pattern='[0-9]+([\.][0-9]+)?' <?php echo "name='4_3_5_genap/7/no".$no."/sub".$sub."'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
											<td><input type='text' class='form-control' onchange='updateNilai3435(7,"table435genap","4.3.5.genap/counter","4.3.5.genap/8/jumlah")' pattern='[0-9]+([\.][0-9]+)?' <?php echo "name='4_3_5_genap/8/no".$no."/sub".$sub."'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
										</tr>
										<?php
									}

									//add row button
									?>
									<tr>
										<td colspan='5'><button type='button' <?php echo "onclick='addrowtbody35genap(\"table435genap\",".$no.",\"4.3.5.genap/counter/no".$no."\")'";?>><span class="glyphicon glyphicon-plus"></span></button>
										<button type='button' <?php echo "onclick='removerowtbody(\"table435genap\",".$no.",\"4.3.5.genap/counter/no".$no."\",3)'";?>><span class="glyphicon glyphicon-minus"></span></button></td>
									</tr>
									<?php
									echo "</tbody>";
								}
								?>
								<tbody>
									<tr>
										<td colspan="8"><button type="button" onclick="addtbody35genap('table435genap','4.3.5.genap/counter')"><span class="glyphicon glyphicon-plus"></span></button>
										<button type="button" onclick="removetbody('table435genap','4.3.5.genap/counter')"><span class="glyphicon glyphicon-minus"></span></button></td>
									</tr>
								</tbody>
								<tbody>
									<tr>
										<td colspan="6">Jumlah</td>
										<td id="4.3.5.genap/7/jumlah"></td>
										<td id="4.3.5.genap/8/jumlah"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center" colspan="2">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="6">4.3.4/5.a</td>
									<td colspan="3">Kesesuaian keahlian (pend. terakhir) dosen dengan MK yang diajarkannya.</td>
								</tr>
								<tr>
									<td>1</td>
									<td>Lebih dari 7 mata kuliah diajar oleh dosen yang tidak sesuai keahliannya.</td>
									<td></td>
								</tr>
								<tr>
									<td>2</td>
									<td>4 - 7 mata kuliah diajar oleh dosen yang tidak sesuai keahliannya.</td>
									<td></td>
								</tr>
								<tr>
									<td>3</td>
									<td>1 - 3 mata kuliah diajar oleh dosen yang tidak sesuai keahliannya.</td>
									<td></td>
								</tr>
								<tr>
									<td>4</td>
									<td>Semua mata kuliah diajar oleh dosen yang sesuai keahliannya.</td>
									<td></td>
								</tr>
								<tr>
									<td colspan="2">Nilai</td>
									<?php 
									//retrieve Nilai
									$butir = "4.3.4/5.a";
									$tabledata = array_fill(0, 1, '');
									$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY sub_butir";
									$data = mysqli_query($db, $query);
									$count = mysqli_num_rows($data);
									if ($count>0) {
										while ($row = mysqli_fetch_assoc($data)) {
											$tabledata[0] = $row['isi'];
										}
									}
									?>
									<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="nilai/4_3_4/5_a" value=<?php echo "'".$tabledata[0]."'";?>></td>
								</tr>
							</table>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="5">4.3.4/5.b</td>
									<td colspan="2">Tingkat kehadiran dosen tetap dalam mengajar</td>
								</tr>
								<tr>
									<td>Kehadiran direncanakan</td>
									<td id="nilai/4.3.4/5.b/1"></td>
								</tr>
								<tr>
									<td>Kehadiran dilaksanakan</td>
									<td id="nilai/4.3.4/5.b/2"></td>
								</tr>
								<tr>
									<td>Persentase kehadiran dosen tetap</td>
									<td id="nilai/4.3.4/5.b/3"></td>
								</tr>
								<tr>
									<td>Nilai</td>
									<td id="nilai/4.3.4/5.b/nilai"></td>
									<input type="hidden" id="nilai/4.3.4/5.b/nilaihid" name="nilai/4_3_4/5_b" value="1">
								</tr>
							</table>
						</div>
					</div>


					<div class="form-group">
						<label class="col-sm-2 text-right">4.4</label> 
						<label class="col-sm-9">Dosen Tidak Tetap</label>
					</div>

					<div class="form-group">
						<label class="col-sm-2 text-right">4.4.1</label> 
						<div class="col-sm-8"><p>Data dosen tidak tetap pada PS:</p></div>
					</div>

					<div class="form-group">
						<?php
						//data retrieve for table
						$butir = "dosen_xtp";
						//retrieve counter
						$query="SELECT isi_float FROM isi_borang WHERE username='".$usernameProfil."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom='0' ORDER BY no ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0){
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$counterdata[$index] = $row['isi_float'];	
								$index++;
							}
						}else{
							$counterdata[0] = 1;
							$counterdata[1] = 1;
						}

						//retrieve data
						$query="SELECT isi_char FROM isi_borang WHERE username='".$usernameProfil."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom!='0' ORDER BY no,sub_no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$tabledata[$index] = $row['isi_char'];
								$index++;
							}
						}else{
							$tabledata[0] = "";
							$tabledata[1] = "";
							$tabledata[2] = "";
							$tabledata[3] = "";
							$tabledata[4] = "";
							$tabledata[5] = "";
							$tabledata[6] = "";
							$tabledata[7] = "";
						}
						?>
						<div class="col-sm-offset-2 col-sm-9 table-wrapper">
							<input type="hidden" id="4.4.1/counter" name="4_4_1/counter" <?php echo "value='".$counterdata[0]."'";?>>
							<table class="tableBorang" id="table441" style="width:195%;">
								<thead>
									<tr>
										<th>No.</th>
										<th>Nama Dosen</th>
										<th>NIDN</th>
										<th>Tgl. Lahir</th>
										<th>Jabatatan Akademik</th>
										<th>Gelar Akademik</th>
										<th colspan="2">Pendidikan S1, S2, S3 dan Asal PT</th>
										<th>Bidang Keahlian Untuk Setiap Jenjang Pendidikan</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="width:3%;">(1)</td>
										<td style="width:16%;">(2)</td>
										<td style="width:9%;">(3)</td>
										<td style="width:9%;">(4)</td>
										<td style="width:12%;">(5)</td>
										<td style="width:6%;">(6)</td>
										<td style="width:6%;">(7)</td>
										<td style="width:23%;">(8)</td>
										<td>(9)</td>
									</tr>
								</tbody>

								<?php 
								$index = 0;
								for ($no=1; $no <= $counterdata[0] ; $no++) { 
									echo "<tbody>";
									echo "<input type='hidden' id='4.4.1/counter/no".$no."' value='".$counterdata[$no]."'>";
									//first tr
									?>
									<tr>
										<td <?php echo "rowspan='".$counterdata[$no]."'" ?>><?php echo $no; ?></td>
										<td <?php echo "rowspan='".$counterdata[$no]."' id='4.4.1/2/no".$no."'";?>><?php echo $tabledata[$index]; $index++;?></td>
										<td <?php echo "rowspan='".$counterdata[$no]."'" ?>><?php echo $tabledata[$index]; $index++;?></td>
										<td <?php echo "rowspan='".$counterdata[$no]."'" ?>><?php echo $tabledata[$index]; $index++;?></td>
										<td <?php echo "rowspan='".$counterdata[$no]."'" ?>><?php echo $tabledata[$index]; $index++;?></td>
										<td><?php echo $tabledata[$index]; $index++;?></td>
										<td><?php echo $tabledata[$index]; $index++;?></td>
										<td><?php echo $tabledata[$index]; $index++;?></td>
										<td><?php echo $tabledata[$index]; $index++;?></td>
									</tr>

									<?php
									//sub tr
									for ($sub=2; $sub <= $counterdata[$no]; $sub++) { 
										?>
										<tr>
											<td><?php echo $tabledata[$index]; $index++;?></td>
											<td><?php echo $tabledata[$index]; $index++;?></td>
											<td><?php echo $tabledata[$index]; $index++;?></td>
											<td><?php echo $tabledata[$index]; $index++;?></td>
										</tr>
										<?php
									}
									echo "</tbody>";
								}
								?>
							</table>
						</div>
					</div>


					<div class="form-group">
						<label class="col-sm-2 text-right">4.4.2</label>
						<div class="col-sm-8"><p>Data aktivitas mengajar dosen tetap yang bidang keahliannya di luar PS, dalam satu tahun akademik terakhir di PS ini:</p></div>
					</div>

					<div class="form-group">
						<?php
						//data retrieve for table
						$butir = "4.4.2.gan";
						//retrieve counter
						$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom='0' ORDER BY no ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0){
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$counterdata[$index] = $row['isi_float'];	
								$index++;
							}
						}else{
							$counterdata[0] = 1;
							$counterdata[1] = 1;
						}

						//retrieve data
						$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom!='0' ORDER BY no,sub_no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$tabledata[$index] = $row['isi_char'];
								$index++;
							}
						}else{
							$tabledata[0] = "";
							$tabledata[1] = "";
							$tabledata[2] = "";
							$tabledata[3] = "";
							$tabledata[4] = "";
							$tabledata[5] = "";
							$tabledata[6] = "";
						}
						?>

						<label class="col-sm-offset-2 col-sm-8">Semester ganjil</label>
						<div class="col-sm-offset-2 col-sm-9 table-wrapper">
							<input type="hidden" id="4.4.2.ganjil/counter" name="4_4_2_ganjil/counter" <?php echo "value='".$counterdata[0]."'";?>>
							<table class="tableBorang" id="table442ganjil" style="width:150%;">
								<thead>
									<tr>
										<th>No.</th>
										<th>Nama Dosen</th>
										<th>Bidang Keahlian</th>
										<th>Kode Mata Kuliah</th>
										<th>Nama Mata Kuliah</th>
										<th>Jumlah Kelas</th>
										<th>Jumlah Pertemuan yang direncanakan</th>
										<th>Jumlah Pertemuan yang dilaksanakan</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="width:4%;">(1)</td>
										<td>(2)</td>
										<td style="width:15%;">(3)</td>
										<td style="width:9%;">(4)</td>
										<td style="width:17%;">(5)</td>
										<td style="width:6%;">(6)</td>
										<td style="width:10%;">(7)</td>
										<td style="width:10%;">(8)</td>
									</tr>
								</tbody>

								<?php 
								$index = 0;
								for ($no=1; $no <= $counterdata[0] ; $no++) { 
									echo "<tbody>";
									echo "<input type='hidden' id='4.4.2.ganjil/counter/no".$no."' value='".$counterdata[$no]."' name='4_4_2_ganjil/counter/no".$no."'>";
									//first tr
									?>
									<tr>
										<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>><?php echo $no; ?></td>
										<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>>
											<?php
											echo "<select class='form-control' name='4_4_2_ganjil/2/no".$no."/sub1' id='4.4.2.ganjil/2/no".$no."'>
											<option>-</option>";
											for ($i=1; $i <= $countDosenXTP; $i++) {
												if ($tabledata[$index]==$dosenXTP[$i]) {
													echo "<option selected>".$dosenXTP[$i]."</option>";
												}
												else{
													echo "<option>".$dosenXTP[$i]."</option>";	
												}
												
											}
											$index++;
											echo "</select>";
											?>
										</td>
										<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>><textarea rows='1' style='width:100%;' <?php echo "name='4_4_2_ganjil/3/no".$no."/sub1'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
										<td><input type='text' class='form-control' <?php echo "name='4_4_2_ganjil/4/no".$no."/sub1'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
										<td><textarea rows='1' style='width:100%;' <?php echo "name='4_4_2_ganjil/5/no".$no."/sub1'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
										<td><input type='text' class='form-control' <?php echo "name='4_4_2_ganjil/6/no".$no."/sub1'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
										<td><input type='text' class='form-control' onchange='updateNilai42b(6,"table442ganjil","4.4.2.ganjil/counter","4.4.2.ganjil/7/jumlah")' pattern='[0-9]+([\.][0-9]+)?' <?php echo "name='4_4_2_ganjil/7/no".$no."/sub1'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
										<td><input type='text' class='form-control' onchange='updateNilai42b(7,"table442ganjil","4.4.2.ganjil/counter","4.4.2.ganjil/8/jumlah")' pattern='[0-9]+([\.][0-9]+)?' <?php echo "name='4_4_2_ganjil/8/no".$no."/sub1'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
										
									</tr>

									<?php
									//sub tr
									for ($sub=2; $sub <= $counterdata[$no]; $sub++) { 
										?>
										<tr>
											<td><input type='text' class='form-control' <?php echo "name='4_4_2_ganjil/4/no".$no."/sub".$sub."'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
											<td><textarea rows='1' style='width:100%;' <?php echo "name='4_4_2_ganjil/5/no".$no."/sub".$sub."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td><input type='text' class='form-control' <?php echo "name='4_4_2_ganjil/6/no".$no."/sub".$sub."'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
											<td><input type='text' class='form-control' onchange='updateNilai42b(6,"table442ganjil","4.4.2.ganjil/counter","4.4.2.ganjil/7/jumlah")' pattern='[0-9]+([\.][0-9]+)?' <?php echo "name='4_4_2_ganjil/7/no".$no."/sub".$sub."'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
											<td><input type='text' class='form-control' onchange='updateNilai42b(7,"table442ganjil","4.4.2.ganjil/counter","4.4.2.ganjil/8/jumlah")' pattern='[0-9]+([\.][0-9]+)?' <?php echo "name='4_4_2_ganjil/8/no".$no."/sub".$sub."'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
										</tr>
										<?php
									}

									//add row button
									?>
									<tr>
										<td colspan='5'><button type='button' <?php echo "onclick='addrowtbody42ganjil(\"table442ganjil\",".$no.",\"4.4.2.ganjil/counter/no".$no."\")'";?>><span class="glyphicon glyphicon-plus"></span></button>
										<button type='button' <?php echo "onclick='removerowtbody(\"table442ganjil\",".$no.",\"4.4.2.ganjil/counter/no".$no."\",3)'";?>><span class="glyphicon glyphicon-minus"></span></button></td>
									</tr>
									<?php
									echo "</tbody>";
								}
								?>
								<tbody>
									<tr>
										<td colspan="8"><button type="button" onclick="addtbody42ganjil('table442ganjil','4.4.2.ganjil/counter')"><span class="glyphicon glyphicon-plus"></span></button>
										<button type="button" onclick="removetbody('table442ganjil','4.4.2.ganjil/counter')"><span class="glyphicon glyphicon-minus"></span></button></td>
									</tr>
								</tbody>
								<tbody>
									<tr>
										<td colspan="6">Jumlah</td>
										<td id="4.4.2.ganjil/7/jumlah"></td>
										<td id="4.4.2.ganjil/8/jumlah"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="form-group">
						<?php
						//data retrieve for table
						$butir = "4.4.2.gen";
						//retrieve counter
						$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom='0' ORDER BY no ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0){
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$counterdata[$index] = $row['isi_float'];	
								$index++;
							}
						}else{
							$counterdata[0] = 1;
							$counterdata[1] = 1;
						}

						//retrieve data
						$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom!='0' ORDER BY no,sub_no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$tabledata[$index] = $row['isi_char'];
								$index++;
							}
						}else{
							$tabledata[0] = "";
							$tabledata[1] = "";
							$tabledata[2] = "";
							$tabledata[3] = "";
							$tabledata[4] = "";
							$tabledata[5] = "";
							$tabledata[6] = "";
						}
						?>

						<label class="col-sm-offset-2 col-sm-8">Semester genap</label>
						<div class="col-sm-offset-2 col-sm-9 table-wrapper">
							<input type="hidden" id="4.4.2.genap/counter" name="4_4_2_genap/counter" <?php echo "value='".$counterdata[0]."'";?>>
							<table class="tableBorang" id="table442genap" style="width:150%;">
								<thead>
									<tr>
										<th>No.</th>
										<th>Nama Dosen</th>
										<th>Bidang Keahlian</th>
										<th>Kode Mata Kuliah</th>
										<th>Nama Mata Kuliah</th>
										<th>Jumlah Kelas</th>
										<th>Jumlah Pertemuan yang direncanakan</th>
										<th>Jumlah Pertemuan yang dilaksanakan</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="width:4%;">(1)</td>
										<td>(2)</td>
										<td style="width:15%;">(3)</td>
										<td style="width:9%;">(4)</td>
										<td style="width:17%;">(5)</td>
										<td style="width:6%;">(6)</td>
										<td style="width:10%;">(7)</td>
										<td style="width:10%;">(8)</td>
									</tr>
								</tbody>

								<?php 
								$index = 0;
								for ($no=1; $no <= $counterdata[0] ; $no++) { 
									echo "<tbody>";
									echo "<input type='hidden' id='4.4.2.genap/counter/no".$no."' value='".$counterdata[$no]."' name='4_4_2_genap/counter/no".$no."'>";
									//first tr
									?>
									<tr>
										<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>><?php echo $no; ?></td>
										<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>>
											<?php
											echo "<select class='form-control' name='4_4_2_genap/2/no".$no."/sub1' id='4.4.2.genap/2/no".$no."'>
											<option>-</option>";
											for ($i=1; $i <= $countDosenXTP; $i++) {
												if ($tabledata[$index]==$dosenXTP[$i]) {
													echo "<option selected>".$dosenXTP[$i]."</option>";
												}
												else{
													echo "<option>".$dosenXTP[$i]."</option>";	
												}
											}
											$index++;
											echo "</select>";
											?> 
										</td>
										<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>><textarea rows='1' style='width:100%;' <?php echo "name='4_4_2_genap/3/no".$no."/sub1'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
										<td><input type='text' class='form-control' <?php echo "name='4_4_2_genap/4/no".$no."/sub1'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
										<td><textarea rows='1' style='width:100%;' <?php echo "name='4_4_2_genap/5/no".$no."/sub1'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
										<td><input type='text' class='form-control' <?php echo "name='4_4_2_genap/6/no".$no."/sub1'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
										<td><input type='text' class='form-control' onchange='updateNilai42b(6,"table442genap","4.4.2.genap/counter","4.4.2.genap/7/jumlah")' pattern='[0-9]+([\.][0-9]+)?' <?php echo "name='4_4_2_genap/7/no".$no."/sub1'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
										<td><input type='text' class='form-control' onchange='updateNilai42b(7,"table442genap","4.4.2.genap/counter","4.4.2.genap/8/jumlah")' pattern='[0-9]+([\.][0-9]+)?' <?php echo "name='4_4_2_genap/8/no".$no."/sub1'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
									</tr>
									<?php
									//sub tr
									for ($sub=2; $sub <= $counterdata[$no]; $sub++) { 
										?>
										<tr>
											<td><input type='text' class='form-control' <?php echo "name='4_4_2_genap/4/no".$no."/sub".$sub."'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
											<td><textarea rows='1' style='width:100%;' <?php echo "name='4_4_2_genap/5/no".$no."/sub".$sub."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td><input type='text' class='form-control' <?php echo "name='4_4_2_genap/6/no".$no."/sub".$sub."'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
											<td><input type='text' class='form-control' onchange='updateNilai42b(6,"table442genap","4.4.2.genap/counter","4.4.2.genap/7/jumlah")' pattern='[0-9]+([\.][0-9]+)?' <?php echo "name='4_4_2_genap/7/no".$no."/sub".$sub."'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
											<td><input type='text' class='form-control' onchange='updateNilai42b(7,"table442genap","4.4.2.genap/counter","4.4.2.genap/8/jumlah")' pattern='[0-9]+([\.][0-9]+)?' <?php echo "name='4_4_2_genap/8/no".$no."/sub".$sub."'"; ?> <?php echo "value='".$tabledata[$index]."'"; $index++; ?>></td>
										</tr>
										<?php
									}

									//add row button
									?>
									<tr>
										<td colspan='5'><button type='button' <?php echo "onclick='addrowtbody42genap(\"table442genap\",".$no.",\"4.4.2.genap/counter/no".$no."\")'";?>><span class="glyphicon glyphicon-plus"></span></button>
										<button type='button' <?php echo "onclick='removerowtbody(\"table442genap\",".$no.",\"4.4.2.genap/counter/no".$no."\",3)'";?>><span class="glyphicon glyphicon-minus"></span></button></td>
									</tr>
									<?php
									echo "</tbody>";
								}
								?>
								<tbody>
									<tr>
										<td colspan="8"><button type="button" onclick="addtbody42genap('table442genap','4.4.2.genap/counter')"><span class="glyphicon glyphicon-plus"></span></button>
										<button type="button" onclick="removetbody('table442genap','4.4.2.genap/counter')"><span class="glyphicon glyphicon-minus"></span></button></td>
									</tr>
								</tbody>
								<tbody>
									<tr>
										<td colspan="6">Jumlah</td>
										<td id="4.4.2.genap/7/jumlah"></td>
										<td id="4.4.2.genap/8/jumlah"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="5">4.4.1</td>
									<td colspan="2">Persentase jumlah dosen tidak tetap, terhadap jumlah seluruh dosen (baik yang bidangnya sesuai dan yang bidangnya tidak sesuai PS)</td>
								</tr>
								<tr>
									<td>Jumlah dosen tetap</td>
									<td id="nilai/4.4.1/1"><?php echo $countDosenPS+$countDosenXPS; ?></td>
								</tr>
								<tr>
									<td>Jml dosen tidak tetap</td>
									<td id="nilai/4.4.1/2"><?php echo $countDosenXTP; ?></td>
								</tr>
								<tr>
									<td>Persentase dosen tidak tetap terhadap jumlah seluruh dosen (Pdtt)</td>
									<td id="nilai/4.4.1/3"><?php
										$persentase = $countDosenXTP/($countDosenPS+$countDosenXPS)*100; 
										echo round($persentase,1)."%";
										?>
										
									</td>
								</tr>
								<tr>
									<td>Nilai</td>
									<td id="nilai/4.4.1/nilai">
										<?php 
										if ($persentase<=10) {
											$nilai= 4;
										}else if ($persentase<50) {
											$nilai=round(10*(0.5-($persentase/100)),2);
										}else{
											$nilai=0;
										}
										echo $nilai;
										?>
									</td>
									<input type="hidden" id="nilai/4.4.1/nilaihid" name="nilai/4_4_1" <?php echo "value='".$nilai."'" ?>>
								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center" colspan="2">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="7">4.4.2.a</td>
									<td colspan="3">Kesesuaian keahlian dosen tidak tetap dengan mata kuliah yang diampu. Skor butir ini = 4 bila tidak punya dosen tidak tetap</td>
								</tr>
								<tr>
									<td>0</td>
									<td>Lebih dari 6 mata kuliah diajar oleh dosen tidak tetap yang tidak sesuai keahliannya.</td>
									<td></td>
								</tr>
								<tr>
									<td>1</td>
									<td>5-6 mata kuliah diajar oleh dosen tidak tetap yang tidak sesuai keahliannya.</td>
									<td></td>
								</tr>
								<tr>
									<td>2</td>
									<td>3-4 mata kuliah diajar oleh dosen tidak tetap yang tidak sesuai keahliannya.</td>
									<td></td>
								</tr>
								<tr>
									<td>3</td>
									<td>1-2 mata kuliah diajar oleh dosen tidak tetap yang tidak sesuai keahliannya.</td>
									<td></td>
								</tr>
								<tr>
									<td>4</td>
									<td>Semua dosen tidak tetap mengajar mata kuliah yang sesuai keahliannya</td>
									<td></td>
								</tr>
								<tr>
									<td colspan="2">Nilai</td>
									<?php 
									//retrieve Nilai
									$butir = "4.4.2.a";
									$tabledata = array_fill(0, 1, '');
									$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY sub_butir";
									$data = mysqli_query($db, $query);
									$count = mysqli_num_rows($data);
									if ($count>0) {
										while ($row = mysqli_fetch_assoc($data)) {
											$tabledata[0] = $row['isi'];
										}
									}
									?>
									<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="nilai/4_4_2_a" value=<?php echo "'".$tabledata[0]."'";?>></td>
								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="5">4.4.2.b</td>
									<td colspan="2">Persentase kehadiran dosen tidak tetap dalam perkuliahan (terhadap jumlah kehadiran yang direncanakan). Skor butir ini = 4 bila tidak punya dosen tidak tetap</td>
								</tr>
								<tr>
									<td>Kehadiran direncanakan</td>
									<td id="nilai/4.4.2.b/1"></td>
								</tr>
								<tr>
									<td>Kehadiran dilaksanakan</td>
									<td id="nilai/4.4.2.b/2"></td>
								</tr>
								<tr>
									<td>Persentase kehadiran dosen tidak tetap</td>
									<td id="nilai/4.4.2.b/3"></td>
								</tr>
								<tr>
									<td>Nilai</td>
									<td id="nilai/4.4.2.b/nilai"></td>
									<input type="hidden" id="nilai/4.4.2.b/nilaihid" name="nilai/4_4_2_b" value="4">
								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 text-right">4.5</label> 
						<label class="col-sm-9">Upaya Peningkatan Sumber Daya Manusia (SDM) dalam tiga tahun terakhir</label>
					</div>

					<div class="form-group">
						<label class="col-sm-2 text-right">4.5.1</label> 
						<div class="col-sm-8"><p>Kegiatan tenaga ahli/pakar sebagai pembicara dalam seminar/pelatihan, pembicara tamu, dsb, dari luar PT sendiri (tidak termasuk dosen tidak tetap)</p></div>
					</div>


					<div class="form-group" align="center">
						<?php
							//data retrieve
						$butir = "4.5.1";
						$query="SELECT isi_char, isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								if ($index==0) {
									$tabledata[$index] = $row['isi_float'];
								}else{
									$tabledata[$index] = $row['isi_char'];
								}

								$index++;
							}
						}else{
							$tabledata[0] = 1;
							$tabledata[1] = "";
							$tabledata[2] = "";
							$tabledata[3] = "";

						}
						?>
						<div class="col-sm-8 col-sm-offset-2">
							<?php 
							echo "<input type='hidden' id='4.5.1/counter' name='4_5_1/counter' value=".$tabledata[0].">";
							?>
							<table class="tableBorang" id="tabel451">
								<thead>
									<tr>
										<th>No.</th>
										<th>Nama Tenaga Ahli/Pakar</th>
										<th>Nama dan Judul Kegiatan</th>
										<th width="20%">Waktu Pelaksanaan</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>(1)</td>
										<td>(2)</td>
										<td>(3)</td>
										<td>(4)</td>
									</tr>
									<?php 
									$index = 1;
									for ($row=1; $row <= $tabledata[0]; $row++) { 
										?>
										<tr>
											<td><?php echo $row;?></td>
											<td><textarea rows="2" style="width:100%;" onchange='updateNilai51()' <?php echo "name='4_5_1/2/no".$row."' id='4.5.1/2/no".$row."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td><textarea rows="2" style="width:100%;" <?php echo "name='4_5_1/3/no".$row."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td><textarea rows="2" style="width:100%;" <?php echo "name='4_5_1/4/no".$row."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
										</tr>

										<?php
									}


									?>

									<tr>
										<td colspan="4"><button type="button" onclick="addRow51('tabel451','4.5.1/counter')"><span class="glyphicon glyphicon-plus"></span></button>
										<button type="button" onclick="removerow('tabel451','4.5.1/counter')"><span class="glyphicon glyphicon-minus"></span></button></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="2">4.5.1</td>
									<td>Jumlah Tenaga Ahli/Pakar (sebagai pembicara dalam seminar/pelatihan, pembicara tamu, dsb, dari luar PT sendiri (tidak termasuk dosen tidak tetap).</td>
									<td id="nilai/4.5.1/1"></td>
								</tr>
								<tr>
									<td>Nilai</td>
									<td id="nilai/4.5.1/nilai"></td>
									<input type="hidden" id="nilai/4.5.1/nilaihid" name="nilai/4_5_1" value="">
								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 text-right">4.5.2</label> 
						<div class="col-sm-8"><p>Peningkatan kemampuan dosen tetap melalui program tugas belajar dalam bidang yang sesuai dengan bidang PS</p></div>
					</div>


					<div class="form-group" align="center">
						<?php
							//data retrieve
						$butir = "4.5.2";
						$query="SELECT isi_char, isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								if ($index==0) {
									$tabledata[$index] = $row['isi_float'];
								}else{
									$tabledata[$index] = $row['isi_char'];
								}

								$index++;
							}
						}else{
							$tabledata[0] = 1;
							$tabledata[1] = "";
							$tabledata[2] = "";
							$tabledata[3] = "";
							$tabledata[4] = "";
							$tabledata[5] = "";
							$tabledata[6] = "";

						}
						?>
						<div class="col-sm-8 col-sm-offset-2 table-wrapper">
							<?php 
							echo "<input type='hidden' id='4.5.2/counter' name='4_5_2/counter' value=".$tabledata[0].">";
							?>
							<table class="tableBorang" id="tabel452" style="width:130%;">
								<thead>
									<tr>
										<th>No.</th>
										<th>Nama Dosen</th>
										<th>Jenjang Pendidikan Lanjut</th>
										<th>Bidang Studi</th>
										<th>Perguruan Tinggi</th>
										<th>Negara</th>
										<th>Tahun Mulai Studi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="width:4%">(1)</td>
										<td>(2)</td>
										<td style="width:9%">(3)</td>
										<td>(4)</td>
										<td>(5)</td>
										<td style="width:12%">(6)</td>
										<td style="width:6%">(7)</td>
									</tr>
									<?php 
									$index = 1;
									for ($row=1; $row <= $tabledata[0]; $row++) { 
										?>
										<tr>
											<td><?php echo $row;?></td>
											<?php
											echo "<td><select class='form-control' name='4_5_2/2/no".$row."'>
											<option>-</option>";
											for ($i=1; $i <= $countDosenPS; $i++) {
												if ($tabledata[$index]==$dosenPS[$i]) {
													echo "<option selected>".$dosenPS[$i]."</option>";
												}
												else{
													echo "<option>".$dosenPS[$i]."</option>";	
												}

											}
											$index++;
											echo "</select></td>";
											?>

											<td>
												<select class='form-control' onchange="updateNilai52()" <?php echo "name='4_5_2/3/no".$row."'"; ?>>
													<option <?php if ($tabledata[$index]=="-") {echo "selected";} ?>>-</option>
													<option <?php if ($tabledata[$index]=="S1") {echo "selected";} ?>>S1</option>
													<option <?php if ($tabledata[$index]=="S2") {echo "selected";} ?>>S2</option>
													<option <?php if ($tabledata[$index]=="S3") {echo "selected";} ?>>S3</option>
												</select> <?php $index++; ?>
											</td>

											<td><textarea rows="1" style="width:100%;" <?php echo "name='4_5_2/4/no".$row."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td><textarea rows="1" style="width:100%;" <?php echo "name='4_5_2/5/no".$row."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td><textarea rows="1" style="width:100%;" <?php echo "name='4_5_2/6/no".$row."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td><textarea rows="1" style="width:100%;" <?php echo "name='4_5_2/7/no".$row."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
										</tr>

										<?php
									}


									?>

									<tr>
										<td colspan="7"><button type="button" onclick="addRow52('tabel452','4.5.2/counter')"><span class="glyphicon glyphicon-plus"></span></button>
										<button type="button" onclick="removerow('tabel452','4.5.2/counter')"><span class="glyphicon glyphicon-minus"></span></button></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="7">4.5.2</td>
									<td colspan="2">Peningkatan kemampuan dosen tetap melalui program tugas belajar dalam bidang yang sesuai dengan bidang PS.</td>
								</tr>
								<tr>
									<td>Persentase dosen berpendidikan S2 dan S3 (dalam %)</td>
									<td id="nilai/4.5.2/1"></td>
								</tr>
								<tr>
									<td>Persentase dosen berpendidikan S3 (dalam %)</td>
									<td id="nilai/4.5.2/2"></td>
								</tr>
								<tr>
									<td>N2 = Jumlah dosen tugas belajar S2/Sp-1</td>
									<td id="nilai/4.5.2/3"></td>
								</tr>
								<tr>
									<td>N3 = Jumlah dosen tugas belajar S3/Sp-2</td>
									<td id="nilai/4.5.2/4"></td>
								</tr>
								<tr>
									<td>SD = (0.75 N2 + 1.25 N3)</td>
									<td id="nilai/4.5.2/5"></td>
								</tr>
								<tr>
									<td>Nilai</td>
									<td id="nilai/4.5.2/nilai"></td>
									<input type="hidden" id="nilai/4.5.2/nilaihid" name="nilai/4_5_2" value="">
								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 text-right">4.5.3</label> 
						<div class="col-sm-8"><p>Kegiatan dosen tetap yang bidang keahliannya sesuai dengan PS dalam seminar ilmiah/lokakarya/penataran/workshop/ pagelaran/ pameran/peragaan yang tidak hanya melibatkan dosen PT sendiri:</p></div>
					</div>

					<div class="form-group">
						<?php
							//data retrieve for table
						$butir = "4.5.3";
							//retrieve counter
						$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom='0' ORDER BY no ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0){
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$counterdata[$index] = $row['isi_float'];	
								$index++;
							}
						}else{
							$counterdata[0] = 1;
							$counterdata[1] = 1;
						}

							//retrieve data
						$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom!='0' ORDER BY no,sub_no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$tabledata[$index] = $row['isi_char'];
								$index++;
							}
						}else{
							$tabledata[0] = "";
							$tabledata[1] = "";
							$tabledata[2] = "";
							$tabledata[3] = "";
							$tabledata[4] = "";
							$tabledata[5] = "";
						}
						?>

						<div class="col-sm-offset-2 col-sm-9 table-wrapper">
							<input type="hidden" id="4.5.3/counter" name="4_5_3/counter" <?php echo "value='".$counterdata[0]."'";?>>
							<table class="tableBorang" id="table453" style="width:140%;">
								<thead>
									<tr>
										<th rowspan="2">No.</th>
										<th rowspan="2">Nama Dosen</th>
										<th rowspan="2">Jenis Kegiatan</th>
										<th rowspan="2">Tempat</th>
										<th rowspan="2">Waktu</th>
										<th colspan="2">Sebagai</th>
									</tr>
									<tr>
										<th>Penyaji</th>
										<th>Peserta</th>
									</tr>

								</thead>
								<tbody>
									<tr>
										<td style="width:4%;">(1)</td>
										<td>(2)</td>
										<td style="width:25%;">(3)</td>
										<td style="width:18%;">(4)</td>
										<td style="width:11%;">(5)</td>
										<td style="width:6%;">(6)</td>
										<td style="width:6%;">(7)</td>
									</tr>
								</tbody>

								<?php 
								$index = 0;
								for ($no=1; $no <= $counterdata[0] ; $no++) { 
									echo "<tbody>";
									echo "<input type='hidden' id='4.5.3/counter/no".$no."' value='".$counterdata[$no]."' name='4_5_3/counter/no".$no."'>";
									//first tr
									?>
									<tr>
										<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>><?php echo $no; ?></td>
										<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>>
											<?php
											echo "<select class='form-control' name='4_5_3/2/no".$no."/sub1' id='4.5.3/2/no".$no."'>
											<option>-</option>";
											for ($i=1; $i <= $countDosenPS; $i++) {
												if ($tabledata[$index]==$dosenPS[$i]) {
													echo "<option selected>".$dosenPS[$i]."</option>";
												}
												else{
													echo "<option>".$dosenPS[$i]."</option>";	
												}

											}
											$index++;
											echo "</select>";
											?> 
										</td>
										<td><textarea rows='2' style='width:100%;' <?php echo "name='4_5_3/3/no".$no."/sub1'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
										<td><textarea rows='2' style='width:100%;' <?php echo "name='4_5_3/4/no".$no."/sub1'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
										<td><textarea rows='2' style='width:100%;' <?php echo "name='4_5_3/5/no".$no."/sub1'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
										<td><input type='checkbox' class='form-control' value='1' onchange='updateNilai53(5)' <?php echo "name='4_5_3/6/no".$no."/sub1' "; if ($tabledata[$index]=="1") { echo "checked";} $index++; ?>></td>
										<td><input type='checkbox' class='form-control' value='1' onchange='updateNilai53(6)' <?php echo "name='4_5_3/7/no".$no."/sub1' "; if ($tabledata[$index]=="1") { echo "checked";} $index++; ?>></td>
									</tr>

									<?php
									//sub tr
									for ($sub=2; $sub <= $counterdata[$no]; $sub++) { 
										?>
										<tr>
											<td><textarea rows='2' style='width:100%;' <?php echo "name='4_5_3/3/no".$no."/sub".$sub."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td><textarea rows='2' style='width:100%;' <?php echo "name='4_5_3/4/no".$no."/sub".$sub."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td><textarea rows='2' style='width:100%;' <?php echo "name='4_5_3/5/no".$no."/sub".$sub."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td><input type='checkbox' class='form-control' value='1' onchange='updateNilai53(5)' <?php echo "name='4_5_3/6/no".$no."/sub".$sub."'"; if ($tabledata[$index]=="1") { echo "checked";} $index++; ?>></td>
											<td><input type='checkbox' class='form-control' value='1' onchange='updateNilai53(6)' <?php echo "name='4_5_3/7/no".$no."/sub".$sub."'"; if ($tabledata[$index]=="1") { echo "checked";} $index++; ?>></td>
										</tr>
										<?php
									}

									//add row button
									?>
									<tr>
										<td colspan='6'><button type='button' <?php echo "onclick='addrowtbody53(\"table453\",".$no.",\"4.5.3/counter/no".$no."\")'";?>><span class="glyphicon glyphicon-plus"></span></button>
										<button type='button' <?php echo "onclick='removerowtbody(\"table453\",".$no.",\"4.5.3/counter/no".$no."\",2)'";?>><span class="glyphicon glyphicon-minus"></span></button></td>
									</tr>
									<?php
									echo "</tbody>";
								}
								?>
								<tbody>
									<tr>
										<td colspan="7"><button type="button" onclick="addtbody53('table453','4.5.3/counter')"><span class="glyphicon glyphicon-plus"></span></button>
										<button type="button" onclick="removetbody('table453','4.5.3/counter')"><span class="glyphicon glyphicon-minus"></span></button></td>
									</tr>
								</tbody>
								<tbody>
									<tr>
										<td colspan="5">Jumlah</td>
										<td id="4.5.3/6/jumlah"></td>
										<td id="4.5.3/7/jumlah"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="6">4.5.3</td>
									<td colspan="2">Kegiatan dosen tetap yang bidang keahliannya sesuai dengan PS dalam seminar ilmiah/ lokakarya/ penataran/ workshop/ pagelaran/ pameran/peragaan yang tidak hanya melibatkan dosen PT sendiri.</td>
								</tr>
								<tr>
									<td>a = Jumlah kehadiran sebagai penyaji</td>
									<td id="nilai/4.5.3/1"></td>
								</tr>
								<tr>
									<td>b = Jumlah kehadiran sebagai peserta</td>
									<td id="nilai/4.5.3/2"></td>
								</tr>
								<tr>
									<td>N = Jumlah dosen tetap</td>
									<td id="nilai/4.5.3/3"></td>
								</tr>
								<tr>
									<td>SP = (a+b/4)/N</td>
									<td id="nilai/4.5.3/4"></td>
								</tr>
								<tr>
									<td>Nilai</td>
									<td id="nilai/4.5.3/nilai"></td>
									<input type="hidden" id="nilai/4.5.3/nilaihid" name="nilai/4_5_3" value="">
								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 text-right">4.5.4</label> 
						<div class="col-sm-8"><p>Sebutkan pencapaian prestasi/reputasi dosen (misalnya prestasi dalam pendidikan, penelitian dan pelayanan/pengabdian kepada masyarakat).</p></div>
					</div>

					<div class="form-group">
						<?php
							//data retrieve for table
						$butir = "4.5.4";
							//retrieve counter
						$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom='0' ORDER BY no ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0){
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$counterdata[$index] = $row['isi_float'];	
								$index++;
							}
						}else{
							$counterdata[0] = 1;
							$counterdata[1] = 1;
						}

							//retrieve data
						$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom!='0' ORDER BY no,sub_no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$tabledata[$index] = $row['isi_char'];
								$index++;
							}
						}else{
							$tabledata[0] = "";
							$tabledata[1] = "";
							$tabledata[2] = "";
							$tabledata[3] = "";
						}
						?>

						<div class="col-sm-offset-2 col-sm-9 table-wrapper">
							<input type="hidden" id="4.5.4/counter" name="4_5_4/counter" <?php echo "value='".$counterdata[0]."'";?>>
							<table class="tableBorang" id="table454" style="width:130%;">
								<thead>
									<tr>
										<th>No.</th>
										<th>Nama Dosen</th>
										<th>Prestasi yang Dicapai</th>
										<th>Waktu Pencapaian</th>
										<th>Tingkat</th>
									</tr>
									

								</thead>
								<tbody>
									<tr>
										<td style="width:4%;">(1)</td>
										<td style="width:32%;">(2)</td>
										<td >(3)</td>
										<td style="width:10%;">(4)</td>
										<td style="width:15%;">(5)</td>
									</tr>
								</tbody>

								<?php 
								$index = 0;
								for ($no=1; $no <= $counterdata[0] ; $no++) { 
									echo "<tbody>";
									echo "<input type='hidden' id='4.5.4/counter/no".$no."' value='".$counterdata[$no]."' name='4_5_4/counter/no".$no."'>";
									//first tr
									?>
									<tr>
										<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>><?php echo $no; ?></td>
										<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>>
											<?php
											echo "<select class='form-control' name='4_5_4/2/no".$no."/sub1' id='4.5.4/2/no".$no."'>
											<option>-</option>";
											for ($i=1; $i <= $countDosenPS; $i++) {
												if ($tabledata[$index]==$dosenPS[$i]) {
													echo "<option selected>".$dosenPS[$i]."</option>";
												}
												else{
													echo "<option>".$dosenPS[$i]."</option>";	
												}

											}
											$index++;
											echo "</select>";
											?> 
										</td>
										<td><textarea rows='2' style='width:100%;' <?php echo "name='4_5_4/3/no".$no."/sub1'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
										<td><textarea rows='2' style='width:100%;' <?php echo "name='4_5_4/4/no".$no."/sub1'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
										<td><select class="form-control" <?php echo "name='4_5_4/5/no".$no."/sub1'"; ?> >
											<option>-</option>
											<option <?php if ($tabledata[$index]=="Lokal") {echo "selected";} ?>>Lokal</option>
											<option <?php if ($tabledata[$index]=="Nasional") {echo "selected";} ?>>Nasional</option>
											<option <?php if ($tabledata[$index]=="Internasional") {echo "selected";} ?>>Internasional</option>			
										</select></td> <?php $index++; ?>
									</tr>

									<?php
										//sub tr
									for ($sub=2; $sub <= $counterdata[$no]; $sub++) { 
										?>
										<tr>
											<td><textarea rows='2' style='width:100%;' <?php echo "name='4_5_4/3/no".$no."/sub".$sub."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td><textarea rows='2' style='width:100%;' <?php echo "name='4_5_4/4/no".$no."/sub".$sub."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td><select class="form-control" <?php echo "name='4_5_4/5/no".$no."/sub".$sub."'"; ?> >
												<option>-</option>
												<option <?php if ($tabledata[$index]=="Lokal") {echo "selected";} ?>>Lokal</option>
												<option <?php if ($tabledata[$index]=="Nasional") {echo "selected";} ?>>Nasional</option>
												<option <?php if ($tabledata[$index]=="Internasional") {echo "selected";} ?>>Internasional</option>			
											</select></td> <?php $index++; ?>
										</tr>
										<?php
									}

										//add row button
									?>
									<tr>
										<td colspan='4'><button type='button' <?php echo "onclick='addrowtbody54(\"table454\",".$no.",\"4.5.4/counter/no".$no."\")'";?>><span class="glyphicon glyphicon-plus"></span></button>
										<button type='button' <?php echo "onclick='removerowtbody(\"table454\",".$no.",\"4.5.4/counter/no".$no."\",2)'";?>><span class="glyphicon glyphicon-minus"></span></button></td>
									</tr>
									<?php
									echo "</tbody>";
								}
								?>
								<tbody>
									<tr>
										<td colspan="5"><button type="button" onclick="addtbody54('table454','4.5.4/counter')"><span class="glyphicon glyphicon-plus"></span></button>
										<button type="button" onclick="removetbody('table454','4.5.4/counter')"><span class="glyphicon glyphicon-minus"></span></button></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center" colspan="2">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="7">4.5.4</td>
									<td colspan="3">Prestasi dalam mendapatkan penghargaan hibah dalam tiga tahun terakhir.</td>
								</tr>
								<tr>
									<td>0</td>
									<td>Tidak pernah mendapat penghargaan.</td>
									<td></td>
								</tr>
								<tr>
									<td>1</td>
									<td>Mendapatkan penghargaan, hibah, pendanaan program dan kegiatan akademik yang berupa hibah dana  dari PT sendiri (disertai bukti).</td>
									<td></td>
								</tr>
								<tr>
									<td>2</td>
									<td>Mendapatkan penghargaan hibah, pendanaan program dan kegiatan akademik dari institusi regional/lokal (disertai bukti).</td>
									<td></td>
								</tr>
								<tr>
									<td>3</td>
									<td>Mendapatkan penghargaan hibah, pendanaan program dan kegiatan akademik dari institusi nasional (disertai bukti).</td>
									<td></td>
								</tr>
								<tr>
									<td>4</td>
									<td>Mendapatkan penghargaan hibah, pendanaan program dan kegiatan akademik dari institusi internasional (disertai bukti).</td>
									<td></td>
								</tr>
								<tr>
									<td colspan="2">Nilai</td>
									<?php 
									//retrieve Nilai
									$butir = "4.5.4";
									$tabledata = array_fill(0, 1, '');
									$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY sub_butir";
									$data = mysqli_query($db, $query);
									$count = mysqli_num_rows($data);
									if ($count>0) {
										while ($row = mysqli_fetch_assoc($data)) {
											$tabledata[0] = $row['isi'];
										}
									}
									?>
									<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="nilai/4_5_4" value=<?php echo "'".$tabledata[0]."'";?>></td>
								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 text-right">4.5.5</label> 
						<div class="col-sm-8"><p>Sebutkan keikutsertaan dosen tetap dalam organisasi keilmuan atau organisasi profesi.</p></div>
					</div>

					<div class="form-group">
						<?php
							//data retrieve for table
						$butir = "4.5.5";
							//retrieve counter
						$query="SELECT isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom='0' ORDER BY no ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0){
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$counterdata[$index] = $row['isi_float'];	
								$index++;
							}
						}else{
							$counterdata[0] = 1;
							$counterdata[1] = 1;
						}

							//retrieve data
						$query="SELECT isi_char FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' AND kolom!='0' ORDER BY no,sub_no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								$tabledata[$index] = $row['isi_char'];
								$index++;
							}
						}else{
							$tabledata[0] = "";
							$tabledata[1] = "";
							$tabledata[2] = "";
							$tabledata[3] = "";
						}
						?>

						<div class="col-sm-offset-2 col-sm-9 table-wrapper">
							<input type="hidden" id="4.5.5/counter" name="4_5_5/counter" <?php echo "value='".$counterdata[0]."'";?>>
							<table class="tableBorang" id="table455" style="width:120%;">
								<thead>
									<tr>
										<th>No.</th>
										<th>Nama Dosen</th>
										<th>Nama Organisasi Keilmuan atau Organisasi Profesi</th>
										<th>Kurun Waktu</th>
										<th>Tingkat</th>
									</tr>
									

								</thead>
								<tbody>
									<tr>
										<td style="width:4%;">(1)</td>
										<td style="width:34%;">(2)</td>
										<td >(3)</td>
										<td style="width:11%;">(4)</td>
										<td style="width:16%;">(5)</td>
									</tr>
								</tbody>

								<?php 
								$index = 0;
								for ($no=1; $no <= $counterdata[0] ; $no++) { 
									echo "<tbody>";
									echo "<input type='hidden' id='4.5.5/counter/no".$no."' value='".$counterdata[$no]."' name='4_5_5/counter/no".$no."'>";
									//first tr
									?>
									<tr>
										<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>><?php echo $no; ?></td>
										<td <?php echo "rowspan='".($counterdata[$no]+1)."'" ?>>
											<?php
											echo "<select class='form-control' name='4_5_5/2/no".$no."/sub1' id='4.5.5/2/no".$no."'>
											<option>-</option>";
											for ($i=1; $i <= $countDosenPS; $i++) {
												if ($tabledata[$index]==$dosenPS[$i]) {
													echo "<option selected>".$dosenPS[$i]."</option>";
												}
												else{
													echo "<option>".$dosenPS[$i]."</option>";	
												}

											}
											$index++;
											echo "</select>";
											?> 
										</td>
										<td><textarea rows='1' style='width:100%;' <?php echo "name='4_5_5/3/no".$no."/sub1'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
										<td><textarea rows='1' style='width:100%;' <?php echo "name='4_5_5/4/no".$no."/sub1'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
										<td><select class='form-control' onchange='updateNilai55()' <?php echo "name='4_5_5/5/no".$no."/sub1'"; ?> >
											<option>-</option>
											<option <?php if ($tabledata[$index]=="Lokal") {echo "selected";} ?>>Lokal</option>
											<option <?php if ($tabledata[$index]=="Nasional") {echo "selected";} ?>>Nasional</option>
											<option <?php if ($tabledata[$index]=="Internasional") {echo "selected";} ?>>Internasional</option>			
										</select></td> <?php $index++; ?>
									</tr>

									<?php
										//sub tr
									for ($sub=2; $sub <= $counterdata[$no]; $sub++) { 
										?>
										<tr>
											<td><textarea rows='1' style='width:100%;' <?php echo "name='4_5_5/3/no".$no."/sub".$sub."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td><textarea rows='1' style='width:100%;' <?php echo "name='4_5_5/4/no".$no."/sub".$sub."'"; ?>><?php echo $tabledata[$index]; $index++;?></textarea></td>
											<td><select class="form-control" onchange='updateNilai55()' <?php echo "name='4_5_5/5/no".$no."/sub".$sub."'"; ?> >
												<option>-</option>
												<option <?php if ($tabledata[$index]=="Lokal") {echo "selected";} ?>>Lokal</option>
												<option <?php if ($tabledata[$index]=="Nasional") {echo "selected";} ?>>Nasional</option>
												<option <?php if ($tabledata[$index]=="Internasional") {echo "selected";} ?>>Internasional</option>			
											</select></td> <?php $index++; ?>
										</tr>
										<?php
									}

										//add row button
									?>
									<tr>
										<td colspan='4'><button type='button' <?php echo "onclick='addrowtbody55(\"table455\",".$no.",\"4.5.5/counter/no".$no."\")'";?>><span class="glyphicon glyphicon-plus"></span></button>
										<button type='button' <?php echo "onclick='removerowtbody(\"table455\",".$no.",\"4.5.5/counter/no".$no."\",2)'";?>><span class="glyphicon glyphicon-minus"></span></button></td>
									</tr>
									<?php
									echo "</tbody>";
								}
								?>
								<tbody>
									<tr>
										<td colspan="5"><button type="button" onclick="addtbody55('table455','4.5.5/counter')"><span class="glyphicon glyphicon-plus"></span></button>
										<button type="button" onclick="removetbody('table455','4.5.5/counter')"><span class="glyphicon glyphicon-minus"></span></button></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="7">4.5.5</td>
									<td colspan="2">Reputasi dan keluasan jejaring dosen dalam bidang akademik dan profesi.</td>
								</tr>
								<tr>
									<td>Jumlah dosen tetap</td>
									<td id="nilai/4.5.5/1"></td>
								</tr>
								<tr>
									<td>Jumlah dosen tetap yang menjadi anggota masyarakat bidang ilmu tingkat internasional</td>
									<td id="nilai/4.5.5/2"></td>
								</tr>
								<tr>
									<td>Jumlah dosen tetap yang menjadi anggota masyarakat bidang ilmu tingkat nasional</td>
									<td id="nilai/4.5.5/3"></td>
								</tr>
								<tr>
									<td>Persentase dosen tetap yang menjadi anggota masyarakat bidang ilmu tingkat internasional</td>
									<td id="nilai/4.5.5/4"></td>
								</tr>
								<tr>
									<td>Persentase dosen tetap yang menjadi anggota masyarakat bidang ilmu tingkat nasional</td>
									<td id="nilai/4.5.5/5"></td>
								</tr>
								<tr>
									<td>Nilai</td>
									<td id="nilai/4.5.5/nilai"></td>
									<input type="hidden" id="nilai/4.5.5/nilaihid" name="nilai/4_5_5">
								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 text-right">4.6</label> 
						<label class="col-sm-8">Tenaga kependidikan</label>
					</div>

					<div class="form-group">
						<label class="col-sm-2 text-right">4.6.1</label> 
						<label class="col-sm-8">Data tenaga kependidikan yang ada di PS, Jurusan, Fakultas atau PT yang melayani mahasiswa PS:</label>
					</div>

					<div class="form-group">
						<?php
							//data retrieve
						$butir = "4.6.1";
						$query="SELECT isi_char, isi_float FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							$index = 0;
							while ($row = mysqli_fetch_assoc($data)) {
								if ($index==0) {
									$tabledata[$index] = $row['isi_float'];
								}else{
									$tabledata[$index] = $row['isi_char'];
								}
								$index++;
							}	
						}else{
							$tabledata = array_fill(0, 34, '');
							$tabledata[0]=5;
						}
						?>

						<div class="col-sm-8 col-sm-offset-2">
							<input type="hidden" id="4.6.1/counter" name="4_6_1/counter" <?php echo "value='".$tabledata[0]."'";?>>
							<table class="tableBorang" style="width:100%;" id="tabel461">
								<thead>
									<tr>
										<th rowspan="2">No.</th>
										<th rowspan="2">Jenis Tenaga Kependidikan</th>
										<th colspan="8">Jumlah Tenaga Kependidikan di Fakultas/Sekolah Tinggi dengan Pendidikan Terakhir</th>
									</tr>
									<tr>
										<th>S3</th>
										<th>S2</th>
										<th>S1</th>
										<th>D4</th>
										<th>D3</th>
										<th>D2</th>
										<th>D1</th>
										<th>SMA/SMK</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="width:4%">(1)</td>
										<td style="width:30%">(2)</td>
										<td>(3)</td>
										<td>(4)</td>
										<td>(5)</td>
										<td>(6)</td>
										<td>(7)</td>
										<td>(8)</td>
										<td>(9)</td>
										<td>(10)</td>
									</tr>
									<?php

									$namaSDM[0]="Pustakawan";
									$namaSDM[1]="Laboran/Teknisi/Analis/Operator/Programer";
									$namaSDM[2]="Administrasi";
									$namaSDM[3]="Lainnya:";

									$index = 1;
									for ($row=1; $row <= 4; $row++) {
										echo "<tr>";
										echo "<td>".$row."</td>";
										echo "<td>".$namaSDM[$row-1]."</td>";
										if ($row==4) {
											for ($column=3; $column <= 10; $column++) { 
												echo "<td class='voidColor'><div></div></td>";
											}
										}else{
												//kolom input
											for ($column=3; $column <= 10; $column++) { 
												echo "<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_6_1/".$column."/no".$row."' onchange='updateNilai61(".($column-1).")' value='".$tabledata[$index]."'></td>";
												$index++;
											}
										}
										echo "</tr>";
									}

									for ($row=5; $row <= $tabledata[0]; $row++) { 
										echo "<tr>";
										echo "<td></td>";
										echo "<td><textarea rows='1' style='width:100%;' name='4_6_1/2/no".$row."'>".$tabledata[$index]."</textarea></td>"; $index++;
											//kolom input
										for ($column=3; $column <= 10; $column++) { 
											echo "<td><input type='text' pattern='[0-9]+([\.][0-9]+)?' class='form-control' name='4_6_1/".$column."/no".$row."' onchange='updateNilai61(".($column-1).")' value='".$tabledata[$index]."'></td>";
											$index++;
										}
										echo "</tr>";
									}
									?>

									<tr>
										<td colspan="10"><button type="button" onclick="addRow61('tabel461','4.6.1/counter')"><span class="glyphicon glyphicon-plus"></span></button>
										<button type="button" onclick="removerowstatictable('tabel461','4.6.1/counter',5)"><span class="glyphicon glyphicon-minus"></span></button></td>
									</tr>

									<tr>
										<td colspan="2">Jumlah</td>
										<td id="4.6.1/3"></td>
										<td id="4.6.1/4"></td>
										<td id="4.6.1/5"></td>
										<td id="4.6.1/6"></td>
										<td id="4.6.1/7"></td>
										<td id="4.6.1/8"></td>
										<td id="4.6.1/9"></td>
										<td id="4.6.1/10"></td>

									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="6">4.6.1.a</td>
									<td colspan="2">Pustakawan dan kualifikasinya</td>
								</tr>
								<tr>
									<td>X1 = Jumlah pustakawan S2 atau S3</td>
									<td id="nilai/4.6.1.a/1"></td>
								</tr>
								<tr>
									<td>X2 = Jumlah pustakawan D4 atau S1</td>
									<td id="nilai/4.6.1.a/2"></td>
								</tr>
								<tr>
									<td>X3 = Jumlah pustakawan D1, D2, D3, atau bersertifikat pelatihan</td>
									<td id="nilai/4.6.1.a/3"></td>
								</tr>
								<tr>
									<td>A = (4 X1 + 3 X2 + 2 X3)/4</td>
									<td id="nilai/4.6.1.a/4"></td>
								</tr>
								<tr>
									<td>Nilai</td>
									<td id="nilai/4.6.1.a/nilai"></td>
									<input type="hidden" id="nilai/4.6.1.a/nilaihid" name="nilai/4_6_1_a" value="">
								</tr>
							</table>
						</div>
					</div>


					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center" colspan="2">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="6">4.6.1.b</td>
									<td colspan="3">Kualitas laboran, teknisi, operator, programer.</td>
								</tr>
								<tr>
									<td>1</td>
									<td>Kurang dalam jumlah atau terlalu banyak sehingga kurang kegiatannya.</td>
									<td></td>
								</tr>
								<tr>
									<td>2</td>
									<td>Cukup dalam jumlah dan kualifikasi tetapi mutu kerjanya sedang-sedang saja.</td>
									<td></td>
								</tr>
								<tr>
									<td>3</td>
									<td>Jumlah cukup dan memadai kegiatannya.</td>
									<td></td>
								</tr>
								<tr>
									<td>4</td>
									<td>Jumlah cukup dan sangat baik kegiatannya.</td>
									<td></td>
								</tr>
								<tr>
									<td colspan="2">Nilai</td>
									<?php 
										//retrieve Nilai
									$butir = "4.6.1.b";
									$tabledata = array_fill(0, 1, '');
									$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY sub_butir";
									$data = mysqli_query($db, $query);
									$count = mysqli_num_rows($data);
									if ($count>0) {
										while ($row = mysqli_fetch_assoc($data)) {
											$tabledata[0] = $row['isi'];
										}
									}
									?>
									<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="nilai/4_6_1_b" value=<?php echo "'".$tabledata[0]."'";?>></td>
								</tr>
							</table>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="7">4.6.1.c</td>
									<td colspan="2">Tenaga administrasi, jumlah dan kualifikasinya.</td>
								</tr>
								<tr>
									<td>X1 = Jumlah tenaga admin D4 atau S1</td>
									<td id="nilai/4.6.1.c/1"></td>
								</tr>
								<tr>
									<td>X2 = Jumlah tenaga admin D3</td>
									<td id="nilai/4.6.1.c/2"></td>
								</tr>
								<tr>
									<td>X3 = Jumlah tenaga admin D1 atau D2</td>
									<td id="nilai/4.6.1.c/3"></td>
								</tr>
								<tr>
									<td>X4 = Jumlah tenaga admin SMU/SMK</td>
									<td id="nilai/4.6.1.c/4"></td>
								</tr>
								<tr>
									<td>D = (4X1+3X2+2X3+X4)/4</td>
									<td id="nilai/4.6.1.c/5"></td>
								</tr>
								<tr>
									<td>Nilai</td>
									<td id="nilai/4.6.1.c/nilai"></td>
									<input type="hidden" id="nilai/4.6.1.c/nilaihid" name="nilai/4_6_1_c" value="">
								</tr>
							</table>
						</div>
					</div>


					<div class="form-group">
						<label class="col-sm-2 text-right">4.6.2</label> 
						<div class="col-sm-8">Upaya yang telah dilakukan PS dalam meningkatkan kualifikasi dan kompetensi tenaga kependidikan.</div>
					</div>


					<div class="form-group">
						<?php
							//data retrieve
						$butir = "4.6.2";
						$tabledata = array_fill(0, 1, '');
						$query="SELECT isi_text FROM isi_borang WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY no,kolom ASC;";
						$data = mysqli_query($db, $query);
						$count = mysqli_num_rows($data);
						if ($count>0) {
							while ($row = mysqli_fetch_assoc($data)) {
								$tabledata[0] = $row['isi_text'];
							}
						}
						?>
						<div class="col-sm-offset-2 col-sm-8">
							<textarea type="text" class="form-control" rows="15" name="4_6_2" placeholder=""><?php echo $tabledata[0]; ?></textarea>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<table class="table table-bordered tableNilai">
								<tr>
									<th>NO. Item</th>
									<th class="text-center" colspan="2">Keterangan</th>
									<th>Nilai</th>
								</tr>
								<tr>
									<td rowspan="6">4.6.2</td>
									<td colspan="3">Upaya yang telah dilakukan PS untuk meningkatkan kualifikasi dan kompetensi tenaga kependidikan</td>
								</tr>
								<tr>
									<td>1</td>
									<td>Tidak ada upaya pengembangan, padahal kualifikasi dan kompetensi  tenaga kependidikan relatif masih kurang.</td>
									<td></td>
								</tr>
								<tr>
									<td>2</td>
									<td>Upaya pengembangan telah dilakukan dengan  cukup sehingga dapat meningkatkan kualifikasi dan kompetensi tenaga kependidikan.</td>
									<td></td>
								</tr>
								<tr>
									<td>3</td>
									<td>Upaya pengembangan telah dilakukan dengan  baik sehingga dapat meningkatkan kualifikasi dan kompetensi tenaga kependidikan.</td>
									<td></td>
								</tr>
								<tr>
									<td>4</td>
									<td>Upaya pengembangan telah dilakukan dengan sangat baik sehingga dapat meningkatkan kualifikasi dan kompetensi tenaga kependidikan.</td>
									<td></td>
								</tr>
								<tr>
									<td colspan="2">Nilai</td>
									<?php 
									//retrieve Nilai
									$butir = "4.6.2";
									$tabledata = array_fill(0, 1, '');
									$query="SELECT isi FROM isi_form_penilaian WHERE username='".$username."' AND idProdi='".$prodi."' AND idPeriode='".$periode."' AND butir ='".$butir."' ORDER BY sub_butir";
									$data = mysqli_query($db, $query);
									$count = mysqli_num_rows($data);
									if ($count>0) {
										while ($row = mysqli_fetch_assoc($data)) {
											$tabledata[0] = $row['isi'];
										}
									}
									?>
									<td><input type="text" pattern="[0-9]+([\.][0-9]+)?" class="form-control" name="nilai/4_6_2" value=<?php echo "'".$tabledata[0]."'";?>></td>
								</tr>
							</table>
						</div>
					</div>








				</form>
			</div>
		</div>
	</div>
</body>
<script>
	$(document).ready(function() {
		$("form input, form select, form textarea").attr('disabled',true);
		$("button").hide();
		$("#editButton").show();
		$("#hideButton").show();
	});

	$("#editButton").click(function() {
		$("form input, form select, form textarea").attr('disabled',false);
		$("button").not($("#hideButton,#showButton")).show();
		$("#editButton").hide();
	});

	$("#hideButton").click(function() {
		$(".form-group").not($(".tableNilai").parent().parent()).hide();
		$("#showButton").show();
		$("#hideButton").hide();
	});

	$("#showButton").click(function() {
		$(".form-group").show();
		$("#showButton").hide();
		$("#hideButton").show();
	});

	$('form').submit(function() {
		$('body').hide();
		$("header").hide();
		$("html").prepend("<div class='ball'></div><div class='ball1'></div><h1 style='margin-left:43%'>MEMPROSES<h1>");
	});
</script>

</html>