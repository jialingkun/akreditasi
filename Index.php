<!doctype html>
<html>
<?php
	require "Redirect.php";
?>
<head>
	<title>Login</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>
	<div class='wrapper'>
		<header>
			<div class='text-center' style="margin-top:3%">
				<h1>Login Sistem Evaluasi Program Studi</h1>
			</div>
		</header>
	</div>
	
	<div class = "container">
		<form action="ProsesLogin.php" method="post" class="form-signin">       
			<hr class="colorgraph"><br>
			<div class='col-md-6 col-md-offset-3 login-box'>
				<div class="text-center" style="margin-bottom:3%;">
					<img src="css/lock.png" width="40%">
				</div>
				<div class="form-group">
					<label>Username</label>
					<input type="text" class="form-control" name="username" placeholder="Username" required="" autofocus="" />
				</div>
				<div class="form-group">
				</span>Password</label>
				<input type="password" class="form-control" name="password" placeholder="Password" required=""/>   
			</div>  		  
			<div class="checkbox col-md-offset-9">
				<label title="Warning For Private Computer">
					<input type="checkbox" name="remember"> Keep logged in
				</label>
			</div>
			<br>
			<button class="btn btn-lg btn-primary btn-block"  name="Submit">Login</button>  			
		</div>
	</form>			
</div>
</body>

</html>